// -*- C -*-

/** @module
 **
 ** @brief ensure that the appropriate linker option is added if the
 ** full OpenMP support is needed
 **/

#pragma CMOD link "-fopenmp"
