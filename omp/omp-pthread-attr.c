#include <errno.h>
#include <stddef.h>

static
int attr_setaffinity_np(void*attr, size_t cpusetsize, void const*cpuset) {
  return ENOSYS;
}

static
int attr_getaffinity_np(void const*attr, size_t cpusetsize, void*cpuset) {
  return ENOSYS;
}

__attribute__((__weak__, __alias__("attr_setaffinity_np")))
int pthread_attr_setaffinity_np(void*attr, size_t cpusetsize, void const*cpuset);

__attribute__((__weak__, __alias__("attr_getaffinity_np")))
int pthread_attr_getaffinity_np(void*attr, size_t cpusetsize, void const*cpuset);
