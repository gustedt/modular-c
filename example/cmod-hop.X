// This may look like garbage but is actually -*- C -*-
#pragma CMOD module hop		=	cmod◼hop
/* Import library */
#pragma CMOD import def			C◼def
#pragma CMOD import io		=	C◼io
#pragma CMOD import printf	=	C◼io◼printf
#pragma CMOD import fputs	=	C◼io◼fputs
#pragma CMOD import va		=	C◼va
#pragma CMOD import lib		=	C◼lib
/* Import other stuff */
#pragma CMOD import symb	=	cmod◼symbols
#pragma CMOD import rand	=	C◼lib◼rand

#pragma CMOD declaration

struct hop {
  unsigned a;
  double b;
};

/* Import code snippets */
#pragma CMOD import			C◼snippet◼INITIALIZER
#pragma CMOD import			C◼snippet◼init
#pragma CMOD import			C◼snippet◼alloc

#pragma CMOD definition

int func(symb◼ttt a) {
  int c;
  hop A = { .b = symb◼oo_b+symb◼func(a), };
  rand◼set((C◼ptrdiff)(void*)&c);
  return printf("hop: %g %d\n", A.b, rand());
}

int vfprintf(hop* h, io *stream, const char *format, va ap) {
  io◼printf(stream, "----- %s: hop of %u and %g -------------------\n", __MODULE__, h->a, h->b);
  int ret = io◼vfprintf(stream, format, ap);
  fputs("-------------------------------------------\n", stream);
  return ret;
}

int fprintf(hop* h, io *stream, const char *format, ...) {
  va ap;
  va◼start(ap, format);
  int ret = vfprintf(h, stream, format, ap);
  va◼end(ap);
  return ret;
}

#pragma CMOD import tap		= C◼snippet◼tap
#pragma CMOD fill tap		= hop
#pragma CMOD fill tap◼test	= mytest
#pragma CMOD fill tap◼start	= start
#pragma CMOD entry start

void mytest(void) {
  ok(1, "first");
  ok(0 ≡ 1, "second");
  is(9, 3*4, "third");
  char const* aa = "some";
  char const* bb = "thing";
  char const* cc = "some";
  diag("tell the whole story of %s", bb);
  SKIP() {
    cmp(aa, bb, "fourth:\t%s %s", aa, bb);
    diag("tell the whole story of %s", aa);
    cmp(aa, cc, "fifth:\t%s %s", aa, cc);
  }
  cmp(aa, bb, "fourth:\t%s %s", aa, bb);
  cmp(aa, cc, "fifth:\t%s %s", aa, cc);
  SKIP("not yet implemented") {
    cmp(aa, bb, "fourth:\t%s %s", aa, bb);
    diag("tell the whole story of %s", aa);
    cmp(aa, cc, "fifth:\t%s %s", aa, cc);
  }
  TODO("better do it some time") {
    cmp(aa, bb, "fourth:\t%s %s", aa, bb);
    diag("tell the whole story of %s", aa);
    cmp(aa, cc, "fifth:\t%s %s", aa, cc);
  }
}
