/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module iconv =
#pragma CMOD separator ¯

#pragma CMOD mimic <iconv.h>

#pragma CMOD defexp SIZE  = sizeof(iconv_t)
#pragma CMOD defexp ALIGN = _Alignof(iconv_t)

#pragma CMOD import opaque  = C¯tmpl¯opaque
#pragma CMOD fill opaque¯T = type
#pragma CMOD fill opaque¯SIZE  = SIZE
#pragma CMOD fill opaque¯ALIGN = ALIGN


#pragma CMOD alias iconv
#pragma CMOD alias close=iconv_close
#pragma CMOD alias open=iconv_open

#pragma CMOD declaration

C¯size iconv(type, char **restrict, C¯size *restrict,
              char **restrict, C¯size *restrict);
int close(type);
type open(const char *, const char *);
