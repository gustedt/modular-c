/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module sys   =

#pragma CMOD mimic <unistd.h>

#pragma CMOD defrex \2 = \(_SC_\([A-Z][A-Z0-9_]*\)\)
#pragma CMOD defrex \2 = \(_SC_2_\(PBS_[A-Z][A-Z0-9_]*\)\)
#pragma CMOD defrex \2 = \(_SC_2_\(C_[A-Z][A-Z0-9_]*\)\)
#pragma CMOD defrex \2 = \(_SC_2_\(FORT_[A-Z][A-Z0-9_]*\)\)

#pragma CMOD defexp CHAR_TERM = ∷_SC_2_CHAR_TERM
#pragma CMOD defexp LOCALEDEF = ∷_SC_2_LOCALEDEF
#pragma CMOD defexp PBS = ∷_SC_2_PBS
#pragma CMOD defexp SW_DEV  = ∷_SC_2_SW_DEV
#pragma CMOD defexp UPE = ∷_SC_2_UPE
#pragma CMOD defexp VERSION2  = ∷_SC_2_VERSION

#pragma CMOD declaration

#ifdef V7_ILP32_OFF32
# define ILP32_OFF32 V7_ILP32_OFF32
#else
# ifdef V6_ILP32_OFF32
#  define ILP32_OFF32 V6_ILP32_OFF32
# endif
#endif
#ifdef V7_ILP32_OFFBIG
# define ILP32_OFFBIG V7_ILP32_OFFBIG
#else
# ifdef V6_ILP32_OFFBIG
#  define ILP32_OFFBIG V6_ILP32_OFFBIG
# endif
#endif
#ifdef V7_ILP64_OFF64
# define ILP64_OFF64 V7_ILP64_OFF64
#else
# ifdef V6_ILP64_OFF64
#  define ILP64_OFF64 V6_ILP64_OFF64
# endif
#endif
#ifdef V7_ILPBIG_OFFBIG
# define ILPBIG_OFFBIG V7_ILPBIG_OFFBIG
#else
# ifdef V6_ILPBIG_OFFBIG
#  define ILPBIG_OFFBIG V6_ILPBIG_OFFBIG
# endif
#endif
