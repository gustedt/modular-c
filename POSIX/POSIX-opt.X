/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ¯

#pragma CMOD mimic <unistd.h>
#pragma CMOD mimic <stdlib.h>

#pragma CMOD alias get  = getopt
#pragma CMOD alias arg  = optarg
#pragma CMOD alias ind  = optind
#pragma CMOD alias err  = opterr
#pragma CMOD alias opt  = optopt
#pragma CMOD alias sub  = getsubopt

#pragma CMOD declaration

int get(int argc, char * const argv[], const char *optstring);
extern char* optarg;
extern int optind;
extern int opterr;
extern int optopt;
int sub(char **, char *const *, char **);
