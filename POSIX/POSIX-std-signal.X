/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ¯

/**
 ** @brief A module hierarchy that imports C library features one to
 ** one directly into the name space of the importer.
 **
 ** You only want to use these transitionally when migrating existing
 ** C source to Modular C.
 **/

#pragma CMOD snippet none
#pragma CMOD declaration

/* inheritance from C */

typedef C¯sig¯atomic sig_atomic_t;

#define SIGABRT C¯sig¯ABRT
#define SIGFPE  C¯sig¯FPE
#define SIGILL  C¯sig¯ILL
#define SIGINT  C¯sig¯INT
#define SIGSEGV C¯sig¯SEGV
#define SIGTERM C¯sig¯TERM
#define SIG_DFL C¯sig¯DFL
#define SIG_ERR C¯sig¯ERR
#define SIG_IGN C¯sig¯IGN

#define signal  C¯sig¯signal
#define raise C¯sig¯raise

/* POSIX proper */

#define SIGRTMIN POSIX¯sig¯RTMIN
#define SIGRTMAX POSIX¯sig¯RTMAX

#define SIGALRM POSIX¯sig¯ALRM
#define SIGBUS POSIX¯sig¯BUS
#define SIGCHLD POSIX¯sig¯CHLD
#define SIGCONT POSIX¯sig¯CONT
#define SIGHUP POSIX¯sig¯HUP
#define SIGKILL POSIX¯sig¯KILL
#define SIGPIPE POSIX¯sig¯PIPE
#define SIGQUIT POSIX¯sig¯QUIT
#define SIGSTOP POSIX¯sig¯STOP
#define SIGTSTP POSIX¯sig¯TSTP
#define SIGTTIN POSIX¯sig¯TTIN
#define SIGTTOU POSIX¯sig¯TTOU
#define SIGUSR1 POSIX¯sig¯USR1
#define SIGUSR2 POSIX¯sig¯USR2
#define SIGPOLL POSIX¯sig¯POLL
#define SIGPROF POSIX¯sig¯PROF
#define SIGSYS POSIX¯sig¯SYS
#define SIGTRAP POSIX¯sig¯TRAP
#define SIGURG POSIX¯sig¯URG
#define SIGVTALRM POSIX¯sig¯VTALRM
#define SIGXCPU POSIX¯sig¯XCPU
#define SIGXFSZ POSIX¯sig¯XFSZ

#define SA_NOCLDSTOP POSIX¯sig¯action¯NOCLDSTOP
#define SA_NOCLDWAIT POSIX¯sig¯action¯NOCLDWAIT
#define SA_NODEFER POSIX¯sig¯action¯NODEFER
#define SA_ONSTACK POSIX¯sig¯action¯ONSTACK
#define SA_RESETHAND POSIX¯sig¯action¯RESETHAND
#define SA_RESTART POSIX¯sig¯action¯RESTART
#define SA_RESTORER POSIX¯sig¯action¯RESTORER
#define SA_SIGINFO POSIX¯sig¯action¯SIGINFO

#define SS_ONSTACK POSIX¯sig¯stack¯ONSTACK
#define SS_DISABLE POSIX¯sig¯stack¯DISABLE
#define SIGSTKSZ POSIX¯sig¯stack¯MIN
#define MINSIGSTKSZ POSIX¯sig¯stack¯DEFAULT

/* This replaces struct sigaction. We can't do the later because there
   also is a function sigaction. */
typedef POSIX¯sig¯action sigaction_t;

#define sigaction POSIX¯sig¯action¯set

typedef POSIX¯sig¯set sigset_t;
typedef POSIX¯sig¯val sigval_t;
typedef POSIX¯sig¯info siginfo_t;
typedef POSIX¯sig¯stack stack_t;

#define kill POSIX¯sig¯kill
#define killpg POSIX¯sig¯killpg
#define psignal POSIX¯sig¯psignal

#define sigaction POSIX¯sig¯action¯set

#define sigaltstack POSIX¯sig¯stack¯set

#define sighold POSIX¯sig¯sighold
#define sigignore POSIX¯sig¯sigignore
#define siginterrupt POSIX¯sig¯interrupt
//void (*signal(int, void (*)(int)))(int);
#define sigpause POSIX¯sig¯sigpause
#define sigqueue POSIX¯sig¯val¯queue
#define sigrelse POSIX¯sig¯sigrelse
#define sigset POSIX¯sig¯sigset
#define psiginfo POSIX¯sig¯info¯puts

#define sigaddset POSIX¯sig¯set¯add
#define sigdelset POSIX¯sig¯set¯del
#define sigemptyset POSIX¯sig¯set¯empty
#define sigfillset POSIX¯sig¯set¯fill
#define sigismember POSIX¯sig¯set¯member
#define sigpending POSIX¯sig¯set¯pending
#define sigsuspend POSIX¯sig¯set¯suspend
#define sigtimedwait POSIX¯sig¯set¯timedwait
#define sigwait POSIX¯sig¯set¯wait
#define sigwaitinfo POSIX¯sig¯set¯waitinfo
#define sigprocmask POSIX¯sig¯set¯mask_proc
#define pthread_sigmask POSIX¯sig¯set¯mask_thread
