/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module process =

#pragma CMOD mimic <unistd.h>
#pragma CMOD mimic <stdalign.h>

#pragma CMOD defexp __pid = "#define __pid %s", cmod_type(pid_t)

#pragma CMOD alias execl
#pragma CMOD alias execle
#pragma CMOD alias execlp
#pragma CMOD alias execv
#pragma CMOD alias execve
#pragma CMOD alias execvp
#pragma CMOD alias fork
#pragma CMOD alias nice
#pragma CMOD alias pause
#pragma CMOD alias sleep
#pragma CMOD alias group       = getpgid
#pragma CMOD alias grp         = getpgrp
#pragma CMOD alias process     = getpid
#pragma CMOD alias parent      = getppid
#pragma CMOD alias session     = getsid
#pragma CMOD alias group_set   = setpgid
#pragma CMOD alias grp_set     = setpgrp
#pragma CMOD alias separate    = setsid

#pragma CMOD declaration

typedef __pid id;

int execl(const char *, const char *, ...);
int execle(const char *, const char *, ...);
int execlp(const char *, const char *, ...);
int execv(const char *, char *const []);
int execve(const char *, char *const [], char *const []);
int execvp(const char *, char *const []);
id fork(void);
int nice(int);
int pause(void);
unsigned sleep(unsigned);
id group(id);
id grp(void);
id process(void);
id parent(void);
id session(id);
int group_set(id, id);
id grp_set(void);
id separate(void);


#define exec(_0, ...) _Generic((__VA_ARGS__ + 0), char*const*: execv, default: execl)(_0, __VA_ARGS__)
#define execp(_0, ...) _Generic((__VA_ARGS__ + 0), char*const*: execvp, default: execlp)(_0, __VA_ARGS__)
#define exece(_0, _1, ...) _Generic((_1 + 0), char*const*: execve, default: execle)(_0, _1, __VA_ARGS__)
