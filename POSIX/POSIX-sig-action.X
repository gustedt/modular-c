/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module action  =
#pragma CMOD separator ¯
#pragma CMOD import info  = ..¯info
#pragma CMOD import sigset  = ..¯set

/**
 ** @file
 ** @brief Try to implement the POSIX sigaction interfaces
 **
 ** This is incomplete because POSIX has had two very bad ideas.
 **
 ** - The @c sa_handler and @c sa_sigaction field of the structure may
 **   overlap.
 **
 ** - The structure type and the function use the same identifier.
 **
 ** For the first, we simply don't implement the @c sa_handler
 ** field. Use C¯sig¯signal if you want to deal with this variant of
 ** signal handlers.
 **/

#pragma CMOD mimic <signal.h>
#pragma CMOD mimic <sys/types.h>

#pragma CMOD defexp SIZE  = sizeof(struct sigaction)
#pragma CMOD defexp ALIGN = _Alignof(struct sigaction)
#pragma CMOD defexp HANDLER_OFFSET  = offsetof(struct sigaction, sa_handler)
#pragma CMOD defexp HANDLER_OFFSET2 = offsetof(struct sigaction, sa_handler)+sizeof(void(*)(void))
#pragma CMOD defexp SIGACTION_OFFSET  = offsetof(struct sigaction, sa_sigaction)
#pragma CMOD defexp SIGACTION_OFFSET2 = offsetof(struct sigaction, sa_sigaction)+sizeof(void(*)(void))

#pragma CMOD defexp ACTION_OFFSET =                                                  \
  (offsetof(struct sigaction, sa_sigaction) < offsetof(struct sigaction, sa_handler) \
   ? offsetof(struct sigaction, sa_handler)                                          \
   : offsetof(struct sigaction, sa_sigaction))

#pragma CMOD defexp ACTION_PADDING  =                                                   \
  (offsetof(struct sigaction, sa_mask)                                                  \
   -((offsetof(struct sigaction, sa_sigaction) < offsetof(struct sigaction, sa_handler) \
      ? offsetof(struct sigaction, sa_sigaction)                                        \
      : offsetof(struct sigaction, sa_handler))+sizeof(void(*)(void))))

#pragma CMOD defexp MASK_OFFSET =      offsetof(struct sigaction, sa_mask)
#pragma CMOD defexp MASK_PADDING  =      offsetof(struct sigaction, sa_flags)-(offsetof(struct sigaction, sa_mask)+sizeof(sigset_t))
//#pragma CMOD defexp RESTORER_OFFSET = offsetof(struct sigaction, sa_restorer)
#pragma CMOD defexp FLAGS_OFFSET  = offsetof(struct sigaction, sa_flags)
#pragma CMOD defexp FLAGS_PADDING = sizeof(struct sigaction)-(offsetof(struct sigaction, sa_flags)+sizeof(int))

#pragma CMOD defrex \2=\(SA_\([a-zA-Z][a-zA-Z0-9_]*\)\)

#pragma CMOD declaration

// Do a case analysis for the ordering of the sigaction and handler
// fields and provide padding between them if necessary. This is an
// anonymous struct or union, such that it integrates well to the
// structure as a whole.
#if HANDLER_OFFSET == SIGACTION_OFFSET
# define __action_type                                         \
union {                                                        \
  void (*sa_sigaction)(int, info*, void *);                    \
  void (*sa_handler)(int);                                     \
}
#else
# if SIGACTION_OFFSET < HANDLER_OFFSET
#  if SIGACTION_OFFSET2 == HANDLER_OFFSET
#   define __action_type                                       \
struct {                                                       \
  void (*sa_sigaction)(int, info*, void *);                    \
  void (*sa_handler)(int);                                     \
}
#  else
#   define __action_type                                       \
struct {                                                       \
  void (*sa_sigaction)(int, info*, void *);                    \
  unsigned char[HANDLER_OFFSET-SIGACTION_OFFSET2];             \
  void (*sa_handler)(int);                                     \
}
#  endif
# else
#  if HANDLER_OFFSET2 == SIGACTION_OFFSET
#   define __action_type                                       \
struct {                                                       \
  void (*sa_handler)(int, info*, void *);                      \
  void (*sa_sigaction)(int);                                   \
}
#  else
#   define __action_type                                       \
struct {                                                       \
  void (*sa_handler)(int, info*, void *);                      \
  unsigned char[SIGACTION_OFFSET-HANDLER_OFFSET2];             \
  void (*sa_sigaction)(int);                                   \
}
#  endif
# endif
#endif

/* This only works when the order of the three fields is as given
   here. */
struct action {
  __action_type;
#if ACTION_PADDING
  unsigned char __dummy0[ACTION_PADDING];
#endif
  sigset sa_mask;
#if MASK_PADDING
  unsigned char __dummy1[MASK_PADDING];
#endif
  int sa_flags;
#if FLAGS_PADDING
  unsigned char __dummy2[FLAGS_PADDING];
#endif
};

#pragma CMOD alias set  = sigaction

int    set(int, const struct action *restrict, struct action *restrict);

#pragma CMOD definition

static_assert(ALIGN == alignof(action), "import of struct sigaction as action failed, wrong alignment");
static_assert(SIZE == sizeof(action), "import of struct sigaction as action failed, wrong size");
static_assert(HANDLER_OFFSET == offsetof(action, sa_handler), "import of struct sigaction as action failed, wrong offset for sa_handler");
static_assert(SIGACTION_OFFSET == offsetof(action, sa_sigaction), "import of struct sigaction as action failed, wrong offset for sa_sigaction");
static_assert(MASK_OFFSET == offsetof(action, sa_mask), "import of struct sigaction as action failed, wrong offset for sa_mask");
static_assert(FLAGS_OFFSET == offsetof(action, sa_flags), "import of struct sigaction as action failed, wrong offset for sa_flags");
//static_assert(RESTORER_OFFSET == offsetof(POSIX¯sig¯action, sa_restorer), "import of struct sigaction as action failed, wrong offset for sa_restorer");
