/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module unistd =
#pragma CMOD separator ¯
#pragma CMOD import OS    = ..¯..
#pragma CMOD import conf_sys  = ..¯..¯conf¯sys

/**
 ** @brief A module hierarchy that imports C library features one to
 ** one directly into the name space of the importer.
 **
 ** You only want to use these transitionally when migrating existing
 ** C source to Modular C.
 **/

#pragma CMOD snippet none
#pragma CMOD declaration

#define _POSIX2_CHAR_TERM OS¯CHAR_TERM
#define _POSIX2_C_BIND  OS¯C_BIND
#define _POSIX2_C_DEV OS¯C_DEV
#define _POSIX2_FORT_DEV  OS¯FORT_DEV
#define _POSIX2_FORT_RUN  OS¯FORT_RUN
#define _POSIX2_LOCALEDEF OS¯LOCALEDEF
#define _POSIX2_PBS OS¯PBS
#define _POSIX2_PBS_ACCOUNTING  OS¯PBS_ACCOUNTING
#define _POSIX2_PBS_CHECKPOINT  OS¯PBS_CHECKPOINT
#define _POSIX2_PBS_LOCATE  OS¯PBS_LOCATE
#define _POSIX2_PBS_MESSAGE OS¯PBS_MESSAGE
#define _POSIX2_PBS_TRACK OS¯PBS_TRACK
#define _POSIX2_SW_DEV  OS¯SW_DEV
#define _POSIX2_UPE OS¯UPE
#define _POSIX2_VERSION OS¯VERSION2
#define _POSIX_ADVISORY_INFO  OS¯ADVISORY_INFO
#define _POSIX_ASYNCHRONOUS_IO  OS¯ASYNCHRONOUS_IO
#define _POSIX_BARRIERS OS¯BARRIERS
#define _POSIX_CHOWN_RESTRICTED OS¯CHOWN_RESTRICTED
#define _POSIX_CLOCK_SELECTION  OS¯CLOCK_SELECTION
#define _POSIX_CPUTIME  OS¯CPUTIME
#define _POSIX_FSYNC  OS¯FSYNC
#define _POSIX_IPV6 OS¯IPV6
#define _POSIX_JOB_CONTROL  OS¯JOB_CONTROL
#define _POSIX_MAPPED_FILES OS¯MAPPED_FILES
#define _POSIX_MEMLOCK  OS¯MEMLOCK
#define _POSIX_MEMLOCK_RANGE  OS¯MEMLOCK_RANGE
#define _POSIX_MEMORY_PROTECTION  OS¯MEMORY_PROTECTION
#define _POSIX_MESSAGE_PASSING  OS¯MESSAGE_PASSING
#define _POSIX_MONOTONIC_CLOCK  OS¯MONOTONIC_CLOCK
#define _POSIX_NO_TRUNC OS¯NO_TRUNC
#define _POSIX_PRIORITIZED_IO OS¯PRIORITIZED_IO
#define _POSIX_PRIORITY_SCHEDULING  OS¯PRIORITY_SCHEDULING
#define _POSIX_RAW_SOCKETS  OS¯RAW_SOCKETS
#define _POSIX_READER_WRITER_LOCKS  OS¯READER_WRITER_LOCKS
#define _POSIX_REALTIME_SIGNALS OS¯REALTIME_SIGNALS
#define _POSIX_REGEXP OS¯REGEXP
#define _POSIX_SAVED_IDS  OS¯SAVED_IDS
#define _POSIX_SEMAPHORES OS¯SEMAPHORES
#define _POSIX_SHARED_MEMORY_OBJECTS  OS¯SHARED_MEMORY_OBJECTS
#define _POSIX_SHELL  OS¯SHELL
#define _POSIX_SPAWN  OS¯SPAWN
#define _POSIX_SPIN_LOCKS OS¯SPIN_LOCKS
#define _POSIX_SPORADIC_SERVER  OS¯SPORADIC_SERVER
#define _POSIX_SYNCHRONIZED_IO  OS¯SYNCHRONIZED_IO
#define _POSIX_THREADS  OS¯THREADS
#define _POSIX_THREAD_ATTR_STACKADDR  OS¯THREAD_ATTR_STACKADDR
#define _POSIX_THREAD_ATTR_STACKSIZE  OS¯THREAD_ATTR_STACKSIZE
#define _POSIX_THREAD_CPUTIME OS¯THREAD_CPUTIME
#define _POSIX_THREAD_PRIORITY_SCHEDULING OS¯THREAD_PRIORITY_SCHEDULING
#define _POSIX_THREAD_PRIO_INHERIT  OS¯THREAD_PRIO_INHERIT
#define _POSIX_THREAD_PRIO_PROTECT  OS¯THREAD_PRIO_PROTECT
#define _POSIX_THREAD_PROCESS_SHARED  OS¯THREAD_PROCESS_SHARED
#define _POSIX_THREAD_ROBUST_PRIO_INHERIT OS¯THREAD_ROBUST_PRIO_INHERIT
#define _POSIX_THREAD_ROBUST_PRIO_PROTECT OS¯THREAD_ROBUST_PRIO_PROTECT
#define _POSIX_THREAD_SAFE_FUNCTIONS  OS¯THREAD_SAFE_FUNCTIONS
#define _POSIX_THREAD_SPORADIC_SERVER OS¯THREAD_SPORADIC_SERVER
#define _POSIX_TIMEOUTS OS¯TIMEOUTS
#define _POSIX_TIMERS OS¯TIMERS
#define _POSIX_TRACE  OS¯TRACE
#define _POSIX_TRACE_EVENT_FILTER OS¯TRACE_EVENT_FILTER
#define _POSIX_TRACE_INHERIT  OS¯TRACE_INHERIT
#define _POSIX_TRACE_LOG  OS¯TRACE_LOG
#define _POSIX_TYPED_MEMORY_OBJECTS OS¯TYPED_MEMORY_OBJECTS
#define _POSIX_V6_ILP32_OFF32 OS¯V6_ILP32_OFF32
#define _POSIX_V6_ILP32_OFFBIG  OS¯V6_ILP32_OFFBIG
#define _POSIX_V6_LP64_OFF64  OS¯V6_LP64_OFF64
#define _POSIX_V6_LPBIG_OFFBIG  OS¯V6_LPBIG_OFFBIG
#define _POSIX_V7_ILP32_OFF32 OS¯V7_ILP32_OFF32
#define _POSIX_V7_ILP32_OFFBIG  OS¯V7_ILP32_OFFBIG
#define _POSIX_V7_LP64_OFF64  OS¯V7_LP64_OFF64
#define _POSIX_V7_LPBIG_OFFBIG  OS¯V7_LPBIG_OFFBIG
#define _POSIX_VERSION  OS¯VERSION
#define _SC_2_CHAR_TERM conf_sys¯CHAR_TERM
#define _SC_2_C_BIND  conf_sys¯C_BIND
#define _SC_2_C_DEV conf_sys¯C_DEV
#define _SC_2_FORT_DEV  conf_sys¯FORT_DEV
#define _SC_2_FORT_RUN  conf_sys¯FORT_RUN
#define _SC_2_LOCALEDEF conf_sys¯LOCALEDEF
#define _SC_2_PBS conf_sys¯PBS
#define _SC_2_PBS_ACCOUNTING  conf_sys¯PBS_ACCOUNTING
#define _SC_2_PBS_CHECKPOINT  conf_sys¯PBS_CHECKPOINT
#define _SC_2_PBS_LOCATE  conf_sys¯PBS_LOCATE
#define _SC_2_PBS_MESSAGE conf_sys¯PBS_MESSAGE
#define _SC_2_PBS_TRACK conf_sys¯PBS_TRACK
#define _SC_2_SW_DEV  conf_sys¯SW_DEV
#define _SC_2_UPE conf_sys¯UPE
#define _SC_2_VERSION conf_sys¯VERSION2
#define _SC_CHILD_MAX conf_sys¯CHILD_MAX
#define _SC_CLK_TCK conf_sys¯CLK_TCK
#define _SC_CLOCK_SELECTION conf_sys¯CLOCK_SELECTION
#define _SC_COLL_WEIGHTS_MAX  conf_sys¯COLL_WEIGHTS_MAX
#define _SC_CPUTIME conf_sys¯CPUTIME
#define _SC_DELAYTIMER_MAX  conf_sys¯DELAYTIMER_MAX
#define _SC_EXPR_NEST_MAX conf_sys¯EXPR_NEST_MAX
#define _SC_FSYNC conf_sys¯FSYNC
#define _SC_GETGR_R_SIZE_MAX  conf_sys¯GETGR_R_SIZE_MAX
#define _SC_GETPW_R_SIZE_MAX  conf_sys¯GETPW_R_SIZE_MAX
#define _SC_HOST_NAME_MAX conf_sys¯HOST_NAME_MAX
#define _SC_IOV_MAX conf_sys¯IOV_MAX
#define _SC_IPV6  conf_sys¯IPV6
#define _SC_JOB_CONTROL conf_sys¯JOB_CONTROL
#define _SC_LINE_MAX  conf_sys¯LINE_MAX
#define _SC_LOGIN_NAME_MAX  conf_sys¯LOGIN_NAME_MAX
#define _SC_MAPPED_FILES  conf_sys¯MAPPED_FILES
#define _SC_MEMLOCK conf_sys¯MEMLOCK
#define _SC_MEMLOCK_RANGE conf_sys¯MEMLOCK_RANGE
#define _SC_MEMORY_PROTECTION conf_sys¯MEMORY_PROTECTION
#define _SC_MESSAGE_PASSING conf_sys¯MESSAGE_PASSING
#define _SC_MONOTONIC_CLOCK conf_sys¯MONOTONIC_CLOCK
#define _SC_MQ_OPEN_MAX conf_sys¯MQ_OPEN_MAX
#define _SC_MQ_PRIO_MAX conf_sys¯MQ_PRIO_MAX
#define _SC_NGROUPS_MAX conf_sys¯NGROUPS_MAX
#define _SC_OPEN_MAX  conf_sys¯OPEN_MAX
#define _SC_PAGESIZE  conf_sys¯PAGESIZE
#define _SC_PAGE_SIZE conf_sys¯PAGE_SIZE
#define _SC_PRIORITIZED_IO  conf_sys¯PRIORITIZED_IO
#define _SC_PRIORITY_SCHEDULING conf_sys¯PRIORITY_SCHEDULING
#define _SC_RAW_SOCKETS conf_sys¯RAW_SOCKETS
#define _SC_READER_WRITER_LOCKS conf_sys¯READER_WRITER_LOCKS
#define _SC_REALTIME_SIGNALS  conf_sys¯REALTIME_SIGNALS
#define _SC_REGEXP  conf_sys¯REGEXP
#define _SC_RE_DUP_MAX  conf_sys¯RE_DUP_MAX
#define _SC_RTSIG_MAX conf_sys¯RTSIG_MAX
#define _SC_SAVED_IDS conf_sys¯SAVED_IDS
#define _SC_SEMAPHORES  conf_sys¯SEMAPHORES
#define _SC_SEM_NSEMS_MAX conf_sys¯SEM_NSEMS_MAX
#define _SC_SEM_VALUE_MAX conf_sys¯SEM_VALUE_MAX
#define _SC_SHARED_MEMORY_OBJECTS conf_sys¯SHARED_MEMORY_OBJECTS
#define _SC_SHELL conf_sys¯SHELL
#define _SC_SIGQUEUE_MAX  conf_sys¯SIGQUEUE_MAX
#define _SC_SPAWN conf_sys¯SPAWN
#define _SC_SPIN_LOCKS  conf_sys¯SPIN_LOCKS
#define _SC_SPORADIC_SERVER conf_sys¯SPORADIC_SERVER
#define _SC_SS_REPL_MAX conf_sys¯SS_REPL_MAX
#define _SC_STREAM_MAX  conf_sys¯STREAM_MAX
#define _SC_SYMLOOP_MAX conf_sys¯SYMLOOP_MAX
#define _SC_SYNCHRONIZED_IO conf_sys¯SYNCHRONIZED_IO
#define _SC_THREADS conf_sys¯THREADS
#define _SC_THREAD_ATTR_STACKADDR conf_sys¯THREAD_ATTR_STACKADDR
#define _SC_THREAD_ATTR_STACKSIZE conf_sys¯THREAD_ATTR_STACKSIZE
#define _SC_THREAD_CPUTIME  conf_sys¯THREAD_CPUTIME
#define _SC_THREAD_DESTRUCTOR_ITERATIONS  conf_sys¯THREAD_DESTRUCTOR_ITERATIONS
#define _SC_THREAD_KEYS_MAX conf_sys¯THREAD_KEYS_MAX
#define _SC_THREAD_PRIORITY_SCHEDULING  conf_sys¯THREAD_PRIORITY_SCHEDULING
#define _SC_THREAD_PRIO_INHERIT conf_sys¯THREAD_PRIO_INHERIT
#define _SC_THREAD_PRIO_PROTECT conf_sys¯THREAD_PRIO_PROTECT
#define _SC_THREAD_PROCESS_SHARED conf_sys¯THREAD_PROCESS_SHARED
#define _SC_THREAD_ROBUST_PRIO_INHERIT  conf_sys¯THREAD_ROBUST_PRIO_INHERIT
#define _SC_THREAD_ROBUST_PRIO_PROTECT  conf_sys¯THREAD_ROBUST_PRIO_PROTECT
#define _SC_THREAD_SAFE_FUNCTIONS conf_sys¯THREAD_SAFE_FUNCTIONS
#define _SC_THREAD_SPORADIC_SERVER  conf_sys¯THREAD_SPORADIC_SERVER
#define _SC_THREAD_STACK_MIN  conf_sys¯THREAD_STACK_MIN
#define _SC_THREAD_THREADS_MAX  conf_sys¯THREAD_THREADS_MAX
#define _SC_TIMEOUTS  conf_sys¯TIMEOUTS
#define _SC_TIMERS  conf_sys¯TIMERS
#define _SC_TIMER_MAX conf_sys¯TIMER_MAX
#define _SC_TRACE conf_sys¯TRACE
#define _SC_TRACE_EVENT_FILTER  conf_sys¯TRACE_EVENT_FILTER
#define _SC_TRACE_EVENT_NAME_MAX  conf_sys¯TRACE_EVENT_NAME_MAX
#define _SC_TRACE_INHERIT conf_sys¯TRACE_INHERIT
#define _SC_TRACE_LOG conf_sys¯TRACE_LOG
#define _SC_TRACE_NAME_MAX  conf_sys¯TRACE_NAME_MAX
#define _SC_TRACE_SYS_MAX conf_sys¯TRACE_SYS_MAX
#define _SC_TRACE_USER_EVENT_MAX  conf_sys¯TRACE_USER_EVENT_MAX
#define _SC_TTY_NAME_MAX  conf_sys¯TTY_NAME_MAX
#define _SC_TYPED_MEMORY_OBJECTS  conf_sys¯TYPED_MEMORY_OBJECTS
#define _SC_TZNAME_MAX  conf_sys¯TZNAME_MAX
#define _SC_V6_ILP32_OFF32  conf_sys¯V6_ILP32_OFF32
#define _SC_V6_ILP32_OFFBIG conf_sys¯V6_ILP32_OFFBIG
#define _SC_V6_LP64_OFF64 conf_sys¯V6_LP64_OFF64
#define _SC_V6_LPBIG_OFFBIG conf_sys¯V6_LPBIG_OFFBIG
#define _SC_V7_ILP32_OFF32  conf_sys¯V7_ILP32_OFF32
#define _SC_V7_ILP32_OFFBIG conf_sys¯V7_ILP32_OFFBIG
#define _SC_V7_LP64_OFF64 conf_sys¯V7_LP64_OFF64
#define _SC_V7_LPBIG_OFFBIG conf_sys¯V7_LPBIG_OFFBIG
#define _SC_VERSION conf_sys¯VERSION
#define _SC_XOPEN_CRYPT conf_sys¯XOPEN_CRYPT
#define _SC_XOPEN_ENH_I18N  conf_sys¯XOPEN_ENH_I18N
#define _SC_XOPEN_REALTIME  conf_sys¯XOPEN_REALTIME
#define _SC_XOPEN_REALTIME_THREADS  conf_sys¯XOPEN_REALTIME_THREADS
#define _SC_XOPEN_SHM conf_sys¯XOPEN_SHM
#define _SC_XOPEN_STREAMS conf_sys¯XOPEN_STREAMS
#define _SC_XOPEN_UNIX  conf_sys¯XOPEN_UNIX
#define _SC_XOPEN_UUCP  conf_sys¯XOPEN_UUCP
#define _SC_XOPEN_VERSION conf_sys¯XOPEN_VERSION
#define _XOPEN_CRYPT  OS¯CRYPT
#define _XOPEN_ENH_I18N OS¯ENH_I18N
#define _XOPEN_REALTIME OS¯REALTIME
#define _XOPEN_REALTIME_THREADS OS¯REALTIME_THREADS
#define _XOPEN_SHM  OS¯SHM
#define _XOPEN_STREAMS  OS¯STREAMS
#define _XOPEN_UNIX OS¯UNIX
#define _XOPEN_UUCP OS¯UUCP
#define _XOPEN_VERSION  OS¯XOPEN
