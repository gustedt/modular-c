/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ¯
#pragma CMOD import psocket = ..¯..¯socket
#pragma CMOD import addr    = psocket¯addr

/**
 ** @brief A module hierarchy that imports POSIX library features one to
 ** one directly into the name space of the importer.
 **
 ** You only want to use these transitionally when migrating existing
 ** C source to Modular C.
 **/

#pragma CMOD snippet none
#pragma CMOD declaration

typedef psocket¯length socklen_t;
typedef psocket¯family sa_family_t;
typedef addr sockaddr;
typedef addr¯storage sockaddr_storage;

#define SOCK_CLOEXEC psocket¯CLOEXEC
#define SOCK_DCCP psocket¯DCCP
#define SOCK_DGRAM psocket¯DGRAM
#define SOCK_NONBLOCK psocket¯NONBLOCK
#define SOCK_PACKET psocket¯PACKET
#define SOCK_RAW psocket¯RAW
#define SOCK_RDM psocket¯RDM
#define SOCK_SEQPACKET psocket¯SEQPACKET
#define SOCK_STREAM psocket¯STREAM

#define accept psocket¯accept
#define bind psocket¯bind
#define connect psocket¯connect
#define getpeername psocket¯getpeername
#define getsockname psocket¯getsockname
#define listen psocket¯listen
#define shutdown psocket¯shutdown
#define sockatmark psocket¯atmark
#define socket psocket
#define socketpair psocket¯pair

#define AF_INET psocket¯family¯INET
#define AF_INET6 psocket¯family¯INET6
#define AF_UNIX psocket¯family¯UNIX
#define AF_UNSPEC psocket¯family¯UNSPEC
