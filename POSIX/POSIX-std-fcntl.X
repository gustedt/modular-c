/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ¯
#pragma CMOD import OS    = ..¯..
#pragma CMOD import fd    = OS¯fd
#pragma CMOD import fd_cntl = fd¯cntl
#pragma CMOD import fd_advise = fd¯advise
#pragma CMOD import fd_open = fd¯open

/**
 ** @brief A module hierarchy that imports C library features one to
 ** one directly into the name space of the importer.
 **
 ** You only want to use these transitionally when migrating existing
 ** C source to Modular C.
 **/

#pragma CMOD snippet none
#pragma CMOD declaration

#define POSIX_FADV_SEQUENTIAL fd_advise¯SEQUENTIAL
#define POSIX_FADV_RANDOM fd_advise¯RANDOM
#define POSIX_FADV_DONTNEED fd_advise¯DONTNEED
#define POSIX_FADV_NOREUSE  fd_advise¯NOREUSE
#define POSIX_FADV_NORMAL fd_advise¯NORMAL
#define POSIX_FADV_WILLNEED fd_advise¯WILLNEED

#define FD_CLOEXEC  fd¯CLOEXEC
#define F_DUPFD fd_cntl¯DUPFD
#define F_DUPFD_CLOEXEC fd_cntl¯DUPFD_CLOEXEC
#define F_EXLCK fd_cntl¯EXLCK
#define F_GETFD fd_cntl¯GETFD
#define F_GETFL fd_cntl¯GETFL
#define F_GETLK fd_cntl¯GETLK
#define F_GETOWN  fd_cntl¯GETOWN
#define F_LOCK  fd_cntl¯LOCK
#define F_OK  fd_cntl¯OK
#define F_RDLCK fd_cntl¯RDLCK
#define F_SETFD fd_cntl¯SETFD
#define F_SETFL fd_cntl¯SETFL
#define F_SETLK fd_cntl¯SETLK
#define F_SETLKW  fd_cntl¯SETLKW
#define F_SETOWN  fd_cntl¯SETOWN
#define F_SHLCK fd_cntl¯SHLCK
#define F_TEST  fd_cntl¯TEST
#define F_TLOCK fd_cntl¯TLOCK
#define F_ULOCK fd_cntl¯ULOCK
#define F_UNLCK fd_cntl¯UNLCK
#define F_WRLCK fd_cntl¯WRLCK

#define O_ACCMODE fd_open¯ACCMODE
#define O_APPEND  fd_open¯APPEND
#define O_ASYNC fd_open¯ASYNC
#define O_CLOEXEC fd_open¯CLOEXEC
#define O_CREAT fd_open¯CREAT
#define O_DIRECTORY fd_open¯DIRECTORY
#define O_DSYNC fd_open¯DSYNC
#define O_EXCL  fd_open¯EXCL
#define O_FSYNC fd_open¯FSYNC
#define O_NDELAY  fd_open¯NDELAY
#define O_NOCTTY  fd_open¯NOCTTY
#define O_NOFOLLOW  fd_open¯NOFOLLOW
#define O_NONBLOCK  fd_open¯NONBLOCK
#define O_RDONLY  fd_open¯RDONLY
#define O_RDWR  fd_open¯RDWR
#define O_RSYNC fd_open¯RSYNC
#define O_SYNC  fd_open¯SYNC
#define O_TRUNC fd_open¯TRUNC
#define O_TTY_INIT  fd_open¯TTY_INIT
#define O_WRONLY  fd_open¯WRONLY
