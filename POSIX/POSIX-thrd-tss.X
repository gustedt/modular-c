/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module tss   =
#pragma CMOD separator ¯
#pragma CMOD import thrd  = ..
#pragma CMOD import OS    = ..¯..
#pragma CMOD import key   = OS¯thread¯key

#pragma CMOD declaration

/**
 ** @addtogroup threads C11 thread emulation on top of POSIX threads
 **
 ** This is a relatively straightforward implementation of the C11
 ** thread model on top of POSIX threads. The main difficulty this presents
 ** is that the thread entry function signature differs between the
 ** two. C11 thread returns an <code>int</code> whereas POSIX returns
 ** a <code>void*</code>.
 **
 ** @{
 **/

/**
 ** @addtogroup thread_macros
 ** @{
 **/

#ifndef key¯DESTRUCTOR_ITERATIONS
# warning "definition of pthread¯DESTRUCTOR_ITERATIONS is missing"
/**
 ** @brief expands to an integer constant expression representing the
 ** maximum number of times that destructors will be called when a
 ** thread terminates
 ** @see tss
 **/
# define DTOR_ITERATIONS 1
#else
# define DTOR_ITERATIONS key¯DESTRUCTOR_ITERATIONS
#endif

/**
 ** @}
 **/


/**
 ** @addtogroup thread_types
 ** @{
 **/

/**
 ** @brief complete object type that holds an identifier for a
 ** thread-specific storage pointer
 **
 ** @see DECLARE_THREAD for a more comfortable interface to
 ** thread local variables
 **/
typedef key tss;

/**
 ** @brief which is the function pointer type <code>void
 ** (*)(void*)</code>, used for a destructor for a thread-specific
 ** storage pointer
 ** @see tss
 **/
typedef C¯interface¯dtor* dtor;

/**
 ** @}
 **/


/**
 ** @related tss
 ** @return ::thrd¯success on success, or ::thrd¯error if the request
 ** could not be honored.
 **
 ** If successful, sets the thread-specific storage pointed to by key
 ** to a value that uniquely identifies the newly created
 ** pointer. Otherwise, the thread-specific storage pointed to by key
 ** is set to an undefined value.
 **/
inline
int tss¯create(tss *key, dtor d) {
  return key¯create(key, d) ? (thrd¯status)thrd¯error : (thrd¯status)thrd¯success;
}

/**
 ** @related tss
 **/
inline
void tss¯delete(tss key) {
  (void)key¯delete(key);
}

/**
 ** @related tss
 **
 ** @return the value for the current thread if successful, or @c 0 if
 ** unsuccessful.
 **/
inline
void *tss¯get(tss key) {
  return key¯get(key);
}

/**
 ** @related tss
 ** @return ::thrd¯success on success, or ::thrd¯error if the request
 ** could not be honored.
 **/
inline
int tss¯set(tss key, void *val) {
  return key¯set(key, val) ? (thrd¯status)thrd¯error : (thrd¯status)thrd¯success;
}



/**
 ** @}
 **/
