/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module thrd  =
#pragma CMOD separator ¯
#pragma CMOD import io    = ..¯io
#pragma CMOD import pthread = ..¯thread
#pragma CMOD import attr  = ..¯thread¯attr
#pragma CMOD import sched = ..¯sched
#pragma CMOD import errno = ..¯errno
#pragma CMOD import spec  = ..¯time¯spec

#pragma CMOD init   bootstrap

#pragma CMOD intern cntxt
#pragma CMOD intern context
#pragma CMOD intern alloc

#pragma CMOD entry test

#pragma CMOD declaration

/**
 ** @brief C11 thread function return values
 **/
enum status {
  /**
   ** @brief returned by a timed wait function to indicate that the time specified in the call was reached without acquiring the requested resource
   **/
  timedout = errno¯TIMEDOUT,
  /**
   ** @brief returned by a function to indicate that the requested operation succeeded
   **/
  success = 0,
  /**
   ** @brief returned by a function to indicate that the requested
   ** operation failed because a resource requested by a test and
   ** return function is already in use
   **/
  busy = errno¯BUSY,
  /**
   ** @brief returned by a function to indicate that the requested operation failed
   **
   ** We choose a negative value such that we are call compatible
   ** between POSIX::time::spec::sleep and POSIX::thrd::sleep
   **/
  error = -2,
  /**
   ** @brief returned by a function to indicate that the requested
   ** operation failed because it was unable to allocate memory
   **/
  nomem = errno¯NOMEM,
  /**
   ** @brief (extension) returned by ::sleep to indicate that the
   ** corresponding request has been interrupted by a signal
   **/
  intr = -1
};

#define sleep spec¯sleep

inline
void yield(void) {
  if (sched¯yield()) errno = 0;
}

typedef struct cntxt cntxt;

typedef cntxt* thrd;

typedef C¯interface¯thrd* start;

/**
 ** @brief Test if two threads are equal.
 **
 ** @warning Only use this if both @a a and @a b still valid. A thrd
 ** becomes invalid once it has exited and it has either been joined
 ** or detached.
 **/
inline
int equal(thrd a, thrd b) {
  return a ≡ b;
}


#pragma CMOD definition

struct cntxt {
  pthread id;
  // Two different threads may want to change the state of a thread
  // concurrently: the thread itself on exit and another one that
  // either detaches or joins it. This flag is in the "clear" state as
  // long as none of these actions have happened, and in "set" state
  // once it has. Thereby any thread that finds the flag in the "set"
  // state can assume that it is the last action for this thread and
  // safely free this structure, later.
  C¯atomic¯flag last;
  // The function pointer and argument are never used concurrently
  // with the return value.
  union {
    struct {
      void* a;
      start f;
    };
    struct {
      C¯uintmax numb;
      int ret;
    };
  };
};

_Thread_local cntxt* context = nullptr;

static
bool make_last(cntxt* ctx) {
  return C¯atomic¯flag¯test_and_set(&ctx→last);
}

static
cntxt* init(cntxt*restrict ctx, start f, void*restrict a) {
  if (ctx) {
    *ctx = (cntxt) { .f = f, .a = a, .last = C¯atomic¯flag¯INIT, };
  }
  return ctx;
}

cntxt* alloc(bool last, start f, void*restrict a) {
  cntxt*restrict ctx = init(C¯lib¯malloc(sizeof(cntxt)), f, a);
  if (ctx ∧ last) make_last(ctx);
  return ctx;
}

#pragma CMOD declaration

/**
 ** @brief Return the current thread ID.
 **
 ** The use of this function in a thread that has not been created
 ** with thrd::create may lead to a small memory leak, just as all
 ** threads that are created and never joined or detached.
 **
 ** In particular, if you use this from the thread of "main" (or any
 ** @c entry of your program) and you are not planing to join this
 ** thread, later, detach it by using thrd::detach.
 **/
inline
thrd current(void) {
  // If we never generated an ID before, we should be the thread of
  // "main".
  if (¬context)
    context = alloc(false, nullptr, nullptr);
  return context;
}

#pragma CMOD definition

/**
 ** @brief A running ID of all threads that are created.
 **
 ** @remark This is an extension of the C standard.
 **
 ** @warning Only use this if @a thr is still valid. A thrd becomes
 ** invalid once it has exited and it has either been joined or
 ** detached.
 **/
C¯size number(thrd thr) {
  return thr→numb;
}

static
_Atomic(C¯size) count = 0;

static
void free(cntxt* context) {
  C¯lib¯free(context);
}

static
void givefree(cntxt* ctx) {
  bool islast = make_last(ctx);
  if (islast) free(ctx);
}


noreturn
void exit(int ret) {
  // If we have not been started as thrd and we never generated an ID,
  // we should be the thread of "main".
  if (context) {
    context→ret = ret;
    givefree(context);
    context = nullptr;
  }
  pthread¯exit(nullptr);
}

static
noreturn
void* run(void*restrict Context) {
  context = Context;
  start f = context→f;
  void* a = context→a;
  context→numb = count++;
  exit(f(a));
}

static
attr attr_detach;

/**
 ** @brief create a thread with start function @a f and argument @a a.
 **
 ** If @a tp is non-null, this returns the ID of the newly created
 ** thread in @c *tp.
 **
 ** If @a tp is null, we suppose that the new thread will never
 ** interact with the context of the caller, in particular we suppose
 ** that the thread will never be joined, and will be started
 ** detached.
 **
 ** @warning Don't use thrd::detach for such a thread.
 **
 ** This case of @a tp being null is undefined by the C standard, so
 ** this behavior implements an extension.
 **/
int create(thrd* tp, start f, void* a) {
  int pret = errno¯NOMEM;
  attr* detached = ¬tp ? &attr_detach : nullptr;
  cntxt* ctx = alloc(¬tp, f, a);
  if (ctx) {
    pret = pthread¯create(&ctx→id, detached, run, ctx);
    if (¬pret) {
      if (tp) *tp = ctx;
      return success;
    }
  }
  free(ctx);
  return  (pret ≡ errno¯NOMEM)
          ? nomem
          : error;
}

/**
 ** @brief Wait for the termination of thread @a thr, and return the
 ** exit code of that thread in @c *retp, if @a retp is non-null.
 **
 ** @a thr must not have been detached via thrd::detach or been
 ** started detached by thrd::create.
 **/
int join(thrd thr, int* retp) {
  int stat =
    pthread¯join(thr→id, nullptr)
    ? error
    : success;
  if (retp) *retp = thr→ret;
  givefree(thr);
  return stat;
}

/**
 ** @brief Declare that thread @a t will never be joined.
 **/
int detach(thrd thr) {
  int stat = success;
  pthread id = thr→id;
  givefree(thr);
  if(pthread¯detach(id)) stat = error;
  return stat;
}

void bootstrap(void) {
  attr¯init(&attr_detach);
  attr¯setdetachstate(&attr_detach, attr¯DETACHED);
}

static
int worker(void* arg) {
  double* x = arg;
  int ret = io¯printf("%zu: we have %g\n", number(current()), *x);
  C¯size sec = *x;
  spec t = {
    .tv_sec = sec,
    .tv_nsec = (*x-sec)×1E9,
  } ;
  while (sleep(&t, &t) ≡ -1) {
    io¯printf("%zu: we were interupted\n", number(current()));
  }
  return ret;
}

noreturn
static
int test(int argc, char* argv[argc+1]) {
  double X[argc+1];
  thrd  id[argc+1];
  //detach(current());
  for (C¯size i = 0; i < argc-1; ++i) {
    X[i] = C¯str¯tod(argv[i+1], 0);
  }
  for (unsigned round = 0; round < 10; ++round) {
    for (C¯size i = 0; i < argc-1; ++i) {
      X[i] ×= 0.95;
      create((i%2 ? 0 : &id[i]), worker, &X[i]);
    }
    for (C¯size i = 0; i < argc-1; i+=2) {
      int res;
      // id[i] is invalid as soon as we join.
      C¯uintmax numb = number(id[i]);
      join(id[i], &res);
      io¯printf("result for %ju (%zu): %d\n", numb, i, res);
    }
  }
  exit(0);
}
