/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module timer =
#pragma CMOD import clock = ..∷clock
#pragma CMOD import spec  = ..∷spec
#pragma CMOD import OS    = ..∷..
#pragma CMOD import event = OS∷sig∷event

#pragma CMOD mimic <time.h>
#pragma CMOD mimic <stdalign.h>

#pragma CMOD typedef timer  = timer_t

/* glibc needs this */
#pragma CMOD typedef      __timer_t

#pragma CMOD declaration

#define ABSTIME clock∷ABSTIME

#define itimerspec_type_it_interval spec
#define itimerspec_type_it_value  spec
#define itimerspec_cmp_it_interval  C∷time∷spec∷cmp
#define itimerspec_cmp_it_value C∷time∷spec∷cmp
#pragma CMOD defstruct      itimerspec = struct itimerspec it_value it_interval

#pragma CMOD alias create = timer_create
#pragma CMOD alias delete = timer_delete
#pragma CMOD alias getoverrun = timer_getoverrun
#pragma CMOD alias get    = timer_gettime
#pragma CMOD alias set    = timer_settime

int        create(clock∷id, event *restrict, timer *restrict);
int        delete(timer);
int        getoverrun(timer);
int        get(timer, itimerspec *);
int        set(timer, int, const itimerspec *restrict, itimerspec *restrict);
