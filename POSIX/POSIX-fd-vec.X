/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module vec  =
#pragma CMOD separator ¯
#pragma CMOD import OS  = ..¯..
#pragma CMOD import fd   = ..

#pragma CMOD mimic <sys/uio.h>
#pragma CMOD mimic <limits.h>
#pragma CMOD mimic <unistd.h>

#pragma CMOD defexp MAX = ¯IOV_MAX
#pragma CMOD defexp _MAX = ¯_SC_IOV_MAX, sysconf(_SC_IOV_MAX)

#pragma CMOD alias read = readv
#pragma CMOD alias write = writev

#pragma CMOD declaration

#ifndef MAX
# define MAX _MAX
#endif

typedef void* vec_type_iov_base;
typedef C¯size vec_type_iov_len;

#define equal vec¯vec_equal

#pragma CMOD defstruct vec = struct iovec iov_base iov_len

OS¯ssize read(fd, const vec*, int);
OS¯ssize write(fd, const vec*, int);
