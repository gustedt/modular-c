/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module uid =

#pragma CMOD mimic <unistd.h>
#pragma CMOD mimic <stdalign.h>

#pragma CMOD defexp __uid = "#define __uid %s", cmod_type(uid_t)


#pragma CMOD alias gete = geteuid
#pragma CMOD alias sete = seteuid
#pragma CMOD alias get  = getuid
#pragma CMOD alias set  = setuid
#pragma CMOD alias setre= setreuid

#pragma CMOD alias getlogin
#pragma CMOD alias getlogin_r

#pragma CMOD declaration

typedef __uid uid;

uid gete(void);
uid sete(uid);
uid get(void);
int set(uid);
int setre(uid, uid);

char* getlogin(void);
int getlogin_r(char*, C∷size);
