/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ¯
#pragma CMOD composer  —
#pragma CMOD import tsearch = ..¯..¯search

/**
 ** @file
 **
 ** @brief A typesafe interface for the POSIX tsearch functions.
 **/

#pragma CMOD snippet search = none // inline node search(key const k[static 1], node ro[static 1]);


/** @brief The sort key type for the search tree. **/
//#pragma CMOD slot key   = typedef
/** @brief A comparison function or macro that follows the usual C comparison conventions **/
//#pragma CMOD slot compar  = compatible int, key const*, key const*

#pragma CMOD declaration

/**
 ** @brief A function to be used with _Importer::walk.
 **
 ** For internal tree nodes such a function will be called three
 ** times, with @a v having three different values:
 **
 ** - tsearch¯first, before all other nodes in the subtree under the
 **   node for @a k are touched.
 **
 ** - tsearch¯second, after all nodes with a smaller key, and before
 **   all nodes with a larger key
 **
 ** - tsearch¯third, after all other nodes in the subtree under the
 **   node for @a k are touched.
 **
 ** For leaf nodes, the function is called exactly once, with @a v
 ** equal to tsearch¯leaf.
 **/
typedef void action(key*const k[static 1], tsearch¯visit v, int);

typedef void free(key* k);

/* This is used to hide the members of _Importer::node */
#pragma CMOD slot root    = intern
#pragma CMOD slot keyp    = intern

/**
 ** @brief An opaque type to handle a search tree with keys of type
 ** _Importer::key
 **
 ** You should use neither of the fields directly. These use internal
 ** names that change with every compilation.
 **/
union node {
  tsearch¯node* root;
  key const*const keyp;
};

/**
 ** @brief Obtain an expression that has the base type of the keys
 ** that are stored in this search tree.
 **
 ** You should only use this if
 ** - the key type is known to be a complete type
 ** - the expression itself is not evaluated, that is it should only
 **   appear in @c sizeof, @c alignof and @c _Generic.
 **/
#define TYPE(NODE) (*((NODE).keyp))

/**
 ** @brief return @c true iff @a r represents an empty search tree
 **/
inline
bool empty(node r) {
  return !r.root;
}

/**
 ** @brief Compare two search tree nodes for equality.
 **
 ** This is not the same as asking if the keys of the two nodes
 ** represent the same search value.
 **/
inline
bool equal(node r, node s) {
  return r.root == s.root;
}

/**
 ** @brief Get the key that is registered with the (root) node.
 **/
inline
key* get_key(node r) {
  key* ret = 0;
  if (!empty(r)) {
    void*const* k = (void*const*)r.root;
    ret = *k;
  }
  return ret;
}

#pragma CMOD slot kompar  = intern
inline
int kompar(tsearch¯key const* a, tsearch¯key const* b) {
  key const* A = a;
  key const* B = b;
  return compar(A, B);
}

/**
 ** @brief Search for key @a k and insert it, if it is not found.
 **
 ** @return the node that represents @a k
 **/
inline
node search(key const k[static restrict 1], node ro[static 1]) {
  tsearch¯node* r   = ro[0].root;
  node ret = { .root = tsearch¯search(k, &r, kompar), };
  ro[0].root = r;
  return ret;
}

/**
 ** @brief Find key @a k, if it is already in.
 **
 ** @return a node that represents @a k, if any or an empty node
 ** otherwise.
 **
 ** @see _Importer::empty to test if the returned node is empty.
 **
 ** As long as there are no key added or removed from the tree
 ** represented by @a ro, and as long as the system does no internal
 ** re-organization of the tree under such conditions, this function
 ** should be thread safe.
 **
 ** @remark Accesses the tree represented by @a ro read-only.
 **/
inline
node find(key const k[static restrict 1], node ro[static 1]) {
  tsearch¯node* r   = ro[0].root;
  node ret = { .root = tsearch¯find(k, &r, kompar), };
  ro[0].root = r;
  return ret;
}

/**
 ** @brief Delete key @a k from @a ro if it is already in.
 **
 ** @return The parent node of the node that represented @a k, if any or
 ** an empty node otherwise.
 **
 ** @see _Importer::empty to test if the returned node is empty.
 **/
inline
node delete(key const k[static restrict 1], node ro[static 1]) {
  tsearch¯node* r   = ro[0].root;
  node ret = { .root = tsearch¯delete(k, &r, kompar), };
  ro[0].root = r;
  return ret;
}

/**
 ** @brief Apply @a action to each node of @a ro.
 **
 ** @a act is a function with a prototype of _Importer::action.
 **/
inline
void walk(node ro, action act) {
  tsearch¯walk(ro.root, (tsearch¯action*)act);
}

/**
 ** @see POSIX::search::minimum
 **/
inline
key* minimum(node ro) {
  return tsearch¯minimum(ro.root);
}

/**
 ** @see POSIX::search::count
 **/
inline
C¯size count(node ro) {
  return tsearch¯count(ro.root);
}


/**
 ** @brief Destroy a whole search tree.
 **
 ** This deletes all nodes in the search tree and calls @a f for all
 ** keys, which usually would be a function that frees the storage of
 ** the key.
 **
 ** If @a f is @c 0 or omitted, no key-specific action is taken.
 **/
inline
void (destroy)(node ro[static 1], free f) {
  while(!empty(ro[0])) {
    key* k = get_key(ro[0]);
    delete(k, ro);
    if (f && k) f(k);
  }
}

#define destroy(...) destroy—3(__VA_ARGS__, 0, 0)
#define destroy—3(R, F, ...) destroy((R), F)

#pragma CMOD definition

#if C¯thrd¯local
static  C¯thrd¯local key**  lin—act;
#else
static  key**  lin—act;
#endif

static void lin—action(tsearch¯node const* k, tsearch¯visit v, int level) {
  switch(v) {
  case tsearch¯leaf:;
  case tsearch¯second:;
    lin—act[0] = *(void*const*)k;
    ++lin—act;
  default:;
  }
}

/**
 ** @brief Linearize the values in a search tree in sort order.
 **
 ** This uses a thread local variable to help the function to be
 ** thread safe. This is, as long as there are no key added or removed
 ** from the tree represented by @a ro, and as long as the system does
 ** no internal re-organization of the tree under such conditions,
 ** this function should be thread safe.
 **
 ** @remark Accesses the tree represented by @a ro read-only.
 **
 ** @return A pointer to a table <code>key*tab[len+1]</code>, where
 ** @c is the number of elements in the search tree. Element
 ** <code>tab[len]</code> is a null pointer that indicates the end of
 ** the table.
 **
 ** @remark The caller is responsible to free the returned table.
 **/
key** linearize(node ro) {
  C¯size len = tsearch¯count(ro.root);
  key** ret = C¯lib¯malloc(sizeof(key*[len+1]));
  if (ret) {
    lin—act = ret;
    tsearch¯walk(ro.root, lin—action);
    lin—act[0] = 0;
    lin—act    = nullptr;
  }
  return ret;
}
