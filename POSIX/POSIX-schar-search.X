/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module search  =
#pragma CMOD separator ¯
#pragma CMOD import tsearch = ..¯..¯search

#pragma CMOD declaration

#define key C¯schar
#define compar  C¯schar¯compar

#pragma CMOD import     = ..¯..¯snippet¯search

#pragma CMOD entry test

#pragma CMOD definition

void print(C¯schar*const node[1], tsearch¯visit v, int level) {
  switch (v) {
  case tsearch¯leaf:;
    if (level) C¯io¯fputs("  ", C¯io¯out);
    for (C¯size i = 1; i < level; ++i)
      C¯io¯fputs("| ", C¯io¯out);
    C¯io¯printf("|-%hhd\n", node[0][0]);
    break;
  case tsearch¯second:;
    C¯io¯fputs("  ", C¯io¯out);
    for (C¯size i = 0; i < level; ++i)
      C¯io¯fputs("| ", C¯io¯out);
    C¯io¯puts("/");
    if (level) C¯io¯fputs("  ", C¯io¯out);
    for (C¯size i = 1; i < level; ++i)
      C¯io¯fputs("| ", C¯io¯out);
    C¯io¯printf("|-%hhd\n", node[0][0]);
    C¯io¯fputs("  ", C¯io¯out);
    for (C¯size i = 0; i < level; ++i)
      C¯io¯fputs("| ", C¯io¯out);
    C¯io¯puts("\\");
    break;
  default:;
  }
}

int test(int argc, char* argv[argc+1]) {
  node r[1] = { 0 };
  C¯schar keys[50];
  for (C¯size i = 0; i < 50; ++i) {
    keys[i] = i+23;
    search(&keys[i], r);
  }
  walk(r[0], print);
  return 0;
}
