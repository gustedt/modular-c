/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module search  =
#pragma CMOD separator ¯
#pragma CMOD import tsearch = ..¯..¯search

#pragma CMOD declaration

#define key C¯char
#define compar  C¯str¯cmp

#pragma CMOD import     = ..¯..¯snippet¯search

#pragma CMOD entry test

#pragma CMOD definition

void print(char*const node[1], tsearch¯visit v, int level) {
  switch (v) {
  case tsearch¯leaf:;
    if (level) C¯io¯fputs("  ", C¯io¯out);
    for (C¯size i = 1; i < level; ++i)
      C¯io¯fputs("| ", C¯io¯out);
    C¯io¯printf("|-%s\n", node[0]);
    break;
  case tsearch¯second:;
    C¯io¯fputs("  ", C¯io¯out);
    for (C¯size i = 0; i < level; ++i)
      C¯io¯fputs("| ", C¯io¯out);
    C¯io¯puts("/");
    if (level) C¯io¯fputs("  ", C¯io¯out);
    for (C¯size i = 1; i < level; ++i)
      C¯io¯fputs("| ", C¯io¯out);
    C¯io¯printf("|-%s\n", node[0]);
    C¯io¯fputs("  ", C¯io¯out);
    for (C¯size i = 0; i < level; ++i)
      C¯io¯fputs("| ", C¯io¯out);
    C¯io¯puts("\\");
    break;
  default:;
  }
}

void not—free(char* k) {
  C¯io¯printf("\t!%s", k);
}

int test(int argc, char* argv[argc+1]) {
  node r[1] = { 0 };
  char* keys[20] = {
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
    (char[10]){ 0 },
  };
  for (C¯size i = 0; i < 20; ++i) {
    C¯io¯snprintf(keys[i], 10, "%c%zu", (char)(i+57), i+23);
    search(keys[i], r);
  }
  walk(r[0], print);
  char* minval = search¯minimum(r[0]);
  char* topval = search¯get_key(r[0]);
  C¯io¯printf("minimum value is %s, top value is %s\n", minval, topval);
  search¯delete(minval, r);
  minval = search¯minimum(r[0]);
  topval = search¯get_key(r[0]);
  C¯io¯printf("minimum value is %s, top value is %s\n", minval, topval);
  search¯delete(topval, r);
  minval = search¯minimum(r[0]);
  topval = search¯get_key(r[0]);
  C¯io¯printf("minimum value is %s, top value is %s\n", minval, topval);
  walk(r[0], print);
  char** tab = linearize(r[0]);
  for (char** p = tab; p[0]; ++p)
    C¯io¯printf("\t%s", p[0]);
  C¯io¯puts("");
  C¯lib¯free(tab);
  C¯io¯puts("freeing search tree:");
  destroy(r, not—free);
  C¯io¯puts("");
  return 0;
}
