/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module family =
#pragma CMOD separator ¯

#pragma CMOD mimic <sys/socket.h>

#pragma CMOD defrex \2 = \(AF_\([A-Z][A-Z0-9]*\)\)

#pragma CMOD defexp __family = "#define __family %s", cmod_type(sa_family_t)

#pragma CMOD declaration

typedef __family family;
