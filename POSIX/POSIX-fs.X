/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module fs  =
#pragma CMOD import OS  = ..
#pragma CMOD import uid = OS∷uid
#pragma CMOD import gid = OS∷gid
#pragma CMOD import mode= OS∷mode

#pragma CMOD mimic <sys/types.h>
#pragma CMOD mimic <sys/stat.h>
#pragma CMOD mimic <unistd.h>
#pragma CMOD mimic <stdalign.h>
#pragma CMOD mimic <libgen.h>

#pragma CMOD alias access
#pragma CMOD alias basename
#pragma CMOD alias chdir
#pragma CMOD alias chown
#pragma CMOD alias dirname
#pragma CMOD alias getcwd
#pragma CMOD alias lchown
#pragma CMOD alias link
#pragma CMOD alias pathconf
#pragma CMOD alias readlink
#pragma CMOD alias realpath
#pragma CMOD alias rmdir
#pragma CMOD alias symlink
#pragma CMOD alias truncate
#pragma CMOD alias unlink
#pragma CMOD alias chmod

#pragma CMOD define basename
#pragma CMOD define dirname

#pragma CMOD declaration

#define charp char*

#pragma CMOD defexp offset = "#define __dev %s", cmod_type(dev_t)
#pragma CMOD defexp offset = "#define __ino %s", cmod_type(ino_t)
#pragma CMOD defexp offset = "#define __nlink %s", cmod_type(nlink_t)
#pragma CMOD defexp offset = "#define __blksize %s", cmod_type(blksize_t)

typedef __dev device;
typedef __ino inode;
typedef __nlink nlink;
typedef __blksize blksize;

int access(const char*, int);
charp (basename)(char *);
int chdir(const char *);
int chown(const char *, uid, gid);
charp (dirname)(char *);
char* getcwd(char*, C∷size);
int lchown(const char*, uid, gid);
int link(const char*, const char*);
long pathconf(const char*, int);
OS∷ssize readlink(const char*restrict, char*restrict, C∷size);
int rmdir(const char*);
int symlink(const char *, const char *);
int truncate(const char *, OS∷offset);
int unlink(const char*);
extern char* realpath(char const*restrict, char *restrict);
int chmod(char const*, mode);

#define remove  C∷fs∷remove
#define rename  C∷fs∷rename
