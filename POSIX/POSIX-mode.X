/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module mode  =

#pragma CMOD mimic <sys/types.h>
#pragma CMOD mimic <sys/stat.h>

#pragma CMOD declaration

#pragma CMOD defexp _POSIX_mode = "#define _POSIX_mode %s", cmod_type(mode_t)

#pragma CMOD alias umask

typedef _POSIX_mode mode;

mode umask(mode);
