To compile all of cmod you should usually not much more than a C
compiler and a Linux system

make -j 4

(replace the 4 with the number of cores in your system)

*should* produce all the needed libraries in the directory lib.  But
the devil is in the details.

The current build system works a lot with temporary files that are
setup in a temporary directory, usually underneath /tmp. Best
performance (for the compilation) is achieved when that directory is
on an in-memory filesystem like Linux' tmpfs. Also, if you run in
parallel and you have a small /tmp you may run out of space,
there. You can provide a different temporary directory with the TMP
environment variable.

We don't provide an "install" target, yet. If you'd want to use Cmod
from outside of the tree, in a directory CMODDIR, say, you'd need to
copy over

CMODDIR/bin
CMODDIR/lib

"bin" contains some soft links, so you'd have to make sure that for an
installation you do a real copy of the files, not only the link.

If you find such a thing useful, you could dig into providing a make
target that does that, don't be shy and send me patches.

* C library

Linux can come with different versions of the C library. We have
tested and interfaced

 glibc
 musl libc

but the success may depend on the version of the library that you
have.

If you have an older glibc system, you might need to add

CMOD_UCONTEXT=ucontex

because their API has changed over time, here.

* C compiler

To change the C compiler to something else than the default use

CC=gcc-7

or similar. The newer the C compiler the better, it should at least be
conforming to C11.

* Atomics and threads

Not all C compilers and libraries nowadays provide valid stdatomic or
C11 threads, we try to work with what we find. Older versions of
stdatomic can sometimes fail to provide all call interfaces that are
needed to implement the standard. In particular, older versions of gcc
miss out calls for the standard type atomic_flag.

* OpenMP

The OpenMP interface is quite simple, still. To enable it you need to
pass

CMOD_OPENMP=1

to the compilation.

But notice that atomics and OpenMP are incompatible for gcc before
version 7.

* EiLck

Compilation of this standalone library may seem to fail if you don't
add

CMOD_OPENMP=1

To compile the C++ stubs for it you need a C++ compiler. If you want
to change it, e.g to make it compatible with the C compiler use
something like

CXX=g++-7
