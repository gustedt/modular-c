/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module machine = Linux¯machine

#pragma CMOD mimic <signal.h>
#pragma CMOD mimic <ucontext.h>

#pragma CMOD defexp SIZE = sizeof(mcontext_t)
#pragma CMOD defexp ALIGN = _Alignof(mcontext_t)

#pragma CMOD declaration

typedef Linux¯greg¯set machine_type_gregs;
typedef Linux¯fpreg¯set machine_type_fpregs;

#pragma CMOD defstruct machine = mcontext_t gregs fpregs
