/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module futex =
#pragma CMOD separator  ∷
#pragma CMOD composer —


#pragma CMOD declaration

#define WAIT      0
#define WAKE      1
#define FD      2
#define REQUEUE     3
#define CMP_REQUEUE   4
#define WAKE_OP     5
#define LOCK_PI     6
#define UNLOCK_PI   7
#define TRYLOCK_PI    8
#define WAIT_BITSET   9
#define FUTEX_WAKE_BITSET 10
#define FUTEX_WAIT_REQUEUE_PI 11
#define FUTEX_CMP_REQUEUE_PI  12

#define PRIVATE     128
#define CLOCK_REALTIME    256

inline
int timed(int const volatile _Atomic* uaddr, int futex_op, int val,
          C∷time∷spec const* timeout,
          int const volatile _Atomic* uaddr2, int val3) {
  return Linux∷sys∷call((long)Linux∷sys∷futex,
                            (long)uaddr,
                            (long)futex_op,
                            (long)val,
                            (long)timeout,
                            (long)uaddr2,
                            (long)val3);
}

inline
int valued(int const volatile _Atomic* uaddr, int futex_op, int val,
           C∷integer∷u32 val2,
           int const volatile _Atomic* uaddr2, int val3) {
  return Linux∷sys∷call((long)Linux∷sys∷futex,
                            (long)uaddr,
                            (long)futex_op,
                            (long)val,
                            (long)val2,
                            (long)uaddr2,
                            (long)val3);
}

#define generic(_0, _1, _2, _3, _4, _5, _6, ...)               \
_Generic(_3,                                                   \
         C∷time∷spec*      : timed,                            \
         C∷time∷spec const*: timed,                            \
         default           : valued)                           \
(_0, _1, _2, _3, _4, _5, _6)

#define futex(...) generic(__VA_ARGS__, 0L, 0L, 0L, 0L, 0L, 0L, 0L, )

/**
 **
 ** @brief Suspend execution of the calling thread until it is woken
 ** up, again.
 **
 ** @param uaddr address of the futex
 **
 ** @param val expected value of the
 ** futex. The operation is aborted if there is no match.
 **
 ** @param shared if @c 1, the futex is shared with other
 ** processes. Defaults to @c 0.
 **
 ** @param t a relative monotonic time to wait. If @c 0 or omitted,
 ** wait indefinitively until woke up.
 **
 ** @param absolute @a t is iterpreted as an absolute time.
 **
 ** @param realtime the @c CLOCK_REALTIME is used instead of @c
 ** CLOCK_MONOTONIC.
 **/
inline
int (wait)(int const volatile _Atomic* uaddr, int val, int shared, C∷time∷spec const* t, int absolute, int realtime) {
  int cmd =
    (absolute ? WAIT_BITSET : WAIT)
    ∪ (shared ? 0 : PRIVATE)
    ∪ (realtime ? 0 : CLOCK_REALTIME);
  // The final -1 is the correct mask for WAIT_BITSET and ignored for
  // WAIT.
  return timed(uaddr, cmd, val, t, 0, -1);
}

#define wait—def(_0, _1, _2, _3, _4, _5, ...) wait(_0, _1, _2, _3, _4, _5)

#define wait(...) wait—def(__VA_ARGS__, 0, 0, 0, 0, 0, )

/**
 ** @brief Wake up waiting threads, if any.
 **
 ** @param uaddr address of the futex
 **
 ** @param cnt the maximal number of threads to wake.
 **
 ** @param shared if @c 1, the futex is shared with other
 ** processes. Defaults to @c 0.
 **/
inline
int (wake)(int const volatile _Atomic* uaddr, int cnt, int shared) {
  return valued(uaddr, WAKE ∪ (shared ? 0 : PRIVATE), cnt, 0, 0, 0);
}

#define wake—def(_0, _1, _2, ...) wake(_0, _1, _2)

#define wake(...) wake—def(__VA_ARGS__, 0, 0, )

/**
 ** @brief Wake up one waiting thread, if any.
 **
 ** @param uaddr address of the futex
 **
 ** @param shared if @c 1, the futex is shared with other
 ** processes. Defaults to @c 0.
 **/
inline
int (signal)(int const volatile _Atomic* uaddr, int shared) {
  return wake(uaddr, 1, shared);
}

#define signal—def(_0, _1, ...) signal(_0, _1)

#define signal(...) signal—def(__VA_ARGS__, 0, )

/**
 ** @brief Wake up all waiting thread, if any.
 **
 ** @param uaddr address of the futex
 **
 ** @param shared if @c 1, the futex is shared with other
 ** processes. Defaults to @c 0.
 **/
inline
int (broadcast)(int const volatile _Atomic* uaddr, int shared) {
  return wake(uaddr, C∷int∷MAX, shared);
}

#define broadcast—def(_0, _1, ...) broadcast(_0, _1)

#define broadcast(...) broadcast—def(__VA_ARGS__, 0, )

/**
 **
 ** @brief Unconditionally wake up threads and requeue others.
 **
 ** @param uaddr address of the futex
 **
 ** @param cnt the maximal number of threads to wake.
 **
 ** @param target an alternative futex to which remaining waiters will
 ** be requeued
 **
 ** @param tcnt the maximal number of threads to requeue
 **
 ** @param shared if @c 1, the futex is shared with other
 ** processes. Defaults to @c 0.
 **/
inline
int (requeue)(int const volatile _Atomic* uaddr, int cnt, int const volatile _Atomic* target, int tcnt, int shared) {
  return valued(uaddr, REQUEUE ∪ (shared ? 0 : PRIVATE), cnt, tcnt, target, 0);
}

#define requeue—def(_0, _1, _2, _3, _4, ...) requeue(_0, _1, _2, _3, _4)

#define requeue(...) requeue—def(__VA_ARGS__, 0, 0, 0, 0, )

inline
int (move)(int const volatile _Atomic* uaddr, int const volatile _Atomic* target, int cnt, int shared) {
  return requeue(uaddr, 0, target, cnt, shared);
}

#define move—def(_0, _1, _2, ...) move(_0, _1, _2)

#define move(...) move—def(__VA_ARGS__, 0, )

/**
 **
 ** @brief Conditionally wake up threads and requeue others.
 **
 ** @param uaddr address of the futex
 **
 ** @param val expected value of the futex. The operation is aborted
 ** if there is no match.
 **
 ** @param cnt the maximal number of threads to wake.
 **
 ** @param target an alternative futex to which remaining waiters will
 ** be requeued
 **
 ** @param tcnt the maximal number of threads to requeue
 **
 ** @param shared if @c 1, the futex is shared with other
 ** processes. Defaults to @c 0.
 **/
inline
int (crequeue)(int const volatile _Atomic* uaddr, int val, int cnt, int const volatile _Atomic* target, int tcnt, int shared) {
  return valued(uaddr, CMP_REQUEUE ∪ (shared ? 0 : PRIVATE), cnt, tcnt, target, val);
}

#define crequeue—def(_0, _1, _2, _3, _4, _5, ...) crequeue(_0, _1, _2, _3, _4, _5)

#define crequeue(...) crequeue—def(__VA_ARGS__, 0, 0, 0, 0, )

inline
int (cwake)(int const volatile _Atomic* uaddr, int val, int cnt, int shared) {
  return crequeue(uaddr, val, cnt, 0, 0, shared);
}

#define cwake—def(_0, _1, _2, _3, ...) cwake(_0, _1, _2, _3)

#define cwake(...) cwake—def(__VA_ARGS__, 0, )

inline
int (cmove)(int const volatile _Atomic* uaddr, int val, int const volatile _Atomic* target, int cnt, int shared) {
  return crequeue(uaddr, val, 0, target, cnt, shared);
}

#define cmove—def(_0, _1, _2, _3, ...) cmove(_0, _1, _2, _3)

#define cmove(...) cmove—def(__VA_ARGS__, 0, )
