/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ¯

/**
 ** @brief A module hierarchy that imports C library features one to
 ** one directly into the name space of the importer.
 **
 ** You only want to use these transitionally when migrating existing
 ** C source to Modular C.
 **/

#pragma CMOD snippet none
#pragma CMOD declaration

/* inheritance from C */

typedef C¯sig¯atomic sig_atomic_t;

#define SIGABRT C¯sig¯ABRT
#define SIGFPE  C¯sig¯FPE
#define SIGILL  C¯sig¯ILL
#define SIGINT  C¯sig¯INT
#define SIGSEGV C¯sig¯SEGV
#define SIGTERM C¯sig¯TERM
#define SIG_DFL C¯sig¯DFL
#define SIG_ERR C¯sig¯ERR
#define SIG_IGN C¯sig¯IGN

#define signal  C¯sig¯signal
#define raise C¯sig¯raise

/* POSIX proper */

#define SIGRTMIN Linux¯sig¯RTMIN
#define SIGRTMAX Linux¯sig¯RTMAX

#define SIGALRM Linux¯sig¯ALRM
#define SIGBUS Linux¯sig¯BUS
#define SIGCHLD Linux¯sig¯CHLD
#define SIGCONT Linux¯sig¯CONT
#define SIGHUP Linux¯sig¯HUP
#define SIGKILL Linux¯sig¯KILL
#define SIGPIPE Linux¯sig¯PIPE
#define SIGQUIT Linux¯sig¯QUIT
#define SIGSTOP Linux¯sig¯STOP
#define SIGTSTP Linux¯sig¯TSTP
#define SIGTTIN Linux¯sig¯TTIN
#define SIGTTOU Linux¯sig¯TTOU
#define SIGUSR1 Linux¯sig¯USR1
#define SIGUSR2 Linux¯sig¯USR2
#define SIGPOLL Linux¯sig¯POLL
#define SIGPROF Linux¯sig¯PROF
#define SIGSYS Linux¯sig¯SYS
#define SIGTRAP Linux¯sig¯TRAP
#define SIGURG Linux¯sig¯URG
#define SIGVTALRM Linux¯sig¯VTALRM
#define SIGXCPU Linux¯sig¯XCPU
#define SIGXFSZ Linux¯sig¯XFSZ

#define SA_NOCLDSTOP Linux¯sig¯action¯NOCLDSTOP
#define SA_NOCLDWAIT Linux¯sig¯action¯NOCLDWAIT
#define SA_NODEFER Linux¯sig¯action¯NODEFER
#define SA_ONSTACK Linux¯sig¯action¯ONSTACK
#define SA_RESETHAND Linux¯sig¯action¯RESETHAND
#define SA_RESTART Linux¯sig¯action¯RESTART
#define SA_RESTORER Linux¯sig¯action¯RESTORER
#define SA_SIGINFO Linux¯sig¯action¯SIGINFO

#define SI_USER Linux¯sig¯info¯USER
#define SI_KERNEL Linux¯sig¯info¯KERNEL
#define SI_QUEUE Linux¯sig¯info¯QUEUE
#define SI_TIMER Linux¯sig¯info¯TIMER
#define SI_MESGQ Linux¯sig¯info¯MESGQ
#define SI_ASYNCIO Linux¯sig¯info¯ASYNCIO
#define SI_SIGIO Linux¯sig¯info¯SIGIO
#define SI_TKILL Linux¯sig¯info¯TKILL
#define ILL_ILLOPC Linux¯sig¯info¯ILLOPC
#define ILL_ILLOPN Linux¯sig¯info¯ILLOPN
#define ILL_ILLADR Linux¯sig¯info¯ILLADR
#define ILL_ILLTRP Linux¯sig¯info¯ILLTRP
#define ILL_PRVOPC Linux¯sig¯info¯PRVOPC
#define ILL_PRVREG Linux¯sig¯info¯PRVREG
#define ILL_COPROC Linux¯sig¯info¯COPROC
#define ILL_BADSTK Linux¯sig¯info¯BADSTK
#define FPE_INTDIV Linux¯sig¯info¯INTDIV
#define FPE_INTOVF Linux¯sig¯info¯INTOVF
#define FPE_FLTDIV Linux¯sig¯info¯FLTDIV
#define FPE_FLTOVF Linux¯sig¯info¯FLTOVF
#define FPE_FLTUND Linux¯sig¯info¯FLTUND
#define FPE_FLTRES Linux¯sig¯info¯FLTRES
#define FPE_FLTINV Linux¯sig¯info¯FLTINV
#define FPE_FLTSUB Linux¯sig¯info¯FLTSUB
#define SEGV_MAPERR Linux¯sig¯info¯MAPERR
#define SEGV_ACCERR Linux¯sig¯info¯ACCERR
#define SEGV_BNDERR Linux¯sig¯info¯BNDERR
#define SEGV_PKUERR Linux¯sig¯info¯PKUERR
#define BUS_ADRALN Linux¯sig¯info¯ADRALN
#define BUS_ADRERR Linux¯sig¯info¯ADRERR
#define BUS_OBJERR Linux¯sig¯info¯OBJERR
#define BUS_MCEERR_AR Linux¯sig¯info¯MCEERR_AR
#define BUS_MCEERR_AO Linux¯sig¯info¯MCEERR_AO
#define TRAP_BRKPT Linux¯sig¯info¯BRKPT
#define TRAP_TRACE Linux¯sig¯info¯TRACE
#define TRAP_BRANCH Linux¯sig¯info¯BRANCH
#define TRAP_HWBKPT Linux¯sig¯info¯HWBKPT
#define CLD_EXITED Linux¯sig¯info¯EXITED
#define CLD_KILLED Linux¯sig¯info¯KILLED
#define CLD_DUMPED Linux¯sig¯info¯DUMPED
#define CLD_TRAPPED Linux¯sig¯info¯TRAPPED
#define CLD_STOPPED Linux¯sig¯info¯STOPPED
#define CLD_CONTINUED Linux¯sig¯info¯CONTINUED
#define POLL_IN Linux¯sig¯info¯IN
#define POLL_OUT Linux¯sig¯info¯OUT
#define POLL_MSG Linux¯sig¯info¯MSG
#define POLL_ERR Linux¯sig¯info¯ERR
#define POLL_PRI Linux¯sig¯info¯PRI
#define POLL_HUP Linux¯sig¯info¯HUP
#define SYS_SECCOMP Linux¯sig¯info¯SECCOMP

#define SS_ONSTACK Linux¯sig¯stack¯ONSTACK
#define SS_DISABLE Linux¯sig¯stack¯DISABLE
#define SIGSTKSZ Linux¯sig¯stack¯MIN
#define MINSIGSTKSZ Linux¯sig¯stack¯DEFAULT

/* This replaces struct sigaction. We can't do the later because there
   also is a function sigaction. */
typedef Linux¯sig¯action sigaction_t;

#define sigaction Linux¯sig¯action¯set

typedef Linux¯sig¯set sigset_t;
typedef Linux¯sig¯val sigval_t;
typedef Linux¯sig¯info siginfo_t;
typedef Linux¯sig¯stack stack_t;

#define kill Linux¯sig¯kill
#define killpg Linux¯sig¯killpg
#define psignal Linux¯sig¯psignal

#define sigaction Linux¯sig¯action¯set

#define sigaltstack Linux¯sig¯stack¯set

#define sighold Linux¯sig¯sighold
#define sigignore Linux¯sig¯sigignore
#define siginterrupt Linux¯sig¯interrupt
//void (*signal(int, void (*)(int)))(int);
#define sigpause Linux¯sig¯sigpause
#define sigqueue Linux¯sig¯val¯queue
#define sigrelse Linux¯sig¯sigrelse
#define sigset Linux¯sig¯sigset
#define psiginfo Linux¯sig¯info¯puts

#define sigaddset Linux¯sig¯set¯add
#define sigdelset Linux¯sig¯set¯del
#define sigemptyset Linux¯sig¯set¯empty
#define sigfillset Linux¯sig¯set¯fill
#define sigismember Linux¯sig¯set¯member
#define sigpending Linux¯sig¯set¯pending
#define sigsuspend Linux¯sig¯set¯suspend
#define sigtimedwait Linux¯sig¯set¯timedwait
#define sigwait Linux¯sig¯set¯wait
#define sigwaitinfo Linux¯sig¯set¯waitinfo
#define sigprocmask Linux¯sig¯set¯mask_proc
#define pthread_sigmask Linux¯sig¯set¯mask_thread
