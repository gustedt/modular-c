/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module info =
#pragma CMOD separator ¯
#pragma CMOD import pid = ..¯..¯process¯id
#pragma CMOD import uid = ..¯..¯uid
#pragma CMOD import val = ..¯val

#pragma CMOD mimic <signal.h>
#pragma CMOD mimic <sys/types.h>

#pragma CMOD defexp SIZE  = sizeof(siginfo_t)
#pragma CMOD defexp ALIGN = _Alignof(siginfo_t)

#pragma CMOD defexp FIELDS_SIZE = sizeof(siginfo_t)-(offsetof(siginfo_t, si_code)+sizeof((siginfo_t){0}.si_code))

#pragma CMOD defrex \2=\(SI_\([a-zA-Z][a-zA-Z0-9_]*\)\)
#pragma CMOD defrex \2=\(ILL_\([a-zA-Z][a-zA-Z0-9_]*\)\)
#pragma CMOD defrex \2=\(FPE_\([a-zA-Z][a-zA-Z0-9_]*\)\)
#pragma CMOD defrex \2=\(SEGV_\([a-zA-Z][a-zA-Z0-9_]*\)\)
#pragma CMOD defrex \2=\(BUS_\([a-zA-Z][a-zA-Z0-9_]*\)\)
#pragma CMOD defrex \2=\(TRAP_\([a-zA-Z][a-zA-Z0-9_]*\)\)
#pragma CMOD defrex \2=\(CLD_\([a-zA-Z][a-zA-Z0-9_]*\)\)
#pragma CMOD defrex \2=\(POLL_\([a-zA-Z][a-zA-Z0-9_]*\)\)
#pragma CMOD defrex \2=\(SYS_\([a-zA-Z][a-zA-Z0-9_]*\)\)

#pragma CMOD declaration

struct info {
  _Alignas(ALIGN)
  int si_signo;
  int si_errno;
  int si_code;
  union {
    char all[FIELDS_SIZE];
    /* timers */
    struct {
      int si_tid;
      int si_overrun;
    };
    /* kill().  */
    struct {
      pid si_pid;
      uid si_uid;
      union {
        /* RT signals.  */
        val si_value;
        /* SIGCHLD.  */
        int si_status;
      };
    };
    /* SIGBUS, SIGFPE, SIGILL, SIGSEGV.  */
    struct {
      void *si_addr;
      short si_addr_lsb;
    };
    /* SIGPOLL.  */
    struct {
      long si_band;
      int si_fd;
    };
    /* SIGSYS.  */
    struct {
      void *_call_addr;
      int _syscall;
      unsigned _arch;
    };
  };
};

#pragma CMOD alias puts = psiginfo

void puts(const info*, const char*);
