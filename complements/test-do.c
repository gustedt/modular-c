
// A pragma that has no corresponding "done" directive spans the rest
// of the whole file.
#pragma CMOD eval ALL DIM
// Similarly, since this directive has no 'done', these environment
// variable can be used everywhere.
#pragma CMOD env MUMP DIM

#include "test-do.h"

// Variable "DIMENSION" has a default value, the `-ter -nar` being
// some cryptic form of ternary evaluation.
#pragma DIM let DIMENSION = (${DIM}0 -ter ${DIM} -nar 4)

// Initialize a 1D array

double A[] = {
#pragma ALL do N = ${DIMENSION}
[${N}] = ${N} + 1,
#pragma ALL done
};

// Initialize a 2D array
double B[][${DIMENSION}] = {

#pragma ALL do N = ${DIMENSION}
  [${N}] = {
#pragma ALL do M = ${DIMENSION}
  [${M}] = ${M},
#pragma ALL done
  },
#pragma ALL done

};

// Initialize a 2D array with dependent 2nd loop bound,
// two expansions
double C[][${DIMENSION}] = {

// vvvvv 2nd expansion INNER
#pragma ALL eval INNER
// vvvvv 1st expansion DIM
#pragma ALL do L = 1 6
  [${L}-1] = {
#pragma INNER do M = ${L}
  [${M}] = ${M},
#pragma INNER done
  },
#pragma ALL done
// ^^^^^ 1st expansion
#pragma ALL done
// ^^^^^ 2nd expansion

};

// Initialize a 2D array with dependent 2nd loop bound,
// three expansions
double D[][${DIMENSION}] = {

// vvvvv 3rd expansion INNER
// vvvvv 2nd expansion EVAL
#pragma ALL eval INNER EVAL
// vvvvv 1st expansion ALL
#pragma ALL do N = ${DIMENSION}
#pragma EVAL let L = ${N} + 1
  [${N}] = {
#pragma INNER do M = ${L}
  [${M}] = ${M},
#pragma INNER done
  },
#pragma EVAL done
#pragma ALL done
// ^^^^^ 1st expansion
#pragma ALL done
// ^^^^^ 2nd expansion
// ^^^^^ 3rd expansion

};


int main(void) {
  return UINT16_MAX * (D[1][3] ≡ D[0][6]);
}
