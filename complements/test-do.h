#ifndef TEST_DO_H
#define TEST_DO_H

// A pragma that has no corresponding "done" directive spans the rest
// of the whole file.
#pragma CMOD eval ALL DIM
// So this environment variable can be used everywhere.
#pragma CMOD env MUMP DIM
// But also this that has a default value
#pragma DIM let DIMENSION = (${DIM}0 -ter ${DIM} -nar 4)

// A first example is how to compute the bounds for the fixed-width
// integer types. This is expanded twice

// vvvvv 2nd expansion EVAL (per default)
#pragma ALL eval
// vvvvv 1st expansion ALL
#pragma ALL do N = 8 128 *2
#pragma EVAL let M = ((1 -ls (${N} - 1))-1)
#define  INT${N}_MAX ${M}
#define  INT${N}_MIN (-${M} - 1)
#define  UINT${N}_MAX (2*${M}U + 1U)
#pragma EVAL done
#pragma ALL done
// ^^^^^ 1st expansion
#pragma ALL done
// ^^^^^ 2nd expansion

// Initialize a 1D array

extern double A[];

// Initialize a 2D array
extern double B[][${DIMENSION}];

// Initialize a 2D array with dependent 2nd loop bound,
// two expansions
extern double C[][${DIMENSION}];

// Initialize a 2D array with dependent 2nd loop bound,
// three expansions
extern double D[][${DIMENSION}];

#endif
