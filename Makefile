.NOTPARALLEL :

LIBRARIES = libC.a libPOSIX.a

ifeq ($(shell uname),Linux)
LIBRARIES += libLinux.a
endif

TARGET = ${LIBRARIES}

CMOD=./bin/Cmod
TOOLSC=./tools-c
DUMP8=${TOOLSC}/tools-dump8

BACKWARD = tools

CTOOLS = ${DUMP8} ${TOOLSC}/makeheaders ${TOOLSC}/tools-hide ${TOOLSC}/tools-show ${TOOLSC}/tools-undump8

TOOLS =  toolsc ${CMOD}

CMODEP = ${CMOD} -M

COPTS ?= -O3 -march=native

CFLAGS	?= -std=c11 -Wall -fno-common -fdata-sections -ffunction-sections ${COPTS}
LDFLAGS ?= -Wl,--gc-sections

LIBTOOLC = ${TOOLSC}/libtoolc.a
LIBC = C/libC.a
LIBPOSIX = POSIX/libPOSIX.a
LIBLINUX = Linux/libLinux.a
LIBOMP = omp/libomp.a

ifeq (${CMOD_STATIC},)
TARGET   += ${LIBRARIES:.a=.so}
LIBTOOLC += ${TOOLSC}/libtoolc.so
LIBC     += C/libC.so
LIBPOSIX += POSIX/libPOSIX.so
LIBLINUX += Linux/libLinux.so
LIBOMP   += omp/libomp.so
else
LDFLAGS += -static
endif

ifneq (${STDATOMIC},)
	CFLAGS := ${CFLAGS} -I- -I ${STDATOMIC}
	LDFLAGS := ${LDFLAGS} -L ${STDATOMIC} -l atomic
endif

target : ${TOOLSC} libparser ${BACKWARD}

${CTOOLS} : toolsc

toolsc :
	${MAKE} -C ${TOOLSC}
	mkdir lib 2>/dev/null >/dev/null || true
	cp -p ${LIBTOOLC} lib/

target : libcmod ${TARGET}

ifeq (${NOEILCK},)
eilck : ${LIBRARIES}
	${MAKE} -C eilck

target : eilck
endif

target : libomp
eilck : libomp

libC-nocontext : ${TOOLS}
	NOCONTEXT=1 ${MAKE} -C C depend
	NOCONTEXT=1 ${MAKE} -C C
	mkdir lib 2>/dev/null >/dev/null || true
	cp -p ${LIBC} lib/

libC.a libC.so : libC

libC : ${TOOLS} libparser
	${MAKE} -C C depend
	${MAKE} -C C
	mkdir lib 2>/dev/null >/dev/null || true
	cp -p ${LIBC} lib/

libcmod : libC
	${MAKE} -C example depend
	${MAKE} -C example

libparser : libC-nocontext
	${MAKE} -C parser depend
	${MAKE} -C parser

libPOSIX.a libPOSIX.so : libPOSIX

libPOSIX : libC
	${MAKE} -C POSIX depend
	${MAKE} -C POSIX
	cp -p ${LIBPOSIX} lib/

libLinux.a libLinux.so : libLinux

libLinux : libC.a libPOSIX.a
	${MAKE} -C Linux depend
	${MAKE} -C Linux
	cp -p ${LIBLINUX} lib/

libomp : libC.a
	${MAKE} -C omp depend
	${MAKE} -C omp
	cp -p ${LIBOMP} lib/

tools : libLinux
	${MAKE} -C tools depend
	${MAKE} -C tools

doxygen :
	${MAKE} -C C cexp
	${MAKE} -C POSIX cexp
	${MAKE} -C Linux cexp
	${MAKE} -C tools cexp
	${MAKE} -C eilck cexp
	./bin/Cmod_header > ./c-expand/Cmod_header.c
	./bin/Cmod --doxy > ./c-expand/Cmod.c
	(cd c-expand ; doxygen)

%.a : %.X ${CMOD}
	CMOD_ARCHIVES=./libC.a ${CMOD} -c ${CFLAGS} ${LDFLAGS} $*.X

clean :
	make -C C clean
	make -C parser clean
	make -C POSIX clean
	make -C Linux clean
	make -C example clean
	make -C tools clean
	make -C omp clean
	make -C eilck clean
	make -C ${TOOLSC} clean
	rm -f  ${LIBRARIES} ${LIBRARIES:.a=.so} ${OBJECTS} ${TARGET}

tidy :
	${MAKE} -C C tidy
	${MAKE} -C POSIX tidy
	${MAKE} -C Linux tidy
	${MAKE} -C tools-c tidy
	${MAKE} -C eilck tidy
