/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module impl  =
#pragma CMOD separator  ∷
#pragma CMOD composer   —
#pragma CMOD import obj = ..∷obj

// protect some function parameters
#pragma CMOD intern h
#pragma CMOD intern h0
#pragma CMOD intern h1
#pragma CMOD intern cntx
#pragma CMOD intern nsize
#pragma CMOD intern excl
#pragma CMOD intern ob0
#pragma CMOD intern sooner
#pragma CMOD intern later
#pragma CMOD intern front


#pragma CMOD declaration

struct impl {
  void* front;  // this is just a place holder, don't use it
  obj*   ob;
  C∷size pos;
};

#define INITIALIZER { .front = 0, .ob = 0, .pos = 0, }

C∷attr∷pure
inline
C∷size length(impl* h) {
  return h ? obj∷length(h→ob) : 0;
}

C∷attr∷pure
inline
C∷size length—2(impl*restrict h0, impl*restrict h1) {
  bool phase = ¬h0→ob;
  if (phase) return length(h1);
  else return length(h0);
}

C∷attr∷pure
inline
int length2(impl (*h)[2]) {
  return length—2(&(*h)[0], &(*h)[1]);
}

inline
int acq(impl* h) {
  return obj∷acq(h→ob, h→pos);
}

inline
int acq—2(impl*restrict h0, impl*restrict h1) {
  bool phase = ¬h0→ob;
  if (phase) return acq(h1);
  else return acq(h0);
}

inline
int acq2(impl (*h)[2]) {
  return acq—2(&(*h)[0], &(*h)[1]);
}

inline
bool test(impl* h) {
  return (h ∧ h→ob)
         ? obj∷test(h→ob, h→pos)
         : 0;
}

inline
bool test—2(impl*restrict h0, impl*restrict h1) {
  bool phase = ¬h0→ob;
  if (phase) return test(h1);
  else return test(h0);
}

inline
bool test2(impl (*h)[2]) {
  if (¬h) return 0;
  return test—2(&(*h)[0], &(*h)[1]);
}

inline
void rel(impl* h) {
  if (h ∧ h→ob) {
    obj∷rel(h→ob, h→pos);
    *h = (impl)INITIALIZER;
  }
}

inline
void* map(impl* h) {
  if (h→ob ∧ acq(h))
    return obj∷map(h→ob);
  return 0;
}

inline
void* map—2(impl*restrict h0, impl*restrict h1) {
  bool phase = ¬h0→ob;
  if (phase) return map(h1);
  else return map(h0);
}

inline
void* map2(impl (*h)[2]) {
  return map—2(&(*h)[0], &(*h)[1]);
}

inline
void drop(impl* h) {
  if (h ∧ h→ob) {
    obj∷acq(h→ob, h→pos);
    obj∷rel(h→ob, h→pos);
    *h = (impl)INITIALIZER;
  }
}

inline
void drop—2(impl*restrict h0, impl*restrict h1) {
  drop(h0);
  drop(h1);
}

inline
void drop2(impl (*h)[2]) {
  if (¬h) return;
  return drop—2(&(*h)[0], &(*h)[1]);
}

inline
int req(impl* h, obj* ob0, bool excl) {
  h→pos = excl ? obj∷ereq(ob0) : obj∷ireq(ob0);
  if (h→pos) {
    h→ob = ob0;
  } else {
    *h = (impl)INITIALIZER;
  }
  return h→pos;
}

//++++++++++++++++++++++++++++++++++++++++

inline
impl const* chain(impl const* sooner, impl* later, bool excl) {
  req(later, sooner→ob, excl);
  return later;
}

inline
void rel—2(impl*restrict h0, impl*restrict h1, bool excl) {
  bool phase = ¬h0→ob;
  if (phase) {
    req(h0, h1→ob, excl);
    rel(h1);
  } else {
    req(h1, h0→ob, excl);
    rel(h0);
  }
}

inline
void rel2(impl (*h)[2], bool excl) {
  if (¬h) return;
  rel—2(&(*h)[0], &(*h)[1], excl);
}


//#define scale—3(H, N, C, ...) scale(H, N, C)
//#define scale(...) scale—3(__VA_ARGS__, 0, 0, 0, )

inline
void* scale(impl* h, C∷size nsize, void* cntx) {
  if (¬test(h)) return 0;
  return obj∷scale(h→ob, nsize, cntx);
}

//#define scale2—3(H, N, C, ...) scale2(H, N, C)
//#define scale2(...) scale2—3(__VA_ARGS__, 0, 0, 0, )

inline
void* scale—2(impl*restrict h0, impl*restrict h1, C∷size nsize, void* cntx) {
  bool phase = ¬h0→ob;
  if (phase) return scale(h1, nsize, cntx);
  else return scale(h0, nsize, cntx);
}

inline
void* scale2(impl (*h)[2], C∷size nsize, void* cntx) {
  if (¬h) return 0;
  return scale—2(&(*h)[0], &(*h)[1], nsize, cntx);
}
