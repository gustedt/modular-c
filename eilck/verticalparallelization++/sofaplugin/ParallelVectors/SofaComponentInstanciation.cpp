/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_INSTANCIATION_CPP
#define SOFA_COMPONENT_INSTANCIATION_CPP

#include "ParallelTypes.h"
#include "SofaComponentInstanciation.inl"


namespace sofa
{

namespace component
{

namespace container
{

using namespace core::behavior;
using namespace defaulttype;



// template specialization must be in the same namespace as original namespace for GCC 4.1
// g++ 4.1 requires template instantiations to be declared on a parent namespace from the template class.
//#ifdef SOFA_FLOAT
template class MechanicalObject<ParallelVec3fTypes>;
template class MechanicalObject<ParallelVec2fTypes>;
template class MechanicalObject<ParallelVec1fTypes>;
template class MechanicalObject<ParallelVec6fTypes>;
//#endif // SOFA_FLOAT
//#ifdef SOFA_DOUBLE
template class MechanicalObject<ParallelVec3dTypes>;
template class MechanicalObject<ParallelVec2dTypes>;
template class MechanicalObject<ParallelVec1dTypes>;
template class MechanicalObject<ParallelVec6dTypes>;
//#endif // SOFA_DOUBLE



SOFA_DECL_CLASS(ParallelMechanicalObject)

int ParallelMechanicalObjectClass = core::RegisterObject("parallel mechanical state vectors")

#ifndef SOFA_DOUBLE
        .add< MechanicalObject<ParallelVec3fTypes> >()
        .add< MechanicalObject<ParallelVec2fTypes> >()
        .add< MechanicalObject<ParallelVec1fTypes> >()
        .add< MechanicalObject<ParallelVec6fTypes> >()
//        .add< MechanicalObject<ParallelRigid3fTypes> >()
//        .add< MechanicalObject<ParallelRigid2fTypes> >()
#endif
#ifndef SOFA_FLOAT
        .add< MechanicalObject<ParallelVec3dTypes> >()
        .add< MechanicalObject<ParallelVec2dTypes> >()
        .add< MechanicalObject<ParallelVec1dTypes> >()
        .add< MechanicalObject<ParallelVec6dTypes> >()
//        .add< MechanicalObject<ParallelRigid3dTypes> >()
//        .add< MechanicalObject<ParallelRigid2dTypes> >()
#endif
        ;

} // namespace container

} // namespace component

} // namespace sofa







namespace sofa
{

namespace component
{

namespace mapping
{

////////  BARYCENTRICMAPPING /////////

using namespace sofa::defaulttype;

// Register in the Factory
int ParallelBarycentricMappingClass = core::RegisterObject("Mapping using barycentric coordinates of the child with respect to cells of its parent")
#ifndef SOFA_FLOAT
        .add< BarycentricMapping< ParallelVec3dTypes, ParallelVec3dTypes > >()
//        .add< BarycentricMapping< ParallelVec3dTypes, ParallelExtVec3fTypes > >()
#endif
#ifndef SOFA_DOUBLE
        .add< BarycentricMapping< ParallelVec3fTypes, ParallelVec3fTypes > >()
//        .add< BarycentricMapping< ParallelVec3fTypes, ParallelExtVec3fTypes > >()
#endif
#ifndef SOFA_FLOAT
#ifndef SOFA_DOUBLE
        .add< BarycentricMapping< ParallelVec3fTypes, ParallelVec3dTypes > >()
        .add< BarycentricMapping< ParallelVec3dTypes, ParallelVec3fTypes > >()
#endif
#endif
        ;



#ifndef SOFA_FLOAT
template class SOFA_BASE_MECHANICS_API BarycentricMapping< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapping< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapper< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapper< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API TopologyBarycentricMapper< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API TopologyBarycentricMapper< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperRegularGridTopology< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperRegularGridTopology< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperSparseGridTopology< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperSparseGridTopology< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperMeshTopology< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperMeshTopology< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperEdgeSetTopology< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperEdgeSetTopology< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperTriangleSetTopology< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperTriangleSetTopology< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperQuadSetTopology< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperQuadSetTopology< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperTetrahedronSetTopology< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperTetrahedronSetTopology< ParallelVec3dTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperHexahedronSetTopology< ParallelVec3dTypes, ParallelVec3dTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperHexahedronSetTopology< ParallelVec3dTypes, ParallelExtVec3fTypes >;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_BASE_MECHANICS_API BarycentricMapping< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapping< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapper< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapper< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API TopologyBarycentricMapper< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API TopologyBarycentricMapper< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperRegularGridTopology< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperRegularGridTopology< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperSparseGridTopology< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperSparseGridTopology< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperMeshTopology< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperMeshTopology< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperEdgeSetTopology< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperEdgeSetTopology< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperTriangleSetTopology< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperTriangleSetTopology< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperQuadSetTopology< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperQuadSetTopology< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperTetrahedronSetTopology< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperTetrahedronSetTopology< ParallelVec3fTypes, ParallelExtVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperHexahedronSetTopology< ParallelVec3fTypes, ParallelVec3fTypes >;
//template class SOFA_BASE_MECHANICS_API BarycentricMapperHexahedronSetTopology< ParallelVec3fTypes, ParallelExtVec3fTypes >;
#endif
#ifndef SOFA_FLOAT
#ifndef SOFA_DOUBLE
template class SOFA_BASE_MECHANICS_API BarycentricMapping< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapping< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapper< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapper< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API TopologyBarycentricMapper< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API TopologyBarycentricMapper< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperRegularGridTopology< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperRegularGridTopology< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperSparseGridTopology< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperSparseGridTopology< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperMeshTopology< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperMeshTopology< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperEdgeSetTopology< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperEdgeSetTopology< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperTriangleSetTopology< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperTriangleSetTopology< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperQuadSetTopology< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperQuadSetTopology< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperTetrahedronSetTopology< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperTetrahedronSetTopology< ParallelVec3fTypes, ParallelVec3dTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperHexahedronSetTopology< ParallelVec3dTypes, ParallelVec3fTypes >;
template class SOFA_BASE_MECHANICS_API BarycentricMapperHexahedronSetTopology< ParallelVec3fTypes, ParallelVec3dTypes >;
#endif
#endif

} // namespace mapping

} // namespace component

} // namespace sofa









namespace sofa
{

namespace component
{

namespace topology
{
using namespace sofa::defaulttype;

SOFA_DECL_CLASS(TriangleSetGeometryAlgorithms)
int ParallelTriangleSetGeometryAlgorithmsClass = core::RegisterObject("Triangle set geometry algorithms")
#ifdef SOFA_FLOAT
        .add< TriangleSetGeometryAlgorithms<ParallelVec3fTypes> >()
#else
        .add< TriangleSetGeometryAlgorithms<ParallelVec3dTypes> >()
#ifndef SOFA_DOUBLE
        .add< TriangleSetGeometryAlgorithms<ParallelVec3fTypes> >()
#endif
#endif
#ifndef SOFA_FLOAT
        .add< TriangleSetGeometryAlgorithms<ParallelVec2dTypes> >()
        .add< TriangleSetGeometryAlgorithms<ParallelVec1dTypes> >()
#endif
#ifndef SOFA_DOUBLE
        .add< TriangleSetGeometryAlgorithms<ParallelVec2fTypes> >()
        .add< TriangleSetGeometryAlgorithms<ParallelVec1fTypes> >()
#endif
        ;

#ifndef SOFA_FLOAT
template class SOFA_BASE_TOPOLOGY_API TriangleSetGeometryAlgorithms<ParallelVec3dTypes>;
template class SOFA_BASE_TOPOLOGY_API TriangleSetGeometryAlgorithms<ParallelVec2dTypes>;
template class SOFA_BASE_TOPOLOGY_API TriangleSetGeometryAlgorithms<ParallelVec1dTypes>;
#endif

#ifndef SOFA_DOUBLE
template class SOFA_BASE_TOPOLOGY_API TriangleSetGeometryAlgorithms<ParallelVec3fTypes>;
template class SOFA_BASE_TOPOLOGY_API TriangleSetGeometryAlgorithms<ParallelVec2fTypes>;
template class SOFA_BASE_TOPOLOGY_API TriangleSetGeometryAlgorithms<ParallelVec1fTypes>;
#endif

} // namespace topology

} // namespace component

} // namespace sofa





namespace sofa
{

namespace component
{

namespace topology
{
using namespace sofa::defaulttype;
SOFA_DECL_CLASS(TetrahedronSetGeometryAlgorithms)
int ParallelTetrahedronSetGeometryAlgorithmsClass = core::RegisterObject("Tetrahedron set geometry algorithms")
#ifdef SOFA_FLOAT
        .add< TetrahedronSetGeometryAlgorithms<ParallelVec3fTypes> >()
#else
        .add< TetrahedronSetGeometryAlgorithms<ParallelVec3dTypes> >()
#ifndef SOFA_DOUBLE
        .add< TetrahedronSetGeometryAlgorithms<ParallelVec3fTypes> >()
#endif
#endif
#ifndef SOFA_FLOAT
        .add< TetrahedronSetGeometryAlgorithms<ParallelVec2dTypes> >()
        .add< TetrahedronSetGeometryAlgorithms<ParallelVec1dTypes> >()
#endif
#ifndef SOFA_DOUBLE
        .add< TetrahedronSetGeometryAlgorithms<ParallelVec2fTypes> >()
        .add< TetrahedronSetGeometryAlgorithms<ParallelVec1fTypes> >()
#endif
        ;

#ifndef SOFA_FLOAT
template class SOFA_BASE_TOPOLOGY_API TetrahedronSetGeometryAlgorithms<ParallelVec3dTypes>;
template class SOFA_BASE_TOPOLOGY_API TetrahedronSetGeometryAlgorithms<ParallelVec2dTypes>;
template class SOFA_BASE_TOPOLOGY_API TetrahedronSetGeometryAlgorithms<ParallelVec1dTypes>;
#endif

#ifndef SOFA_DOUBLE
template class SOFA_BASE_TOPOLOGY_API TetrahedronSetGeometryAlgorithms<ParallelVec3fTypes>;
template class SOFA_BASE_TOPOLOGY_API TetrahedronSetGeometryAlgorithms<ParallelVec2fTypes>;
template class SOFA_BASE_TOPOLOGY_API TetrahedronSetGeometryAlgorithms<ParallelVec1fTypes>;
#endif




SOFA_DECL_CLASS(QuadSetGeometryAlgorithms)
int ParallelQuadSetGeometryAlgorithmsClass = core::RegisterObject("Quad set geometry algorithms")
#ifdef SOFA_FLOAT
        .add< QuadSetGeometryAlgorithms<sofa::defaulttype::ParallelVec3fTypes> >()
#else
        .add< QuadSetGeometryAlgorithms<sofa::defaulttype::ParallelVec3dTypes> >()
#ifndef SOFA_DOUBLE
        .add< QuadSetGeometryAlgorithms<sofa::defaulttype::ParallelVec3fTypes> >()
#endif
#endif
#ifndef SOFA_FLOAT
        .add< QuadSetGeometryAlgorithms<ParallelVec2dTypes> >()
        .add< QuadSetGeometryAlgorithms<ParallelVec1dTypes> >()
#endif
#ifndef SOFA_DOUBLE
        .add< QuadSetGeometryAlgorithms<ParallelVec2fTypes> >()
        .add< QuadSetGeometryAlgorithms<ParallelVec1fTypes> >()
#endif
        ;

#ifndef SOFA_FLOAT
template class SOFA_BASE_TOPOLOGY_API QuadSetGeometryAlgorithms<sofa::defaulttype::ParallelVec3dTypes>;
template class SOFA_BASE_TOPOLOGY_API QuadSetGeometryAlgorithms<ParallelVec2dTypes>;
template class SOFA_BASE_TOPOLOGY_API QuadSetGeometryAlgorithms<ParallelVec1dTypes>;
#endif

#ifndef SOFA_DOUBLE
template class SOFA_BASE_TOPOLOGY_API QuadSetGeometryAlgorithms<sofa::defaulttype::ParallelVec3fTypes>;
template class SOFA_BASE_TOPOLOGY_API QuadSetGeometryAlgorithms<ParallelVec2fTypes>;
template class SOFA_BASE_TOPOLOGY_API QuadSetGeometryAlgorithms<ParallelVec1fTypes>;
#endif




SOFA_DECL_CLASS(EdgeSetGeometryAlgorithms)
int ParallelEdgeSetGeometryAlgorithmsClass = core::RegisterObject("Edge set geometry algorithms")

#ifdef SOFA_FLOAT
        .add< EdgeSetGeometryAlgorithms<ParallelVec3fTypes> >()
#else
        .add< EdgeSetGeometryAlgorithms<ParallelVec3dTypes> >()
#ifndef SOFA_DOUBLE
        .add< EdgeSetGeometryAlgorithms<ParallelVec3fTypes> >() // default template
#endif
#endif
#ifndef SOFA_FLOAT
        .add< EdgeSetGeometryAlgorithms<ParallelVec2dTypes> >()
        .add< EdgeSetGeometryAlgorithms<ParallelVec1dTypes> >()
//        .add< EdgeSetGeometryAlgorithms<ParallelRigid3dTypes> >()
//        .add< EdgeSetGeometryAlgorithms<ParallelRigid2dTypes> >()
#endif
#ifndef SOFA_DOUBLE
        .add< EdgeSetGeometryAlgorithms<ParallelVec2fTypes> >()
        .add< EdgeSetGeometryAlgorithms<ParallelVec1fTypes> >()
//        .add< EdgeSetGeometryAlgorithms<ParallelRigid3fTypes> >()
//        .add< EdgeSetGeometryAlgorithms<ParallelRigid2fTypes> >()
#endif
        ;

#ifndef SOFA_FLOAT
template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelVec3dTypes>;
template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelVec2dTypes>;
template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelVec1dTypes>;
//template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelRigid3dTypes>;
//template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelRigid2dTypes>;
#endif

#ifndef SOFA_DOUBLE
template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelVec3fTypes>;
template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelVec2fTypes>;
template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelVec1fTypes>;
//template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelRigid3fTypes>;
//template class SOFA_BASE_TOPOLOGY_API EdgeSetGeometryAlgorithms<ParallelRigid2fTypes>;
#endif




using namespace sofa::defaulttype;
SOFA_DECL_CLASS(HexahedronSetGeometryAlgorithms)
int ParallelHexahedronSetGeometryAlgorithmsClass = core::RegisterObject("Hexahedron set geometry algorithms")
#ifdef SOFA_FLOAT
        .add< HexahedronSetGeometryAlgorithms<ParallelVec3fTypes> >()
#else
        .add< HexahedronSetGeometryAlgorithms<ParallelVec3dTypes> >()
#ifndef SOFA_DOUBLE
        .add< HexahedronSetGeometryAlgorithms<ParallelVec3fTypes> >()
#endif
#endif
#ifndef SOFA_FLOAT
        .add< HexahedronSetGeometryAlgorithms<ParallelVec2dTypes> >()
        .add< HexahedronSetGeometryAlgorithms<ParallelVec1dTypes> >()
#endif
#ifndef SOFA_DOUBLE
        .add< HexahedronSetGeometryAlgorithms<ParallelVec2fTypes> >()
        .add< HexahedronSetGeometryAlgorithms<ParallelVec1fTypes> >()
#endif
        ;

#ifndef SOFA_FLOAT
template class SOFA_BASE_TOPOLOGY_API HexahedronSetGeometryAlgorithms<ParallelVec3dTypes>;
template class SOFA_BASE_TOPOLOGY_API HexahedronSetGeometryAlgorithms<ParallelVec2dTypes>;
template class SOFA_BASE_TOPOLOGY_API HexahedronSetGeometryAlgorithms<ParallelVec1dTypes>;
#endif

#ifndef SOFA_DOUBLE
template class SOFA_BASE_TOPOLOGY_API HexahedronSetGeometryAlgorithms<ParallelVec3fTypes>;
template class SOFA_BASE_TOPOLOGY_API HexahedronSetGeometryAlgorithms<ParallelVec2fTypes>;
template class SOFA_BASE_TOPOLOGY_API HexahedronSetGeometryAlgorithms<ParallelVec1fTypes>;
#endif

} // namespace topology

} // namespace component

} // namespace sofa












namespace sofa
{

namespace component
{

namespace forcefield
{

using namespace sofa::defaulttype;


SOFA_DECL_CLASS(ConstantForceField)

int ParallelConstantForceFieldClass = core::RegisterObject("Constant forces applied to given degrees of freedom")
#ifndef SOFA_FLOAT
        .add< ConstantForceField<ParallelVec3dTypes> >()
        .add< ConstantForceField<ParallelVec2dTypes> >()
        .add< ConstantForceField<ParallelVec1dTypes> >()
        .add< ConstantForceField<ParallelVec6dTypes> >()
//        .add< ConstantForceField<Rigid3dTypes> >()
//        .add< ConstantForceField<Rigid2dTypes> >()
#endif
#ifndef SOFA_DOUBLE
        .add< ConstantForceField<ParallelVec3fTypes> >()
        .add< ConstantForceField<ParallelVec2fTypes> >()
        .add< ConstantForceField<ParallelVec1fTypes> >()
        .add< ConstantForceField<ParallelVec6fTypes> >()
//        .add< ConstantForceField<Rigid3fTypes> >()
//        .add< ConstantForceField<Rigid2fTypes> >()
#endif
        ;
#ifndef SOFA_FLOAT
template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<ParallelVec3dTypes>;
template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<ParallelVec2dTypes>;
template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<ParallelVec1dTypes>;
template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<ParallelVec6dTypes>;
//template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<Rigid3dTypes>;
//template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<Rigid2dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<ParallelVec3fTypes>;
template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<ParallelVec2fTypes>;
template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<ParallelVec1fTypes>;
template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<ParallelVec6fTypes>;
//template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<Rigid3fTypes>;
//template class SOFA_BOUNDARY_CONDITION_API ConstantForceField<Rigid2fTypes>;
#endif





SOFA_DECL_CLASS(TetrahedronFEMForceField)

// Register in the Factory
int ParallelTetrahedronFEMForceFieldClass = core::RegisterObject("Tetrahedral finite elements")
#ifndef SOFA_FLOAT
        .add< TetrahedronFEMForceField<ParallelVec3dTypes> >()
#endif
#ifndef SOFA_DOUBLE
        .add< TetrahedronFEMForceField<ParallelVec3fTypes> >()
#endif
        ;

#ifndef SOFA_FLOAT
template class SOFA_SIMPLE_FEM_API TetrahedronFEMForceField<ParallelVec3dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_SIMPLE_FEM_API TetrahedronFEMForceField<ParallelVec3fTypes>;
#endif




SOFA_DECL_CLASS(TetrahedralCorotationalFEMForceField)

// Register in the Factory
int ParallelTetrahedralCorotationalFEMForceFieldClass = core::RegisterObject("Corotational FEM Tetrahedral finite elements")
#ifndef SOFA_FLOAT
        .add< TetrahedralCorotationalFEMForceField<ParallelVec3dTypes> >()
#endif
#ifndef SOFA_DOUBLE
        .add< TetrahedralCorotationalFEMForceField<ParallelVec3fTypes> >()
#endif
        ;

#ifndef SOFA_FLOAT
template class SOFA_GENERAL_SIMPLE_FEM_API TetrahedralCorotationalFEMForceField<ParallelVec3dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_GENERAL_SIMPLE_FEM_API TetrahedralCorotationalFEMForceField<ParallelVec3fTypes>;
#endif

} // namespace forcefield

} // namespace component

} // namespace sofa





namespace sofa
{

namespace component
{

namespace projectiveconstraintset
{

using namespace sofa::defaulttype;
using namespace sofa::helper;


SOFA_DECL_CLASS(FixedConstraint)

int ParallelFixedConstraintClass = core::RegisterObject("Attach given particles to their initial positions")
#ifndef SOFA_FLOAT
        .add< FixedConstraint<ParallelVec3dTypes> >()
        .add< FixedConstraint<ParallelVec2dTypes> >()
        .add< FixedConstraint<ParallelVec1dTypes> >()
        .add< FixedConstraint<ParallelVec6dTypes> >()
//        .add< FixedConstraint<Rigid3dTypes> >()
//        .add< FixedConstraint<Rigid2dTypes> >()
#endif
#ifndef SOFA_DOUBLE
        .add< FixedConstraint<ParallelVec3fTypes> >()
        .add< FixedConstraint<ParallelVec2fTypes> >()
        .add< FixedConstraint<ParallelVec1fTypes> >()
        .add< FixedConstraint<ParallelVec6fTypes> >()
//        .add< FixedConstraint<Rigid3fTypes> >()
//        .add< FixedConstraint<Rigid2fTypes> >()
#endif
        ;

#ifndef SOFA_FLOAT
template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<ParallelVec3dTypes>;
template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<ParallelVec2dTypes>;
template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<ParallelVec1dTypes>;
template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<ParallelVec6dTypes>;
//template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<Rigid3dTypes>;
//template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<Rigid2dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<ParallelVec3fTypes>;
template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<ParallelVec2fTypes>;
template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<ParallelVec1fTypes>;
template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<ParallelVec6fTypes>;
//template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<Rigid3fTypes>;
//template class SOFA_BOUNDARY_CONDITION_API FixedConstraint<Rigid2fTypes>;
#endif

} // namespace projectiveconstraintset

} // namespace component

} // namespace sofa







namespace sofa
{

namespace component
{

namespace mass
{

SOFA_DECL_CLASS(DiagonalMass)

// Register in the Factory
int ParallelDiagonalMassClass = core::RegisterObject("Define a specific mass for each particle")
#ifdef SOFA_WITH_DOUBLE
        .add< DiagonalMass<defaulttype::ParallelVec3dTypes,double> >()
        .add< DiagonalMass<defaulttype::ParallelVec2dTypes,double> >()
        .add< DiagonalMass<defaulttype::ParallelVec1dTypes,double> >()
//        .add< DiagonalMass<Rigid3dTypes,Rigid3dMass> >()
//        .add< DiagonalMass<Rigid2dTypes,Rigid2dMass> >()
#endif
#ifdef SOFA_WITH_FLOAT
        .add< DiagonalMass<defaulttype::ParallelVec3fTypes,float> >()
        .add< DiagonalMass<defaulttype::ParallelVec2fTypes,float> >()
        .add< DiagonalMass<defaulttype::ParallelVec1fTypes,float> >()
//        .add< DiagonalMass<ParallelRigid3fTypes,Rigid3fMass> >()
//        .add< DiagonalMass<ParallelRigid2fTypes,Rigid2fMass> >()
#endif
        ;

#ifdef SOFA_WITH_DOUBLE
template class SOFA_BASE_MECHANICS_API DiagonalMass<defaulttype::Vec3dTypes,double>;
template class SOFA_BASE_MECHANICS_API DiagonalMass<defaulttype::ParallelVec2dTypes,double>;
template class SOFA_BASE_MECHANICS_API DiagonalMass<defaulttype::ParallelVec1dTypes,double>;
//template class SOFA_BASE_MECHANICS_API DiagonalMass<Rigid3dTypes,Rigid3dMass>;
//template class SOFA_BASE_MECHANICS_API DiagonalMass<Rigid2dTypes,Rigid2dMass>;
#endif
#ifdef SOFA_WITH_FLOAT
template class SOFA_BASE_MECHANICS_API DiagonalMass<defaulttype::ParallelVec3fTypes,float>;
template class SOFA_BASE_MECHANICS_API DiagonalMass<defaulttype::ParallelVec2fTypes,float>;
template class SOFA_BASE_MECHANICS_API DiagonalMass<defaulttype::ParallelVec1fTypes,float>;
//template class SOFA_BASE_MECHANICS_API DiagonalMass<Rigid3fTypes,Rigid3fMass>;
//template class SOFA_BASE_MECHANICS_API DiagonalMass<Rigid2fTypes,Rigid2fMass>;
#endif



SOFA_DECL_CLASS(MeshMatrixMass)

// Register in the Factory
int ParallelMeshMatrixMassClass = core::RegisterObject("Define a specific mass for each particle")
#ifndef SOFA_FLOAT
        .add< MeshMatrixMass<defaulttype::ParallelVec3dTypes,double> >()
        .add< MeshMatrixMass<defaulttype::ParallelVec2dTypes,double> >()
        .add< MeshMatrixMass<defaulttype::ParallelVec1dTypes,double> >()
#endif
#ifndef SOFA_DOUBLE
        .add< MeshMatrixMass<defaulttype::ParallelVec3fTypes,float> >()
        .add< MeshMatrixMass<defaulttype::ParallelVec2fTypes,float> >()
        .add< MeshMatrixMass<defaulttype::ParallelVec1fTypes,float> >()
#endif
        ;

#ifndef SOFA_FLOAT
template class SOFA_MISC_FORCEFIELD_API MeshMatrixMass<defaulttype::ParallelVec3dTypes,double>;
template class SOFA_MISC_FORCEFIELD_API MeshMatrixMass<defaulttype::ParallelVec2dTypes,double>;
template class SOFA_MISC_FORCEFIELD_API MeshMatrixMass<defaulttype::ParallelVec1dTypes,double>;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_MISC_FORCEFIELD_API MeshMatrixMass<defaulttype::ParallelVec3fTypes,float>;
template class SOFA_MISC_FORCEFIELD_API MeshMatrixMass<defaulttype::ParallelVec2fTypes,float>;
template class SOFA_MISC_FORCEFIELD_API MeshMatrixMass<defaulttype::ParallelVec1fTypes,float>;
#endif


} // namespace mass

} // namespace component

} // namespace sofa






#endif // SOFA_COMPONENT_INSTANCIATION_CPP
