/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#include "ParallelTypes.h"

#include <sofa/core/ObjectFactory.h>

#include <sofa/core/MultiMapping.inl>

namespace sofa
{
//
//template class  defaulttype::ParallelVector<defaulttype::Vec<3,double>>;
//template class  defaulttype::ParallelVector<defaulttype::Vec<2,double>>;
//template class  defaulttype::ParallelVector<defaulttype::Vec<1,double>>;
//template class  defaulttype::ParallelVector<defaulttype::Vec<6,double>>;
//template class  defaulttype::ParallelVector<defaulttype::Vec<3,float>>;
//template class  defaulttype::ParallelVector<defaulttype::Vec<2,float>>;
//template class  defaulttype::ParallelVector<defaulttype::Vec<1,float>>;
//
//template class  defaulttype::ParallelVector<defaulttype::Vec<6,float>>;
}
namespace sofa
{

namespace core
{

using namespace sofa::defaulttype;



//#ifndef SOFA_FLOAT
//template class SOFA_PARALLELVECTORS_API MultiMapping<ParallelVec3fTypes, Vec1dTypes>;
//template class SOFA_PARALLELVECTORS_API MultiMapping<ParallelVec3fTypes, Vec2dTypes>;
//template class SOFA_PARALLELVECTORS_API MultiMapping<ParallelVec3fTypes, Vec3dTypes>;
//
//template class SOFA_PARALLELVECTORS_API MultiMapping<ParallelVec3dTypes, Vec1dTypes>;
//template class SOFA_PARALLELVECTORS_API MultiMapping<ParallelVec3dTypes, Vec2dTypes>;
//template class SOFA_PARALLELVECTORS_API MultiMapping<ParallelVec3dTypes, Vec3dTypes>;
//#endif
//#ifndef SOFA_DOUBLE
//template class SOFA_PARALLELVECTORS_API MultiMapping<ParallelVec3fTypes, Vec1fTypes>;
//template class SOFA_PARALLELVECTORS_API MultiMapping<ParallelVec3fTypes, Vec2fTypes>;
//template class SOFA_PARALLELVECTORS_API MultiMapping<ParallelVec3fTypes, Vec3fTypes>;
//#endif

} // namespace core

} // namespace sofa
