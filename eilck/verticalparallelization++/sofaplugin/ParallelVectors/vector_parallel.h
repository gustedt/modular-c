/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_HELPER_VECTOR_PARALLEL_H
#define SOFA_HELPER_VECTOR_PARALLEL_H

#include <sofa/helper/helper.h>

#include <sofa/helper/vector.h>
#include "ParallelMemoryManager.h"

#include <sofa/verticalparallelization/DataWrapper.h>

namespace sofa
{

namespace helper
{


/// Parallel vector
/// Using ParallelMemoryManager
template <class T>
class vector<T, ParallelMemoryManager<T> > : public sofa::verticalparallelization::DataVectorWrapper<std::vector<T, std::allocator<T> >>
{

public:
    typedef T value_type;
    typedef std::allocator<T> Alloc;
    /// size_type
    typedef typename std::vector<T,Alloc>::size_type size_type;


    std::ostream& write(std::ostream& os) const
    {
        if( this->size()>0 )
        {
            for( unsigned int i=0; i<this->size()-1; ++i )
                os<<(*this)[i]<<" ";
            os<<(*this)[this->size()-1];
        }
        return os;
    }

    std::istream& read(std::istream& in)
    {
        T t=T();
        this->clear();
        while(in>>t)
        {
            this->push_back(t);
        }
        if( in.rdstate() & std::ios_base::eofbit ) { in.clear(); }
        return in;
    }

/// Output stream
    inline friend std::ostream& operator<< ( std::ostream& os, const vector<T, Alloc>& vec )
    {
        return vec.write(os);
    }

/// Input stream
    inline friend std::istream& operator>> ( std::istream& in, vector<T, Alloc>& vec )
    {
        return vec.read(in);
    }

    /// Sets every element to 'value'
    void fill( const T& value )
    {
        std::fill( this->begin(), this->end(), value );
    }

    /// this function is useful for vector_device because it resize the vector without device operation (if device is not valid).
    /// Therefore the function is used in asynchronous code to safly resize a vector which is either cuda of helper::vector
    void fastResize(size_type n) {
        this->resize(n);
    }



public:

    template<class T1, class T2>
    void remove( T1& v, const T2& elem )
    {
        std::cout << "vector " << this << "  :  calling remove......" << std::endl;
        typename T1::iterator e = std::find( v.begin(), v.end(), elem );
        if( e != v.end() )
        {
            typename T1::iterator next = e;
            next++;
            for( ; next != v.end(); ++e, ++next )
                *e = *next;
        }
        v.pop_back();
    }

    /** Remove the first occurence of a given value.

    The last value is moved to where the value was found, and the other values are not shifted.
    */
    template<class T1, class T2>
    void removeValue( T1& v, const T2& elem )
    {
        std::cout << "vector " << this << "  :  calling removeValue......" << std::endl;
        typename T1::iterator e = std::find( v.begin(), v.end(), elem );
        if( e != v.end() )
        {
            if (e != v.end()-1)
                *e = v.back();
            v.pop_back();
        }
    }

    /// Remove value at given index, replace it by the value at the last index, other values are not changed
    template<class T1, class T2>
    void removeIndex( std::vector<T1,T2>& v, size_t index )
    {
        std::cout << "vector " << this << "  :  calling removeIndex......" << std::endl;
    #if defined(SOFA_VECTOR_ACCESS_FAILURE)
        //assert( 0<= static_cast<int>(index) && index <v.size() );
        if (index>=v.size())
            vector_access_failure(&v, v.size(), index, typeid(T));
    #endif
        if (index != v.size()-1)
            v[index] = v.back();
        v.pop_back();
    }

    //@}
    };

} // namespace helper

} // namespace sofa
#endif // SOFA_HELPER_VECTOR_PARALLEL_H

