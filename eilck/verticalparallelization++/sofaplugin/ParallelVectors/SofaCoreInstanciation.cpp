/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_CORE_INSTANCIATION_CPP
#define SOFA_CORE_INSTANCIATION_CPP

#include "ParallelTypes.h"
#include "SofaCoreInstanciation.inl"


namespace sofa
{

namespace core
{

////////  MAPPING /////////

#ifndef SOFA_FLOAT
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1dTypes, sofa::defaulttype::ParallelVec1dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1dTypes, sofa::defaulttype::ParallelVec2dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1dTypes, sofa::defaulttype::ParallelVec3dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec2dTypes, sofa::defaulttype::ParallelVec2dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec2dTypes, sofa::defaulttype::ParallelVec1dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3dTypes, sofa::defaulttype::ParallelVec3dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3dTypes, sofa::defaulttype::ParallelVec2dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3dTypes, sofa::defaulttype::ParallelVec1dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6dTypes, sofa::defaulttype::ParallelVec6dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6dTypes, sofa::defaulttype::ParallelVec3dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6dTypes, sofa::defaulttype::ParallelVec1dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3dTypes, sofa::defaulttype::ParallelVec3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3dTypes, sofa::defaulttype::ParallelVec1dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3dTypes, sofa::defaulttype::ParallelRigid3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid2dTypes, sofa::defaulttype::ParallelVec2dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid2dTypes, sofa::defaulttype::ParallelRigid2dTypes >;
// This one is special: ExtVec3fTypes are used for output outside of Sofa.
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3dTypes, sofa::defaulttype::ParallelExtVec3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3dTypes, sofa::defaulttype::ParallelExtVec3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6dTypes, sofa::defaulttype::ParallelExtVec3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec2dTypes, sofa::defaulttype::ParallelExtVec2fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3dTypes, sofa::defaulttype::ParallelExtVec3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3dTypes, sofa::defaulttype::ParallelExtVec3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3dTypes, sofa::defaulttype::ParallelRigid3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3dTypes, sofa::defaulttype::ParallelVec6dTypes >;
#endif

#ifndef SOFA_DOUBLE
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1fTypes, sofa::defaulttype::ParallelVec1fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1fTypes, sofa::defaulttype::ParallelVec2fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1fTypes, sofa::defaulttype::ParallelVec3fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec2fTypes, sofa::defaulttype::ParallelVec2fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec2fTypes, sofa::defaulttype::ParallelVec1fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3fTypes, sofa::defaulttype::ParallelVec3fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3fTypes, sofa::defaulttype::ParallelVec2fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3fTypes, sofa::defaulttype::ParallelVec1fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6fTypes, sofa::defaulttype::ParallelVec6fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6fTypes, sofa::defaulttype::ParallelVec3fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6fTypes, sofa::defaulttype::ParallelVec1fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3fTypes, sofa::defaulttype::ParallelExtVec3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6fTypes, sofa::defaulttype::ParallelExtVec3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec2fTypes, sofa::defaulttype::ParallelExtVec2fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3fTypes, sofa::defaulttype::ParallelVec3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3fTypes, sofa::defaulttype::ParallelVec1fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3fTypes, sofa::defaulttype::ParallelRigid3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid2fTypes, sofa::defaulttype::ParallelVec2fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid2fTypes, sofa::defaulttype::ParallelRigid2fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3fTypes, sofa::defaulttype::ParallelExtVec3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3fTypes, sofa::defaulttype::ParallelRigid3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3fTypes, sofa::defaulttype::ParallelVec6fTypes >;
#endif

#ifndef SOFA_FLOAT
#ifndef SOFA_DOUBLE
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1dTypes, sofa::defaulttype::ParallelVec1fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1dTypes, sofa::defaulttype::ParallelVec2fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1dTypes, sofa::defaulttype::ParallelVec3fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1fTypes, sofa::defaulttype::ParallelVec2dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1fTypes, sofa::defaulttype::ParallelVec3dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec1fTypes, sofa::defaulttype::ParallelVec1dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec2dTypes, sofa::defaulttype::ParallelVec2fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec2fTypes, sofa::defaulttype::ParallelVec2dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3dTypes, sofa::defaulttype::ParallelVec3fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3fTypes, sofa::defaulttype::ParallelVec3dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3dTypes, sofa::defaulttype::ParallelVec2fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3fTypes, sofa::defaulttype::ParallelVec2dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6dTypes, sofa::defaulttype::ParallelVec6fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6fTypes, sofa::defaulttype::ParallelVec6dTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6dTypes, sofa::defaulttype::ParallelVec3fTypes >;
template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec6fTypes, sofa::defaulttype::ParallelVec3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3dTypes, sofa::defaulttype::ParallelVec3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3fTypes, sofa::defaulttype::ParallelVec3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3dTypes, sofa::defaulttype::ParallelVec1fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3fTypes, sofa::defaulttype::ParallelVec1dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3dTypes, sofa::defaulttype::ParallelRigid3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3fTypes, sofa::defaulttype::ParallelRigid3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid2dTypes, sofa::defaulttype::ParallelVec2fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid2fTypes, sofa::defaulttype::ParallelVec2dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid2dTypes, sofa::defaulttype::ParallelRigid2fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid2fTypes, sofa::defaulttype::ParallelRigid2dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3dTypes, sofa::defaulttype::ParallelRigid3fTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3fTypes, sofa::defaulttype::ParallelRigid3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelVec3fTypes, sofa::defaulttype::ParallelExtVec3dTypes >;
//template class SOFA_CORE_API Mapping< sofa::defaulttype::ParallelRigid3fTypes, sofa::defaulttype::ParallelExtVec3dTypes >;
#endif
#endif


}   //namespace core

}   //namespace sofa





namespace sofa
{

namespace core
{

namespace behavior
{

using namespace sofa::defaulttype;
#ifndef SOFA_FLOAT
template class SOFA_CORE_API ProjectiveConstraintSet<ParallelVec6dTypes>;
template class SOFA_CORE_API ProjectiveConstraintSet<ParallelVec3dTypes>;
template class SOFA_CORE_API ProjectiveConstraintSet<ParallelVec2dTypes>;
template class SOFA_CORE_API ProjectiveConstraintSet<ParallelVec1dTypes>;
//template class SOFA_CORE_API ProjectiveConstraintSet<Rigid3dTypes>;
//template class SOFA_CORE_API ProjectiveConstraintSet<Rigid2dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_CORE_API ProjectiveConstraintSet<ParallelVec6fTypes>;
template class SOFA_CORE_API ProjectiveConstraintSet<ParallelVec3fTypes>;
template class SOFA_CORE_API ProjectiveConstraintSet<ParallelVec2fTypes>;
template class SOFA_CORE_API ProjectiveConstraintSet<ParallelVec1fTypes>;
//template class SOFA_CORE_API ProjectiveConstraintSet<Rigid3fTypes>;
//template class SOFA_CORE_API ProjectiveConstraintSet<Rigid2fTypes>;
#endif




#ifndef SOFA_FLOAT
template class SOFA_CORE_API Mass<ParallelVec3dTypes>;
template class SOFA_CORE_API Mass<ParallelVec2dTypes>;
template class SOFA_CORE_API Mass<ParallelVec1dTypes>;
template class SOFA_CORE_API Mass<ParallelVec6dTypes>;
//template class SOFA_CORE_API Mass<Rigid3dTypes>;
//template class SOFA_CORE_API Mass<Rigid2dTypes>;
#endif

#ifndef SOFA_DOUBLE
template class SOFA_CORE_API Mass<ParallelVec3fTypes>;
template class SOFA_CORE_API Mass<ParallelVec2fTypes>;
template class SOFA_CORE_API Mass<ParallelVec1fTypes>;
template class SOFA_CORE_API Mass<ParallelVec6fTypes>;
//template class SOFA_CORE_API Mass<Rigid3fTypes>;
//template class SOFA_CORE_API Mass<Rigid2fTypes>;
#endif



#ifndef SOFA_FLOAT
template class SOFA_CORE_API ForceField<ParallelVec3dTypes>;
template class SOFA_CORE_API ForceField<ParallelVec2dTypes>;
template class SOFA_CORE_API ForceField<ParallelVec1dTypes>;
template class SOFA_CORE_API ForceField<ParallelVec6dTypes>;
//template class SOFA_CORE_API ForceField<ParallelRigid3dTypes>;
//template class SOFA_CORE_API ForceField<ParallelRigid2dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_CORE_API ForceField<ParallelVec3fTypes>;
template class SOFA_CORE_API ForceField<ParallelVec2fTypes>;
template class SOFA_CORE_API ForceField<ParallelVec1fTypes>;
template class SOFA_CORE_API ForceField<ParallelVec6fTypes>;
//template class SOFA_CORE_API ForceField<ParallelRigid3fTypes>;
//template class SOFA_CORE_API ForceField<ParallelRigid2fTypes>;
#endif

} // namespace behavior

} // namespace core

} // namespace sofa












#endif // SOFA_CORE_INSTANCIATION_CPP
