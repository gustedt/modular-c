///
/// \file   DataWrapper.h
/// \brief  wrapper classes for C style array and std::vector-like data with operator[] overloading
/// \author Maxime Mogé
/// \date   2018
///
/// Wrapper class for C style array and std::vector-like data with operator[] overloading.
/// Class DataArrayWrapper wraps around C-style arrays.
/// Class DataVectorWrapper wraps around std::vector-like data.
/// operator[] is overloaded, and calls subscriptOperatorOverloadWrite or
/// subscriptOperatorOverloadRead before calling the original operator[].
/// Note that operator[] has  __attribute__((__always_inline__)) for performance reasons.
/// The rest of the classes is just the interface and does nothing more than the original
/// data structures member functions.
///
#ifndef HELPER_DATA_WRAPPER_H
#define HELPER_DATA_WRAPPER_H

#include <iostream>
#include <vector>
#include "memory_helper_functions.h"

namespace verticalparallelization
{

/// \class DataArrayWrapper
/// \brief Wrapper for C style array
/// Templated on the type of the data that is wrapped
template <class DataType>
class DataArrayWrapper
{

    DataType m_originalData;
    uintptr_t m_thisAddr;
public :
    typedef typename DataType::size_type size_type;
    typedef typename DataType::reference reference;
    typedef typename DataType::const_reference const_reference;
    DataArrayWrapper( DataType& data ) : m_originalData(data)
    {
        m_thisAddr = reinterpret_cast<uintptr_t>(&(this->m_originalData));
    }
    /// \brief Read/write random access with overloading
    /// operator[] is overloaded, and calls subscriptOperatorOverloadWrite
    /// to register the memory address that is accessed (if needed),
    /// before calling the original operator[].
    /// Note that operator[] has  __attribute__((__always_inline__)) for performance reasons.
    reference operator[](size_type n) __attribute__((__always_inline__))
    {
        subscriptOperatorOverloadWrite(n, this->m_thisAddr);
        return this->m_originalData.DataType::operator[](n);
    }
    /// \brief Read-only random access with overloading
    /// operator[] is overloaded, and calls subscriptOperatorOverloadRead
    /// to register the memory address that is accessed (if needed),
    /// before calling the original operator[].
    /// Note that operator[] has  __attribute__((__always_inline__)) for performance reasons.
    const_reference operator[](size_type n) const __attribute__((__always_inline__))
    {
        subscriptOperatorOverloadRead(n, this->m_thisAddr);
        return this->m_originalData.DataType::operator[](n);
    }
//    DataType*& operator->()
//    {
//        return this->m_originalData;
//    }
};



/// \class DataVectorWrapper
/// \brief Wrapper for std::vector-like data
/// Templated on the type of the data that is wrapped
template <class DataType>
class DataVectorWrapper
{

    DataType m_originalData;
    uintptr_t m_thisAddr;
public :
    typedef typename DataType::value_type value_type;
    typedef typename DataType::size_type size_type;
    typedef typename DataType::reference reference;
    typedef typename DataType::const_reference const_reference;
    typedef typename DataType::iterator iterator;
    typedef typename DataType::const_iterator const_iterator;
    DataVectorWrapper() : m_originalData()
    {
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
    }
    DataVectorWrapper(size_t size) : m_originalData(size)
    {
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
    }
    DataVectorWrapper(DataType defaultdata) : m_originalData(defaultdata)
    {
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
    }
    DataVectorWrapper( DataType* data ) : m_originalData(*data)
    {
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
    }
    /// Copy constructor
    DataVectorWrapper(const DataVectorWrapper& x)
      : m_originalData(x.m_originalData)
    {
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
    }
    /// Move constructor
    DataVectorWrapper(DataVectorWrapper&& v)
      : m_originalData(std::move(v.m_originalData))
    {
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
    }
    /// \brief Read/write random access with overloading
    /// operator[] is overloaded, and calls subscriptOperatorOverloadWrite
    /// to register the memory address that is accessed (if needed),
    /// before calling the original operator[].
    /// Note that operator[] has  __attribute__((__always_inline__)) for performance reasons.
    reference operator[](size_type n) __attribute__((__always_inline__))
    {
        subscriptOperatorOverloadWrite(n, this->m_thisAddr);
        return this->m_originalData.DataType::operator[](n);
    }
    /// \brief Read-only random access with overloading
    /// operator[] is overloaded, and calls subscriptOperatorOverloadRead
    /// to register the memory address that is accessed (if needed),
    /// before calling the original operator[].
    /// Note that operator[] has  __attribute__((__always_inline__)) for performance reasons.
    const_reference operator[](size_type n) const __attribute__((__always_inline__))
    {
        subscriptOperatorOverloadRead(n, this->m_thisAddr);
        return this->m_originalData.DataType::operator[](n);
    }
//    DataType* operator->()
//    {
//        return &this->m_originalData;
//    }

    size_type size() const
    {
        return this->m_originalData.DataType::size();
    }
    void resize(size_type n, value_type val = value_type())
    {
        this->m_originalData.DataType::resize(n,val);
    }
    void clear()
    {
        this->m_originalData.DataType::clear();
    }
    void reserve(size_type n)
    {
        this->m_originalData.DataType::reserve(n);
    }
    void push_back(const value_type& val)
    {
        this->m_originalData.DataType::push_back(val);
    }
    void push_back(value_type&& val)
    {
        this->m_originalData.DataType::push_back(val);
    }
    bool empty() const
    {
        return this->m_originalData.DataType::empty();
    }
    /// Copy operator
    DataVectorWrapper<DataType>& operator=(const DataVectorWrapper<DataType>& x)
    {
        this->m_originalData.DataType::operator=((x.m_originalData));
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
        return *this;
    }
    /// Move assignment operator
    DataVectorWrapper<DataType>& operator=(DataVectorWrapper<DataType>&& v)
    {
        this->m_originalData.DataType::operator=(*(v.m_originalData));
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
        return *this;
    }
    /// Copy operator
    constexpr DataVectorWrapper<DataType>& operator=(const DataType& x)
    {
        this->m_originalData.DataType::operator=(x);
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
        return *this;
    }
    /// Move assignment operator
    DataVectorWrapper<DataType>& operator=(DataType&& v)
    {
        this->m_originalData.DataType::operator=(v);
        m_thisAddr = reinterpret_cast<uintptr_t>(&this->m_originalData);
        return *this;
    }
    iterator begin()
    {
        return this->m_originalData.DataType::begin();
    }
    const_iterator begin() const
    {
        return this->m_originalData.DataType::begin();
    }
    iterator end()
    {
        return this->m_originalData.DataType::end();
    }
    const_iterator end() const
    {
        return this->m_originalData.DataType::end();
    }

};

} // namespace verticalparallelization

#endif // HELPER_DATA_WRAPPER_H
