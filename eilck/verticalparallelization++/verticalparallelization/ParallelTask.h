///
/// \file   ParallelTask.h
/// \brief  Implementation of class ParallelTask
/// \author Maxime Mogé
/// \date   2018
///
///  Remark: Maybe this could be merged with ThreadTask ?
///
#ifndef HELPER_PARALLEL_TASK_H
#define HELPER_PARALLEL_TASK_H


#include "ParallelPhase.h"

namespace verticalparallelization
{

/// \brief A Task represented by its memory accesses and decomposition into Phases
/// This is the class used to analyse dependencies, not to run the Task
class ParallelTask
{
public:
    /// id of attached task in enclosing parallel section
    unsigned int m_taskId;
    /// m_phases is the decomposition of the task into phases
    std::vector< ParallelPhase* > m_phases;
    /// pointer on the current phase
    ParallelPhase* m_currentPhase;
    /// pointer on the previous phase
    ParallelPhase* m_previousPhase;
    /// counter with value from -1 to m_currentPhaseLastAccess / -1 mean first access of the phase, m_currentPhaseLastAccess mean last access of the phase - remark : size_t in unsigned, so -1 means max value of size_t
    size_t m_currentPhaseAccessCounter;
    /// index of the last access of current phase - the value is the number of accesses in the phase-2 (since we start at index -1 with the first access) - remark : size_t in unsigned, so -1 means max value of size_t
    size_t m_currentPhaseLastAccess;
    /// number of phases in this task
    unsigned int m_nbPhases;
public:
    ParallelTask( unsigned int tid )
    : m_taskId( tid ),
      m_phases(),
      m_currentPhase(),
      m_previousPhase(),
      m_currentPhaseAccessCounter(-1),
      m_currentPhaseLastAccess(-1),
      m_nbPhases(0)
    {
        this->m_phases.clear();
        this->m_phases.resize(0);
    }
    ~ParallelTask()
    {
        for ( std::vector< ParallelPhase* >::iterator it = this->m_phases.begin()
                ; it != this->m_phases.end()
                ; ++it )
        {
            delete *it;
        }
        this->m_phases.clear();
        this->m_phases.resize(0);
    }
    ParallelPhase* addPhase()
    {
        PRINT_MSG_PARALLEL("    from addPhase - task " << m_taskId << " already has " << this->m_phases.size()
                << " phases - adding a new one with pid = " << this->m_nbPhases );
        PRINT_LOG_STREAM_TO_FILE();
        this->m_phases.push_back( new ParallelPhase( this->m_nbPhases, this->m_taskId) );
        this->m_nbPhases++;
//        std::cout << "add phase " << this->m_nbPhases << std::endl;
        PRINT_MSG_PARALLEL("   from addPhase, phase " << reinterpret_cast<std::uintptr_t>(this->m_phases.back()) << " added");
        PRINT_LOG_STREAM_TO_FILE();
        return this->m_phases.back();
    }
    /// returns a ptr to the first ParallelPhase of the task
    ParallelPhase* getFirstPhase() const
    {
        if ( !this->hasPhases() )
        {
            return reinterpret_cast<ParallelPhase*>(NULL);
        }
        else
        {
            return *(this->m_phases.begin());
        }
    }
    /// hasPhases returns true if this has at least one Phase
    bool hasPhases() const
    {
        return this->m_nbPhases;
    }
    /// isEmpty returns true if this contains no memory accesses
    bool isEmpty() const
    {
        bool empty = true;
        std::vector< ParallelPhase* >::const_iterator itPhase = this->m_phases.begin();
        while ( itPhase != this->m_phases.end() && empty )
        {
            empty = (*itPhase)->isEmpty();
            itPhase++;
        }
        return empty;
    }
    unsigned int getId() const
    {
        return this->m_taskId;
    }
};

} // namespace verticalparallelization

#endif //HELPER_PARALLEL_TASK_H

