///
/// \file   FunctionTaskVoid.h
/// \brief  child class of ThreadTask for tasks consisting in a function call that returns void.
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_FUNCTION_TASK_VOID_H
#define HELPER_FUNCTION_TASK_VOID_H

#include <future>
#include "ThreadTask.h"

namespace verticalparallelization
{


/// \class FunctionTaskVoid
/// \brief child class of ThreadTask for tasks consisting in a function call that returns void.
/// A reference to the function is stored in attribute std::function<Result()>& m_fn.
/// Templated on the return type of function m_fn.
/// m_fun takes no parameter. When creating an instance of FunctionTask we need to bind
/// all parameters values using std::bind (+ std::ref for references).
/// Remark: if the return type is not void, use FunctionTask.
class FunctionTaskVoid : public ThreadTask
{

protected:
    /// reference to the function constituting the task
    std::function<void()>& m_fn;

public:
    /// m_fun takes no parameter. When creating an instance of FunctionTask we need to bind
    /// all parameters values using std::bind (+ std::ref for references).
    /// Remark: if the return type is not void, use FunctionTask.
    FunctionTaskVoid( std::function<void()>& fn, unsigned int tid = 0)
    : ThreadTask(tid)
    , m_fn(fn)
    {
    }

    /// \brief runs the task - i.e. runs the function m_fn -
    /// then release the handles that are still acquire using releaseRemainingHandles
    virtual void run()
    {
        this->m_fn();
        this->releaseRemainingHandles();
        this->m_pr.set_value(1);    //we assign a meaningless value to the promise, it is only used to wait for the completion of the task
    }

    ///gets a void ptr to the result returned by the execution of the Task
    ///the result needs to get casted to the right type after the call to getResult
    ///this way we can manage tasks returning different types of results
    virtual void* getResult()
    {
        std::cerr << "ERROR: FunctionTaskVoid::getResult() should never be called (since there is no result to get" << std::endl;
        return reinterpret_cast<void*>(&this->m_fn);
    }

};

} // namespace verticalparallelization

#endif //HELPER_FUNCTION_TASK_VOID_H

