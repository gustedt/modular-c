///
/// \file   memory_var.h
/// \brief  Definition of the flag execType (CLASSICAL, PARALLEL or LOGGING) + size of phases (meta steps) m_memBlockSize
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_MEMORY_VAR_H
#define HELPER_MEMORY_VAR_H

#include <assert.h>
//#include <cstdint>
#ifdef PRINT_FIFOS
#include <list>
#include <map>
#include <mutex>
#endif // PRINT_FIFOS

//// Debugging options ////
//#define LOG_THREADS
//#define LOG_MEMORY_ACCESSES
//#define PRINT_FIFOS

namespace verticalparallelization
{

enum ExecType
{
    CLASSICAL,
    PARALLEL,
    LOGGING,
};

struct simuInfo
{
    /// flag to enable CLASSICAL, PARALLEL or LOGGING execution of the parallel sections
    static ExecType execType;
};

struct addressProfiling
{
    /// size of phases (meta steps) m_memBlockSize - number of memory accesses in a task phase/meta step (this is used to split a Task in phases)
    /// WARNING: this is defined by the used at the compilation on libverticalparallelization
    /// TODO: the lib could try to compute an appropriate value, or we could find a more user friendly way to set its value
    static unsigned int m_memBlockSize;
    ////DEBUG////
#ifdef PRINT_FIFOS
    static std::map< ParallelSectionIdentifier, std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*,eilck_ehdl*>>> > > m_fifoTracker;    //for each eilck_object, a list (representing a FIFO) of unsigned int[4] containing [isRead, sectionId, taskId, phaseId]
    static std::map< eilck_obj*, ParallelPhase* > m_objTracker;   //for each eilck_obj, the corresponding parallel section + the address of the vector and the index of the block
    static std::mutex* m_fifoTrackerMutex;   //for thread safe use of m_fifoTracker
#endif // PRINT_FIFOS
    /////////////
};

}   //namespace verticalparallelization

#endif  //HELPER_MEMORY_VAR_H
