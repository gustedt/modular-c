///
/// \file   thread_info.h
/// \brief  Set number of threads to launch and store ID of the thread in a thread_local var+ define debugging macros
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_THREAD_INFO_H
#define HELPER_THREAD_INFO_H

#include <iostream>

#include <vector>
#include <thread>
#include <cstdint>

//// DEBUGGING ////  -> WARNING: might not work anymore
//#define LOG_THREADS
//#define LOG_MEMORY_ACCESSES
////

struct threadInfo
{
    /// ID of current thread - this is a thread_local variable, so each thread can have a different value
    static thread_local unsigned int m_workerThreadId;
    /// number of worker threads to launch
    static unsigned int m_nbWorkerThreads;
    /// number of worker threads allowed to run tasks (<=m_nbWorkerThreads)
    static unsigned int m_workerThreadAllowed;
#ifdef PRINT_FIFOS
    static size_t m_nbReq;
    static size_t m_nbAcq;
    static size_t m_nbRel;
#endif // PRINT_FIFOS
};


#ifdef LOG_THREADS
#include <fstream>
#include <sstream>
#include <sys/time.h>
    extern std::vector< std::stringstream >* threadLogMsgs;

    #define PRINT_LOG_STREAM_TO_FILE() do {  \
        std::ofstream outfile; \
        std::stringstream filename; \
        filename << "log_thread_" << threadInfo::m_workerThreadId << ".log"; \
        outfile.open ( filename.str(), std::ios::out | std::ios::app ); \
        outfile << ((*threadLogMsgs)[threadInfo::m_workerThreadId].str()); \
        outfile.close(); \
        (*threadLogMsgs)[threadInfo::m_workerThreadId] = std::stringstream();\
        } while(0)   /* (no trailing ; ) */

    #define PRINT_MSG_PARALLEL(msg) do {  \
      struct timeval c_start; \
      gettimeofday(&c_start,NULL); \
      (*threadLogMsgs)[threadInfo::m_workerThreadId] << "time " << c_start.tv_sec << "." << c_start.tv_usec  << "s. |    " << msg << "\n"; \
  } while(0)   /* (no trailing ; ) */
#else //LOG_THREADS
    #define OPEN_PARALLEL_LOG_FILES() do {} while(0)
    #define CLOSE_PARALLEL_LOG_FILES() do {} while(0)
    #define PRINT_MSG_PARALLEL(msg) do {} while(0)
    #define CREATE_LOG_STREAM(msg) do {} while(0)
    #define PRINT_LOG_STREAM_TO_FILE() do {} while(0)
#endif //LOG_THREADS


#ifdef LOG_MEMORY_ACCESSES
#include <fstream>
#include <sstream>
#include <sys/time.h>
    extern std::vector< std::stringstream >* threadLogMem;

    #define PRINT_MEM_LOG_TO_FILE() do {  \
        std::ofstream outfile; \
        std::stringstream filename; \
        filename << "log_mem_" << threadInfo::m_workerThreadId << ".log"; \
        outfile.open ( filename.str(), std::ios::out | std::ios::app ); \
        outfile << ((*threadLogMem)[threadInfo::m_workerThreadId].str()); \
        outfile.close(); \
        (*threadLogMem)[threadInfo::m_workerThreadId] = std::stringstream();\
        } while(0)   /* (no trailing ; ) */

    #define ADD_TO_MEM_LOG(msg) do {  \
      struct timeval c_start; \
      gettimeofday(&c_start,NULL); \
      (*threadLogMem)[threadInfo::m_workerThreadId] << "time " << c_start.tv_sec << " s. + " << c_start.tv_usec  << " us. |    " << msg << "\n"; \
  } while(0)   /* (no trailing ; ) */
#else //LOG_MEMORY_ACCESSES
    #define PRINT_MEM_LOG_TO_FILE() do {} while(0)
    #define ADD_TO_MEM_LOG(msg) do {} while(0)
#endif //LOG_MEMORY_ACCESSES


#endif  //HELPER_THREAD_INFO_H
