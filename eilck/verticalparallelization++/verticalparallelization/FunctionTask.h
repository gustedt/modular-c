///
/// \file   FunctionTask.h
/// \brief  child class FunctionTask of ThreadTask for tasks consisting in a function call.
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_FUNCTION_TASK_H
#define HELPER_FUNCTION_TASK_H

#include <future>
#include "ThreadTask.h"

namespace verticalparallelization
{


/// \brief child class of ThreadTask for tasks consisting in a function call.
/// A reference to the function is stored in attribute std::function<Result()>& m_fn.
/// Templated on the return type of function m_fn.
/// m_fun takes no parameter. When creating an instance of FunctionTask we need to bind
/// all parameters values using std::bind (+ std::ref for references).
/// Remark: if the return type is void, use FunctionTaskVoid to avoid error
template < class Result >
class FunctionTask : public ThreadTask
{

protected:
    /// reference to the function constituting the task
    std::function<Result()>& m_fn;
    /// pointer to the result of the function
    Result* m_res;

public:
    /// m_fun takes no parameter. When creating an instance of FunctionTask we need to bind
    /// all parameters values using std::bind (+ std::ref for references).
    /// Remark: if the return type is void, use FunctionTaskVoid to avoid error
    FunctionTask( std::function<Result()>& fn, unsigned int tid = 0)
    : ThreadTask(tid)
    , m_fn(fn)
    , m_res()
    {
        m_res = new Result();
    }

    /// \brief runs the task - i.e. runs the function m_fn -
    /// then release the handles that are still acquire using releaseRemainingHandles
    virtual void run()
    {
        *m_res = m_fn();
        this->releaseRemainingHandles();
        this->m_pr.set_value(1);    //we assign a meaningless value to the promise, it is only used to wait for the completion of the task
    }

    ///gets a void ptr to the result returned by the execution of the Task
    ///the result needs to get casted to the right type after the call to getResult
    ///this way we can manage tasks returning different types of results
    virtual void* getResult()
    {
        return reinterpret_cast<void*>(this->m_res);
    }
};

} // namespace verticalparallelization

#endif //HELPER_FUNCTION_TASK_H

