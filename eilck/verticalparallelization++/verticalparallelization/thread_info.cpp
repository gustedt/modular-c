///
/// \file   thread_info.cpp
/// \brief  Set number of threads to launch and store ID of the thread in a thread_local var+ define debugging macros
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_THREAD_INFO_CPP
#define HELPER_THREAD_INFO_CPP

#include "thread_info.h"

    // For Master thread we initialize m_workerThreadId with value 0
    thread_local unsigned int threadInfo::m_workerThreadId = 0;

    // We allow only 1 thread to run at the beginning of the execution
    // Then we logging is finished we will set the value of m_workerThreadAllowed to m_nbWorkerThreads
    unsigned int threadInfo::m_workerThreadAllowed = 1;

    // TODO: set the value of the number of threads to create more "elegantly" / in a more user fliendly way
    unsigned int threadInfo::m_nbWorkerThreads = std::thread::hardware_concurrency();
//    unsigned int threadInfo::m_nbWorkerThreads = 4;

#ifdef PRINT_FIFOS
    size_t threadInfo::m_nbReq = 0;
    size_t threadInfo::m_nbAcq = 0;
    size_t threadInfo::m_nbRel = 0;
#endif // PRINT_FIFOS


#ifdef LOG_THREADS
      std::vector< std::stringstream >* threadLogMsgs = new std::vector< std::stringstream >(threadInfo::m_nbWorkerThreads+1);
#else //LOG_THREADS
#endif //LOG_THREADS

#ifdef LOG_MEMORY_ACCESSES
    std::vector< std::stringstream >* threadLogMem = new std::vector< std::stringstream >(threadInfo::m_nbWorkerThreads+1);
#endif //LOG_MEMORY_ACCESSES


#endif  //HELPER_THREAD_INFO_CPP
