///
/// \file   ThreadPool.h
/// \brief  Implementation of a ThreadPool using a FIFO
/// \author Maxime Mogé
/// \date   2018
///
/// Largely inspired by  C++ Concurrency in Action - Practical Multithreading - Anthony Williams - 2012 - ISBN 9781933988771
///
#ifndef HELPER_THREAD_POOL_H
#define HELPER_THREAD_POOL_H

#include <vector>
#include <thread>
#include <future>
#include <atomic>

#include "thread_info.h"
#include "ThreadSafeQueue.h"
#include "ThreadTask.h"

namespace verticalparallelization
{

class ThreadTask;

/// \brief Implementation of a ThreadPool using a FIFO
class ThreadPool
{
    verticalparallelization::ThreadSafeQueue<verticalparallelization::ThreadTask*> work_queue;
    std::atomic_bool done;
    std::vector<std::thread> threads;
    unsigned int const thread_count;
    std::atomic_uint nb_worker_threads_allowed;

public:
    ThreadPool()
      : done(false)
        , thread_count(threadInfo::m_nbWorkerThreads)
        , nb_worker_threads_allowed( 1 )
    {
        threadInfo::m_workerThreadId = 0; //master thread
        try
        {
//            std::cout << "creating " << thread_count << " threads !!!!!!!!!!!!" << std::endl << std::endl;
            for(unsigned int i=0;i<thread_count;++i)
            {
                threads.push_back( std::thread(&ThreadPool::worker_thread,this, i) );
            }
        }
        catch(...)
        {
            done=true;
            throw;
        }
    }
    ~ThreadPool()
    {
        done = true;
        work_queue.stopAllThreads();
        for(unsigned long i=0;i<threads.size();++i)
        {
            if(threads[i].joinable())
            {
                threads[i].join();
            }
        }
    }

    static ThreadPool& getInstance()
    {
        static ThreadPool instance;
        return instance;
    }

    unsigned int getThreadCount()
    {
        return thread_count;
    }

    std::future<int> submit(verticalparallelization::ThreadTask* task)
    {
        std::future<int> res( task->get_future() );
        work_queue.push(task);
        return res;
    }

    void worker_thread(unsigned int nbWorkerThreads)
    {
        unsigned int currentWorkerThreadId = nbWorkerThreads+1; //nbWorkerThreads is given by master thread
        threadInfo::m_workerThreadId = currentWorkerThreadId;
        //
        while(!done)
        {
            verticalparallelization::ThreadTask* task;
            //WARNING : there is active wait if currentWorkerThreadId <= nb_worker_threads_allowed
            //TODO : use condition var to remove active wait
            if( (currentWorkerThreadId <= threadInfo::m_workerThreadAllowed) && work_queue.pop(task))
            {
                //address profiling
                verticalparallelization::ParallelTask*&  curTask = verticalparallelization::getCurrentTask();
                //we would not be here if the section was empty but I do an assert anyway to be sure
                assert( !verticalparallelization::currentSection::m_currentParallelSection->m_tasks.empty() );
                //taskId should be less than nb tasks
                assert( task->getId() < verticalparallelization::currentSection::m_currentParallelSection->getNbTasks() );
                //
                curTask = verticalparallelization::currentSection::m_currentParallelSection->getTask(task->getId());
                //if the task has no phases we do nothing
                if( curTask->hasPhases() )
                {
                    curTask->m_currentPhase = curTask->getFirstPhase();
                    curTask->m_currentPhaseLastAccess = curTask->m_currentPhase->m_lastAccess;
                }
                else
                {
                    curTask->m_currentPhase = nullptr;
                }
                //run task
                PRINT_MSG_PARALLEL("--------from worker_thread() "
                        << "section " << std::get<0>(verticalparallelization::addressProfiling::m_currentParallelSectionId) << "-" << std::get<1>(verticalparallelization::addressProfiling::m_currentParallelSectionId) << "-" << std::get<2>(verticalparallelization::addressProfiling::m_currentParallelSectionId) << "-" << std::get<3>(verticalparallelization::addressProfiling::m_currentParallelSectionId)
                        << "  -  launching task " << task->getId()
                        << " on CPU " << sched_getcpu());
                ADD_TO_MEM_LOG("---------- launching task " << task << std::endl);
                task->run();
                curTask->m_currentPhaseAccessCounter = -1;
                ADD_TO_MEM_LOG("---------- task " << task << " done " << std::endl
                        << "///////////////////////////////////////////////////////////////////////"
                        << std::endl << std::endl);
                PRINT_MEM_LOG_TO_FILE();
                PRINT_MSG_PARALLEL("--------from worker_thread(), task " << task->getId() << " done ");
                PRINT_MSG_PARALLEL("--------################");
                PRINT_LOG_STREAM_TO_FILE( );
            }
            else
            {
                std::this_thread::yield();
            }
        }
    }
};

} // namespace verticalparallelization

#endif  //HELPER_THREAD_POOL_H
