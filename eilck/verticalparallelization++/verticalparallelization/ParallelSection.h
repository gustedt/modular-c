///
/// \file   ParallelSection.h
/// \brief  Implements parallel sections
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_PARALLEL_SECTION_H
#define HELPER_PARALLEL_SECTION_H

#include <vector>
#include "ParallelTask.h"

namespace verticalparallelization
{

/// \brief Base class for parallel sections
/// Contains a vector a ParallelTask
class BaseParallelSection
{
public:
    /// vector of Tasks to be executed in parallel in this section
    std::vector< ParallelTask* > m_tasks;
    /// true of the first phase of memory access logging is done
    bool m_firstLoggingPhaseDone;
    /// true of the second phase of memory access logging is done
    bool m_secondLoggingPhaseDone;
    /// nb of memory accesses in a phase of a ParallelTask (m_memBlockSize is used to split the tasks in phases)
    unsigned int m_memBlockSize;
    /// counter of number of calls to startParallelization - if ==0 do sequential exec - if ==1 do logging step then parallel exec - set to 0 by stopParallelization
    size_t m_startParallel;


public:
    BaseParallelSection( )
    : m_tasks(),
      m_firstLoggingPhaseDone(false),
      m_secondLoggingPhaseDone(false),
      m_memBlockSize( addressProfiling::m_memBlockSize ),
      m_startParallel(0)
    {
        this->m_tasks.clear();
        assert( threadInfo::m_workerThreadId == 0 );
    }
    virtual ~BaseParallelSection()
    {
        for ( std::vector< ParallelTask* >::iterator it = this->m_tasks.begin()
                ; it != this->m_tasks.end()
                ; ++it )
        {
            (*it)->~ParallelTask();
        }
        this->m_tasks.clear();
    }
    virtual void addTask( const unsigned int tid ) = 0;
    bool hasTask() const
    {
        return !this->m_tasks.empty();
    }
    size_t getNbTasks() const
    {
        return this->m_tasks.size();
    }
    ParallelTask*& getTask( size_t taskid )
    {
        return this->m_tasks[taskid];
    }
    //isEmpty return true if this has no task or if its tasks make no memory accesses
    bool isEmpty() const
    {
        bool empty = true;
        std::vector< ParallelTask* >::const_iterator itTask = this->m_tasks.begin();
        while ( itTask != this->m_tasks.end() && empty )
        {
            empty = (*itTask)->isEmpty();
            itTask++;
        }
        return empty;
    }
    virtual std::map<std::pair<std::uintptr_t, size_t>, ParallelPhase*>* getMemLocationToOwner() = 0;
    virtual void addMemLocationOwner( std::pair<std::uintptr_t, size_t> memloc, ParallelPhase* owner ) = 0;
    virtual bool hasOwner( std::pair<std::uintptr_t, size_t> memloc ) = 0;
    virtual ParallelPhase* getOwner( std::pair<std::uintptr_t, size_t> memloc ) = 0;
};

/// \brief Child class for parallel sections templated on the type of identifier used for the section
template < class ParallelSectionIdentifier >
class ParallelSection : public BaseParallelSection
{
public:
    /// id of enclosing parallel section
    ParallelSectionIdentifier m_sectionId;
    /// maps a pair <memory address, index in the array/vector> (representing a memory location) to the ParallelPhase that owns it
    std::map<std::pair<std::uintptr_t, size_t>, ParallelPhase*> m_memLocationToOwner;
    //contains a ptr to each ParallelSection with ID of type ParallelSectionIdentifier
    static std::map< ParallelSectionIdentifier, ParallelSection<ParallelSectionIdentifier>* > m_existingSections;
public:
    ParallelSection( ParallelSectionIdentifier psid )
    : BaseParallelSection(),
      m_sectionId( psid ),
      m_memLocationToOwner()
    {
        this->m_tasks.clear();
        this->m_memLocationToOwner = std::map<std::pair<std::uintptr_t, size_t>, ParallelPhase*>();
        assert( threadInfo::m_workerThreadId == 0 );
        ParallelSection<ParallelSectionIdentifier>::m_existingSections[this->m_sectionId] = this;
    }
    ~ParallelSection()
    {
    }
    ParallelSectionIdentifier getId()
    {
        return this->m_sectionId;
    }
    void addTask( const unsigned int tid )
    {
//        std::cout << "add task " << tid << std::endl;
        assert( threadInfo::m_workerThreadId == 0 );
        this->m_tasks.push_back( new ParallelTask( tid ) );
    }
    std::map<std::pair<std::uintptr_t, size_t>, ParallelPhase*>* getMemLocationToOwner()
    {
        return &(this->m_memLocationToOwner);
    }
    void addMemLocationOwner( std::pair<std::uintptr_t, size_t> memloc, ParallelPhase* owner )
    {
        this->m_memLocationToOwner[memloc]= owner;
    }
    bool hasOwner( std::pair<std::uintptr_t, size_t> memloc )
    {
        return (  this->m_memLocationToOwner.find( memloc )
                    != this->m_memLocationToOwner.end() );
    }
    ParallelPhase* getOwner( std::pair<std::uintptr_t, size_t> memloc )
    {
        return this->m_memLocationToOwner[memloc];
    }
};

template<class ParallelSectionIdentifier>
std::map< ParallelSectionIdentifier, ParallelSection<ParallelSectionIdentifier>* > ParallelSection<ParallelSectionIdentifier>::m_existingSections = std::map< ParallelSectionIdentifier, ParallelSection<ParallelSectionIdentifier>* >();

} // namespace verticalparallelization

#endif //HELPER_PARALLEL_SECTION_H

