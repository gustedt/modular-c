///
/// \file   memory_helper_functions.cpp
/// \brief
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_MEMORY_HELPER_FUNCTIONS_CPP
#define HELPER_MEMORY_HELPER_FUNCTIONS_CPP

#include "memory_helper_functions.h"

namespace verticalparallelization
{


    BaseParallelSection* currentSection::m_currentParallelSection = nullptr;

    void beginOrEndPhase( size_t* restrict curCounter, ParallelTask*& restrict curTask )
    {
        if (*curCounter == size_t(-1) )
//        if (*curCounter == 0 )
        {
            //this is the first mem access of current phase
            if( curTask->m_currentPhase->m_phaseId > 0 )
            {
                //m_currentPhase is not the first phase of currentTask
                curTask->m_previousPhase->requireHandles();
                curTask->m_previousPhase->releaseHandles();
            }
            //acquire the handles of current phase
            curTask->m_currentPhase->acquireHandles();
            (*curCounter)++;
        }
        else
        {
//            if( addressProfiling::m_currentTask->m_currentPhase->m_accessCounter == 0)
//            {
//                std::cout << "ERROR !!!!! phase with only 1 memory access" << std::endl;
//            }
            //this is the last mem access of current phase
            *curCounter = size_t(-1);
            if ( curTask->m_currentPhase->m_phaseId+1 < curTask->m_phases.size() )
            {
                //m_currentPhase is not the last phase of m_currentTask
                curTask->m_previousPhase = curTask->m_currentPhase;
                curTask->m_currentPhase
                    = (curTask->m_phases)[curTask->m_previousPhase->m_phaseId+1];
                curTask->m_currentPhaseLastAccess = curTask->m_currentPhase->m_lastAccess;
            }
        }
    }
    void loggingPhaseRead(size_t n, uintptr_t thisPtr)
    {
        if ( !currentSection::m_currentParallelSection->m_firstLoggingPhaseDone )
        {
            loggingFirstPhaseRead();
        }
        else if ( !currentSection::m_currentParallelSection->m_secondLoggingPhaseDone )
        {
            loggingSecondPhaseRead(n,thisPtr);
        }
    }
    void loggingPhaseWrite(size_t n, uintptr_t thisPtr)
    {
        if ( !currentSection::m_currentParallelSection->m_firstLoggingPhaseDone )
        {
            loggingFirstPhaseWrite(n,thisPtr);
        }
        else if ( !currentSection::m_currentParallelSection->m_secondLoggingPhaseDone )
        {
            loggingSecondPhaseWrite(n,thisPtr);
        }
    }
    void loggingFirstPhaseRead()
    {
        ParallelTask* restrict  curTask = getCurrentTask();
        //if previous phase is done, addressProfiling::m_currentPhase->m_accessCounter was set to -1 again in last access
        if( !curTask->m_currentPhase || curTask->m_currentPhaseAccessCounter == size_t(-1) )
        {
            //this is the first mem access of a phase, so we create a new Phase
            curTask->m_currentPhase = curTask->addPhase();
        }
        //current access is a read access, so it does not define the owner of current memory location
        curTask->m_currentPhaseAccessCounter++;
        if ( curTask->m_currentPhaseAccessCounter == addressProfiling::m_memBlockSize-1 )
        {
            //current phase is finished
            curTask->m_currentPhaseAccessCounter = size_t(-1);
            curTask->m_currentPhase->m_lastAccess = addressProfiling::m_memBlockSize-2;
        }
    }
    void loggingFirstPhaseWrite(size_t n, uintptr_t thisPtr)
    {
        ParallelTask* restrict  curTask = getCurrentTask();
        //if previous phase is done, addressProfiling::m_currentPhase->m_accessCounter was set to 0 again in last access
        if( !curTask->m_currentPhase || curTask->m_currentPhaseAccessCounter == size_t(-1) )
        {
            //this is the first mem access of a phase, so we create a new Phase
            curTask->m_currentPhase = curTask->addPhase();
        }
        //current memory location is uniquely defined by std::pair<std::uintptr_t, size_t>(reinterpret_cast<uintptr_t>(this), n )
        if ( !currentSection::m_currentParallelSection->hasOwner(std::pair<std::uintptr_t, size_t> (thisPtr, n )) )
        {
            //current memory location does not belong to a Phase yet
            currentSection::m_currentParallelSection->addMemLocationOwner
                ( std::pair<std::uintptr_t, size_t>(thisPtr, n ), curTask->m_currentPhase );
        }
        curTask->m_currentPhaseAccessCounter++;
        if ( curTask->m_currentPhaseAccessCounter == addressProfiling::m_memBlockSize-1 )
        {
            //current phase is finished
            curTask->m_currentPhaseAccessCounter = size_t(-1);
            curTask->m_currentPhase->m_lastAccess = addressProfiling::m_memBlockSize-2;
        }
    }
    void loggingSecondPhaseRead(size_t n, uintptr_t thisPtr)
    {
        ParallelTask* restrict  curTask = getCurrentTask();
        //add current access to dependencies
        if ( !currentSection::m_currentParallelSection->hasOwner(std::pair<std::uintptr_t, size_t> (thisPtr, n )) )
        {
            //current memory location does not belong to a Phase yet
            currentSection::m_currentParallelSection->addMemLocationOwner
                ( std::pair<std::uintptr_t, size_t>(thisPtr, n ), curTask->m_currentPhase );
        }
        ParallelPhase* owner = currentSection::m_currentParallelSection->getOwner(std::pair<std::uintptr_t, size_t>(thisPtr, n ));
        curTask->m_currentPhase->addRAccess( owner );
        if ( curTask->m_currentPhaseAccessCounter == curTask->m_currentPhase->m_lastAccess )
        {
            //end of current phase
            curTask->m_currentPhaseAccessCounter = size_t(-1);
            //require handles on all dependencies
            curTask->m_currentPhase->requireHandles();
            if ( curTask->m_currentPhase->m_phaseId+1 < curTask->m_phases.size() )
            {
                //m_currentPhase is not the last phase of m_currentTask
                curTask->m_previousPhase = curTask->m_currentPhase;
                curTask->m_currentPhase
                    = (curTask->m_phases)[curTask->m_previousPhase->m_phaseId+1];
            }
        }
        else
        {
            curTask->m_currentPhaseAccessCounter++;
        }
    }
    void loggingSecondPhaseWrite(size_t n, uintptr_t thisPtr)
    {
        ParallelTask* restrict  curTask = getCurrentTask();
//            if( addressProfiling::m_memLocationToOwner[addressProfiling::m_currentParallelSectionId].find(std::pair<std::uintptr_t, size_t>(reinterpret_cast<uintptr_t>(this), n ))
//                    == addressProfiling::m_memLocationToOwner[addressProfiling::m_currentParallelSectionId].end() )
//            {
//                //this means there are only Read accesses on this location, so it was not assigned an owner
//                //during first logging step, so currentPhase takes ownership
//                addressProfiling::m_memLocationToOwner[addressProfiling::m_currentParallelSectionId][std::pair<std::uintptr_t, size_t>(reinterpret_cast<uintptr_t>(this), n )]
//                     = addressProfiling::m_currentTask->m_currentPhase;
//            }
        //-> this cannot happen in a Write access
        ParallelPhase* owner = currentSection::m_currentParallelSection->getOwner(std::pair<std::uintptr_t, size_t>(thisPtr, n ));
        curTask->m_currentPhase->addWAccess( owner );

        if ( curTask->m_currentPhaseAccessCounter == curTask->m_currentPhase->m_lastAccess )
        {
            //end of current phase
            curTask->m_currentPhaseAccessCounter = size_t(-1);
            //require handles on all dependencies
            curTask->m_currentPhase->requireHandles();
            if ( curTask->m_currentPhase->m_phaseId+1 < curTask->m_phases.size() )
            {
                //m_currentPhase is not the last phase of m_currentTask
                curTask->m_previousPhase = curTask->m_currentPhase;
                curTask->m_currentPhase
                    = (curTask->m_phases)[curTask->m_previousPhase->m_phaseId+1];
            }
        }
        else
        {
            curTask->m_currentPhaseAccessCounter++;
        }
    }


    void printAllFIFOs()
    {
#ifdef PRINT_FIFOS
        if ( sofa::verticalparallelization::simuInfo::timeStep >= sofa::verticalparallelization::addressProfiling::m_loggingTimeStep+2 )
        {

            int nbSection = 0;
            for( std::map< ParallelSectionIdentifier, std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>> > >::iterator sectionit = addressProfiling::m_fifoTracker.begin()
                    ; sectionit != addressProfiling::m_fifoTracker.end()
                    ; ++sectionit
                )
            {
                int nbTask = 0;
                //for each task
                for( std::vector< ParallelTask* >::iterator taskIt = addressProfiling::m_sectionTasks[sectionit->first]->m_tasks.begin()
                        ; taskIt != addressProfiling::m_sectionTasks[sectionit->first]->m_tasks.end()
                        ; taskIt++ )
                {
                    //for each phase
                    int nbPhases = 0;
                    for ( std::vector< ParallelPhase* >::iterator phaseIt = (*taskIt)->m_phases.begin()
                            ; phaseIt != (*taskIt)->m_phases.end()
                            ; phaseIt++ )
                    {
                        nbPhases++;
                    }
                    nbTask++;
                }
                nbSection++;
            }

            //print the fifo of each eilck_obj
            //loop over all parallel sections
            for( std::map< ParallelSectionIdentifier, std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>> > >::iterator sectionit = addressProfiling::m_fifoTracker.begin()
                    ; sectionit != addressProfiling::m_fifoTracker.end()
                    ; ++sectionit
                )
            {
                std::ofstream outfile;
                std::stringstream filename;
                filename << "fifo_timestep" << sofa::verticalparallelization::simuInfo::timeStep
                        << "_section_" << std::get<0>(sectionit->first) << "-" << std::get<1>(sectionit->first) << "-" << std::get<2>(sectionit->first) << "-" << std::get<3>(sectionit->first)
                        << ".csv";
                outfile.open ( filename.str(), std::ios::out );
                outfile << "parallel section "
                        << ",object "
                        << ",Task "
                        << ",Phase "
                        << ",nb of handles"
                        << std::endl;
                //loop over all eilck_objects in *sectionit
                for( std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>> >::iterator objit = sectionit->second.begin()
                        ; objit != sectionit->second.end()
                        ; ++objit
                    )
                {
                    outfile << std::get<0>(sectionit->first) << "-" << std::get<1>(sectionit->first) << "-" << std::get<2>(sectionit->first) << "-" << std::get<3>(sectionit->first)
                            << "," << objit->first->m_fifo
                            << "," << objit->first->m_taskId
                            << "," << objit->first->m_phaseId
                            << "," << objit->second.size() << ",";
                    for ( std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>>::iterator handleit = objit->second.begin()
                            ; handleit != objit->second.end()
                            ; ++handleit
                            )
                    {
                        if ( (*handleit).first[0] == 1 )
                        {
                            outfile << "(R-t" << (*handleit).first[1] << "-p" << (*handleit).first[2];
                            outfile <<"),";
                        }
                        else if ( (*handleit).first[0] == 0 )
                        {
                            outfile << "(W-t" << (*handleit).first[1] << "-p" << (*handleit).first[2];
                            outfile <<"),";
                        }
                    }
                    outfile << std::endl;
                }
                outfile.close();
            }
        }
        //print the dependencies between the phases
        if ( sofa::verticalparallelization::simuInfo::timeStep >= sofa::verticalparallelization::addressProfiling::m_loggingTimeStep+2 )
        {
            //for each parallel section
            for( std::map< ParallelSectionIdentifier, std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>> > >::iterator sectionit = addressProfiling::m_fifoTracker.begin()
                    ; sectionit != addressProfiling::m_fifoTracker.end()
                    ; ++sectionit
                )
            {
                std::ofstream outfile;
                std::stringstream filename;
                filename << "fifo_phase_dependencies_timestep" << sofa::verticalparallelization::simuInfo::timeStep
                        << "_section_" << std::get<0>(sectionit->first) << "-" << std::get<1>(sectionit->first) << "-" << std::get<2>(sectionit->first) << "-" << std::get<3>(sectionit->first)
                        << ".csv";
                outfile.open ( filename.str(), std::ios::out );
                outfile << "Task "
                        << ",Phase ";

                int nbTasks = 0;
                std::vector<int> nbPhasesInTask;
                //for each task
                for( std::vector< ParallelTask* >::iterator taskIt = addressProfiling::m_sectionTasks[sectionit->first]->m_tasks.begin()
                        ; taskIt != addressProfiling::m_sectionTasks[sectionit->first]->m_tasks.end()
                        ; taskIt++ )
                {
                    int nbPhases = 0;
                    //for each phase
                    for ( std::vector< ParallelPhase* >::iterator phaseIt = (*taskIt)->m_phases.begin()
                            ; phaseIt != (*taskIt)->m_phases.end()
                            ; phaseIt++ )
                    {
                        outfile << ",T "<< nbTasks << "p" << nbPhases;
                        nbPhases++;
                    }
                    nbPhasesInTask.push_back(nbPhases);
                    nbTasks++;
                }
                outfile << std::endl;
                //for each FIFO in this // section (ie for each ParallelPḧase / eilck object)
                //we write a character 'W' (resp. 'R') in a column
                //if the corresponding Phase has a Write (resp. Read) access on this object
                for( std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>> >::iterator objit = sectionit->second.begin()
                        ; objit != sectionit->second.end()
                        ; ++objit
                    )
                {
                    outfile << objit->first->m_taskId         //task Id
                            << "," << objit->first->m_phaseId;        //phase Id

                    //pour chaque phase de chaque tache, on met un caractere 'W' ou 'R' si la phase apparait dans la FIFO
                    // ou une entree vide sinon
                    unsigned int lastTaskWritten = 1;
                    int lastPhaseWritten = -1;
                    //boucle sur la FIFO correspondant au bloc de vecteur / handle courant
//                    unsigned int currentVecBlock = addressProfiling::m_objTracker[objit->first].second.second;
                    for ( std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>>::iterator handleit = objit->second.begin()
                            ; handleit != objit->second.end()
                            ; ++handleit
                            )
                    {
                        unsigned int currentTask = (*handleit).first[1];
                        unsigned int currentPhase = (*handleit).first[2];
                        while ( lastTaskWritten < currentTask )
                        {
                            //on n'est pas dans la meme tache que l'element precedent dans la FIFO
                            //on met une entree vide pour toutes les phases de la tache precedente n'apparaissant pas dans la FIFO entre la derniere phase qui est apparue et la phase correspondant au handle courant
                            for ( int i=lastPhaseWritten+1; i<nbPhasesInTask[lastTaskWritten]; ++i )
                            {
                                outfile << ", ";
                            }
                            lastTaskWritten++;
                            lastPhaseWritten = -1;
                        }
                        //maintenant on est dans la meme tache que l'element precedent dans la FIFO
                        for ( unsigned int i=lastPhaseWritten+1; i<currentPhase; ++i )
                        {
                            //on met une entree vide pour toutes les phases n'apparaissant pas dans la FIFO entre la derniere phase qui est apparue et la phase correspondant au handle courant
                            outfile << ", ";
                        }
                        //on ecrit un W ou un R pour le handle courant
                        if ( (*handleit).first[0] == 0 )
                        {
                            outfile << ",W";
                        }
                        else if ( (*handleit).first[0] == 1 )
                        {
                            outfile << ",R";
                        }
                        lastPhaseWritten = currentPhase;
                    }
                    outfile << std::endl;
                }
                outfile.close();
            }
        }
#endif //PRINT_FIFOS
    }

    //print fifo on eilck_obj obj in CVS format
    void printFIFO( ParallelPhase* obj)
    {
#ifdef PRINT_FIFOS
        if ( sofa::verticalparallelization::simuInfo::timeStep >= sofa::verticalparallelization::addressProfiling::m_loggingTimeStep+1 )
        {
            std::ofstream outfile;
            std::stringstream filename;
            filename << "fifo_timestep" << sofa::verticalparallelization::simuInfo::timeStep
                    << "_obj" << obj
                    << "_s" << std::get<0>(addressProfiling::m_currentParallelSectionId) << "-" << std::get<1>(addressProfiling::m_currentParallelSectionId) << "-" << std::get<2>(addressProfiling::m_currentParallelSectionId) << "-" << std::get<3>(addressProfiling::m_currentParallelSectionId)
                    << "_T" << currentTask->getId()
                    << "_p" << currentTask->m_currentPhase->getId() <<".csv";
            outfile.open ( filename.str(), std::ios::out );
            outfile << "object "
                    << ",parallel section "
                    << ",vector "
                    << ",block "
                    << ",nb of handles"
                    << std::endl;

            outfile << obj->m_fifo << ","
                    << std::get<0>(obj->m_sectionId) << "-" << std::get<1>(obj->m_sectionId) << "-" << std::get<2>(obj->m_sectionId) << ","
                    << obj->m_taskId << ","
                    << obj->m_phaseId << ","
                    << addressProfiling::m_fifoTracker[obj->m_sectionId][obj].size() << ",";
            for ( std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>>::iterator handleit = addressProfiling::m_fifoTracker[obj->m_sectionId][obj].begin()
                    ; handleit != addressProfiling::m_fifoTracker[obj->m_sectionId][obj].end()
                    ; ++handleit
                    )
            {
                if ( (*handleit).first[0] == 0 )
                {
                    outfile << "(W-t" << (*handleit).first[1] << "-p" << (*handleit).first[2];
                    outfile << "-handle " << (*handleit).second.first << "),";
                }
                else if ( (*handleit).first[0] == 1 )
                {
                    outfile << "(R-t" << (*handleit).first[1] << "-p" << (*handleit).first[2];
                    outfile << "-handle" << (*handleit).second.second << "),";
                }

            }
            outfile << std::endl;
            outfile.close();
        }
#endif //PRINT_FIFOS
    }

}   //namespace verticalparallelization

#endif  //HELPER_MEMORY_HELPER_FUNCTIONS_CPP
