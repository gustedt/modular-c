///
/// \file   memory_parallelization.h
/// \brief  This is the User Interface to the library !!!
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_MEMORY_PARALLELIZATION_H
#define HELPER_MEMORY_PARALLELIZATION_H

#include <assert.h>
#include "ThreadPool.h"
#include "memory_helper_functions.h"
#include "FunctionTask.h"
#include "FunctionTaskVoid.h"

namespace verticalparallelization
{

/// \brief global struct to access the ThreadPool and the tasks being run in parallel in current parallel section
struct threadPool
{
    /// ptr to the ThreadPool used by the library to run Tasks in parallel
    static ThreadPool* m_threadPool;
    /// m_taskCounter is used to count the task already created in the parallel section and to define an identifier for each task
    static unsigned int m_taskCounter;
    /// stores the future defined by the task, so we can wait for completion
    static std::vector< std::future<int> > m_futureVec;
    /// stores a pointer to each task in the parallel section
    static std::vector< verticalparallelization::ThreadTask* > m_taskVec;
};

//
// All calls to a DataVectorWrapper<...>::operator[] between beginParallelSection and endParallelSection
// are considered as a Steps (cf article Europar J.Gustedt, M.Moge)
//


/// \brief return a ptr to current parallel section
BaseParallelSection* getCurrentParallelSection();

/// \brief creates parallel section psid and allows parallel and logging execution for this section by incrementing m_startParallel
template < class ParallelSectionIdentifier >
void startParallelization(ParallelSectionIdentifier psid)
{
    if (  ParallelSection<ParallelSectionIdentifier>::m_existingSections.find(psid)
            == ParallelSection<ParallelSectionIdentifier>::m_existingSections.end() )
    {
        //create a parallel section
        currentSection::m_currentParallelSection = new ParallelSection<ParallelSectionIdentifier>( psid );
    }
    else
    {
        currentSection::m_currentParallelSection = ParallelSection<ParallelSectionIdentifier>::m_existingSections[psid];
    }
    currentSection::m_currentParallelSection->m_startParallel++;
}

/// \brief go back to classical execution, invalidates logging and deletes parallel section psid
template < class ParallelSectionIdentifier >
void stopParallelization(ParallelSectionIdentifier psid)
{
    if (  ParallelSection<ParallelSectionIdentifier>::m_existingSections.find(psid)
            == ParallelSection<ParallelSectionIdentifier>::m_existingSections.end() )
    {
        std::cerr << "ERROR: no // section to delete" << std::endl;
        exit(0);
    }
    else
    {
        currentSection::m_currentParallelSection = ParallelSection<ParallelSectionIdentifier>::m_existingSections[psid];
        currentSection::m_currentParallelSection->m_startParallel = 0;
        delete currentSection::m_currentParallelSection;
    }
}

/// \brief Call this function at the beginning of parallel section psid.
/// Creates parallel section psid if needed, then sets the flags for sequential,
/// logging or parallel execution.
template < class Result, class ParallelSectionIdentifier >
void beginParallelSection(ParallelSectionIdentifier psid)
{
    PRINT_MSG_PARALLEL( "--beginning of parallel section " << std::get<0>(verticalparallelization::addressProfiling::m_currentParallelSectionId) << "-" << std::get<1>(verticalparallelization::addressProfiling::m_currentParallelSectionId) << "-" << std::get<2>(verticalparallelization::addressProfiling::m_currentParallelSectionId) << "-" << std::get<3>(verticalparallelization::addressProfiling::m_currentParallelSectionId) );
    PRINT_LOG_STREAM_TO_FILE( );
    //
    if (  ParallelSection<ParallelSectionIdentifier>::m_existingSections.find(psid)
            == ParallelSection<ParallelSectionIdentifier>::m_existingSections.end() )
    {
        //create a parallel section
        currentSection::m_currentParallelSection = new ParallelSection<ParallelSectionIdentifier>( psid );
    }
    else {
        currentSection::m_currentParallelSection = ParallelSection<ParallelSectionIdentifier>::m_existingSections[psid];
    }
    //
    //
    if (  currentSection::m_currentParallelSection->m_startParallel == 0 )
    {
        //initialization step(s) - sequential - before logging time step
        //normal sequential run
        verticalparallelization::simuInfo::execType = verticalparallelization::ExecType::CLASSICAL;
        threadInfo::m_workerThreadAllowed = 1;
    }
    else if ( currentSection::m_currentParallelSection->m_secondLoggingPhaseDone )
    {
        //parallel execution - this is a parallel time step
        simuInfo::execType = ExecType::PARALLEL;
        //WARNING: we can allow >1 Worker thread only when all parallel sections have been logged completely...
        threadInfo::m_workerThreadAllowed = threadInfo::m_nbWorkerThreads;
    }
    else if ( !currentSection::m_currentParallelSection->m_firstLoggingPhaseDone )
    {
        //first logging phase
        //
        //make sure we use only 1 worker thread for the logging phase
        assert( threadInfo::m_workerThreadAllowed == 1 );
        simuInfo::execType = ExecType::LOGGING;
        threadInfo::m_workerThreadAllowed = 1;

    }
    else // ( !currentSection::m_currentParallelSection->m_secondLoggingPhaseDone )
    {
        //second logging phase
        //
        //make sure we use only 1 worker thread for the logging phase
        assert( threadInfo::m_workerThreadAllowed == 1 );
        simuInfo::execType = ExecType::LOGGING;
        threadInfo::m_workerThreadAllowed = 1;
    }
//
    threadPool::m_taskCounter = 0;
}

/// \brief Should be call after beginParallelSection and before endParallelSection. Creates the task defined by function fn in current parallel section.
/// Parameter res is used for the return value of the function only during logging phase and classical exec.
/// During parallel exec, we get the result in endParallelSection.
/// All values of the parameters of the function fn should be binded using std::bind and std::ref (for reference type parameters)
template < class Result >
void runTask( std::function< Result()>& fn, Result& res )
{
    if ( simuInfo::execType == ExecType::CLASSICAL )
    {
        res = fn();
    }
    else
    {
        threadPool::m_taskVec.push_back( new FunctionTask< Result >
            ( fn, threadPool::m_taskCounter) );
        if ( !currentSection::m_currentParallelSection->m_firstLoggingPhaseDone )
        {   //this is the first logging phase of current parallel section
            getCurrentParallelSection()->addTask( threadPool::m_taskVec.back()->getId() );
        }
        threadPool::m_futureVec.push_back( threadPool::m_threadPool->submit( threadPool::m_taskVec.back() ) );
        threadPool::m_taskCounter++;
        //
        if ( simuInfo::execType == ExecType::LOGGING )
        {
             // logging : the tasks must be executed sequentially, so we wait after each submit (they can run on different threads, but not concurrently)
            int toto = threadPool::m_futureVec.back().get();    //the value of the future is meaningless
            res = *(reinterpret_cast<Result*>(threadPool::m_taskVec.back()->getResult()));       //the value of res is overwritten at each iteration but it was like that in the original code...
        }
    }
}
/// \brief Same as runTask for functions returning void
void runTaskVoid( std::function< void()>& fn )
{
    if ( simuInfo::execType == ExecType::CLASSICAL )
    {
        fn();
    }
    else
    {
        threadPool::m_taskVec.push_back( new FunctionTaskVoid( fn, threadPool::m_taskCounter) );
        if ( !currentSection::m_currentParallelSection->m_firstLoggingPhaseDone )
        {   //this is the first logging phase of current parallel section
            getCurrentParallelSection()->addTask( threadPool::m_taskVec.back()->getId() );
        }
        threadPool::m_futureVec.push_back( threadPool::m_threadPool->submit( threadPool::m_taskVec.back() ) );
        threadPool::m_taskCounter++;
        //
        if ( simuInfo::execType == ExecType::LOGGING )
        {
             // logging : the tasks must be executed sequentially, so we wait after each submit (they can run on different threads, but not concurrently)
            int toto = threadPool::m_futureVec.back().get();    //the value of the future is meaningless
        }
    }
}

/// \brief Call this function at the end of parallel section psid.
/// Wait for the tasks to complete, the sets the flags to resume sequential execution.
/// Remark: parameter res is supposed to be used to retrieve the result of the tasks, but it is not valid (each task writes over the result of the previous one) - we kept it that way because in SOFA this is what was originally done with the results of the tasks...)
template < class Result, class ParallelSectionIdentifier >
void endParallelSection(ParallelSectionIdentifier psid, Result& res )
{
    if ( verticalparallelization::simuInfo::execType == verticalparallelization::ExecType::CLASSICAL )
    {
    }
    else
    {
        if ( simuInfo::execType == ExecType::PARALLEL )
        {
            // wait for the tasks to complete
            for ( unsigned int i=0; i<threadPool::m_futureVec.size(); ++i )
            {
                int toto = threadPool::m_futureVec[i].get();    //the value of the future is meaningless
                res = *(reinterpret_cast<Result*>(threadPool::m_taskVec[i]->getResult()));       //the value of res is overwritten at each iteration but it was like that in the original code (in SOFA)...
            }
        }
        //
        if ( simuInfo::execType == ExecType::LOGGING && !currentSection::m_currentParallelSection->m_firstLoggingPhaseDone )
        {
            currentSection::m_currentParallelSection->m_firstLoggingPhaseDone = true;
        }
        else if ( simuInfo::execType == ExecType::LOGGING && !currentSection::m_currentParallelSection->m_secondLoggingPhaseDone )
        {
            currentSection::m_currentParallelSection->m_secondLoggingPhaseDone = true;
        }
    }
    threadPool::m_futureVec.clear();
    for( size_t i=0; i<threadPool::m_taskVec.size(); ++i )
    {
        delete threadPool::m_taskVec[i];
    }
    threadPool::m_taskVec.clear();
    //
    threadPool::m_taskCounter = 0;
    simuInfo::execType = ExecType::CLASSICAL;
    //
    threadInfo::m_workerThreadAllowed = 1;
}

/// \brief same as endParallelSection for tasks that return void (if class Result is void)
template < class ParallelSectionIdentifier >
void endParallelSectionVoid(ParallelSectionIdentifier psid )
{
    if ( verticalparallelization::simuInfo::execType == verticalparallelization::ExecType::CLASSICAL )
    {
    }
    else
    {
        if ( simuInfo::execType == ExecType::PARALLEL )
        {
            // wait for the tasks to complete
            for ( unsigned int i=0; i<threadPool::m_futureVec.size(); ++i )
            {
                int toto = threadPool::m_futureVec[i].get();    //the value of the future is meaningless
    //            res = *(reinterpret_cast<Result*>(taskvec[i]->getResult()));       //the value of res is overwritten at each iteration but it was like that in the original code (in SOFA)...
            }
        }
        //
        if ( simuInfo::execType == ExecType::LOGGING && !currentSection::m_currentParallelSection->m_firstLoggingPhaseDone )
        {
            currentSection::m_currentParallelSection->m_firstLoggingPhaseDone = true;
        }
        else if ( simuInfo::execType == ExecType::LOGGING && !currentSection::m_currentParallelSection->m_secondLoggingPhaseDone )
        {
            currentSection::m_currentParallelSection->m_secondLoggingPhaseDone = true;
        }
    }
    threadPool::m_futureVec.clear();
    for( size_t i=0; i<threadPool::m_taskVec.size(); ++i )
    {
        delete threadPool::m_taskVec[i];
    }
    threadPool::m_taskVec.clear();
    //
    threadPool::m_taskCounter = 0;
    simuInfo::execType = ExecType::CLASSICAL;
}

}   //namespace verticalparallelization

#endif  //HELPER_MEMORY_PARALLELIZATION_H
