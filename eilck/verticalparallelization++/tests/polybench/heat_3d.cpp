/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* heat-3d.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>

/* Include polybench common header. */
#include "polybench_cpp.h"

/* Include benchmark-specific header. */
#include "heat_3d.h"

#include <vector>
#include <sys/time.h>

struct timeval m_loggingTime = {0,0};
struct timeval m_totalComputeTime = {0,0};
static bool loggingtime = false;

/* Array initialization. */
static
void init_array (int n,
		 std::vector< std::vector< std::vector<DATA_TYPE> > >& A,
		 std::vector< std::vector< std::vector<DATA_TYPE> > >& B)
{
  int i, j, k;

  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      for (k = 0; k < n; k++)
        A[i][j][k] = B[i][j][k] = (DATA_TYPE) (i + j + (n-k))* 10 / (n);
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int n,
		 std::vector< std::vector< std::vector<DATA_TYPE> > >& A)

{
  int i, j, k;

  POLYBENCH_DUMP_START;
  POLYBENCH_DUMP_BEGIN("A");
  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      for (k = 0; k < n; k++) {
         if ((i * n * n + j * n + k) % 20 == 0) fprintf(POLYBENCH_DUMP_TARGET, "\n");
         fprintf(POLYBENCH_DUMP_TARGET, DATA_PRINTF_MODIFIER, A[i][j][k]);
      }
  POLYBENCH_DUMP_END("A");
  POLYBENCH_DUMP_FINISH;
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_heat_3d(int tsteps,
		      int n,
		      std::vector< std::vector< std::vector<DATA_TYPE> > >& A,
		      std::vector< std::vector< std::vector<DATA_TYPE> > >& B)
{
std::cout << "nb iterations = " << TSTEPS << std::endl;
    for (int t = 1; t <= TSTEPS; t++) {
    struct  timeval c_start;
    struct  timeval c_end;
    gettimeofday(&c_start,NULL);
    
        for (int i = 1; i < PB_N-1; i++) {
            for (int j = 1; j < PB_N-1; j++) {
                for (int k = 1; k < PB_N-1; k++) {
                    B[i][j][k] =   SCALAR_VAL(0.125) * (A[i+1][j][k] - SCALAR_VAL(2.0) * A[i][j][k] + A[i-1][j][k])
                                 + SCALAR_VAL(0.125) * (A[i][j+1][k] - SCALAR_VAL(2.0) * A[i][j][k] + A[i][j-1][k])
                                 + SCALAR_VAL(0.125) * (A[i][j][k+1] - SCALAR_VAL(2.0) * A[i][j][k] + A[i][j][k-1])
                                 + A[i][j][k];
                }
            }
        }
        for (int i = 1; i < PB_N-1; i++) {
           for (int j = 1; j < PB_N-1; j++) {
               for (int k = 1; k < PB_N-1; k++) {
                   A[i][j][k] =   SCALAR_VAL(0.125) * (B[i+1][j][k] - SCALAR_VAL(2.0) * B[i][j][k] + B[i-1][j][k])
                                + SCALAR_VAL(0.125) * (B[i][j+1][k] - SCALAR_VAL(2.0) * B[i][j][k] + B[i][j-1][k])
                                + SCALAR_VAL(0.125) * (B[i][j][k+1] - SCALAR_VAL(2.0) * B[i][j][k] + B[i][j][k-1])
                                + B[i][j][k];
               }
           }
       }
    gettimeofday(&c_end,NULL);
    timersub( &c_end, &c_start, &c_end );
    if( loggingtime )
    {
        timeradd( &m_loggingTime, &c_end, &m_loggingTime );
    }
    else
    {
        timeradd( &m_totalComputeTime, &c_end, &m_totalComputeTime );
    }
    
    }
}


int main(int argc, char** argv)
{
  /* Retrieve problem size. */
  int n = N;
  int tsteps = TSTEPS;

  /* Variable declaration/allocation. */
  std::vector< std::vector< std::vector<DATA_TYPE> > > A( n, std::vector<std::vector<DATA_TYPE> >( n, std::vector<DATA_TYPE>(n) ) );
  std::vector< std::vector< std::vector<DATA_TYPE> > > B( n, std::vector<std::vector<DATA_TYPE> >( n, std::vector<DATA_TYPE>(n) ) );


  /* Initialize array(s). */
  init_array (n, A, B);

  /* Start timer. */
//    struct  timeval c_start;
//    struct  timeval c_end;
//    gettimeofday(&c_start,NULL);
  polybench_start_instruments;

  /* Run kernel. */
  kernel_heat_3d (tsteps, n, A, B);

  /* Stop and print timer. */
//  gettimeofday(&c_end,NULL);
//  timersub( &c_end, &c_start, &c_end );
//  std::cout << "total time = " << c_end.tv_sec << " s. + "
//                << c_end.tv_usec << " µs." << std::endl;
  polybench_stop_instruments;
  polybench_print_instruments;
  std::cout << "logging time = " << m_loggingTime.tv_sec << "."
                << (m_loggingTime.tv_usec%1000)*1000 << " s." << std::endl;
  std::cout << "total compute time = " << m_totalComputeTime.tv_sec << "."
                << (m_totalComputeTime.tv_usec%1000)*1000 << "s." << std::endl;

  /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
  polybench_prevent_dce(print_array(n, A));

  /* Be clean. */
//  POLYBENCH_FREE_ARRAY(A);

  return 0;
}
