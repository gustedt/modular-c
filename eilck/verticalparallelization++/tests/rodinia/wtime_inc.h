#include <time.h>

inline
double wtime(void) {
  struct timespec ts;
  timespec_get(&ts, TIME_UTC);
  return ts.tv_sec + ts.tv_nsec * 1.0e-9;
}
