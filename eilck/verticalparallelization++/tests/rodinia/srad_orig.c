// srad_orig.c: Defines the entry point for the console application.
//
// This is a cleaned up version of the original srad.cpp, that was C++
// with no reason:
//
// - make all variables as local as possible
// - chase all useless casts (all were useless)
// - change integer types mainly to size_t, there was possible
//   overflow because of the use of just int
// - force output of the result matrix for possible verification of correctness
//   and use a file name for that
//
// run as
// srad_orig 128 128 0 31 0 31 0.5 2

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void random_matrix(float *I, size_t rows, size_t cols);

void usage(int argc, char **argv) {
  fprintf(stderr, "Usage: %s <rows> <cols> <y1> <y2> <x1> <x2> <lamda> <no. of iter>\n", argv[0]);
  fprintf(stderr, "\t<rows>\t- number of rows\n");
  fprintf(stderr, "\t<cols>\t- number of cols\n");
  fprintf(stderr, "\t<y1>\t- y1 value of the speckle\n");
  fprintf(stderr, "\t<y2>\t- y2 value of the speckle\n");
  fprintf(stderr, "\t<x1>\t- x1 value of the speckle\n");
  fprintf(stderr, "\t<x2>\t- x2 value of the speckle\n");
  fprintf(stderr, "\t<lamda>\t- lambda (0,1)\n");
  fprintf(stderr, "\t<no. of iter>\t- number of iterations\n");

  exit(EXIT_FAILURE);
}

int main(int argc, char* argv[]) {
  if (argc != 9) {
    usage(argc, argv);
  }
  size_t rows = strtoull(argv[1], 0, 0); //number of rows in the domain
  size_t cols = strtoull(argv[2], 0, 0); //number of cols in the domain
  if (rows%16 || cols%16) {
    fprintf(stderr, "rows and cols must be multiples of 16\n");
    return EXIT_FAILURE;
  }
  size_t r1   = strtoull(argv[3], 0, 0); //y1 position of the speckle
  size_t r2   = strtoull(argv[4], 0, 0); //y2 position of the speckle
  size_t c1   = strtoull(argv[5], 0, 0); //x1 position of the speckle
  size_t c2   = strtoull(argv[6], 0, 0); //x2 position of the speckle
  float lambda = strtod(argv[7], 0);       //Lambda value
  size_t niter = strtoull(argv[8], 0, 0); //number of iterations

  size_t size_I = cols * rows;
  size_t size_R = (r2-r1+1)*(c2-c1+1);

  float* I = malloc(sizeof(*I)*size_I);
  float* J = malloc(sizeof(*J)*size_I);
  float* c = malloc(sizeof(*c)*size_I);

  // These are kept "unsigned" to save space
  unsigned* iN = malloc(sizeof(*iN)*rows);
  unsigned* iS = malloc(sizeof(*iS)*rows);
  unsigned* jW = malloc(sizeof(*jW)*cols);
  unsigned* jE = malloc(sizeof(*jE)*cols);


  float* dN = malloc(sizeof(*dN)*size_I);
  float* dS = malloc(sizeof(*dS)*size_I);
  float* dW = malloc(sizeof(*dW)*size_I);
  float* dE = malloc(sizeof(*dE)*size_I);


  for (size_t i=0; i< rows; i++) {
    iN[i] = i-1;
    iS[i] = i+1;
  }
  for (size_t j=0; j< cols; j++) {
    jW[j] = j-1;
    jE[j] = j+1;
  }
  iN[0]    = 0;
  iS[rows-1] = rows-1;
  jW[0]    = 0;
  jE[cols-1] = cols-1;

  printf("Randomizing the input matrix\n");

  random_matrix(I, rows, cols);

  for (size_t k = 0; k < size_I; k++) {
    J[k] = expf(I[k]);
  }

  printf("Start the SRAD main loop\n");

  for (size_t iter=0; iter< niter; iter++) {
    float sum=0;
    float sum2=0;
    for (size_t i=r1; i<=r2; i++) {
      for (size_t j=c1; j<=c2; j++) {
        float tmp   = J[i * cols + j];
        sum  += tmp;
        sum2 += tmp*tmp;
      }
    }
    float meanROI = sum / size_R;
    float varROI  = (sum2 / size_R) - meanROI*meanROI;
    float q0sqr   = varROI / (meanROI*meanROI);


    for (size_t i = 0; i < rows; i++) {
      for (size_t j = 0; j < cols; j++) {

        size_t k = i * cols + j;
        float Jc = J[k];

        // directional derivates
        dN[k] = J[iN[i] * cols + j] - Jc;
        dS[k] = J[iS[i] * cols + j] - Jc;
        dW[k] = J[i * cols + jW[j]] - Jc;
        dE[k] = J[i * cols + jE[j]] - Jc;

        float G2 = (dN[k]*dN[k] + dS[k]*dS[k]
              + dW[k]*dW[k] + dE[k]*dE[k]) / (Jc*Jc);

        float L = (dN[k] + dS[k] + dW[k] + dE[k]) / Jc;

        float num  = (0.5*G2) - ((1.0/16.0)*(L*L));
        float den  = 1 + (.25*L);
        float qsqr = num/(den*den);

        // diffusion coefficent (equ 33)
        den = (qsqr-q0sqr) / (q0sqr * (1+q0sqr));
        c[k] = 1.0 / (1.0+den);

        // saturate diffusion coefficent
        if (c[k] < 0) {c[k] = 0;}
        else if (c[k] > 1) {c[k] = 1;}

      }

    }

    for (size_t i = 0; i < rows; i++) {
      for (size_t j = 0; j < cols; j++) {

        // current index
        size_t k = i * cols + j;

        // diffusion coefficent
        float cN = c[k];
        float cS = c[iS[i] * cols + j];
        float cW = c[k];
        float cE = c[i * cols + jE[j]];

        // divergence (equ 58)
        float D = cN * dN[k] + cS * dS[k] + cW * dW[k] + cE * dE[k];

        // image update (equ 61)
        J[k] = J[k] + 0.25*lambda*D;
      }
    }
  }

  char oname[256];
  snprintf(oname, 256, "result%zux%zu.txt", rows, cols);
  FILE* out = fopen(oname, "w");
  for(size_t i = 0; i < rows; i++) {
    for (size_t j = 0; j < cols; j++) {
      fprintf(out, "%a\t", J[i * cols + j]);
    }
    fputc('\n', out);
  }
  fclose(out);

  printf("Computation Done\n");

  free(I);
  free(J);
  free(iN); free(iS); free(jW); free(jE);
  free(dN); free(dS); free(dW); free(dE);

  free(c);
  return EXIT_SUCCESS;
}

void random_matrix(float *I, size_t rows, size_t cols) {
  srand(7);
  for(size_t i = 0; i < rows; i++) {
    for (size_t j = 0; j < cols; j++) {
      I[i * cols + j] = rand()/(float)RAND_MAX;
    }
  }
}

