// INSTALL
install Eilck
compile with C++11


// USAGE
1 - Identify a section of code with multiple tasks you want to execute in parallel.
A Task corresponds to a function call, so you should encapsulate the tasks in functions.
WARNING: The return value of the task is not valid at the moment...

2 - Identify the data vectors/arrays that are shared between the tasks, and wrap them using DataWrapper so that the operator[] is overloaded to track memory accesses and manage locks.
WARNING: when your data is a multidimensional array (e.g. a matrix), overloading the operator[] on the "innermost" dimension can least to degraded performances. 
         Overloading a "outer" dimension can lead to better performances (see example rodinia/hotspot3D_para.cpp, we wrap the 2nd dimension of a 3-dimensional matrix).
         
3 - Define a unique identifier for your parallel section.
call startParallelization and beginParallelSection to get the ParallelSection object and set the flags to enable parallelization.
WARNING: if there are initialization steps in your program before the memory access become constant accross iterations, 
         call startParallelization only after the initialization iterations are done.
         
4 - Bind the values of the functions/Tasks parameters, using std::ref for reference parameters, to get a function of type Result(void).
Call runTask() to add the Task to the ThreadPool and start its execution.

5 - Call endParallelSection to resume sequential execution at the end of the parallel section.



WARNING : the size of the meta-steps/phases is set arbitrarily in memory_var.cpp. It needs to be tuned for each application, and the lib recompiled.





// SOFAPLUGIN
In directory sofaplugin are the file needed to used libverticalparallelization in Sofa.
It consists in 
1 - a plugin ParallelVectors
    It defines a new vector type (ParallelVec3d, etc.) that is just a wrapper around Vec3d with operator[] overloaded (using libverticalparallelization::DataWrapper) 
    + the instanciation of all necessary classes.
2 - a slightly modified version of Simulation.cpp (creation of the ThreadPool, set flags to enable parallel execution when initialization steps are done)
    and Visitor.h (function for_each_r_para to execute the visitor on multiple components in parallel using libverticalparallelization)

Remark: Sofa should be compiled using -std=c++14
