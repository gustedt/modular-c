/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module ihdl  =
#pragma CMOD separator  ∷
#pragma CMOD composer   —
#pragma CMOD import ehdl  = ..∷ehdl
#pragma CMOD import obj = ..∷obj

/**
 ** @module
 ** @brief inclusive access to a lock object
 **/

/**
 ** @struct eilck::ihdl
 ** @brief inclusive access to a lock object
 **/

/**
 ** @defgroup eilck::ihdl::single
 ** @{*/

/** @}*/

/**
 ** @defgroup eilck::ihdl::pairs
 ** @{*/

/** @}*/


#pragma CMOD declaration

#pragma CMOD import impl  = ..∷impl∷snippet
#pragma CMOD fill impl  = ihdl
#pragma CMOD fill impl∷TYPE = eilck∷readHdl
