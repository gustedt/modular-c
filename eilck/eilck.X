/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD import OS  = Linux
#pragma CMOD import m = OS∷m

/**
 ** @module
 ** @brief A library for Exclusive Inclusive Linear loCKs.
 ** @code{.unparsed}
   \ _/\_/            _ _      _             __/\__
    )   (_           (_) |    | |           _) :  (
   ~~)   (_       ___ _| | ___| | __      _)  : __(
      )   (_     / _ \ | |/ __| |/ /    _)    _(
      )    (    |  __/ | | (__|   <    )     (
       `-.  (_   \___|_|_|\___|_|\_\   )     (~~
         /__/                          )__  _(
        /   \                         //  || \\_
 ** @endcode
 **
 ** This is a standalone implementation of ordered read-write locks,
 ** implemented with _Modular C_, that is interfaced with plain C and
 ** C++. As such this is an example of developing with the comfort of
 ** _Modular C_ but remaining compatible with traditional programming
 ** interfaces.
 **
 ** Ordered read-write locks are locks that have the following
 ** properties:
 **
 ** - Lock objects (eilck::obj) are not accessed directly, but through
 **   handles, here eilck::ehdl and eilck::ihdl.

 ** - They know three major events (instead of just two), require, acquire
 **   and release.

 ** - A handle can be typed for exclusive (write) access, eilck::ehdl,
 **   or inclusive (read) access, eilck::ihdl.

 ** - Lock acquisition is attributed to the handle, not the
 **   thread. That is, e.g one thread can request access for a whole
 **   bunch of handles, and other threads can then use these handles
 **   to synchronize.

 ** - Lock acquisition occurs according to a FIFO, that is filled
 **   according to request ordering. In that FIFO, all inclusive
 **   requests are pushed together and granted simultaneously.

 ** - Each lock object represents an abstract data object, that can be
 **   scaled (similar to C::lib::realloc) and mapped into the address space
 **   of a thread, once the lock is acquired. For an exclusive lock
 **   the type is <code>void*</code>, for an inclusive lock it is
 **   <code>void volatile*</code>. That is, the object is only
 **   writable when an exclusive lock is acquired.

 ** - Iterative lock access is easily modeled by regulating the access
 **   through two handles that alternate their roles. If the second
 **   handle requires insertion in the FIFO just before the first
 **   releases the lock, the access is cyclic in a guaranteed
 **   order. All is put in place such that such a pair can simply be
 **   declared as <code>eilck::ehdl[2]</code> or
 **   <code>eilck::ihdl[2]</code>.

 ** eilck is a specialized implementation of that setting:

 ** - It does not need a run-time or special launcher.

 ** - It is Linux only. The main reason for that is the use of Linux'
 **   @c futex interface for efficient locking. It should be
 **   relatively straight forward to port eilck to other architectures
 **   by emulating this interface.

 ** - It only works for inter thread locking, not for processes or in
 **   a distributed setting.

 ** Handles should always be allocated as one of the two types, but
 ** later they may be accessed through type generic interfaces in the
 ** @doxy eilck::hdl module.
 **/

#pragma CMOD declaration

#pragma CMOD constant FAILED  = m∷FAILED
#pragma CMOD constant R   = m∷READ
#pragma CMOD constant RW  = (R ∪ W)
#pragma CMOD constant RWX = (R ∪ W ∪ X)
#pragma CMOD constant RX  = (R ∪ X)
#pragma CMOD constant W   = m∷WRITE
#pragma CMOD constant X   = m∷EXEC

enum { min = 5, len = 1 ⪡ 10, };
// Dummy types for the generic interfaces.

typedef void writeHdl;
typedef void const readHdl;
struct voidBase {
  unsigned char x;
};

#ifndef __cplusplus
#define exclusive(H)                                           \
_Generic((H),                                                  \
         void const*: false,                                   \
         void const volatile*: false,                          \
 default: true)
#else
#define exclusive __excl
#endif

static_assert(sizeof(voidBase) ≡ 1, "void base must be 1");

