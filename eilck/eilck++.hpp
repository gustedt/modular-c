// This may look like C code, but it really is -*- mode: c++; coding: utf-8 -*-
//
// Except for parts copied from previous work and as explicitly stated below,
// the author and copyright holder for this work is
// Copyright (c) 2017 Jens Gustedt, INRIA, France
//
// This file is free software; it is part of the Modular C project.
//
// You can redistribute it and/or modify it under the terms of the BSD
// License as given in the file LICENSE.txt. It is distributed without
// any warranty; without even the implied warranty of merchantability or
// fitness for a particular purpose.
//

#ifndef eilck_hpp_GUARD
#define eilck_hpp_GUARD 1
#ifdef __GNUC__
#pragma interface
#endif

#include "C++.hpp"

template< typename T> constexpr bool __excl(T* h) {
  return !(typeid(h->type) == typeid(void const*) || typeid(h->type) == typeid(void const volatile*));
}

#define Linux_m_FAILED ((void*)-1)

#include "eilck.h"
#include "eilck-state.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-fixed.h"
#include "eilck-obj-file.h"
#include "eilck-obj-segment.h"
#include "eilck-obj-scale.h"
#include "eilck-ehdl.h"
#include "eilck-ihdl.h"

#define EILCK_HDL_SECTION(H)                                           \
    for (register bool _Intern_eilck_hdl_after = 0;                    \
            ! _Intern_eilck_hdl_after;                                 \
        )                                                              \
      /* volatile is needed because of a gcc bug that optimizes too */ \
      /* agressively.                                               */ \
        for (register auto*volatile _Intern_eilck_hdl_ptr = &(H);      \
                ! _Intern_eilck_hdl_after;                             \
            )                                                          \
          for (eilck::hdl::acq(*_Intern_eilck_hdl_ptr);                \
                    ! _Intern_eilck_hdl_after;                         \
                    (_Intern_eilck_hdl_after = 1,                      \
                     eilck::hdl::rel(*_Intern_eilck_hdl_ptr)))

#define EILCK_HDL_SEQUENCE_11(O,                               \
        _0, _1, _2, _3, _4, _5, _6, _7, _8, _9)                \
do {                                                           \
    eilck::hdl::req((O), (_0));                                \
    eilck::hdl::req((O), (_1));                                \
    eilck::hdl::req((O), (_2));                                \
    eilck::hdl::req((O), (_3));                                \
    eilck::hdl::req((O), (_4));                                \
    eilck::hdl::req((O), (_5));                                \
    eilck::hdl::req((O), (_6));                                \
    eilck::hdl::req((O), (_7));                                \
    eilck::hdl::req((O), (_8));                                \
    eilck::hdl::req((O), (_9));                                \
}                                                              \
while (0)

#define EILCK_HDL_SEQUENCE_10(O,                                 \
        _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, ...)             \
EILCK_HDL_SEQUENCE_11(O, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9)

#define EILCK_HDL_SEQUENCE(O, ...)                                    \
    EILCK_HDL_SEQUENCE_10(O, __VA_ARGS__, 0, 0, 0, 0, 0, 0, 0, 0, 0,)

#define EILCK_HDL_FAIL(X, N) eilck::hdl::fail(eilck::hdl::test(X), N)

namespace eilck {

  struct exception : std::runtime_error {
    inline exception(const char* s) : std::runtime_error(s) { }
  };

  /**
   ** @brief Generic interfaces for handles to objects.
   **/
  namespace hdl {

    inline int qere(eilck_obj& ob, eilck_ehdl& h) {
      return eilck_ehdl_req(&h, &ob);
    };

    inline int qeri(eilck_obj& ob, eilck_ihdl& h) {
      return eilck_ihdl_req(&h, &ob);
    };

    inline int req(eilck_ehdl& a1, eilck_obj& a2) {
      return eilck_ehdl_req(&a1, &a2);
    };

    inline int req(eilck_obj& a1, eilck_ihdl& a2) {
      return qeri(a1, a2);
    };

    inline int req(eilck_obj& a1, eilck_ehdl& a2) {
      return qere(a1, a2);
    };

    inline int req(eilck_ihdl& a1, eilck_obj& a2) {
      return eilck_ihdl_req(&a1, &a2);
    };

    inline eilck_ehdl const& chain(eilck_ehdl const& s, eilck_ehdl& l) {
      return *eilck_ehdl_chain(&s, &l);
    };

    inline eilck_ihdl const& chain(eilck_ihdl const& s, eilck_ihdl& l) {
      return *eilck_ihdl_chain(&s, &l);
    };

    inline void drop(eilck_ehdl& x) {
      eilck_ehdl_drop(&x);
    };

    inline void drop(eilck_ihdl(&x)[2]) {
      eilck_ihdl_drop_2(&x[0], &x[1]);
    };

    inline void drop(eilck_ehdl(&x)[2]) {
      eilck_ehdl_drop_2(&x[0], &x[1]);
    };

    inline void drop(eilck_ihdl& x) {
      eilck_ihdl_drop(&x);
    };

    inline void rel(eilck_ehdl& x) {
      eilck_ehdl_rel(&x);
    };

    inline void rel(eilck_ihdl(&x)[2]) {
      eilck_ihdl_rel_2(&x[0], &x[1]);
    };

    inline void rel(eilck_ehdl(&x)[2]) {
      eilck_ehdl_rel_2(&x[0], &x[1]);
    };

    inline void rel(eilck_ihdl& x) {
      eilck_ihdl_rel(&x);
    };

    inline bool test(eilck_ehdl& x) {
      return eilck_ehdl_test(&x);
    };

    inline bool test(eilck_ihdl(&x)[2]) {
      return eilck_ihdl_test_2(&x[0], &x[1]);
    };

    inline bool test(eilck_ehdl(&x)[2]) {
      return eilck_ehdl_test_2(&x[0], &x[1]);
    };

    inline bool test(eilck_ihdl& x) {
      return eilck_ihdl_test(&x);
    };

    inline int acq(eilck_ehdl& x) {
      return eilck_ehdl_acq(&x);
    };

    inline int acq(eilck_ihdl(&x)[2]) {
      return eilck_ihdl_acq_2(&x[0], &x[1]);
    };

    inline int acq(eilck_ehdl(&x)[2]) {
      return eilck_ehdl_acq_2(&x[0], &x[1]);
    };

    inline int acq(eilck_ihdl& x) {
      return eilck_ihdl_acq(&x);
    };

    [[noreturn]]
    inline void fail(bool acquired, char const* name) {
      std::string Name(name);
      if (acquired)
        Name += ": object could not be allocated";
      else
        Name += ": handle not acquired";
      throw exception(Name.data());
    }

    inline void* map(eilck_ehdl& x) {
      auto ret = eilck_ehdl_map(&x);
      if (ret) return ret;
      else EILCK_HDL_FAIL(x, "eilck_ehdl_map");
    };

    inline void const* map(eilck_ihdl(&x)[2]) {
      auto ret = eilck_ihdl_map_2(&x[0], &x[1]);
      if (ret) return ret;
      else EILCK_HDL_FAIL(x, "eilck_ihdl_map");
    };

    inline void* map(eilck_ehdl(&x)[2]) {
      auto ret = eilck_ehdl_map_2(&x[0], &x[1]);
      if (ret) return ret;
      else EILCK_HDL_FAIL(x, "eilck_ehdl_map2");
    };

    inline void const* map(eilck_ihdl& x) {
      auto ret = eilck_ihdl_map(&x);
      if (ret) return ret;
      else EILCK_HDL_FAIL(x, "eilck_ihdl_map2");
    };

    inline void* scale(eilck_ehdl& x, size_t size) {
      auto ret = eilck_ehdl_scale(&x, size, 0);
      if (ret && size) return ret;
      else EILCK_HDL_FAIL(x, "eilck_ehdl_scale");
    };

  }

  template < typename T > struct ehdl;
  template < typename T > struct ihdl;

  /**
   ** @brief A typed version of the Modular C object type @c obj.
   **
   ** This type by itself can only be used through ::ehdl<T> and
   ** ::ihdl<T>. Besides that it can only be constructed and
   ** destroyed.
   **/
  template < typename T >
  struct obj : private eilck_obj {
    friend struct ehdl< T >;
    friend struct ihdl< T >;
    inline obj(size_t len = eilck_len, size_t size = 1, eilck_obj_scaler* al = 0, void* cntx = 0) : eilck_obj() {
      eilck_obj_init(this, len);
      this->alloc = al;
      eilck_obj_scale(this, size*sizeof(T), cntx);
    }
    inline ~obj(void) {
      eilck_obj_scale(this, eilck_obj_scaler_CLOSE, 0);
      eilck_obj_destroy(this);
    }
    inline size_t length(void) { return eilck_obj_length(this)/sizeof(T); }
  };

  namespace hdl {

    template < typename T > inline int acq(ehdl<T> (&x)[2]);
    template < typename T > inline void rel(ehdl<T> (&x)[2]);
    template < typename T > inline bool test(ehdl<T> (&x)[2]);
    template < typename T > inline void drop(ehdl<T> (&x)[2]);
    template < typename T > inline size_t length(ehdl<T> (&x)[2]);
    template < typename T > inline T* map(ehdl<T> (&x)[2]);
    template < typename T > inline int req(ehdl<T> (&x)[2], obj<T>& ob);

  }
  /**
   ** @brief A typed version of the Modular C object type @c ehdl.
   **/
  template < typename T >
  struct ehdl : private eilck_ehdl {
    inline int acq(void) { return hdl::acq(*this); }
    inline void rel(void) { return hdl::rel(*this); }
    inline bool test(void) { return hdl::test(*this); }
    inline void drop(void) { return hdl::drop(*this); }
    inline T* map(void) { return static_cast< T* >(hdl::map(*this)); }
    inline T& operator[](size_t i) { return this->map()[i]; }
    inline int req(obj<T>& ob) { return hdl::req(ob, *this); }
    inline ehdl const& chain(ehdl& later) const { hdl::chain(*this, later); return *this; }
    inline T* scale(size_t size) { return static_cast< T* >(hdl::scale(*this, size*sizeof(T))); }
    inline size_t length(void) { return eilck_ehdl_length(this)/sizeof(T); }
    inline ehdl(obj<T>& ob) : eilck_ehdl() { this->req(ob); }
    inline ehdl(void) : eilck_ehdl() { }
    friend int eilck::hdl::acq< T >(ehdl<T> (&x)[2]);
    friend void hdl::rel< T >(ehdl<T> (&x)[2]);
    friend bool hdl::test< T >(ehdl<T> (&x)[2]);
    friend void hdl::drop< T >(ehdl<T> (&x)[2]);
    friend size_t hdl::length< T >(ehdl<T> (&x)[2]);
    friend T* hdl::map< T >(ehdl<T> (&x)[2]);
    friend int hdl::req< T >(ehdl<T> (&x)[2], obj<T>& ob);
  };

  namespace hdl {

    template < typename T >
    inline int acq(ehdl<T>& x) { return x.acq(); }
    template < typename T >
    inline void rel(ehdl<T>& x) { return x.rel(); }
    template < typename T >
    inline bool test(ehdl<T>& x) { return x.test(); }
    template < typename T >
    inline void drop(ehdl<T>& x) { return x.drop(); }
    template < typename T >
    inline size_t length(ehdl<T>& x) { return x.length(); }
    template < typename T >
    inline T* map(ehdl<T>& x) { return x.map(); }
    template < typename T >
    inline int req(obj<T>& ob, ehdl<T>& x) { return x.req(ob); }
    template < typename T >
    inline ehdl<T> const& chain(ehdl<T> const& x, ehdl<T>& later) { return x.chain(later); }


    template < typename T >
    inline int acq(ehdl<T> (&x)[2]) { return eilck_ehdl_acq_2(&x[0], &x[1]); }
    template < typename T >
    inline void rel(ehdl<T> (&x)[2]) { return eilck_ehdl_rel_2(&x[0], &x[1]); }
    template < typename T >
    inline bool test(ehdl<T> (&x)[2]) { return eilck_ehdl_test_2(&x[0], &x[1]); }
    template < typename T >
    inline void drop(ehdl<T> (&x)[2]) { return eilck_ehdl_drop_2(&x[0], &x[1]); }
    template < typename T >
    inline size_t length(ehdl<T> (&x)[2]) { return eilck_ehdl_length_2(&x[0], &x[1])/sizeof(T); }
    template < typename T >
    inline T* map(ehdl<T> (&x)[2]) { return (T*)eilck_ehdl_map_2(&x[0], &x[1]); }
    template < typename T >
    inline int req(ehdl<T> (&x)[2], obj<T>& ob) { return eilck_ehdl_acq_2(&x[0], &x[1], &ob); }

  }

  namespace hdl {

    template < typename T > inline int acq(ihdl<T> (&x)[2]);
    template < typename T > inline void rel(ihdl<T> (&x)[2]);
    template < typename T > inline bool test(ihdl<T> (&x)[2]);
    template < typename T > inline void drop(ihdl<T> (&x)[2]);
    template < typename T > inline size_t length(ihdl<T> (&x)[2]);
    template < typename T > inline T const* map(ihdl<T> (&x)[2]);
    template < typename T > inline int req(ihdl<T> (&x)[2], obj<T>& ob);

  }
  /**
   ** @brief A typed version of the Modular C object type @c ihdl.
   **/
  template < typename T >
  struct ihdl : private eilck_ihdl {
    inline int acq(void) { return hdl::acq(*this); }
    inline void rel(void) { return hdl::rel(*this); }
    inline bool test(void) { return hdl::test(*this); }
    inline void drop(void) { return hdl::drop(*this); }
    inline T const* map(void) { return static_cast< T const* >(hdl::map(*this)); }
    inline T const& operator[](size_t i) { return this->map()[i]; }
    inline int req(obj<T>& ob) { return hdl::req(ob, *this); }
    inline ihdl const& chain(ihdl& later) const { hdl::chain(*this, later); return *this; }
    inline size_t length(void) { return eilck_ihdl_length(this)/sizeof(T); }
    inline ihdl(obj<T>& ob) : eilck_ihdl() { this->req(ob); }
    inline ihdl(void) : eilck_ihdl() { }
    friend int hdl::acq< T >(ihdl<T> (&x)[2]);
    friend void hdl::rel< T >(ihdl<T> (&x)[2]);
    friend bool hdl::test< T >(ihdl<T> (&x)[2]);
    friend void hdl::drop< T >(ihdl<T> (&x)[2]);
    friend size_t hdl::length< T >(ihdl<T> (&x)[2]);
    friend T const* hdl::map< T >(ihdl<T> (&x)[2]);
    friend int hdl::req< T >(ihdl<T> (&x)[2], obj<T>& ob);
  };


  namespace hdl {

    template < typename T >
    inline int acq(ihdl<T>& x) { return x.acq(); }
    template < typename T >
    inline void rel(ihdl<T>& x) { x.rel(); }
    template < typename T >
    inline bool test(ihdl<T>& x) { return x.test(); }
    template < typename T >
    inline void drop(ihdl<T>& x) { x.drop(); }
    template < typename T >
    inline size_t length(ihdl<T>& x) { return x.length(); }
    template < typename T >
    inline T const* map(ihdl<T>& x) { return x.map(); }
    template < typename T >
    inline int req(obj<T>& ob, ihdl<T>& x) { return x.req(ob); }
    template < typename T >
    inline ihdl<T> const& chain(ihdl<T> const& x, ihdl<T>& later) { return x.chain(later); }


    template < typename T >
    inline int acq(ihdl<T> (&x)[2]) { return eilck_ihdl_acq_2(&x[0], &x[1]); }
    template < typename T >
    inline void rel(ihdl<T> (&x)[2]) { eilck_ihdl_rel_2(&x[0], &x[1]); }
    template < typename T >
    inline bool test(ihdl<T> (&x)[2]) { return eilck_ihdl_test_2(&x[0], &x[1]); }
    template < typename T >
    inline void drop(ihdl<T> (&x)[2]) { eilck_ihdl_drop_2(&x[0], &x[1]); }
    template < typename T >
    inline size_t length(ihdl<T> (&x)[2]) { return eilck_ihdl_length_2(&x[0], &x[1])/sizeof(T); }
    template < typename T >
    inline T const* map(ihdl<T> (&x)[2]) { return (T const*)eilck_ihdl_map_2(&x[0], &x[1]); }
    template < typename T >
    inline int req(ihdl<T> (&x)[2], obj<T>& ob) { return eilck_ihdl_req_2(&x[0], &x[1], &ob); }

  }
}

#endif
