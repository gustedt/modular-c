/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module state =
#pragma CMOD separator  ∷
#pragma CMOD composer —
#pragma CMOD import bit = C∷bitset
#pragma CMOD context      C∷bitset

// protect some function parameters
#pragma CMOD intern x

#pragma CMOD declaration

enum state {
  invalid       = -1,
  unlocked      = ⦃0⦄,
  required      = bit∷setof(0),
  acquired      = bit∷setof(1),
  locked  = ⦃required ∪ acquired⦄,
  increment     = bit∷setof(2),
};

#ifdef __cplusplus
// C and C++ differ in their implicit conversions from int to enum
inline
bool released(int x) {
  return x ≡ locked;
}
#else
inline
bool released(state x) {
  return x ≡ locked;
}
#endif
