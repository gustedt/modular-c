/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017-2018 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module task  =
#pragma CMOD separator ∷
#pragma CMOD composer  —
#pragma CMOD import group = ..∷hdl∷group
#pragma CMOD import blks  = ..∷obj∷blks
#pragma CMOD context      C∷attribute

// protect some function variables and parameters
#pragma CMOD extern breaker
#pragma CMOD extern current
#pragma CMOD extern depLen
#pragma CMOD extern fifos
#pragma CMOD extern cstep
#pragma CMOD extern inner
#pragma CMOD extern itend
#pragma CMOD extern itIncDup
#pragma CMOD extern itIncAlt
#pragma CMOD extern itIncDupg
#pragma CMOD extern itIncAltg
#pragma CMOD extern itOffDup
#pragma CMOD extern itOffAlt
#pragma CMOD extern ittotal
#pragma CMOD extern totgroups
#pragma CMOD extern numgroups
#pragma CMOD extern numtasks
#pragma CMOD extern numthreads
#pragma CMOD extern plen
#pragma CMOD extern cgroup
#pragma CMOD extern qlength
#pragma CMOD extern tasks
#pragma CMOD extern sibling
#pragma CMOD extern sibOffset
#pragma CMOD intern b
#pragma CMOD intern blocks
#pragma CMOD intern depId
#pragma CMOD intern depMi
#pragma CMOD intern flag
#pragma CMOD intern inblock
#pragma CMOD intern iter
#pragma CMOD intern length
#pragma CMOD intern p
#pragma CMOD intern pgrain
#pragma CMOD intern plen1
#pragma CMOD intern ptab
#pragma CMOD intern rest
#pragma CMOD intern ret
#pragma CMOD intern start
#pragma CMOD intern total
#pragma CMOD intern twice

// internal function
#pragma CMOD intern intersection

/**
 ** @module
 **/

#pragma CMOD declaration

enum phase {
  counting,
  owning,
  subscribing,
  all,
};

#pragma CMOD fill   ENUM  = task
#pragma CMOD defill ENUM∷LIST = counting, owning, subscribing, all
#pragma CMOD import ENUM  = C∷snippet∷ENUM

// FIXME
#pragma CMOD declaration

enum files {
  rt = 0,
  fi = 0,
  gr = 0,
};

enum cases {
  inblock = 0,
};

#pragma CMOD fill FA∷T    = group
#pragma CMOD defill FA∷OTHERS =                                \
  C∷size tot;                                                  \
  C∷size grain;                                                \
  C∷size id;
#pragma CMOD import FA    = C∷snippet∷flexible

// FIXME
#pragma CMOD declaration

typedef FA—fa task;

/**
 ** @brief Attribute this to a variable that should be hashed into its
 ** own location.
 **
 ** This is intended to prevent false-sharing hash positions of
 ** control variables.
 **/
#define sole alignas(blks∷grain_default)

#define ONCE for (bool flag = true; flag; flag = false)

#define PRE(...)                                                    \
  for (bool flag = ((void)(__VA_ARGS__), true); flag; flag = false)

#define POST(...)                                                 \
  for (bool flag = true; flag; (void)(__VA_ARGS__), flag = false)

#define PRE—POST(PR, ...)                                      \
  for (bool flag = ((void)(PR), true);                         \
       flag;                                                   \
       (void)(__VA_ARGS__), flag = false)

#define DECL(...)                                              \
  ONCE                                                         \
  for (__VA_ARGS__; flag; flag = false)

/**
 ** @brief return the current micro-task of the current task
 **/
#define micro() (cgroup+0)

/**
 ** @brief return the current iteration of the current task or
 ** micro-task
 **/
#define iteration() (iter*itIncDup*itIncAlt+itOffDup*itIncAlt+itOffAlt)

/**
 ** @brief return the current step of the current task
 **/
#define step() (cstep+0)

/**
 ** @brief return the ID of the current task
 **/
#define ID() (p→id+0)

/**
 ** @brief split an iteration into a sequence of micro-tasks that
 ** regroup a number of STEP
 **
 ** This macro exist in fact in two versions, a sequential and a
 ** parallel one. The amendment @c mctask assembles them to a
 ** functioning executable.
 **/
#define SPLIT

#pragma CMOD foreach VER = seq omp
#define SPLIT_${VER}                                           \
ITERATE_TASK_${VER}()                                          \
DECL(bool breaker = false)                                     \
DECL(C∷size cstep = 0,                                         \
     inner = 0,                                                \
     cgroup = 0,                                               \
     pgrain = p→grain,                                         \
     plen = p→len,                                             \
     plen1 = plen-1)                                           \
DECL(group*const restrict ptab = p→tab)                        \
for (unsigned twice = 0; twice < 2; ++twice)                   \
  if (twice) {                                                 \
    /* advance all remaining handles */                        \
    switch (iter) {                                            \
    case counting:                                             \
      p→tot = cstep;                                           \
      pgrain = cstep/p→len;                                    \
      p→grain = pgrain;                                        \
      break;                                                   \
    case owning:                                               \
      /* a possible back arc 0→plen1 cannot be modeled */      \
      for (unsigned b = 1; b < plen; ++b) {                    \
        dependinner(p, b, ptab[b-1].id);                       \
      }                                                        \
      break;                                                   \
    case subscribing:                                          \
      blks∷imark(ptab[0].blk, &itend, sizeof itend);           \
      if(inner) {                                              \
        group∷req(&ptab[cgroup], all, &up→tab[cgroup]);        \
      }                                                        \
      break;                                                   \
    default:                                                   \
      for (; cgroup < plen; ++cgroup) {                        \
        if (iter < itend ∧ iteration() < ittotal) {            \
          /* Under some circumstances this is */               \
          /* a second time, but acquire is */                  \
          /* designed to deal with this. */                    \
          group∷acq(&ptab[cgroup]);                            \
          group∷rel(&ptab[cgroup]);                            \
        } else {                                               \
          group∷destroy(&ptab[cgroup]);                        \
        }                                                      \
      }                                                        \
    }                                                          \
  } else
#pragma CMOD done

/**
 ** @brief A step that will be grouped into a micro-task
 **/
#define STEP                                                   \
BREAKER                                                        \
POST((cgroup < plen1)                                          \
     ∧ ((inner < pgrain)                                       \
        ∨ (inner = 0,                                          \
           (iter < all ? 0 : group∷rel(&ptab[cgroup])),        \
           ((iter ≡ subscribing)                               \
            ? group∷req(&ptab[cgroup], all, &up→tab[cgroup])   \
            : 0),                                              \
           ((iter ≠ counting) ? ++cgroup : 0))))               \
POST(++cstep)                                                  \
POST(++inner)                                                  \
PRE(inner ∨ (iter < all ? 0 : group∷acq(&ptab[cgroup])))       \
 if (!inner ∧ (iter ≥ itend)) {                                \
   breaker = true;                                             \
   continue;                                                   \
 } else

#define BREAKER                                                \
  if (breaker) {                                               \
    breaker = false;                                           \
    break;                                                     \
  } else

/**
 ** @brief Cut out of an @c iterate or @c split loop.
 **
 ** For syntactic reasons we can't use a normal @c break statement to
 ** cut out such a loop. This should behave just as if.
 **
 ** To be valid for terminating an @c iterate loop, this must be
 ** placed into a @c block statement, otherwise this would just
 ** concern the innermost SPLIT.
 **
 ** @remark This cannot be used in a @c steps loop.
 **
 ** @remark This is not completely equivalent to a break statement,
 ** because of the asynchronicity of the micro-tasks. If this is used
 ** inside a block to conditionally terminate at a specific iteration,
 ** all running @c steps and @c split loops may first terminate their
 ** current iteration before checking. If you need strict dependency
 ** from a termination condition you should use task::strict.
 **
 ** The implementation of this looks weird, but this is necessary to
 ** avoid spurious dangling else warnings. Therefore it uses a @c
 ** switch statement and inside that a @c continue. Another @c break
 ** inside that wouldn't work, because this would only break the
 ** switch and not the enclosing loop.
 **
 ** @see task::continue
 ** @see task::strict
 **/
#define task∷break    switch ((inblock ? (itend = iter) : (breaker = true)))  case 0: case 1: default: continue

/**
 ** @brief Ensure strict dependency of all micro-tasks of the
 ** termination condition for the @c iterate loop.
 **
 ** Place this inside the @c block with the task::break that is meant
 ** to terminate all iterations.
 **
 ** Introducing such a dependency may hurt the parallelism of the @c
 ** iterate block, therefore this is not set by default.
 **
 **/
#define strict() emark(itend)


/**
 ** @brief Go on with a SPLIT loop.
 **
 ** For syntactic reasons we can't use a normal @c continue statement
 ** to go on with such a loop. This should behave just as if.
 **
 ** @remark This cannot be used in a @c steps loop.
 **
 ** @remark This has no effect in a @a block, and in particular this
 ** cannot be used to skip an part or all of an @c iterate loop.
 **
 ** @see task::break
 **/
#define task∷continue switch ((breaker = false)) default: continue

/**
 ** @brief Implement a SPLIT @c for loop.
 **
 ** @see task::break
 **/
#define task∷for(...) for (__VA_ARGS__) STEP

/**
 ** @brief Implement a SPLIT @c while loop.
 **
 ** @see task::break
 **/
#define task∷while(...) while (__VA_ARGS__) STEP

/**
 ** @brief Iterate @a TOTAL times over the depending block and split
 ** it up into a set of independent micro-task.
 **
 ** This macro exist in fact in two versions, a sequential and a
 ** parallel one. The amendment @c mctask assembles them to a
 ** functioning executable.
 **/
#define STEPS(TOTAL)

#pragma CMOD foreach VER = seq omp
#define STEPS_${VER}(TOTAL)                                             \
  /* Collect the invariants for this section. */                        \
  DECL(task*const p = tasks[current],                                   \
       *const up = tasks[sibling])                                      \
  DECL(C∷size const plen = p→len)                                       \
  DECL(C∷size pgrain = p→grain)                                         \
  DECL(group*const restrict ptab = p→tab)                               \
  DECL(C∷size const total = (TOTAL))                                    \
  /* p→plen micro-tasks */                                              \
  ITERATE_TASK_INNER_${VER}(plen)                                       \
  /* Keep track of overall iterations */                                \
  DECL(C∷size cstep = cgroup*pgrain,                                    \
       rest = (total > cstep) ? total-cstep : 0,                        \
       /* Each consisting of pgrain steps, only the last */             \
       /* may be longer if this is really needed. Before */             \
       stop = ((rest < pgrain)∨(cgroup ≡ plen-1)) ? rest : pgrain)      \
  /* Before each micro-task, the corresponding acquire. */              \
  /* After each micro-task, the corresponding release.  */              \
  if (iter ≥ itend) { /*empty*/ } else                                  \
  PRE—POST(((start < all ∧ iter < all) ? 0 : group∷acq(&ptab[cgroup])), \
           ((start < all ∧ iter < all) ? 0 : group∷rel(&ptab[cgroup]))) \
  POST((void)((iter ≡ subscribing)                                      \
              ∧ (blks∷imark(ptab[0].blk, &itend, sizeof itend),         \
                 group∷req(&ptab[cgroup], all, &up→tab[cgroup]))))      \
  POST((iter ≡ counting)                                                \
       ∧ (start < all)                                                  \
       ∧ (p→tot = cstep)                                                \
       ∧ (pgrain = (p→grain = cstep/p→len)))                            \
  if (iter ≥ itend) { /*empty*/ } else                                  \
   for (C∷size inner = 0; inner < stop; ++inner, ++cstep)
#pragma CMOD done

#pragma CMOD foreach VER = seq omp
#define BLOCK_${VER}                                               \
  /* Collect the invariants for this section. */                   \
  DECL(task*const p = tasks[current],                              \
         *const up = tasks[sibling])                               \
  DECL(group*const restrict ptab = p→tab)                          \
  /* 1 micro-task */                                               \
  ITERATE_BLOCK_INNER_${VER}(1)                                    \
  /* Before each micro-task, the corresponding acquire. */         \
  /* After each micro-task, the corresponding release.  */         \
  PRE—POST(((start < all ∧ iter < all) ? 0 : group∷acq(&ptab[0])), \
           ((start < all ∧ iter < all) ? 0 : group∷rel(&ptab[0]))) \
  POST((void)((iter ≡ subscribing)                                 \
              ∧ (blks∷imark(ptab[0].blk, &itend, sizeof itend),    \
                 group∷req(&ptab[0], all, &up→tab[0]))))           \
  POST((iter ≡ counting)                                           \
       ∧ (start < all)                                             \
       ∧ (p→tot = 1))
#pragma CMOD done

#define DEPENDS_seq(Id, Mi)                                    \
  DECL(C∷size depId = (Id)%numtasks,                           \
         depLen = tasks[depId]->len,                           \
         depMi = (Mi)%(depLen))                                \
  PRE((iter ≡ subscribing)                                     \
      ∧ (¬inner)                                               \
      ∧ depend(tasks[depId]→tab[depMi].id))

#define DEPENDS_par(Id, Mi)


#define NUMTHREAD0(X) _Generic((X), task*: ((task*)X)→len, default: (X))

#pragma CMOD fill   number∷OUTER  = NUMTHREAD1
#pragma CMOD fill   number∷INNER  = NUMTHREAD0
#pragma CMOD defill number∷SEP    = +
#pragma CMOD import number    = C∷snippet∷map

// FIXME
#pragma CMOD declaration

/**
 ** @def THREADS
 **
 ** @brief Reserve the number of threads that a set of tasks need to
 ** run all their micro-tasks as separate threads.
 **
 ** The tasks are given as a list that contains numbers or pointers to
 ** task variables.
 **
 ** @remark This should prefix an ITERATE directive.
 **/

/**
 ** @def ITERATE
 **
 ** @brief Iterate over a set of tasks that are launched in the
 ** depending block.
 **
 ** Tasks are identified by SPLIT or STEPS directives.
 **/

/**
 ** @def ONCE_TASKS
 **
 ** @brief Run a set of independent tasks that are launched in the
 ** depending block.
 **
 ** The task variables should be given as list of arguments to this
 ** directive. Their number is the number of threads that will be
 ** used.
 **
 ** Tasks are identified by a ONCE_TASK directive.
 **/

/**
 ** @def ONCE_TASK
 **
 ** @brief Run a task of a ONCE_TASK group exactly once.
 **/


#define THREADS                                                \
  〚std∷maybe_unused〛 C∷size current = 0;                       \
  〚std∷maybe_unused〛 C∷size sibling = 0;                       \
  〚std∷maybe_unused〛 C∷size sibOffset = 0;                     \
  〚std∷maybe_unused〛 C∷size blocks = 0

#define ITERATE_seq(START, NUMBER, ENUMBER)                                           \
  〚std∷maybe_unused〛 register C∷size const start = (START);                           \
  itend = (ENUMBER);                                                                  \
  ittotal = (NUMBER);                                                                 \
  if (¬ittotal) ittotal = itend*itIncDup*itIncAlt;                                    \
  for(register C∷size iter = start; (iter < itend) ∧ (iteration() < ittotal); ++iter) \
    PRE(iter < all                                                                    \
        ∧ C∷io∷fprintf(C∷io∷err, "mctask: %s phase starts\t%g sec\n",                 \
                     getshort(iter), eilck∷csv∷stamp()*1E-9))                         \
    POST(iter < all                                                                   \
         ∧ C∷io∷fprintf(C∷io∷err, "mctask: %s phase ended\t%g sec\n",                 \
                        getshort(iter), eilck∷csv∷stamp()*1E-9))                      \
    PRE(start < all ∧ iter ≡ counting ∧ (fifos = bind(qlength, numtasks, tasks)))     \
    PRE(start < all ∧ iter ≡ owning   ∧ (blks∷alloc_owner(fifos, blocks)))

#define ITERATE_TASK_seq()                                     \
  DECL(task*const p = tasks[current],                          \
       *const up = tasks[sibling])                             \
  ONCE

#define ONCE_TASKS_seq(...) ONCE
#define ONCE_TASK_seq ONCE

#define ITERATE_TASK_INNER_seq(PLEN)                           \
  /* p→plen micro-tasks */                                     \
  for(C∷size cgroup = 0; cgroup < PLEN; ++cgroup)

#define ITERATE_BLOCK_INNER_seq(PLEN)                          \
  /* 1 micro-task */                                           \
  DECL(〚std∷maybe_unused〛 bool breaker = false,                \
       inblock = true)                                         \
  for(C∷size cgroup = 0; cgroup < 1; ++cgroup)

#define ITERATE_omp(START, NUMBER, ENUMBER)                                                        \
  〚std∷maybe_unused〛 C∷size const start = (START);                                                 \
  C∷size const stime = eilck∷csv∷stamp();                                                          \
  itend = (ENUMBER);                                                                               \
  ittotal = (NUMBER);                                                                              \
  if (¬ittotal) ittotal = itend*itIncDup*itIncAlt;                                                 \
  POST(C∷io∷printf(C∷io∷err,                                                                       \
                   "mctask: phase%8zu ended %g sec\n"                                              \
                   "%zu iterations, %zu steady state, %g seconds per iteration in steady state\n", \
                   itend, eilck∷csv∷stamp()*1E-9,                                                  \
                   itend*itIncDupg*itIncAltg, ((itend-all)*itIncDupg*itIncAltg),                   \
                   (eilck∷csv∷stamp()-stime)*1E-9/((itend-all)*itIncDupg*itIncAltg)))              \
  omp∷PARALLELIZE(numthreads)

#define ITERATE_TASK_omp()                                     \
  DECL(task*const p = tasks[current],                          \
         *const up = tasks[sibling])                           \
  omp∷JOB                                                      \
  POST(task∷destroy(p))                                        \
  for(register C∷size iter = start;                            \
      (iter < itend) ∧ (iteration() < ittotal);                \
      ++iter)

#define ONCE_TASKS_omp(...) omp∷PARALLELIZE(C∷COUNT(__VA_ARGS__))
#define ONCE_TASK_omp omp∷JOB
#define ITERATE_TASK_INNER_omp(PLEN)                           \
  omp∷JOBS(1)                                                  \
  /* p→plen micro-tasks */                                     \
  for(C∷size cgroup = 0; cgroup < PLEN; ++cgroup)              \
    POST(group∷destroy(&p→tab[cgroup]))                        \
    for(register C∷size iter = (start ≤ all ? all : start);    \
        (iter < itend) ∧ (iteration() < ittotal);              \
        ++iter)

#define ITERATE_BLOCK_INNER_omp(PLEN)                          \
  omp∷JOBS(1)                                                  \
  /* 1 micro-task */                                           \
  for(C∷size cgroup = 0; cgroup < 1; ++cgroup)                 \
    DECL(〚std∷maybe_unused〛 bool breaker = false,              \
         inblock = true)                                       \
    POST(group∷destroy(&p→tab[cgroup]))                        \
      for(register C∷size iter = (start ≤ all ? all : start);  \
          (iter < itend) ∧ (iteration() < ittotal);            \
          ++iter)

#ifndef _OPENMP
#define ITERATE_BLOCK_INNER_par ITERATE_BLOCK_INNER_seq
#define ITERATE_TASK_INNER_par  ITERATE_TASK_INNER_seq
#define ITERATE_par   ITERATE_seq
#define ONCE_TASKS_par    ONCE_TASKS_seq
#define ONCE_TASK_par   ONCE_TASK_seq
#define SPLIT_par   SPLIT_seq
#define STEPS_par   STEPS_seq
#define BLOCK_par   BLOCK_seq
#else
#define ITERATE_BLOCK_INNER_par ITERATE_BLOCK_INNER_omp
#define ITERATE_TASK_INNER_par  ITERATE_TASK_INNER_omp
#define ITERATE_par   ITERATE_omp
#define ONCE_TASKS_par    ONCE_TASKS_omp
#define ONCE_TASK_par   ONCE_TASK_omp
#define SPLIT_par   SPLIT_omp
#define STEPS_par   STEPS_omp
#define BLOCK_par   BLOCK_omp
#endif

#pragma CMOD definition

static C∷size _Atomic ctask = 0;

static
task* bind—one(task* p, blks* b) {
  if (p ∧ b) {
    for (C∷size i = 0, length = p→len; i < length; ++i) {
      group∷init(&p→tab[i], b, p→id, i);
    }
  }
  return p;
}

task* init(task* p) {
  if (p) {
    p→grain = 1;
    p→id    = ctask++;
  }
  return p;
}

task* destroy(task* p) {
  if (p) {
    for (C∷size i = 0, length = p→len; i < length; ++i) {
      group∷destroy(&p→tab[i]);
    }
  }
  return p;
}

blks* bind(C∷size qlen, C∷size tlen, task* τ[tlen]) {
  C∷size length = 0;
  for (C∷size i = 0; i < tlen; ++i) {
    length += τ[i]→len;
  }
  blks* ret = blks∷alloc(length, qlen);
  for (C∷size i = 0; i < tlen; ++i)
    bind—one(τ[i], ret);
  return ret;
}

void greedy(char const name[], C∷size periods, C∷size tlen, task* τ[tlen]) {
  C∷io* log = C∷io∷open(name, "w");
  unsigned*restrict elast = τ[0]→tab[0].blk→elast;
  unsigned*restrict ilast = τ[0]→tab[0].blk→ilast;
  C∷size blen = τ[0]→tab[0].blk→len;
  for (C∷size period = 0; period < periods; ++period) {
    // Compute first fit times for all micro-tasks.
    for (C∷size i = 0; i < tlen; ++i) {
      for (C∷size t = 0; t < τ[i]→len; ++t)
        group∷greedy(&τ[i]→tab[t], log, period);
    }
    // Mark the minimal time that any member of the next period must
    // observe.
    C∷size min = -1;
    for (C∷size b = 0; b < blen; ++b) {
      C∷size now = elast[b] < ilast[b] ? ilast[b] : elast[b];
      if (now ∧ now < min) min = now;
    }
    C∷io∷fprintf(log, "%zu,", min+1);
    for (C∷size b = 0; b < blen; ++b)
      C∷io∷fprintf(log, ",—");
    C∷io∷fprintf(log, "\n");
  }
  C∷io∷close(log);
}

#pragma CMOD declaration

#pragma CMOD intern eown
/**
 ** @brief Register address @a A as being "owned" by the current
 ** micro-task, if possible.
 **
 ** @a A is only registered if it has not been registered before for
 ** another micro-task.
 **/
inline
bool eown(task const*restrict τ, void* addr, C∷size size, C∷size μ) {
  blks∷set(τ→tab[μ].blk, addr, size, τ→tab[μ].id);
  return true;
}

#pragma CMOD intern iown
/**
 ** @brief During the ownership phase of classification there is
 ** nothing to do for inclusive requests.
 **/
inline
void iown(task const*restrict τ, void* addr, C∷size size, C∷size μ) {
  // empty
}

#pragma CMOD intern ecount
/**
 ** @brief Register address @a A as being "owned" by the current
 ** micro-task, if possible.
 **
 ** @a A is only registered if it has not been registered before for
 ** another micro-task.
 **/
inline
C∷size ecount(task const*restrict τ, void* addr, C∷size size) {
  return blks∷count(τ→tab[0].blk, addr, size);
}

#pragma CMOD intern icount
/**
 ** @brief During the ownership phase of classification there is
 ** nothing to do for inclusive requests.
 **/
inline
C∷size icount(task const*restrict τ, void* addr, C∷size size) {
  return 0;
}

#ifdef _OPENMP

#pragma CMOD foreach EI = i e
#pragma CMOD intern ${EI}markinner
#define ${EI}markinner(τ, addr, size, start, it, μ, blocksp)                    \
do {                                                                            \
  if (start < all ∧ it ≡ counting)    *blocksp += ${EI}count(τ, addr, size);    \
  if (start < all ∧ it ≡ owning)      ${EI}own(τ, addr, size, μ);               \
  if (start < all ∧ it ≡ subscribing) blks∷${EI}mark(τ→tab[0].blk, addr, size); \
 } while (false)

#define ${EI}mark2(A, S) ${EI}markinner(p, (A), (S), start, iter, micro(), &blocks)
#pragma CMOD done

inline
bool dependinner(task const*restrict τ, C∷size μ, C∷size id) {
  τ→tab[μ].deps[id] |= eilck∷R;
  return true;
}

#else /* no OpenMP */

#pragma CMOD foreach EI = i e
#define ${EI}mark2(A, S) do {                                  \
    /* Ensure that all of these are "used" */                  \
    (void)p;                                                   \
    (void)(A);                                                 \
    (void)(S);                                                 \
    (void)start;                                               \
    (void)iter;                                                \
    (void)micro();                                             \
    (void)&blocks;                                             \
  } while (false)
#pragma CMOD done

inline
bool dependinner(task const*restrict τ, C∷size μ, C∷size id) {
  return true;
}

#endif

#define depend(X) dependinner(p, micro(), (X))

#pragma CMOD foreach EI = i e

// FIXME
#pragma CMOD declaration

/**
 ** @brief Mark object @a A as being used ${EI}-clusive in the current
 ** micro-task.
 **
 ** @remark This is implemented as a macro. First, it has to suck
 ** information from the calling environment such as the current
 ** iteration and the task data structure. Second, the contents must
 ** be fully exposed such that it is completely optimized out in the
 ** execution context.
 **
 ** @remark @a A must be an addressable lvalue, that is an object of
 ** which we can take the address and for which @c sizeof returns the
 ** proper size of the underlying object.
 **
 ** @see ${EI}mark2 if you have only a pointer value and a size.
 **
 ** @remark If the program is compiled without OpenMP support, this
 ** should completely be optimized out. Therefore the three initial
 ** iterations should then perform exactly the same as the others, and
 ** the overhead of acquiring and releasing empty FIFO groups should
 ** be negligible.
 **/
#define ${EI}mark(A) ${EI}mark2((void*)(&(A)), sizeof(A))


#pragma CMOD fill   ${EI}marksImp∷OUTER  = ${EI}marks
#pragma CMOD fill   ${EI}marksImp∷INNER  = ${EI}mark
#pragma CMOD defill ${EI}marksImp∷SEP  = ;
#pragma CMOD import ${EI}marksImp    = C∷snippet∷map


/**
 ** @def ${EI}mark2
 ** @brief Mark address @a A with size @a S as being used
 ** ${EI}-clusive in the current micro-task.
 **
 ** @see ${EI}mark for more details.
 **/

#pragma CMOD done

// FIXME
#pragma CMOD declaration

#define omark(...) ((void)0)
#define omarks(...) ((void)0)

inline
task* alloc(C∷size length) {
  return init(FA—alloc(length));
}

inline
void free(task* p) {
  C∷lib∷free(destroy(p));
}
