/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
#pragma CMOD definition
#pragma CMOD import task = eilck∷task
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "wtime_inc.h"

/*
 * Improvement of rodinia's hotspot3D code:
 *
 * - beautify the C
 * - use 3D matrices
 * - use size_t
 * - duplicate the inner computation to avoid pointer switching
 */


#define STR_SIZE (256)
#define MAX_PD	(3.0e6)
/* required precision in degrees	*/
#define PRECISION	0.001
#define SPEC_HEAT_SI 1.75e6
#define K_SI 100
/* capacitance fitting factor	*/
#define FACTOR_CHIP	0.5


/* chip parameters	*/
float t_chip = 0.0005;
float chip_height = 0.016; float chip_width = 0.016;
/* ambient temperature, assuming no package at all	*/
float amb_temp = 80.0;

_Noreturn
void fatal(char const* s) {
  perror(s);
  exit(EXIT_FAILURE);
}

void readinput(size_t grid_rows, size_t grid_cols, size_t layers,
               float vect[restrict layers][grid_rows][grid_cols],
               char*restrict file) {
  FILE* fp  = fopen(file, "r");
  if (!fp) fatal(file);

  for (size_t i=0; i <= grid_rows-1; i++)
    for (size_t j=0; j <= grid_cols-1; j++)
      for (size_t k=0; k <= layers-1; k++) {
        char str[STR_SIZE];
        if (!fgets(str, STR_SIZE, fp)) fatal("Error reading file");
        if (feof(fp)) fatal("not enough lines in file");
        char* endptr = 0;
        vect[k][i][j] = strtod(str, &endptr);
        if (!endptr || endptr == str) fatal("invalid file format");
      }
  fclose(fp);
}


void writeoutput(size_t grid_rows, size_t grid_cols, size_t layers,
                 float vect[restrict layers][grid_cols][grid_rows],
                 char*restrict file) {
  FILE* fp = fopen(file, "w");
  if (!fp) fatal(file);
  for (size_t i = 0, index = 0; i < grid_rows; i++)
    for (size_t j = 0; j < grid_cols; j++)
      for (size_t k = 0; k < layers; k++, index++) {
        fprintf(fp, "%zu\t%g\n", index, vect[k][i][j]);
      }
  fclose(fp);
}

void computeTempCPU(
        size_t nx, size_t ny, size_t nz,
        float const pIn[restrict nz][ny][nx], float tIn[restrict nz][ny][nx], float tOut[restrict nz][ny][nx],
        float Cap,
        float Rx, float Ry, float Rz,
        float dt, size_t numiter) {
  float stepDivCap = dt / Cap;
  float cw = stepDivCap/ Rx;
  float ce = cw;
  float cs = stepDivCap/ Ry;
  float cn = cs;
  float cb = stepDivCap/ Rz;
  float ct = cb;
  float cc = 1.0 - (2.0*ce + 2.0*cn + 3.0*ct);

  // Classify the memory access pattern of the four tasks, and log the
  // fifo in a file. The file can be used to find the largest FIFO
  // length L during initialization. The overall FIFO length (qlen
  // below) must then be at least L+1.
#pragma CMOD insert mctask log fifo    "/tmp/fifo-schedule.csv"
  // Also output the effective schedule that was realized at
  // runtime.
#pragma CMOD insert mctask log runtime "/tmp/runtime-schedule.csv"
  // Output the greedy schedule for a unit cost model with arbitrary
  // many processors. This can give you some indication of the
  // possible parallelism.
#pragma CMOD insert mctask log greedy  "/tmp/greedy-schedule.csv"
  // Each task will have as many groups as this.
#pragma CMOD insert mctask groups = 8
#pragma CMOD amend mctask iterate  = numiter
#pragma CMOD amend mctask duplicate = 2
#pragma CMOD amend mctask alternate tOut tIn
#pragma CMOD insert mctask steps = nz
    {
    size_t z = task∷step();
    size_t z0 = (z == 0) ? 0      : z - 1;
    size_t z1 = (z == nz - 1) ? z : z + 1;
    /* mark the items that we acccess */
#pragma CMOD insert mctask lvalue = tOut[z]
#pragma CMOD insert mctask rvalue = tIn[z], tIn[z0], tIn[z1]
    for(size_t y = 0; y < ny; y++) {
      size_t y0 = (y == 0) ? 0      : y - 1;
      size_t y1 = (y == ny - 1) ? y : y + 1;
      for(size_t x = 0; x < nx; x++) {
        size_t x0 = (x == 0) ? 0      : x - 1;
        size_t x1 = (x == nx - 1) ? x : x + 1;
        /* do the computation */
        tOut[z][y][x] = tIn[z][y][x]*cc
          + tIn[z][y0][x]*cn + tIn[z][y1][x]*cs
          + tIn[z][y][x1]*ce + tIn[z][y][x0]*cw
          + tIn[z1][y][x]*ct + tIn[z0][y][x]*cb
          + (dt/Cap) * pIn[z][y][x]
          + ct*amb_temp;
      }
    }
  }
#pragma CMOD done
#pragma CMOD done
#pragma CMOD done
}

float accuracy(size_t len, float arr1 [restrict len], float arr2[restrict len]) {
  float err = 0.0;
  size_t i;
  for(i = 0; i < len; i++) {
    err += (arr1[i]-arr2[i]) * (arr1[i]-arr2[i]);
  }
  return sqrtf(err/len);
}

void usage(int argc, char **argv) {
  fprintf(stderr, "Usage: %s <rows/cols> <layers> <iterations> <powerFile> <tempFile> <outputFile>\n", argv[0]);
  fprintf(stderr, "\t<rows/cols>\t- number of rows/cols in the grid (positive integer)\n");
  fprintf(stderr, "\t<layers>\t- number of layers in the grid (positive integer)\n");

  fprintf(stderr, "\t<iteration>\t- number of iterations (positive, even)\n");
  fprintf(stderr, "\t<powerFile>\t- name of the file containing the initial power values of each cell\n");
  fprintf(stderr, "\t<tempFile>\t- name of the file containing the initial temperature values of each cell\n");
  fprintf(stderr, "\t<outputFile>\t- output file\n");
  exit(EXIT_FAILURE);
}



int main(int argc, char** argv) {
  if (argc != 7) {
    usage(argc,argv);
  }

  char *pfile, *tfile, *ofile;
  size_t iterations = atoi(argv[3]);
  if (iterations%2) ++iterations;

  pfile = argv[4];
  tfile = argv[5];
  ofile = argv[6];

  size_t numCols = atoi(argv[1]);
  size_t numRows = numCols;
  size_t layers = atoi(argv[2]);

  /* calculating parameters*/

  float dx = chip_height/numRows;
  float dy = chip_width/numCols;
  float dz = t_chip/layers;

  float Cap = FACTOR_CHIP * SPEC_HEAT_SI * t_chip * dx * dy;
  float Rx = dy / (2.0 * K_SI * t_chip * dx);
  float Ry = dx / (2.0 * K_SI * t_chip * dy);
  float Rz = dz / (K_SI * dx * dy);

  float max_slope = MAX_PD / (FACTOR_CHIP * t_chip * SPEC_HEAT_SI);
  float dt = PRECISION / max_slope;

  float (*restrict powerIn)[numCols][numRows]  = calloc(layers+1, sizeof(float[numCols][numRows]));
  float (*restrict tempIn)[numCols][numRows]  = calloc(layers+1, sizeof(float[numCols][numRows]));
  float (*restrict answer)[numCols][numRows]  = calloc(layers+1, sizeof(float[numCols][numRows]));

  readinput(numRows, numCols, layers, powerIn, pfile);
  readinput(numRows, numCols, layers, tempIn,  tfile);

  double start = wtime();
  computeTempCPU(numCols, numRows, layers, powerIn, tempIn, answer, Cap, Rx, Ry, Rz, dt, iterations);
  double stop = wtime();
  double time = stop - start;

  printf("Time: %.3f (s)\n",time);
  writeoutput(numRows, numCols, layers, answer, ofile);
  free(tempIn);
  free(powerIn);
  free(answer);
  return 0;
}
