// srad_orig.c: Defines the entry point for the console application.
//
// This is a cleaned up version of the original srad.cpp, that was C++
// with no reason:
//
// - make all variables as local as possible
// - chase all useless casts (all were useless)
// - change integer types mainly to size_t, there was possible
//   overflow because of the use of just int
// - force output of the result matrix for possible verification of correctness
//   and use a file name for that
//
// run as
// srad_orig 128 128 0 31 0 31 0.5 10000

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "wtime_inc.h"

void random_matrix(size_t rows, size_t cols, float matI[rows][cols]);

void usage(int argc, char **argv) {
  fprintf(stderr, "Usage: %s <rows> <cols> <y1> <y2> <x1> <x2> <lamda> <no. of iter>\n", argv[0]);
  fprintf(stderr, "\t<rows>\t- number of rows\n");
  fprintf(stderr, "\t<cols>\t- number of cols\n");
  fprintf(stderr, "\t<y1>\t- y1 value of the speckle\n");
  fprintf(stderr, "\t<y2>\t- y2 value of the speckle\n");
  fprintf(stderr, "\t<x1>\t- x1 value of the speckle\n");
  fprintf(stderr, "\t<x2>\t- x2 value of the speckle\n");
  fprintf(stderr, "\t<lamda>\t- lambda (0,1)\n");
  fprintf(stderr, "\t<no. of iter>\t- number of iterations\n");

  exit(EXIT_FAILURE);
}

int main(int argc, char* argv[]) {
  if (argc != 9) {
    usage(argc, argv);
  }
  size_t const rows = strtoull(argv[1], 0, 0); //number of rows in the domain
  size_t const cols = strtoull(argv[2], 0, 0); //number of cols in the domain
  if (rows%16 || cols%16) {
    fprintf(stderr, "rows and cols must be multiples of 16\n");
    return EXIT_FAILURE;
  }
  size_t const r1   = strtoull(argv[3], 0, 0); //y1 position of the speckle
  size_t const r2   = strtoull(argv[4], 0, 0); //y2 position of the speckle
  size_t const c1   = strtoull(argv[5], 0, 0); //x1 position of the speckle
  size_t const c2   = strtoull(argv[6], 0, 0); //x2 position of the speckle
  float const lambda = strtod(argv[7], 0);       //Lambda value
  size_t const niter = strtoull(argv[8], 0, 0); //number of iterations

  size_t const size_R = (r2-r1+1)*(c2-c1+1);

  typedef float row[cols];
  row*const matI = malloc(sizeof(row[rows]));
  row*const J = malloc(sizeof(row[rows]));
  row*const c = malloc(sizeof(row[rows]));

  // These are kept "unsigned" to save space
  unsigned*const iN = malloc(sizeof(unsigned[rows]));
  unsigned*const iS = malloc(sizeof(unsigned[rows]));
  unsigned*const jW = malloc(sizeof(unsigned[cols]));
  unsigned*const jE = malloc(sizeof(unsigned[cols]));

  row*const dN = malloc(sizeof(row[rows]));
  row*const dS = malloc(sizeof(row[rows]));
  row*const dW = malloc(sizeof(row[rows]));
  row*const dE = malloc(sizeof(row[rows]));


  for (size_t i=0; i< rows; i++) {
    iN[i] = i-1;
    iS[i] = i+1;
  }
  for (size_t j=0; j< cols; j++) {
    jW[j] = j-1;
    jE[j] = j+1;
  }
  iN[0]    = 0;
  iS[rows-1] = rows-1;
  jW[0]    = 0;
  jE[cols-1] = cols-1;

  printf("Randomizing the input matrix\n");

  random_matrix(rows, cols, matI);

  for (size_t i = 0; i < rows; i++) {
    for (size_t j = 0; j < cols; j++) {
      J[i][j] = expf(matI[i][j]);
    }
  }

  printf("Start the SRAD main loop\n");

  double start = wtime();
  for (size_t iter=0; iter< niter; iter++) {
    float sum=0;
    float sum2=0;
    for (size_t i=r1; i<=r2; i++) {
      for (size_t j=c1; j<=c2; j++) {
        float tmp   = J[i][j];
        sum  += tmp;
        sum2 += tmp*tmp;
      }
    }
    float meanROI = sum / size_R;
    float varROI  = (sum2 / size_R) - meanROI*meanROI;
    float q0sqr   = varROI / (meanROI*meanROI);


    for (size_t i = 0; i < rows; i++) {
      for (size_t j = 0; j < cols; j++) {

        float Jc = J[i][j];

        // directional derivates
        dN[i][j] = J[iN[i]][j] - Jc;
        dS[i][j] = J[iS[i]][j] - Jc;
        dW[i][j] = J[i][jW[j]] - Jc;
        dE[i][j] = J[i][jE[j]] - Jc;

        float G2 = (dN[i][j]*dN[i][j] + dS[i][j]*dS[i][j]
              + dW[i][j]*dW[i][j] + dE[i][j]*dE[i][j]) / (Jc*Jc);

        float L = (dN[i][j] + dS[i][j] + dW[i][j] + dE[i][j]) / Jc;

        float num  = (0.5*G2) - ((1.0/16.0)*(L*L));
        float den  = 1 + (.25*L);
        float qsqr = num/(den*den);

        // diffusion coefficent (equ 33)
        den = (qsqr-q0sqr) / (q0sqr * (1+q0sqr));
        c[i][j] = 1.0 / (1.0+den);

        // saturate diffusion coefficent
        if (c[i][j] < 0) {c[i][j] = 0;}
        else if (c[i][j] > 1) {c[i][j] = 1;}

      }

    }

    for (size_t i = 0; i < rows; i++) {
      for (size_t j = 0; j < cols; j++) {

        // diffusion coefficent
        float cN = c[i][j];
        float cS = c[iS[i]][j];
        float cW = c[i][j];
        float cE = c[i][jE[j]];

        // divergence (equ 58)
        float D = cN * dN[i][j] + cS * dS[i][j] + cW * dW[i][j] + cE * dE[i][j];

        // image update (equ 61)
        J[i][j] = J[i][j] + 0.25*lambda*D;
      }
    }
  }
  double stop = wtime();
  double time = stop - start;

  printf("Time: %.3f (s)\n",time);

  char oname[256];
  snprintf(oname, 256, "result%zux%zu.txt", rows, cols);
  FILE* out = fopen(oname, "w");
  for(size_t i = 0; i < rows; i++) {
    for (size_t j = 0; j < cols; j++) {
      fprintf(out, "%a\t", J[i][j]);
    }
    fputc('\n', out);
  }
  fclose(out);

  printf("Computation Done\n");

  free(matI);
  free(J);
  free(iN); free(iS); free(jW); free(jE);
  free(dN); free(dS); free(dW); free(dE);

  free(c);
  return EXIT_SUCCESS;
}

void random_matrix(size_t rows, size_t cols, float matI[rows][cols]) {
  srand(7);
  for(size_t i = 0; i < rows; i++) {
    for (size_t j = 0; j < cols; j++) {
      matI[i][j] = rand()/(float)RAND_MAX;
    }
  }
}

