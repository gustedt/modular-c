#ifndef NN_H

#include <stdnoreturn.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#define REC_LENGTH 49	// size of a record in db
#define REC_WINDOW 1000	// number of records to read at a time
#define LATITUDE_POS 28	// location of latitude coordinates in input record

typedef char dbline[REC_LENGTH];

typedef struct neighbor neighbor;
struct neighbor {
	double dist;
	dbline entry;
};

static inline
void error_exit(char const* msg) {
  perror(msg);
  exit(EXIT_FAILURE);
}

static inline
double convert(char const* str) {
  char* endptr = 0;
  errno = 0;
  double ret = strtod(str, &endptr);
  if (!endptr || (endptr == str) || errno) {
    char const* erstr = errno ? strerror(errno) : "not a number";
    char const* start = str + strspn(str, " \t\n");
    size_t pos = strcspn(start, " \t\n");
    char estr[pos+1];
    memcpy(estr, start, pos);
    estr[pos] = 0;
    fprintf(stderr, "convert(\"%s\"): %s\n", estr, erstr);
    exit(EXIT_FAILURE);
  }
  return ret;
}

#endif
