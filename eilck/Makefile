.PHONY: clean headers

LSOURCES =					\
	eilck-circ.X				\
	eilck-csv.X				\
	eilck-ehdl.X				\
	eilck-error.X				\
	eilck-ftx.X				\
	eilck-hdl-group.X			\
	eilck-hdl.X				\
	eilck-ihdl.X				\
	eilck-impl-SECTION.X			\
	eilck-impl-snippet.X			\
	eilck-impl.X				\
	eilck-lck.X				\
	eilck-obj-blks.X			\
	eilck-obj-fd.X				\
	eilck-obj-file.X			\
	eilck-obj-fixed.X			\
	eilck-obj-heap.X			\
	eilck-obj-scale.X			\
	eilck-obj-scaler.X			\
	eilck-obj-segment.X			\
	eilck-obj.X				\
	eilck-sem.X				\
	eilck-state.X				\
	eilck-task.X				\
	eilck-tmpl.X				\
	eilck-unsigned.X			\
	eilck.X

SOURCES := $(abspath ${LSOURCES})
OBJECTS = ${SOURCES:.X=.a}

HPPSOURCES =					\
	eilck++.hpp				\
	C++.hpp

CPPSOURCES := ${HPPSOURCES:.hpp=.cc}
CPPOBJECTS := ${HPPSOURCES:.hpp=.o}


ILSOURCES =					\
	eilcktasktest.X			\
	eilckunsigned.X

ISOURCES := $(abspath ${ILSOURCES})
IOBJECTS = ${ISOURCES:.X=.a}

DEPS    = ${SOURCES:.X=.dep} ${ISOURCES:.X=.dep}


LIBRARIES = libeilck.a libeilck++.a
TARGET = libeilck.a libeilck_noinit.a libeilck++.a

CMODDIR=../bin
CMODLIB=../lib
CMOD=${CMODDIR}/Cmod
MAKEHEADER=${CMODDIR}/makeheader
CMODHEADER=${CMODDIR}/Cmod_header
CMODEXPAND=${CMODDIR}/Cmod_expand

LICENSE = $(abspath ../SHORTLICENSE.txt)
AUTHORS = $(abspath ../AUTHORS.txt)
TIDY = ${CMODDIR}/findAuthors --scm git --authors=${AUTHORS} --lice ${LICENSE} --ofile

STUBS = ../C/C.a				\
	../C/C-atomic.a				\
	../Linux/Linux-fd-stat.a		\
	../Linux/Linux-futex.a

STUBS := $(abspath ${STUBS})


export CMOD_ARCHIVES := ${CMODLIB}/libLinux.a

TOOLS = ${CMOD} ${MAKEHEADER}

CMODEP = ${CMOD} -M

export CMOD_LEGSEP ?= _

OFLAGS	?= -Wall -O3 -march=native -fno-common -fdata-sections -ffunction-sections ${COPTS}
CFLAGS	?= -std=c11 ${OFLAGS}
CPPFLAGS ?= -IC ${OFLAGS}
LDFLAGS ?= -Wl,--gc-sections

ifeq (${CMOD_STATIC},)
TARGET   +=  libeilck.so libeilck_noinit.so libeilck++.so
LIBRARIES += libeilck.so libeilck++.so
CFLAGS   += -fPIC
CPPFLAGS   += -fPIC
LDSHARED ?= -shared -Bdynamic -lrt -lgcc
else
CFLAGS   += -static -latomic
CPPFLAGS   += -static
LDFLAGS += -static -latomic -lm ${CMODDIR}/../omp/omp-pthread-attr.o
endif

ifneq (${CMOD_OPENMP},)
TARGET   += eilckopenmp
CFLAGS   += -fopenmp
export CMOD_ARCHIVES := ${CMOD_ARCHIVES}:../lib/libomp.a
endif


ifneq (${STDATOMIC},)
	CFLAGS := ${CFLAGS} -I- -I ${STDATOMIC}
	LDFLAGS := -L ${STDATOMIC} -l atomic ${LDFLAGS}
endif

%.dep : %.X ${CMODLIB}/libC.a
	${CMODEP} $*.X -o $*.dep

%.a : %.dep %.X ${CMOD}
	${CMOD} -c ${CFLAGS} $*.X

% : %.a %.dep ${CMOD} ${CMODLIB}/libC.a ${LIBRARIES}
	${CMOD}  $*.a -o $*  -L. -leilck ${CFLAGS} ${LDFLAGS}

CEXPAND = ../c-expand
CEXPA = $(patsubst %.X,%.c,${LSOURCES}) $(patsubst %.X,%-_Snippet.c,${LSOURCES})

target : ${TARGET} eilcktest eilckunsigned

headers : ${OBJECTS}
	make -C C headers

eilck++.o : headers ${HPPSOURCES} ${CPPSOURCES}

C++.o : headers C++.hpp C++.cc

libeilck.a : ${OBJECTS} headers
	${CMOD}_archive -o $@ ${OBJECTS} ${STUBS}

libeilck_noinit.a : libeilck.a
	ln -f libeilck.a libeilck_noinit.a

libeilck.so : ${OBJECTS} headers
	${CMOD}_archive -o $@ ${OBJECTS} ${STUBS}

libeilck_noinit.so : ${OBJECTS} headers
	CMOD_NOINIT=1 ${CMOD}_archive -o $@ ${OBJECTS} ${STUBS}

libeilck++.a : headers ${CPPOBJECTS}
	${CMOD}_archive -o $@ ${CPPOBJECTS}

libeilck++.so : headers ${CPPOBJECTS}
	${CMOD}_archive -o $@ ${CPPOBJECTS}


eilcktest.a : eilcktest.X ${CMODLIB}/libC.a ${LIBRARIES}

eilcktasktest.a : eilcktest.X ${CMODLIB}/libC.a ${LIBRARIES}

eilckopenmp.a : eilckopenmp.X ${CMODLIB}/libC.a ${LIBRARIES}

eilckunsigned.a : ${IOBJECTS} ${CMODLIB}/libC.a ${LIBRARIES}

libeilck.dep : ${DEPS}
	cat ${DEPS} > libeilck.dep

cexp : libeilck.a
	${CMODEXPAND} ${CEXPAND} $< ${CEXPA}

all : depend ${OBJECTS}

clean :
	${MAKE} -C C clean
	rm -f ${DEPS} ${OBJECTS} ${TARGET} ${CPPOBJECTS} ${IOBJECTS} eilcktest.a

depend : ${DEPS}

tidy :
	for f in ${LSOURCES} ${ILSOURCES} ${HPPSOURCES} ; do  ${TIDY} $$f ; done

-include ${DEPS}
