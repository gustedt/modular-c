#ifdef __GNUC__
#pragma implementation
#endif
#include "C++.hpp"

/**
 ** @file Ensure that all interfaces are expanded at compile time.
 **/
