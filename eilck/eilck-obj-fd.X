/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD import obj = ..
#pragma CMOD import scaler  = ..∷scaler
#pragma CMOD import error = ..∷..∷error
#pragma CMOD import OS  = Linux
#pragma CMOD import fd      = OS∷fd
#pragma CMOD import accs  = fd∷open
#pragma CMOD import at      = fd∷at
#pragma CMOD import fmode = OS∷mode
#pragma CMOD import fs      = OS∷fs
#pragma CMOD import m = OS∷m
#pragma CMOD import stat  = OS∷stat
#pragma CMOD separator  ∷
#pragma CMOD composer   —

// protect some function parameters
#pragma CMOD intern inf
#pragma CMOD intern ret
#pragma CMOD intern ob0

#pragma CMOD declaration

#ifndef __CMOD__
typedef int fd;
#endif

struct info {
  fd desc;
  void* addr;
  char* name;
};

/* default values for the slots */
#define REMOVE fs∷unlink
#define OPEN   fd∷open

#pragma CMOD definition

void realpath(info* inf) {
  char* name = fs∷realpath(inf→name, nullptr);
  C∷lib∷free(inf→name);
  inf→name = name;
}

void* cleanup(obj* ob0, info* inf, void* ret) {
  if (inf ∧ ¬(inf→addr ∨ inf→name ∨ (inf→desc ≥ 0))) {
    C∷lib∷free(inf);
    ob0→data = nullptr;
  }
  return ret;
}

void* mapit(obj* ob0, info* inf) {
  void* ret = eilck∷FAILED;
  int mflags = m∷SHARED;
  if (inf→addr) return inf→addr;
  if (inf→desc < 0) {
    mflags ∪= m∷ANONYMOUS;
  } else {
    if (¬ob0→size) {
      stat st;
      if (fd∷stat(inf→desc, &st)) {
        error∷print_clear("stat");
      } else {
        ob0→size = st.st_size;
      }
    }
  }
  if (ob0→size) {
    if (¬inf→addr) {
      inf→addr = m∷map(nullptr, ob0→size, ob0→prot, mflags, inf→desc, 0);
      if (inf→addr ≡ eilck∷FAILED) {
        error∷print_clear("map prot");
        if (ob0→prot ∩ eilck∷W) {
          inf→addr = m∷map(nullptr, ob0→size, (ob0→prot ∩ ~eilck∷W), mflags, inf→desc, 0);
          if (inf→addr ≡ eilck∷FAILED) {
            inf→addr = nullptr;
            ret = eilck∷FAILED;
            error∷print_clear("map rdonly");
          }
        }
      }
    }
    if (inf→addr) return inf→addr;
  } else {
    ret = nullptr;
  }
  return ret;
}

#pragma CMOD snippet  func  = none
/** @remark This slot has a suitable default, but could be replaced if necessary. */
#pragma CMOD slot OPEN  = compatible fd, char const*, int, fmode
/** @remark This slot has a suitable default, but could be replaced if necessary. */
#pragma CMOD slot REMOVE  = compatible int, char const*
#pragma CMOD slot PREFIX  = none
#pragma CMOD slot info  = global
#pragma CMOD slot mapit = global
#pragma CMOD slot cleanup = global
#pragma CMOD slot realpath= global
#pragma CMOD slot copy  = intern
#pragma CMOD slot reopen  = intern

// protect some function parameters
#pragma CMOD slot ob0 = intern
#pragma CMOD slot name0 = intern
#pragma CMOD slot tablen  = intern
#pragma CMOD slot nsize = intern
#pragma CMOD slot cntx  = intern

#pragma CMOD definition

#pragma CMOD intern copy

static
void* copy(obj* ob0, void* cntx) {
  obj nob = { .alloc = func, };
  if (func(&nob, scaler∷CREATE, cntx) ≡ eilck∷FAILED) return eilck∷FAILED;
  void* naddr = obj∷copy(&nob, ob0);
  C∷io∷printf(C∷io∷err, "%s: copied %p to %p (%p)\n", __func__, ob0, &nob, naddr);
  if (naddr ≡ eilck∷FAILED) {
    func(&nob, scaler∷REMOVE, 0);
    func(&nob, scaler∷DESTROY, 0);
    return eilck∷FAILED;
  }
  func(ob0, scaler∷DESTROY, 0);
  ob0→alloc = func;
  ob0→size = nob.size;
  ob0→data = nob.data;
  info* inf = ob0→data;
  if (inf ∧ inf→name[0] ≠ '/') realpath(inf);
  return naddr;
}

static
void* reopen(obj* ob0, info* inf, C∷size len, char const name0[len+1],
             C∷size nsize, int oflags) {
  fmode mode = accs∷IRUSR ∪ accs∷IWUSR;
  char nname[len+1+sizeof PREFIX];
  if (name0 ∧ len) {
    C∷str∷cpy(nname, PREFIX);
    if (name0[0] ≡ '/' ∧ (1 < sizeof PREFIX) ∧ PREFIX[sizeof PREFIX - 2] ≡ '/')
      C∷str∷cat(nname, name0+1);
    else
      C∷str∷cat(nname, name0);
    name0 = nname;
    len = C∷str∷len(name0);
    if (name0[len-1] ≠ '/') {
      inf→name = C∷lib∷realloc(inf→name, len+1);
      C∷mem∷cpy(inf→name, name0, len+1);
    } else {
      if (inf→name) {
        C∷lib∷free(inf→name);
        inf→name = nullptr;
      }
    }
  }
  if (inf→name) {
    /* We come here if the name supplied names a file, not a
       directory. */
    if (inf→desc ≥ 0) {
      /* If this object already has a valid file descriptor, try to
         attach a new inode to this file descriptor. This supposes
         that the file descriptor is already living on the target file
         system, that the OS is Linux and that the /proc special file
         system is mounted. Otherwise we fall back to conventional
         opening the file and copying the contents accross. */
      char const format[] = "/proc/self/fd/%d";
      char tmpname[10 + sizeof format];
      C∷io∷snprintf(tmpname, sizeof tmpname, format, inf→desc);
      C∷io∷printf(C∷io∷err, "%s: linking fd %d (%s) to %s\n", __func__, inf→desc, tmpname, inf→name);
      if (at∷link(at∷FDCWD, tmpname, at∷FDCWD, inf→name, at∷SYMLINK_FOLLOW)) {
        int reason = C∷errno;
        error∷print_clear("at∷link");
        if (reason ≡ C∷errno∷XDEV) return copy(ob0, (void*)inf→name);
        C∷lib∷free(inf→name);
        inf→name = nullptr;
        return cleanup(ob0, inf, eilck∷FAILED);
      }
    } else if (inf→addr) {
      return copy(ob0, (void*)inf→name);
    } else {
      /* Otherwise open at the new place, first rw, and if that false
         rdonly. */
      inf→desc = OPEN(inf→name, oflags, mode);
      if (inf→desc < 0) {
        if (¬(ob0→prot ∩ eilck∷W)) {
          error∷print_clear("open rd");
          return cleanup(ob0, inf, eilck∷FAILED);
        } else {
          error∷print_clear("open rw");
          C∷io∷printf(C∷io∷err, "Now trying to open %s read only.\n", inf→name);
          oflags ^= accs∷RDWR;
          oflags ∪= accs∷RDONLY;
          inf→desc = OPEN(inf→name, oflags, mode);
          if (inf→desc < 0) {
            error∷print_clear("open rd");
            return cleanup(ob0, inf, eilck∷FAILED);
          }
        }
      }
      if (inf→name[0] ≠ '/') realpath(inf);
      C∷io∷printf(C∷io∷err, "%s: opened fd %d on %s\n", __func__, inf→desc, inf→name);
    }
  } else if (-nsize ≡ scaler∷create ∨ name0) {
    /* We come here if name0 has a trailing slash, that is, it is
       considered to be a directory and we are supposed to create a
       temporary on the same file system as that directory. We use a
       Linux specific feature, TMPFILE, but which might not be usable
       for the requested file system. In such cases fall back to
       create an anonymous object, bail out otherwise.*/
#ifdef accs∷TMPFILE
    oflags = ((ob0→prot ∩ eilck∷W) ? accs∷RDWR : accs∷RDONLY) ∪ accs∷TMPFILE;
    C∷io∷printf(C∷io∷err, "Opening temporary in %s, %llX %llX.\n", name0, (long long)oflags, (long long)mode);
    inf→desc = OPEN(name0, oflags, mode);
    // This has been the last access to name0, thus to fname, if it
    // came from there.
    if (inf→desc < 0) {
      int reason = C∷errno;
      error∷print_clear("open anonymous");
      switch (reason) {
      case C∷errno∷ISDIR: break;
      case C∷errno∷NOENT: break;
      case C∷errno∷OPNOTSUPP: break;
      default: return cleanup(ob0, inf, eilck∷FAILED);
      }
    } else {
      return nullptr;
    }
#endif
  }
  return mapit(ob0, inf);
}


/**
 ** @brief An allocator function for file descriptors.
 **
 ** This snippet must be instantiated with proper functions for OPEN
 ** and REMOVE.
 **
 ** Any instance of this will behave the same when never called with a
 ** @a cntx other than @c nullptr. Namely they will allocate an
 ** anonymous memory segment.
 **/
void* func(obj* ob0, C∷size nsize, void* cntx) {
  if (¬ob0) return eilck∷FAILED;
  int oflags = (ob0→prot ∩ eilck∷W) ? accs∷RDWR : accs∷RDONLY;
  char const*const name = cntx;
  C∷size const len = name ? C∷str∷len(name) : 0;
  if (¬ob0 ∨ ob0→alloc ≠ func) return eilck∷FAILED;
  info* inf = ob0→data;
  if (¬inf) {
    inf = C∷lib∷malloc(sizeof(info));
    if (¬inf) return eilck∷FAILED;
    *inf = (info) { .desc = -1, };
    ob0→data = inf;
  }
  // First, catch the special commands and treat them accordingly.
  if (-nsize ≤ scaler∷command—MAX) {
    switch (-nsize) {
    case scaler∷destroy:;
      C∷io∷printf(C∷io∷err, "%s: destroying %p\n", __func__, ob0);
      if (inf→name) {
        C∷lib∷free(inf→name);
        inf→name = nullptr;
      }
    case scaler∷close:;
    case scaler∷unmap:;
      if (inf→addr) {
        if (m∷unmap(inf→addr, ob0→size)) error∷print_clear("unmap");
        inf→addr = nullptr;
      }
      if (-nsize ≡ scaler∷close ∨ -nsize ≡ scaler∷destroy) {
        if (inf→desc ≥ 0) {
          if (fd∷close(inf→desc)) error∷print_clear("close");
          inf→desc = -1;
        }
        ob0→size = 0;
      }
      return cleanup(ob0, inf, nullptr);
    case scaler∷remove:;
      if (inf→name) REMOVE(inf→name);
      C∷lib∷free(inf→name);
      inf→name = nullptr;
      return cleanup(ob0, inf, nullptr);
    case scaler∷create:;
      oflags ∪= accs∷CREAT ∪ accs∷EXCL;
      return reopen(ob0, inf, len, name, nsize, oflags);
    case scaler∷open:;
      if (inf→desc ≥ 0) {
        fd∷close(inf→desc);
        inf→desc = -1;
      }
      return reopen(ob0, inf, len, name, nsize, oflags);
    case scaler∷map:;
      return mapit(ob0, inf);
    case scaler∷clear:;
      if (¬ob0→size) {
        return nullptr;
      }
      break;
    default:;
      C∷io∷printf(C∷io∷err, "%s: missing command %zu\n", __func__, -nsize);
      return eilck∷FAILED;
    }
  }
  // Otherwise, this is a real scaling request.
  if (nsize ∧ nsize ≡ ob0→size) {
    return mapit(ob0, inf);
  }
  if (inf→addr) m∷unmap(inf→addr, ob0→size);
  inf→addr = nullptr;
  if (inf→desc ≥ 0) {
    if (fd∷truncate(inf→desc, nsize)) {
      error∷print_clear("truncate");
      return eilck∷FAILED;
    }
    ob0→size = nsize;
    if (nsize) return mapit(ob0, inf);
  }
  return cleanup(ob0, inf, nullptr);
}

/**
 ** @brief Initialize @a obj such that it uses a file descriptor under
 ** the hood.
 **/
obj* init(obj* ob0, C∷size tablen) {
  obj∷init(ob0, tablen);
  if (ob0) {
    ob0→alloc = func;
  }
  return ob0;
}

#pragma CMOD declaration

#pragma CMOD foreach COMMAND = open create
inline
void* ${COMMAND}(obj* ob0, char const* name0) {
  if (ob0→data ∧ obj∷scale(ob0, scaler∷CLOSE, nullptr))
    return eilck∷FAILED;
  ob0→alloc = func;
  return func(ob0, ((C∷size)-scaler∷${COMMAND}), (void*)name0);
}
#pragma CMOD done

// FIXME
#pragma CMOD definition
