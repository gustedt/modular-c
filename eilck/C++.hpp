// This may look like C code, but it really is -*- mode: c++; coding: utf-8 -*-
//
// Except for parts copied from previous work and as explicitly stated below,
// the author and copyright holder for this work is
// Copyright (c) 2017 Jens Gustedt, INRIA, France
//
// This file is free software; it is part of the Modular C project.
//
// You can redistribute it and/or modify it under the terms of the BSD
// License as given in the file LICENSE.txt. It is distributed without
// any warranty; without even the implied warranty of merchantability or
// fitness for a particular purpose.
//
#ifndef C_hpp_GUARD
#define C_hpp_GUARD 1
#ifdef __GNUC__
#pragma interface
#endif

// Compatibility with plain C.
#include <atomic>
#include <stdexcept>
#include <typeinfo>
#include <type_traits>
#define _Atomic(T) std::atomic < T >
#define restrict

#include "C.h"
#include "C-attr.h"

template< typename T > constexpr int C_rankof        (T   ) { return 6; }
template<            > constexpr int C_rankof< bool >(bool) { return 0; }
template<            > constexpr int C_rankof< char >(char) { return 1; }
template<            > constexpr int C_rankof< unsigned char >(unsigned char) { return 1; }
template<            > constexpr int C_rankof< signed char >(signed char) { return 1; }
template<            > constexpr int C_rankof< unsigned short >(unsigned short) { return 2; }
template<            > constexpr int C_rankof< signed short >(signed short) { return 2; }
template<            > constexpr int C_rankof< unsigned int >(unsigned int) { return 3; }
template<            > constexpr int C_rankof< signed int >(signed int) { return 3; }
template<            > constexpr int C_rankof< unsigned long >(unsigned long) { return 4; }
template<            > constexpr int C_rankof< signed long >(signed long) { return 4; }
template<            > constexpr int C_rankof< unsigned long long >(unsigned long long) { return 5; }
template<            > constexpr int C_rankof< signed long long >(signed long long) { return 5; }

template< typename T > constexpr C_size C_widthof        (T    x) { return sizeof(x)*C_CHAR_WIDTH; }
template<            > constexpr C_size C_widthof< bool >(bool  ) { return C_BOOL_WIDTH; }
template<            > constexpr C_size C_widthof< char >(char  ) { return  C_CHAR_WIDTH; }
template<            > constexpr C_size C_widthof< unsigned char >(unsigned char) { return  C_UCHAR_WIDTH; }
template<            > constexpr C_size C_widthof< signed char >(signed char) { return  C_SCHAR_WIDTH; }
template<            > constexpr C_size C_widthof< unsigned short >(unsigned short) { return  C_USHRT_WIDTH; }
template<            > constexpr C_size C_widthof< signed short >(signed short) { return  C_SHRT_WIDTH; }
template<            > constexpr C_size C_widthof< unsigned int >(unsigned int) { return  C_UINT_WIDTH; }
template<            > constexpr C_size C_widthof< signed int >(signed int) { return  C_INT_WIDTH; }
template<            > constexpr C_size C_widthof< unsigned long >(unsigned long) { return  C_ULONG_WIDTH; }
template<            > constexpr C_size C_widthof< signed long >(signed long) { return  C_LONG_WIDTH; }
template<            > constexpr C_size C_widthof< unsigned long long >(unsigned long long) { return  C_ULLONG_WIDTH; }
template<            > constexpr C_size C_widthof< signed long long >(signed long long) { return  C_LLONG_WIDTH; }

template< typename T >
struct C_types {
  typedef typename std::make_unsigned< T >::type unsignedof;
  typedef typename std::make_signed< T >::type signedof;
};

template<            >
struct C_types< bool > {
  typedef bool unsignedof;
  typedef int signedof;
};

template< typename T > constexpr typename C_types< T >::unsignedof C_unsignedof        (T    x) { return x; }

template< typename T > constexpr int C_signof        (T) { return std::is_signed< T >::value; }

template< typename T > constexpr typename C_types< T >::signedof C_signedof        (T    x) { return x; }

#endif
