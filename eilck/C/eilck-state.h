/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_state__GUARD
#define eilck_state__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-bitset.h"
#include "C-snippet.h"
#include "C-attr.h"
#include "eilck.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_state
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E79435752775653476145344A65715A532F65696C636B2D73746174652E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E79435752775653476145344A65715A532F65696C636B2D73746174652E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E79435752775653476145344A65715A532F65696C636B2D73746174652E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E79435752775653476145344A65715A532F65696C636B2D73746174652E63_HEADER
#ifndef C_bitset__Operator_eval
# define C_bitset__Operator_eval(...) __VA_ARGS__
#endif

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_state_MODULE eilck_state module internals
 ** @ingroup eilck_state
 ** @{ */

enum eilck_state {
    eilck_state_invalid = -1,
    eilck_state_unlocked = C_bitset__Operator_eval(0 ),
    eilck_state_required = C_bitset_setof(0),
    eilck_state_acquired = C_bitset_setof(1),
    eilck_state_locked = C_bitset__Operator_bor(eilck_state_required, eilck_state_acquired ),
    eilck_state_increment = C_bitset_setof(2),
};
typedef enum eilck_state eilck_state;

#ifdef __cplusplus
inline
bool eilck_state_released(int _Intern__aE4JeqZS_eilck_state_x) {
    return _Intern__aE4JeqZS_eilck_state_x == eilck_state_locked;
}
#else
inline
bool eilck_state_released(eilck_state _Intern__aE4JeqZS_eilck_state_x) {
    return _Intern__aE4JeqZS_eilck_state_x == eilck_state_locked;
}
#endif

#ifdef CMOD_2F746D702F636D6F642D746D70642E79435752775653476145344A65715A532F65696C636B2D73746174652E63_HEADER_INSTANTIATE
extern inline
bool eilck_state_released(int _Intern__aE4JeqZS_eilck_state_x) ;

extern inline
bool eilck_state_released(eilck_state _Intern__aE4JeqZS_eilck_state_x) ;
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E79435752775653476145344A65715A532F65696C636B2D73746174652E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-state */
