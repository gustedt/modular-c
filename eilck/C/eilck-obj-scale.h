/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_obj_scale__GUARD
#define eilck_obj_scale__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-heap.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_obj_scale
 ** @ingroup eilck_obj
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E726642336E6F48307344795A6F5769352F65696C636B2D6F626A2D7363616C652E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E726642336E6F48307344795A6F5769352F65696C636B2D6F626A2D7363616C652E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E726642336E6F48307344795A6F5769352F65696C636B2D6F626A2D7363616C652E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E726642336E6F48307344795A6F5769352F65696C636B2D6F626A2D7363616C652E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_obj_scale_MODULE eilck_obj_scale module internals
 ** @ingroup eilck_obj_scale
 ** @{ */

inline
void* eilck_obj_scale_open(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0, void* _Intern__sDyZoWi5_eilck_obj_scale_cntx) {
    if (_Intern__sDyZoWi5_eilck_obj_scale_ob0-> data && eilck_obj_scale(_Intern__sDyZoWi5_eilck_obj_scale_ob0, eilck_obj_scaler_CLOSE, nullptr))
        return eilck_FAILED;
    return eilck_obj_scale(_Intern__sDyZoWi5_eilck_obj_scale_ob0, ((C_size)-eilck_obj_scaler_open), _Intern__sDyZoWi5_eilck_obj_scale_cntx);
}

inline
void* eilck_obj_scale_create(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0, void* _Intern__sDyZoWi5_eilck_obj_scale_cntx) {
    if (_Intern__sDyZoWi5_eilck_obj_scale_ob0-> data && eilck_obj_scale(_Intern__sDyZoWi5_eilck_obj_scale_ob0, eilck_obj_scaler_CLOSE, nullptr))
        return eilck_FAILED;
    return eilck_obj_scale(_Intern__sDyZoWi5_eilck_obj_scale_ob0, ((C_size)-eilck_obj_scaler_create), _Intern__sDyZoWi5_eilck_obj_scale_cntx);
}

inline
void* eilck_obj_scale_clear(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0) {
    return eilck_obj_scale(_Intern__sDyZoWi5_eilck_obj_scale_ob0, ((C_size)-eilck_obj_scaler_clear), nullptr);
}

inline
void* eilck_obj_scale_close(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0) {
    return eilck_obj_scale(_Intern__sDyZoWi5_eilck_obj_scale_ob0, ((C_size)-eilck_obj_scaler_close), nullptr);
}

inline
void* eilck_obj_scale_remove(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0) {
    return eilck_obj_scale(_Intern__sDyZoWi5_eilck_obj_scale_ob0, ((C_size)-eilck_obj_scaler_remove), nullptr);
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E726642336E6F48307344795A6F5769352F65696C636B2D6F626A2D7363616C652E63_HEADER_INSTANTIATE
extern inline
void* eilck_obj_scale_open(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0, void* _Intern__sDyZoWi5_eilck_obj_scale_cntx) ;

extern inline
void* eilck_obj_scale_create(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0, void* _Intern__sDyZoWi5_eilck_obj_scale_cntx) ;

extern inline
void* eilck_obj_scale_clear(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0) ;

extern inline
void* eilck_obj_scale_close(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0) ;

extern inline
void* eilck_obj_scale_remove(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0) ;
#endif

extern void* eilck_obj_scale(eilck_obj* _Intern__sDyZoWi5_eilck_obj_scale_ob0, C_size nsize, void* _Intern__sDyZoWi5_eilck_obj_scale_cntx);
#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E726642336E6F48307344795A6F5769352F65696C636B2D6F626A2D7363616C652E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-obj-scale */
