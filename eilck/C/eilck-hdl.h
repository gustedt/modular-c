/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_hdl__GUARD
#define eilck_hdl__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"
#include "eilck-impl.h"
#include "eilck-impl-SECTION.h"
#include "eilck-impl-snippet.h"
#include "eilck-ehdl.h"
#include "eilck-ihdl.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_hdl
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E507A477250504551754263724A64526E2F65696C636B2D68646C2E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E507A477250504551754263724A64526E2F65696C636B2D68646C2E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E507A477250504551754263724A64526E2F65696C636B2D68646C2E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E507A477250504551754263724A64526E2F65696C636B2D68646C2E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_hdl_MODULE eilck_hdl module internals
 ** @ingroup eilck_hdl
 ** @{ */

#ifndef __cplusplus
#define eilck_hdl_acq(H)                                                        \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].type,                                                       \
             void* : eilck_ehdl_acq, void const* : eilck_ihdl_acq) )(H)

#define eilck_hdl_acq2(H)                                                       \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].type,                                                    \
             void* : eilck_ehdl_acq2, void const* : eilck_ihdl_acq2) )((void*)(H))
#endif

#ifndef __cplusplus
#define eilck_hdl_test(H)                                                       \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].type,                                                       \
             void* : eilck_ehdl_test, void const* : eilck_ihdl_test) )(H)

#define eilck_hdl_test2(H)                                                      \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].type,                                                    \
             void* : eilck_ehdl_test2, void const* : eilck_ihdl_test2) )((void*)(H))
#endif

#ifndef __cplusplus
#define eilck_hdl_rel(H)                                                        \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].type,                                                       \
             void* : eilck_ehdl_rel, void const* : eilck_ihdl_rel) )(H)

#define eilck_hdl_rel2(H)                                                       \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].type,                                                    \
             void* : eilck_ehdl_rel2, void const* : eilck_ihdl_rel2) )((void*)(H))
#endif

#ifndef __cplusplus
#define eilck_hdl_drop(H)                                                       \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].type,                                                       \
             void* : eilck_ehdl_drop, void const* : eilck_ihdl_drop) )(H)

#define eilck_hdl_drop2(H)                                                      \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].type,                                                    \
             void* : eilck_ehdl_drop2, void const* : eilck_ihdl_drop2) )((void*)(H))
#endif

#ifndef __cplusplus
#define eilck_hdl_map(H)                                                        \
    ((H)[0].base =                                                              \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].type,                                                       \
             void* : eilck_ehdl_map, void const* : eilck_ihdl_map ))((void*)H))

#define eilck_hdl_map2(H)                                                       \
    ((H)[0]-> base =                                                            \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].type,                                                    \
             void* : eilck_ehdl_map2, void const* : eilck_ihdl_map2 ))((void*)(H)))
#endif

#ifndef __cplusplus
#define eilck_hdl_scale(H, N, C)                                                \
    ((H)[0].base =                                                              \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].type,                                                       \
             void* : eilck_ehdl_scale ))((void*)H,                              \
                     (N)*sizeof((H)[0].baze[0]), (C)))

#define eilck_hdl_scale2(H, N, C)                                               \
    ((H)[0]-> base =                                                            \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].type,                                                    \
             void* : eilck_ehdl_scale2 ))((void*)(H),                           \
                     (N)*sizeof((H)[0]-> baze[0]), (C)))
#endif

#ifndef __cplusplus
#define eilck_hdl_length(H)                                                     \
    (_Generic((char(*)[sizeof((H)[0])]){                                        \
        0}, char(*)[sizeof(eilck_impl)]:                                        \
    _Generic((H)[0].type,                                                       \
             void* : eilck_ehdl_length, void const* : eilck_ihdl_length ))((void*)H)/sizeof((H)[0].baze[0]))

#define eilck_hdl_length2(H)                                                    \
    (_Generic((char(*)[sizeof((H)[0][0])]){                                     \
        0}, char(*)[sizeof(eilck_impl[2])]:                                     \
    _Generic((H)[0][0].type,                                                    \
             void* : eilck_ehdl_length2, void const* : eilck_ihdl_length2 ))((void*)(H))/sizeof((H)[0][0].baze[0]))
#endif

#ifndef __cplusplus
#define eilck_hdl_acq_gen(H, V)                                                 \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].type,                                                       \
             void* : eilck_ehdl_acq, void const* : eilck_ihdl_acq) )(V)

#define eilck_hdl_acq2_gen(H, V)                                                \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].type,                                                    \
             void* : eilck_ehdl_acq2, void const* : eilck_ihdl_acq2) )(V)
#endif

#ifndef __cplusplus
#define eilck_hdl_rel_gen(H, V)                                                 \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].type,                                                       \
             void* : eilck_ehdl_rel, void const* : eilck_ihdl_rel) )(V)

#define eilck_hdl_rel2_gen(H, V)                                                \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].type,                                                    \
             void* : eilck_ehdl_rel2, void const* : eilck_ihdl_rel2) )(V)
#endif

#ifndef __cplusplus
#define eilck_hdl_chain(S, L)                                                   \
    _Generic((S)-> type,                                                        \
             void*: eilck_ehdl_chain, void const*: eilck_ihdl_chain)((void*)(S),\
                     (void*)(L))
#endif

inline
int _Intern__uBcrJdRn_eilck_hdl_qeri(eilck_obj* _Intern__uBcrJdRn_eilck_hdl_ob, eilck_ihdl* _Intern__uBcrJdRn_eilck_hdl_h) {
    return _Intern__uBcrJdRn_eilck_hdl_h ? eilck_ihdl_req(_Intern__uBcrJdRn_eilck_hdl_h, _Intern__uBcrJdRn_eilck_hdl_ob) : 0;
}

inline
int _Intern__uBcrJdRn_eilck_hdl_qere(eilck_obj* _Intern__uBcrJdRn_eilck_hdl_ob, eilck_ehdl* _Intern__uBcrJdRn_eilck_hdl_h) {
    return _Intern__uBcrJdRn_eilck_hdl_h ? eilck_ehdl_req(_Intern__uBcrJdRn_eilck_hdl_h, _Intern__uBcrJdRn_eilck_hdl_ob) : 0;
}

#ifndef __cplusplus
#define eilck_hdl_req(_0, _1)                                                   \
    _Generic((_0)-> type,                                                       \
             void* : eilck_ehdl_req, void const* : eilck_ihdl_req) ((void*)(_0),\
                     (_1))
#endif

#ifdef CMOD_2F746D702F636D6F642D746D70642E507A477250504551754263724A64526E2F65696C636B2D68646C2E63_HEADER_INSTANTIATE
extern inline
int _Intern__uBcrJdRn_eilck_hdl_qeri(eilck_obj* _Intern__uBcrJdRn_eilck_hdl_ob, eilck_ihdl* _Intern__uBcrJdRn_eilck_hdl_h) ;

extern inline
int _Intern__uBcrJdRn_eilck_hdl_qere(eilck_obj* _Intern__uBcrJdRn_eilck_hdl_ob, eilck_ehdl* _Intern__uBcrJdRn_eilck_hdl_h) ;
#endif

#define eilck_hdl_SECTION(H)                                                    \
    for (register bool _Intern__uBcrJdRn_eilck_hdl_se_after = false;            \
            ! _Intern__uBcrJdRn_eilck_hdl_se_after;                             \
        )                                                                       \
        for (register void*volatile _Intern__uBcrJdRn_eilck_hdl_se_point = (H); \
                ! _Intern__uBcrJdRn_eilck_hdl_se_after;                         \
            )                                                                   \
            for (eilck_hdl_acq_gen((H), _Intern__uBcrJdRn_eilck_hdl_se_point);  \
                    ! _Intern__uBcrJdRn_eilck_hdl_se_after;                     \
                    (_Intern__uBcrJdRn_eilck_hdl_se_after = true,               \
                     eilck_hdl_rel_gen((H), _Intern__uBcrJdRn_eilck_hdl_se_point)))

#define eilck_hdl_SECTION2(H)                                                   \
    for (register bool _Intern__uBcrJdRn_eilck_hdl_se2_after = false;           \
            ! _Intern__uBcrJdRn_eilck_hdl_se2_after;                            \
        )                                                                       \
        for (register void*volatile _Intern__uBcrJdRn_eilck_hdl_se2_point = (H);\
                ! _Intern__uBcrJdRn_eilck_hdl_se2_after;                        \
            )                                                                   \
            for (eilck_hdl_acq2_gen((H), _Intern__uBcrJdRn_eilck_hdl_se2_point);\
                    ! _Intern__uBcrJdRn_eilck_hdl_se2_after;                    \
                    (_Intern__uBcrJdRn_eilck_hdl_se2_after = true,              \
                     eilck_hdl_rel2_gen((H), _Intern__uBcrJdRn_eilck_hdl_se2_point)))

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

extern eilck_obj eilck_hdl_object;

extern int eilck_hdl_entry(void);
#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E507A477250504551754263724A64526E2F65696C636B2D68646C2E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-hdl */
