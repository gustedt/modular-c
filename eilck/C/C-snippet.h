/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef C_snippet__GUARD
#define C_snippet__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module C_snippet
 ** @ingroup C
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E764F6F516B686471507A61654171556E2F432D736E69707065742E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E764F6F516B686471507A61654171556E2F432D736E69707065742E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E764F6F516B686471507A61654171556E2F432D736E69707065742E63_HEADER
#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E764F6F516B686471507A61654171556E2F432D736E69707065742E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup C_snippet_MODULE C_snippet module internals
 ** @ingroup C_snippet
 ** @{ */

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E764F6F516B686471507A61654171556E2F432D736E69707065742E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* C-snippet */
