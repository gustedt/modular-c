/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_impl__GUARD
#define eilck_impl__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_impl
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E466F45326C6D417A6D504E78324F6D4B2F65696C636B2D696D706C2E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E466F45326C6D417A6D504E78324F6D4B2F65696C636B2D696D706C2E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E466F45326C6D417A6D504E78324F6D4B2F65696C636B2D696D706C2E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E466F45326C6D417A6D504E78324F6D4B2F65696C636B2D696D706C2E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_impl_MODULE eilck_impl module internals
 ** @ingroup eilck_impl
 ** @{ */

typedef struct eilck_impl eilck_impl;
struct eilck_impl {
    void* _Intern__mPNx2OmK_eilck_impl_front;
    eilck_obj* ob;
    C_size pos;
};

#define eilck_impl_INITIALIZER {                                                \
        ._Intern__mPNx2OmK_eilck_impl_front = 0,                                \
                                                                                \
                .ob = 0,                                                        \
                      .pos = 0, }

C_attr_pure

inline
C_size eilck_impl_length(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) {
    return _Intern__mPNx2OmK_eilck_impl_h ? eilck_obj_length(_Intern__mPNx2OmK_eilck_impl_h-> ob) : 0;
}

C_attr_pure

inline
C_size eilck_impl_length_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) {
    bool phase = ! _Intern__mPNx2OmK_eilck_impl_h0-> ob;
    if (phase) return eilck_impl_length(_Intern__mPNx2OmK_eilck_impl_h1);
    else return eilck_impl_length(_Intern__mPNx2OmK_eilck_impl_h0);
}

C_attr_pure

inline
int eilck_impl_length2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) {
    return eilck_impl_length_2(&(*_Intern__mPNx2OmK_eilck_impl_h)[0], &(*_Intern__mPNx2OmK_eilck_impl_h)[1]);
}

inline
int eilck_impl_acq(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) {
    return eilck_obj_acq(_Intern__mPNx2OmK_eilck_impl_h-> ob, _Intern__mPNx2OmK_eilck_impl_h-> pos);
}

inline
int eilck_impl_acq_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) {
    bool phase = ! _Intern__mPNx2OmK_eilck_impl_h0-> ob;
    if (phase) return eilck_impl_acq(_Intern__mPNx2OmK_eilck_impl_h1);
    else return eilck_impl_acq(_Intern__mPNx2OmK_eilck_impl_h0);
}

inline
int eilck_impl_acq2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) {
    return eilck_impl_acq_2(&(*_Intern__mPNx2OmK_eilck_impl_h)[0], &(*_Intern__mPNx2OmK_eilck_impl_h)[1]);
}

inline
bool eilck_impl_test(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) {
    return (_Intern__mPNx2OmK_eilck_impl_h && _Intern__mPNx2OmK_eilck_impl_h-> ob)
           ? eilck_obj_test(_Intern__mPNx2OmK_eilck_impl_h-> ob, _Intern__mPNx2OmK_eilck_impl_h-> pos)
           : 0;
}

inline
bool eilck_impl_test_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) {
    bool phase = ! _Intern__mPNx2OmK_eilck_impl_h0-> ob;
    if (phase) return eilck_impl_test(_Intern__mPNx2OmK_eilck_impl_h1);
    else return eilck_impl_test(_Intern__mPNx2OmK_eilck_impl_h0);
}

inline
bool eilck_impl_test2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) {
    if (! _Intern__mPNx2OmK_eilck_impl_h) return 0;
    return eilck_impl_test_2(&(*_Intern__mPNx2OmK_eilck_impl_h)[0], &(*_Intern__mPNx2OmK_eilck_impl_h)[1]);
}

inline
void eilck_impl_rel(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) {
    if (_Intern__mPNx2OmK_eilck_impl_h && _Intern__mPNx2OmK_eilck_impl_h-> ob) {
        eilck_obj_rel(_Intern__mPNx2OmK_eilck_impl_h-> ob, _Intern__mPNx2OmK_eilck_impl_h-> pos);
        *_Intern__mPNx2OmK_eilck_impl_h = (eilck_impl)eilck_impl_INITIALIZER;
    }
}

inline
void* eilck_impl_map(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) {
    if (_Intern__mPNx2OmK_eilck_impl_h-> ob && eilck_impl_acq(_Intern__mPNx2OmK_eilck_impl_h))
        return eilck_obj_map(_Intern__mPNx2OmK_eilck_impl_h-> ob);
    return 0;
}

inline
void* eilck_impl_map_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) {
    bool phase = ! _Intern__mPNx2OmK_eilck_impl_h0-> ob;
    if (phase) return eilck_impl_map(_Intern__mPNx2OmK_eilck_impl_h1);
    else return eilck_impl_map(_Intern__mPNx2OmK_eilck_impl_h0);
}

inline
void* eilck_impl_map2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) {
    return eilck_impl_map_2(&(*_Intern__mPNx2OmK_eilck_impl_h)[0], &(*_Intern__mPNx2OmK_eilck_impl_h)[1]);
}

inline
void eilck_impl_drop(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) {
    if (_Intern__mPNx2OmK_eilck_impl_h && _Intern__mPNx2OmK_eilck_impl_h-> ob) {
        eilck_obj_acq(_Intern__mPNx2OmK_eilck_impl_h-> ob, _Intern__mPNx2OmK_eilck_impl_h-> pos);
        eilck_obj_rel(_Intern__mPNx2OmK_eilck_impl_h-> ob, _Intern__mPNx2OmK_eilck_impl_h-> pos);
        *_Intern__mPNx2OmK_eilck_impl_h = (eilck_impl)eilck_impl_INITIALIZER;
    }
}

inline
void eilck_impl_drop_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) {
    eilck_impl_drop(_Intern__mPNx2OmK_eilck_impl_h0);
    eilck_impl_drop(_Intern__mPNx2OmK_eilck_impl_h1);
}

inline
void eilck_impl_drop2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) {
    if (! _Intern__mPNx2OmK_eilck_impl_h) return;
    return eilck_impl_drop_2(&(*_Intern__mPNx2OmK_eilck_impl_h)[0], &(*_Intern__mPNx2OmK_eilck_impl_h)[1]);
}

inline
int eilck_impl_req(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h, eilck_obj* _Intern__mPNx2OmK_eilck_impl_ob0, bool _Intern__mPNx2OmK_eilck_impl_excl) {
    _Intern__mPNx2OmK_eilck_impl_h-> pos = _Intern__mPNx2OmK_eilck_impl_excl ? eilck_obj_ereq(_Intern__mPNx2OmK_eilck_impl_ob0) : eilck_obj_ireq(_Intern__mPNx2OmK_eilck_impl_ob0);
    if (_Intern__mPNx2OmK_eilck_impl_h-> pos)
        _Intern__mPNx2OmK_eilck_impl_h-> ob = _Intern__mPNx2OmK_eilck_impl_ob0;
    else
        *_Intern__mPNx2OmK_eilck_impl_h = (eilck_impl)eilck_impl_INITIALIZER;
    return _Intern__mPNx2OmK_eilck_impl_h-> pos;
}

inline
eilck_impl const* eilck_impl_chain(eilck_impl const* _Intern__mPNx2OmK_eilck_impl_sooner, eilck_impl* _Intern__mPNx2OmK_eilck_impl_later, bool _Intern__mPNx2OmK_eilck_impl_excl) {
    eilck_impl_req(_Intern__mPNx2OmK_eilck_impl_later, _Intern__mPNx2OmK_eilck_impl_sooner-> ob, _Intern__mPNx2OmK_eilck_impl_excl);
    return _Intern__mPNx2OmK_eilck_impl_later;
}

inline
void eilck_impl_rel_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1, bool _Intern__mPNx2OmK_eilck_impl_excl) {
    bool phase = ! _Intern__mPNx2OmK_eilck_impl_h0-> ob;
    if (phase) {
        eilck_impl_req(_Intern__mPNx2OmK_eilck_impl_h0, _Intern__mPNx2OmK_eilck_impl_h1-> ob, _Intern__mPNx2OmK_eilck_impl_excl);
        eilck_impl_rel(_Intern__mPNx2OmK_eilck_impl_h1);
    } else {
        eilck_impl_req(_Intern__mPNx2OmK_eilck_impl_h1, _Intern__mPNx2OmK_eilck_impl_h0-> ob, _Intern__mPNx2OmK_eilck_impl_excl);
        eilck_impl_rel(_Intern__mPNx2OmK_eilck_impl_h0);
    }
}

inline
void eilck_impl_rel2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2], bool _Intern__mPNx2OmK_eilck_impl_excl) {
    if (! _Intern__mPNx2OmK_eilck_impl_h) return;
    eilck_impl_rel_2(&(*_Intern__mPNx2OmK_eilck_impl_h)[0], &(*_Intern__mPNx2OmK_eilck_impl_h)[1], _Intern__mPNx2OmK_eilck_impl_excl);
}

inline
void* eilck_impl_scale(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h, C_size _Intern__mPNx2OmK_eilck_impl_nsize, void* _Intern__mPNx2OmK_eilck_impl_cntx) {
    if (! eilck_impl_test(_Intern__mPNx2OmK_eilck_impl_h)) return 0;
    return eilck_obj_scale(_Intern__mPNx2OmK_eilck_impl_h-> ob, _Intern__mPNx2OmK_eilck_impl_nsize, _Intern__mPNx2OmK_eilck_impl_cntx);
}

inline
void* eilck_impl_scale_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1, C_size _Intern__mPNx2OmK_eilck_impl_nsize, void* _Intern__mPNx2OmK_eilck_impl_cntx) {
    bool phase = ! _Intern__mPNx2OmK_eilck_impl_h0-> ob;
    if (phase) return eilck_impl_scale(_Intern__mPNx2OmK_eilck_impl_h1, _Intern__mPNx2OmK_eilck_impl_nsize, _Intern__mPNx2OmK_eilck_impl_cntx);
    else return eilck_impl_scale(_Intern__mPNx2OmK_eilck_impl_h0, _Intern__mPNx2OmK_eilck_impl_nsize, _Intern__mPNx2OmK_eilck_impl_cntx);
}

inline
void* eilck_impl_scale2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2], C_size _Intern__mPNx2OmK_eilck_impl_nsize, void* _Intern__mPNx2OmK_eilck_impl_cntx) {
    if (! _Intern__mPNx2OmK_eilck_impl_h) return 0;
    return eilck_impl_scale_2(&(*_Intern__mPNx2OmK_eilck_impl_h)[0], &(*_Intern__mPNx2OmK_eilck_impl_h)[1], _Intern__mPNx2OmK_eilck_impl_nsize, _Intern__mPNx2OmK_eilck_impl_cntx);
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E466F45326C6D417A6D504E78324F6D4B2F65696C636B2D696D706C2E63_HEADER_INSTANTIATE
extern C_attr_pure

inline
C_size eilck_impl_length(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) ;

extern C_attr_pure

inline
C_size eilck_impl_length_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) ;

extern C_attr_pure

inline
int eilck_impl_length2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) ;

extern inline
int eilck_impl_acq(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) ;

extern inline
int eilck_impl_acq_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) ;

extern inline
int eilck_impl_acq2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) ;

extern inline
bool eilck_impl_test(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) ;

extern inline
bool eilck_impl_test_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) ;

extern inline
bool eilck_impl_test2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) ;

extern inline
void eilck_impl_rel(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) ;

extern inline
void* eilck_impl_map(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) ;

extern inline
void* eilck_impl_map_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) ;

extern inline
void* eilck_impl_map2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) ;

extern inline
void eilck_impl_drop(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h) ;

extern inline
void eilck_impl_drop_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1) ;

extern inline
void eilck_impl_drop2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2]) ;

extern inline
int eilck_impl_req(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h, eilck_obj* _Intern__mPNx2OmK_eilck_impl_ob0, bool _Intern__mPNx2OmK_eilck_impl_excl) ;

extern inline
eilck_impl const* eilck_impl_chain(eilck_impl const* _Intern__mPNx2OmK_eilck_impl_sooner, eilck_impl* _Intern__mPNx2OmK_eilck_impl_later, bool _Intern__mPNx2OmK_eilck_impl_excl) ;

extern inline
void eilck_impl_rel_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1, bool _Intern__mPNx2OmK_eilck_impl_excl) ;

extern inline
void eilck_impl_rel2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2], bool _Intern__mPNx2OmK_eilck_impl_excl) ;

extern inline
void* eilck_impl_scale(eilck_impl* _Intern__mPNx2OmK_eilck_impl_h, C_size _Intern__mPNx2OmK_eilck_impl_nsize, void* _Intern__mPNx2OmK_eilck_impl_cntx) ;

extern inline
void* eilck_impl_scale_2(eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h0, eilck_impl* restrict _Intern__mPNx2OmK_eilck_impl_h1, C_size _Intern__mPNx2OmK_eilck_impl_nsize, void* _Intern__mPNx2OmK_eilck_impl_cntx) ;

extern inline
void* eilck_impl_scale2(eilck_impl (*_Intern__mPNx2OmK_eilck_impl_h)[2], C_size _Intern__mPNx2OmK_eilck_impl_nsize, void* _Intern__mPNx2OmK_eilck_impl_cntx) ;
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E466F45326C6D417A6D504E78324F6D4B2F65696C636B2D696D706C2E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-impl */
