/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef C__GUARD
#define C__GUARD 1

/* The automatically deduced dependencies: */


#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module C
 ** @ingroup C
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E75683838704D5144417A6C4F67556E492F432E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E75683838704D5144417A6C4F67556E492F432E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E75683838704D5144417A6C4F67556E492F432E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E75683838704D5144417A6C4F67556E492F432E63_HEADER

#define C_GNUC_PATCHLEVEL__ 0

#define __Cmod_offsetof(TYPE,MEMBER) __builtin_offsetof (TYPE, MEMBER)

#define C_GNUC_PREREQ(maj,min)                                                  \
    ((__GNUC__ << 16) + __GNUC_MINOR__ >= ((maj) << 16) + (min))

#define __NULL nullptr

#define C_GNUC_VA_LIST

#define C_GNUC_STDC_INLINE__ 1

#define _Complex_I (__extension__ 1.0iF)

#define C_GNUC_MINOR__ 2

#define __Cmod_I _Complex_I

#define __Cmod_size unsigned long

#define __Cmod_size_signed signed long

#define __Cmod_ptrdiff long

#define __Cmod_ptrdiff_unsigned unsigned long

#define __Cmod_wchar int

#define __Cmod_char16 int

#define __Cmod_char32 unsigned int

#define __Cmod_uintptr unsigned long

#define __Cmod_intptr long

#define __Cmod_uintmax unsigned long

#define __Cmod_intmax long

#define __Cmod_wint unsigned int

#define __Cmod_ascii 1

#define C_LINE_MAX 2147483647

#define C_LINENO_MAX 9223372036854775807L

#define C_STDC 1

#define C_BOOL_WIDTH 8

#define C_CHAR_WIDTH 8

#define C_CHAR_BIT 8

#define C_CHAR_SIGN 1

#define C_UCHAR_WIDTH 8

#define C_USHRT_WIDTH 16

#define C_UINT_WIDTH 32

#define C_ULONG_WIDTH 64

#define C_ULLONG_WIDTH 64

#define C_SCHAR_WIDTH 8

#define C_SHRT_WIDTH 16

#define C_INT_WIDTH 32

#define C_LONG_WIDTH 64

#define C_LLONG_WIDTH 64

#define C_MB_LEN_MAX 16

#define C_SIZE_RANK 4UL

#define C_PTRDIFF_RANK 4UL

#define C_WCHAR_RANK 3UL

#define C_WINT_RANK 3UL

#define C_UINTMAX_RANK 4UL

#define C_INTMAX_RANK 4UL

#define C_SIZE_WIDTH 64UL

#define C_PTRDIFF_WIDTH 64UL

#define C_WCHAR_WIDTH 32UL

#define C_WINT_WIDTH 32UL

#define C_UINTMAX_WIDTH 64UL

#define C_INTMAX_WIDTH 64UL

#define C_SIZE_SIZE 8UL

#define C_PTRDIFF_SIZE 8UL

#define C_WCHAR_SIZE 4UL

#define C_WINT_SIZE 4UL

#define C_UINTMAX_SIZE 8UL

#define C_INTMAX_SIZE 8UL

#define C_RSIZE_MAX 9223372036854775807UL

#define _Intern_C_SIGNATURE 0x0fe1c08e05c09811

#define C_HOSTED 1

#define C_UTF_16 1

#define C_IEC_559 1

#define C_ISO_10646 201706L

#define C_NO_THREADS 1

#define C_IEC_559_COMPLEX 1

#define C_VERSION 201112L

#define C_UTF_32 1

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup C_MODULE C module internals
 ** @ingroup C
 ** @{ */

#if (defined(__GNUC_GNU_INLINE__) || defined(__GNUC_STDC_INLINE__))
#define C_has_gnu_inline 1
#else
#define C_has_gnu_inline 0
#endif

#if C_has_gnu_inline
#define C_own_inline inline __attribute__((__gnu_inline__,                      \
        __visibility__("default")))

#define C_imp_inline extern inline __attribute__((__gnu_inline__,               \
        __visibility__("default")))
#else
#define C_own_inline

#define C_imp_inline inline
#endif

typedef __Cmod_size C_size;

typedef __Cmod_size_signed C_size_signed;

typedef __Cmod_ptrdiff C_ptrdiff;

typedef __Cmod_ptrdiff_unsigned C_ptrdiff_unsigned;

typedef __Cmod_wchar C_wchar;

typedef __Cmod_char16 C_char16;

typedef __Cmod_char32 C_char32;

typedef __Cmod_uintptr C_uintptr;

typedef __Cmod_intptr C_intptr;

typedef __Cmod_uintmax C_uintmax;

typedef __Cmod_intmax C_intmax;

typedef __Cmod_wint C_wint;

enum _MODULE_tag_AzlOgUnI_206 { C_ascii = __Cmod_ascii, };
typedef enum _MODULE_tag_AzlOgUnI_206 _MODULE_tag_AzlOgUnI_206;
#if !defined(__CMOD__)
#if !defined(__cplusplus)
#define nullptr ((void*)0)
#endif
#ifndef NULL
#define NULL __NULL
#endif
#ifndef offsetof
#define offsetof __offsetof
#endif
#endif

#define C_DATE __DATE__

#define C_FILE __FILE__

#define __LINE_MAX 2147483647

#define C_LINE (__LINE__ <= C_LINE_MAX ? __LINE__ : -C_LINE_MAX)

#define C_LINENO ((__Cmod_intmax)+__LINE__)

#define C_TIME __TIME__

#ifndef C_ISO_10646
#define C_ISO_10646 000000L
#endif
#if C_ISO_10646 != __STDC_ISO_10646__
static_assert(false, "inconsistent setting of __STDC_ISO_10646__ with precompiled C library");
#endif
#ifndef C_MB_MIGHT_NEQ_WC
#define C_MB_MIGHT_NEQ_WC 0
#endif
#if C_MB_MIGHT_NEQ_WC != __STDC_MB_MIGHT_NEQ_WC__
static_assert(false, "inconsistent setting of __STDC_MB_MIGHT_NEQ_WC__ with precompiled C library");
#endif
#ifndef C_UTF_16
#define C_UTF_16 0
#endif
#if C_UTF_16 != __STDC_UTF_16__
static_assert(false, "inconsistent setting of __STDC_UTF_16__ with precompiled C library");
#endif
#ifndef C_UTF_32
#define C_UTF_32 0
#endif
#if C_UTF_32 != __STDC_UTF_32__
static_assert(false, "inconsistent setting of __STDC_UTF_32__ with precompiled C library");
#endif
#ifndef C_ANALYZABLE
#define C_ANALYZABLE 0
#endif
#if C_ANALYZABLE != __STDC_ANALYZABLE__
static_assert(false, "inconsistent setting of __STDC_ANALYZABLE__ with precompiled C library");
#endif
#ifndef C_IEC_559
#define C_IEC_559 0
#endif
#if C_IEC_559 != __STDC_IEC_559__
static_assert(false, "inconsistent setting of __STDC_IEC_559__ with precompiled C library");
#endif
#ifndef C_IEC_559_COMPLEX
#define C_IEC_559_COMPLEX 0
#endif
#if C_IEC_559_COMPLEX != __STDC_IEC_559_COMPLEX__
static_assert(false, "inconsistent setting of __STDC_IEC_559_COMPLEX__ with precompiled C library");
#endif
#ifndef C_LIB_EXT1
#define C_LIB_EXT1 000000L
#endif
#if C_LIB_EXT1 != __STDC_LIB_EXT1__
static_assert(false, "inconsistent setting of __STDC_LIB_EXT1__ with precompiled C library");
#endif
#ifndef C_NO_ATOMICS
#define C_NO_ATOMICS 0
#endif
#if C_NO_ATOMICS != __STDC_NO_ATOMICS__
static_assert(false, "inconsistent setting of __STDC_NO_ATOMICS__ with precompiled C library");
#endif
#ifndef C_NO_COMPLEX
#define C_NO_COMPLEX 0
#endif
#if C_NO_COMPLEX != __STDC_NO_COMPLEX__
static_assert(false, "inconsistent setting of __STDC_NO_COMPLEX__ with precompiled C library");
#endif
#ifndef C_NO_THREADS
#define C_NO_THREADS 0
#endif
#if C_NO_THREADS != __STDC_NO_THREADS__
static_assert(false, "inconsistent setting of __STDC_NO_THREADS__ with precompiled C library");
#endif
#ifndef C_NO_VLA
#define C_NO_VLA 0
#endif
#if C_NO_VLA != __STDC_NO_VLA__
static_assert(false, "inconsistent setting of __STDC_NO_VLA__ with precompiled C library");
#endif

#if !defined(__CMOD__) && !defined(__cplusplus)

#define static_assert _Static_assert

#ifdef __I

#define complex _Complex

#define I __I
#ifdef _Imaginary
#define imaginary _Imaginary
#endif
#endif

#define noreturn _Noreturn

#define alignas _Alignas

#define alignof _Alignof

#define bool _Bool

#define false 0

#define true 1
#endif

#define C_and &&

#define C_bitand &

#define C_bitor |

#define C_compl ~

#define C_not !

#define C_or ||

#define C_xor ^

#define C_not_eq !=

#define C_or_eq |=

#define C_and_eq &=

#define C_xor_eq ^=

#ifndef __cplusplus
#define C_rankof(X)                                                             \
    _Generic((X),                                                               \
             bool: 0, char: 1, unsigned char: 1, signed char: 1, unsigned short: 2, signed short: 2, unsigned: 3, signed: 3, unsigned long: 4, signed long: 4, unsigned long long: 5, signed long long: 5,\
             default: 6)

#define C_widthof(X)                                                            \
    _Generic((X),                                                               \
             bool: C_BOOL_WIDTH, char: C_CHAR_WIDTH, unsigned char: C_UCHAR_WIDTH, signed char: C_SCHAR_WIDTH, unsigned short: C_USHRT_WIDTH, signed short: C_SHRT_WIDTH, unsigned: C_UINT_WIDTH, signed: C_INT_WIDTH, unsigned long: C_ULONG_WIDTH, signed long: C_LONG_WIDTH, unsigned long long: C_ULLONG_WIDTH,\
             signed long long: C_LLONG_WIDTH,                                   \
                                                                                \
             default: sizeof(X)*C_CHAR_WIDTH)

#define C_signof(X)                                                             \
    _Generic((X),                                                               \
             bool: 0, char: C_CHAR_SIGN, unsigned char: 0, signed char: 1, unsigned short: 0, signed short: 1, unsigned int: 0, signed int: 1, unsigned long: 0, signed long: 1, unsigned long long: 0, signed long long: 1,\
             default: 0)

#define C_unsignedof(X)                                                         \
    _Generic((X),                                                               \
             char : (unsigned char )(X), signed char : (unsigned char )(X), signed short : (unsigned short )(X), signed : (unsigned )(X), signed long : (unsigned long )(X), signed long long: (unsigned long long)(X),\
                                                                                \
             default: (X))

#define C_signedof(X)                                                           \
    _Generic((X),                                                               \
             bool : !! (X), char : (signed char )(X), unsigned char : (signed char )(X), unsigned short : (signed short )(X), unsigned : (signed )(X), unsigned long : (signed long )(X), unsigned long long: (signed long long)(X),\
                                                                                \
             default: (X))

#endif

#define __FEATURE12_(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _A, _B)            \
    _ZN2_C7_Intern1C51module_features##_##_0##_##_1##_##_2##_##_3##_##_4##_##_5##_##_6##_##_7##_##_8##_##_9##_##_A##_##_B##E

#define __FEATURE12(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _A, _B)             \
    __FEATURE12_(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _A, _B)

typedef signed char C_schar;

typedef unsigned char C_uchar;

typedef unsigned short C_ushort;

typedef unsigned long C_ulong;

typedef signed long long C_llong;

typedef unsigned long long C_ullong;

typedef long double C_ldouble;

typedef char* C_charp;

typedef char const* C_charconstp;

#define C_STRINGIFY(...) C_STRINGIFY0(__VA_ARGS__)

#define C_STRINGIFY0(...)#__VA_ARGS__

#define C_FIRST(...)                                                            \
    _Intern__AzlOgUnI_C_FIRST0(__VA_ARGS__,                                     \
                                       0)

#define _Intern__AzlOgUnI_C_FIRST0(_0, ...) _0

#define C_SECOND(...)                                                           \
    _Intern__AzlOgUnI_C_SECOND0(__VA_ARGS__,                                    \
                                        0, 0)

#define _Intern__AzlOgUnI_C_SECOND0(_0, _1, ...) _1

#define C_HAS2(...)                                                             \
    _Intern__AzlOgUnI_C_HAS0(__VA_ARGS__,                                       \
                                     1, 0, 0)

#define _Intern__AzlOgUnI_C_HAS0(_0, _1, _2, ...) _2

#define C_OVERLAY(S, ...)                                                       \
    ((union {                                                                   \
        __VA_ARGS__ _Intern__AzlOgUnI_C_val;                                    \
        unsigned char _Intern__AzlOgUnI_C_bytes[sizeof(__VA_ARGS__)];           \
    }){                                                                         \
        ._Intern__AzlOgUnI_C_bytes = S,                                         \
    }._Intern__AzlOgUnI_C_val)

extern unsigned long long volatile const C_MODULE_ABI[];

extern unsigned long long const volatile __FEATURE12(
    C_ISO_10646,
    C_MB_MIGHT_NEQ_WC,
    C_UTF_16,
    C_UTF_32,
    C_ANALYZABLE,
    C_IEC_559,
    C_IEC_559_COMPLEX,
    C_LIB_EXT1,
    C_NO_ATOMICS,
    C_NO_COMPLEX,
    C_NO_THREADS,
    C_NO_VLA);

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E75683838704D5144417A6C4F67556E492F432E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* C */
