/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_csv__GUARD
#define eilck_csv__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "eilck.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_csv
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E7A7172314A6E6B6D7245616E506F4D742F65696C636B2D6373762E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E7A7172314A6E6B6D7245616E506F4D742F65696C636B2D6373762E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E7A7172314A6E6B6D7245616E506F4D742F65696C636B2D6373762E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E7A7172314A6E6B6D7245616E506F4D742F65696C636B2D6373762E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_csv_MODULE eilck_csv module internals
 ** @ingroup eilck_csv
 ** @{ */

extern unsigned long long eilck_csv_start;

#define eilck_csv_FIXED ",,,,,,,,,,,,,,,,,,"

enum eilck_csv_dummy { eilck_csv_flen = sizeof eilck_csv_FIXED - 1, };
typedef enum eilck_csv_dummy eilck_csv_dummy;

#ifdef __CMOD__
inline
void eilck_csv_commas(C_io* _Intern__rEanPoMt_eilck_csv_s, eilck_csv_dummy _Intern__rEanPoMt_eilck_csv_n) {
    if (_Intern__rEanPoMt_eilck_csv_s) {
        for (; _Intern__rEanPoMt_eilck_csv_n > 0; _Intern__rEanPoMt_eilck_csv_n -= eilck_csv_flen)
            C_io_fwrite(eilck_csv_FIXED, (_Intern__rEanPoMt_eilck_csv_n < eilck_csv_flen) ? _Intern__rEanPoMt_eilck_csv_n : eilck_csv_flen, 1, _Intern__rEanPoMt_eilck_csv_s);
    }
}

inline
unsigned long long eilck_csv_stamp(void) {
    C_time_spec _Intern__rEanPoMt_eilck_csv_now;
    C_time_spec_get(&_Intern__rEanPoMt_eilck_csv_now, C_time_UTC);
    unsigned long long _Intern__rEanPoMt_eilck_csv_ret = _Intern__rEanPoMt_eilck_csv_now.tv_sec;
    _Intern__rEanPoMt_eilck_csv_ret *= 1000000000;
    _Intern__rEanPoMt_eilck_csv_ret += _Intern__rEanPoMt_eilck_csv_now.tv_nsec;
    return _Intern__rEanPoMt_eilck_csv_ret - eilck_csv_start;
}
#else
void eilck_csv_commas(C_io* _Intern__rEanPoMt_eilck_csv_s, eilck_csv_dummy _Intern__rEanPoMt_eilck_csv_n);
unsigned long long eilck_csv_stamp(void);
#endif

#ifdef CMOD_2F746D702F636D6F642D746D70642E7A7172314A6E6B6D7245616E506F4D742F65696C636B2D6373762E63_HEADER_INSTANTIATE
extern inline
void eilck_csv_commas(C_io* _Intern__rEanPoMt_eilck_csv_s, eilck_csv_dummy _Intern__rEanPoMt_eilck_csv_n) ;

extern inline
unsigned long long eilck_csv_stamp(void) ;
#endif

extern void eilck_csv_mark(C_io* _Intern__rEanPoMt_eilck_csv_log, C_size _Intern__rEanPoMt_eilck_csv_task, C_size _Intern__rEanPoMt_eilck_csv_iter, C_size _Intern__rEanPoMt_eilck_csv_φ);

extern void eilck_csv_header(C_io* _Intern__rEanPoMt_eilck_csv_log, C_size _Intern__rEanPoMt_eilck_csv_n, char const head[], char const format[]);

extern C_io* eilck_csv_open(char const name[], C_size _Intern__rEanPoMt_eilck_csv_n, char const head[], char const format[]);

extern void eilck_csv_startup(void);
#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E7A7172314A6E6B6D7245616E506F4D742F65696C636B2D6373762E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-csv */
