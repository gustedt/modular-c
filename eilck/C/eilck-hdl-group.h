/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_hdl_group__GUARD
#define eilck_hdl_group__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-snippet.h"
#include "C-attr.h"
#include "C-snippet-flexible.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-fixed.h"
#include "eilck-sem.h"
#include "eilck-obj-blks.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"
#include "eilck-impl.h"
#include "eilck-impl-snippet.h"
#include "eilck-ehdl.h"
#include "eilck-ihdl.h"
#include "eilck-csv.h"
#include "eilck-impl-SECTION.h"
#include "eilck-hdl.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_hdl_group
 ** @ingroup eilck_hdl
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E6E72696A7736595A69395574625237592F65696C636B2D68646C2D67726F75702E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E6E72696A7736595A69395574625237592F65696C636B2D68646C2D67726F75702E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E6E72696A7736595A69395574625237592F65696C636B2D68646C2D67726F75702E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E6E72696A7736595A69395574625237592F65696C636B2D68646C2D67726F75702E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_hdl_group_MODULE eilck_hdl_group module internals
 ** @ingroup eilck_hdl_group
 ** @{ */

typedef struct eilck_hdl_group_ihdl2 eilck_hdl_group_ihdl2;
struct eilck_hdl_group_ihdl2 {
    eilck_ihdl pair[2];
};

#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert(sizeof((eilck_hdl_group_ihdl2) {
    0
}), "T in C#snippet#flexible must be a complete type");
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert( !C_HAS2( 1) || (C_FIRST( 1) <= C_SECOND( 1)), "bounds inverted: " C_STRINGIFY(C_FIRST( 1)) " is > " C_STRINGIFY(C_SECOND( 1)));
static_assert(C_FIRST( 1) <= C_snippet_flexible_min, "min is < " C_STRINGIFY(C_FIRST( 1)));
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_hdl_group__MODULE_CON(I6_Block1C7snippet8flexible4INIT10_Snippet_7E)(void) {
    if (0)
        C_attr_maybe_unused eilck_hdl_group_ihdl2 x[C_snippet_flexible_min] = C_snippet_flexible_INIT;
};
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_hdl_group__MODULE_CON(I6_Block1C7snippet8flexible6OTHERS10_Snippet_7E)(void) {
    if (0) {
        C_attr_maybe_unused struct {
            C_size len;
            C_snippet_flexible_OTHERS
        } _Intern__i9UtbR7Y_eilck_hdl_group_ih_dummy = { .len = 67 };
    }
};
#endif

typedef struct {
    C_size len;
    C_snippet_flexible_OTHERS eilck_hdl_group_ihdl2 tab[];
} eilck_hdl_group_ih_offset_type;

enum _Intern__i9UtbR7Y_eilck_hdl_group_ih_edummy {
    _Intern__i9UtbR7Y_eilck_hdl_group_ih_offset = __Cmod_offsetof(eilck_hdl_group_ih_offset_type, tab),
};
typedef enum _Intern__i9UtbR7Y_eilck_hdl_group_ih_edummy _Intern__i9UtbR7Y_eilck_hdl_group_ih_edummy;

typedef struct {
    C_size _Intern__i9UtbR7Y_eilck_hdl_group_ih_mutableLen;
    C_snippet_flexible_OTHERS
} _Intern__i9UtbR7Y_eilck_hdl_group_ih_header;

typedef union eilck_hdl_group_ih_fa eilck_hdl_group_ih_fa;
typedef union eilck_hdl_group_ih_fa eilck_hdl_group_ih_fa;

typedef union eilck_hdl_group_ih_fa eilck_hdl_group_ih_fa;
union eilck_hdl_group_ih_fa {

    struct {

        union {
            unsigned char _Intern__i9UtbR7Y_eilck_hdl_group_ih_chars[_Intern__i9UtbR7Y_eilck_hdl_group_ih_offset];
            _Intern__i9UtbR7Y_eilck_hdl_group_ih_header head;
        };
        eilck_hdl_group_ihdl2 _Intern__i9UtbR7Y_eilck_hdl_group_ih_fixedTab[C_snippet_flexible_min];
    } _Intern__i9UtbR7Y_eilck_hdl_group_ih_overlay;
    _Intern__i9UtbR7Y_eilck_hdl_group_ih_edummy _Intern__i9UtbR7Y_eilck_hdl_group_ih_dummy;

    struct {
        C_size const len;
        C_snippet_flexible_OTHERS
        eilck_hdl_group_ihdl2 tab[];
    };
};

#define eilck_hdl_group_ih_COMPOUND(LEN)                                        \
    &((union {                                                                  \
        unsigned char _Intern__i9UtbR7Y_eilck_hdl_group_ih_maximum[_Intern__i9UtbR7Y_eilck_hdl_group_ih_offset+sizeof(eilck_hdl_group_ihdl2[LEN])];\
        struct {                                                                \
            union {                                                             \
                unsigned char _Intern__i9UtbR7Y_eilck_hdl_group_ih_chars[_Intern__i9UtbR7Y_eilck_hdl_group_ih_offset];\
                _Intern__i9UtbR7Y_eilck_hdl_group_ih_header head;               \
            };                                                                  \
            eilck_hdl_group_ihdl2 _Intern__i9UtbR7Y_eilck_hdl_group_ih_fixedTab[LEN];\
        } _Intern__i9UtbR7Y_eilck_hdl_group_ih_overlay;                         \
        eilck_hdl_group_ih_fa _Intern__i9UtbR7Y_eilck_hdl_group_ih_dummy;       \
    }){                                                                         \
        ._Intern__i9UtbR7Y_eilck_hdl_group_ih_overlay = {                       \
                                                                 .head = {      \
                                                                          ._Intern__i9UtbR7Y_eilck_hdl_group_ih_mutableLen = (LEN),\
                                                                         }, ._Intern__i9UtbR7Y_eilck_hdl_group_ih_fixedTab = C_snippet_flexible_INIT,\
                                                                }, })._Intern__i9UtbR7Y_eilck_hdl_group_ih_dummy

extern eilck_hdl_group_ih_fa* eilck_hdl_group_ih_alloc(C_size _Intern__i9UtbR7Y_eilck_hdl_group_ih_length);

typedef int __internal_dummy_type_to_be_ignored;

typedef struct eilck_hdl_group_ehdl2 eilck_hdl_group_ehdl2;
struct eilck_hdl_group_ehdl2 {
    eilck_ehdl pair[2];
};

#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert(sizeof((eilck_hdl_group_ehdl2) {
    0
}), "T in C#snippet#flexible must be a complete type");
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert( !C_HAS2( 1) || (C_FIRST( 1) <= C_SECOND( 1)), "bounds inverted: " C_STRINGIFY(C_FIRST( 1)) " is > " C_STRINGIFY(C_SECOND( 1)));
static_assert(C_FIRST( 1) <= C_snippet_flexible_min, "min is < " C_STRINGIFY(C_FIRST( 1)));
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_hdl_group__MODULE_CON(I6_Block1C7snippet8flexible4INIT10_Snippet_8E)(void) {
    if (0)
        C_attr_maybe_unused eilck_hdl_group_ehdl2 x[C_snippet_flexible_min] = C_snippet_flexible_INIT;
};
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_hdl_group__MODULE_CON(I6_Block1C7snippet8flexible6OTHERS10_Snippet_8E)(void) {
    if (0) {
        C_attr_maybe_unused struct {
            C_size len;
            C_snippet_flexible_OTHERS
        } _Intern__i9UtbR7Y_eilck_hdl_group_eh_dummy = { .len = 67 };
    }
};
#endif

typedef struct {
    C_size len;
    C_snippet_flexible_OTHERS eilck_hdl_group_ehdl2 tab[];
} eilck_hdl_group_eh_offset_type;

enum _Intern__i9UtbR7Y_eilck_hdl_group_eh_edummy {
    _Intern__i9UtbR7Y_eilck_hdl_group_eh_offset = __Cmod_offsetof(eilck_hdl_group_eh_offset_type, tab),
};
typedef enum _Intern__i9UtbR7Y_eilck_hdl_group_eh_edummy _Intern__i9UtbR7Y_eilck_hdl_group_eh_edummy;

typedef struct {
    C_size _Intern__i9UtbR7Y_eilck_hdl_group_eh_mutableLen;
    C_snippet_flexible_OTHERS
} _Intern__i9UtbR7Y_eilck_hdl_group_eh_header;

typedef union eilck_hdl_group_eh_fa eilck_hdl_group_eh_fa;
typedef union eilck_hdl_group_eh_fa eilck_hdl_group_eh_fa;

typedef union eilck_hdl_group_eh_fa eilck_hdl_group_eh_fa;
union eilck_hdl_group_eh_fa {

    struct {

        union {
            unsigned char _Intern__i9UtbR7Y_eilck_hdl_group_eh_chars[_Intern__i9UtbR7Y_eilck_hdl_group_eh_offset];
            _Intern__i9UtbR7Y_eilck_hdl_group_eh_header head;
        };
        eilck_hdl_group_ehdl2 _Intern__i9UtbR7Y_eilck_hdl_group_eh_fixedTab[C_snippet_flexible_min];
    } _Intern__i9UtbR7Y_eilck_hdl_group_eh_overlay;
    _Intern__i9UtbR7Y_eilck_hdl_group_eh_edummy _Intern__i9UtbR7Y_eilck_hdl_group_eh_dummy;

    struct {
        C_size const len;
        C_snippet_flexible_OTHERS
        eilck_hdl_group_ehdl2 tab[];
    };
};

#define eilck_hdl_group_eh_COMPOUND(LEN)                                        \
    &((union {                                                                  \
        unsigned char _Intern__i9UtbR7Y_eilck_hdl_group_eh_maximum[_Intern__i9UtbR7Y_eilck_hdl_group_eh_offset+sizeof(eilck_hdl_group_ehdl2[LEN])];\
        struct {                                                                \
            union {                                                             \
                unsigned char _Intern__i9UtbR7Y_eilck_hdl_group_eh_chars[_Intern__i9UtbR7Y_eilck_hdl_group_eh_offset];\
                _Intern__i9UtbR7Y_eilck_hdl_group_eh_header head;               \
            };                                                                  \
            eilck_hdl_group_ehdl2 _Intern__i9UtbR7Y_eilck_hdl_group_eh_fixedTab[LEN];\
        } _Intern__i9UtbR7Y_eilck_hdl_group_eh_overlay;                         \
        eilck_hdl_group_eh_fa _Intern__i9UtbR7Y_eilck_hdl_group_eh_dummy;       \
    }){                                                                         \
        ._Intern__i9UtbR7Y_eilck_hdl_group_eh_overlay = {                       \
                                                                 .head = {      \
                                                                          ._Intern__i9UtbR7Y_eilck_hdl_group_eh_mutableLen = (LEN),\
                                                                         }, ._Intern__i9UtbR7Y_eilck_hdl_group_eh_fixedTab = C_snippet_flexible_INIT,\
                                                                }, })._Intern__i9UtbR7Y_eilck_hdl_group_eh_dummy

extern eilck_hdl_group_eh_fa* eilck_hdl_group_eh_alloc(C_size _Intern__i9UtbR7Y_eilck_hdl_group_eh_length);

typedef int __internal_dummy_type_to_be_ignored;

typedef struct eilck_hdl_group eilck_hdl_group;
struct eilck_hdl_group {
    C_size task;
    C_size iter;
    C_size phi;
    C_size id;
    bool active;
    eilck_obj_blks* blk;
    eilck_obj_blks_rw* restrict accs;
    eilck_obj_blks_rw* restrict deps;
    eilck_hdl_group_ih_fa* _Intern__i9UtbR7Y_eilck_hdl_group_ihd;
    eilck_hdl_group_eh_fa* _Intern__i9UtbR7Y_eilck_hdl_group_ehd;
};

inline
eilck_hdl_group* eilck_hdl_group_init(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g, eilck_obj_blks* _Intern__i9UtbR7Y_eilck_hdl_group_b, C_size ta, C_size ph) {
    if (_Intern__i9UtbR7Y_eilck_hdl_group_g) {

        C_size id = _Intern__i9UtbR7Y_eilck_hdl_group_g-> id;
        *_Intern__i9UtbR7Y_eilck_hdl_group_g = (eilck_hdl_group) {
            .blk = _Intern__i9UtbR7Y_eilck_hdl_group_b,
            .task = ta,
            .phi = ph,
            .id = id,
            .accs = C_lib_calloc(_Intern__i9UtbR7Y_eilck_hdl_group_b-> len, sizeof *_Intern__i9UtbR7Y_eilck_hdl_group_g-> accs),
            .deps = C_lib_calloc(_Intern__i9UtbR7Y_eilck_hdl_group_b-> len, sizeof *_Intern__i9UtbR7Y_eilck_hdl_group_g-> deps),
        };
    }
    return _Intern__i9UtbR7Y_eilck_hdl_group_g;
}

inline
eilck_hdl_group* eilck_hdl_group_alloc(eilck_obj_blks* _Intern__i9UtbR7Y_eilck_hdl_group_b, C_size task, C_size ph) {
    return eilck_hdl_group_init(C_lib_malloc(sizeof(eilck_hdl_group)), _Intern__i9UtbR7Y_eilck_hdl_group_b, task, ph);
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E6E72696A7736595A69395574625237592F65696C636B2D68646C2D67726F75702E63_HEADER_INSTANTIATE
extern inline
eilck_hdl_group* eilck_hdl_group_init(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g, eilck_obj_blks* _Intern__i9UtbR7Y_eilck_hdl_group_b, C_size ta, C_size ph) ;

extern inline
eilck_hdl_group* eilck_hdl_group_alloc(eilck_obj_blks* _Intern__i9UtbR7Y_eilck_hdl_group_b, C_size task, C_size ph) ;
#endif

typedef int __internal_dummy_type_to_be_ignored;

extern void eilck_hdl_group_close(void);

extern void eilck_hdl_group_open(char const name[], C_size n, char const head[], char const format[]);

extern eilck_hdl_group* eilck_hdl_group_req(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g, C_size start, eilck_hdl_group* aux);

extern void eilck_hdl_group_greedy(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g, C_io* log, C_size period);

extern eilck_hdl_group_ih_fa* eilck_hdl_group_ih_destroy(eilck_hdl_group_ih_fa* _Intern__i9UtbR7Y_eilck_hdl_group_ieh);

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

extern eilck_hdl_group_eh_fa* eilck_hdl_group_eh_destroy(eilck_hdl_group_eh_fa* _Intern__i9UtbR7Y_eilck_hdl_group_ieh);

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

extern eilck_hdl_group* eilck_hdl_group_destroy(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g);

extern bool eilck_hdl_group_acq(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g);

extern bool eilck_hdl_group_test(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g);

extern bool eilck_hdl_group_rel(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g);

inline
void eilck_hdl_group_ih_free(eilck_hdl_group_ih_fa* const _Intern__i9UtbR7Y_eilck_hdl_group_ieh) {
    C_lib_free(eilck_hdl_group_ih_destroy(_Intern__i9UtbR7Y_eilck_hdl_group_ieh));
}

inline
void eilck_hdl_group_eh_free(eilck_hdl_group_eh_fa* const _Intern__i9UtbR7Y_eilck_hdl_group_ieh) {
    C_lib_free(eilck_hdl_group_eh_destroy(_Intern__i9UtbR7Y_eilck_hdl_group_ieh));
}

inline
void eilck_hdl_group_free(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g) {
    C_lib_free(eilck_hdl_group_destroy(_Intern__i9UtbR7Y_eilck_hdl_group_g));
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E6E72696A7736595A69395574625237592F65696C636B2D68646C2D67726F75702E63_HEADER_INSTANTIATE
extern inline
void eilck_hdl_group_ih_free(eilck_hdl_group_ih_fa* const _Intern__i9UtbR7Y_eilck_hdl_group_ieh) ;

extern inline
void eilck_hdl_group_eh_free(eilck_hdl_group_eh_fa* const _Intern__i9UtbR7Y_eilck_hdl_group_ieh) ;

extern inline
void eilck_hdl_group_free(eilck_hdl_group* _Intern__i9UtbR7Y_eilck_hdl_group_g) ;
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E6E72696A7736595A69395574625237592F65696C636B2D68646C2D67726F75702E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-hdl-group */
