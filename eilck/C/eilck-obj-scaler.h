/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_obj_scaler__GUARD
#define eilck_obj_scaler__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @def eilck_obj_scaler_CLEAR
 ** @brief Macro expansion computed at compile time from expression <code>(( C_size )-clear)</code>
 **/

#define eilck_obj_scaler_CLEAR 0UL
/** @def eilck_obj_scaler_MAP
 ** @brief Macro expansion computed at compile time from expression <code>(( C_size )-map)</code>
 **/

#define eilck_obj_scaler_MAP 0xffffffffffffffffUL
/** @def eilck_obj_scaler_UNMAP
 ** @brief Macro expansion computed at compile time from expression <code>(( C_size )-unmap)</code>
 **/

#define eilck_obj_scaler_UNMAP 0xfffffffffffffffeUL
/** @def eilck_obj_scaler_OPEN
 ** @brief Macro expansion computed at compile time from expression <code>(( C_size )-open)</code>
 **/

#define eilck_obj_scaler_OPEN 0xfffffffffffffffdUL
/** @def eilck_obj_scaler_CLOSE
 ** @brief Macro expansion computed at compile time from expression <code>(( C_size )-close)</code>
 **/

#define eilck_obj_scaler_CLOSE 0xfffffffffffffffcUL
/** @def eilck_obj_scaler_CREATE
 ** @brief Macro expansion computed at compile time from expression <code>(( C_size )-create)</code>
 **/

#define eilck_obj_scaler_CREATE 0xfffffffffffffffbUL
/** @def eilck_obj_scaler_REMOVE
 ** @brief Macro expansion computed at compile time from expression <code>(( C_size )-remove)</code>
 **/

#define eilck_obj_scaler_REMOVE 0xfffffffffffffffaUL
/** @def eilck_obj_scaler_DESTROY
 ** @brief Macro expansion computed at compile time from expression <code>(( C_size )-destroy)</code>
 **/

#define eilck_obj_scaler_DESTROY 0xfffffffffffffff9UL
/**
 ** @module eilck_obj_scaler
 ** @ingroup eilck_obj
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E617A4F51766E6950464D5062546944362F65696C636B2D6F626A2D7363616C65722E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E617A4F51766E6950464D5062546944362F65696C636B2D6F626A2D7363616C65722E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E617A4F51766E6950464D5062546944362F65696C636B2D6F626A2D7363616C65722E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E617A4F51766E6950464D5062546944362F65696C636B2D6F626A2D7363616C65722E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_obj_scaler_MODULE eilck_obj_scaler module internals
 ** @ingroup eilck_obj_scaler
 ** @{ */

#ifdef __CMOD_CONSTANT__
extern int _Intern_eilck_obj_scaler__Constant(int argc, char* argv[argc+1]);
#endif

#define eilck_obj_scaler__Intern_defill_imp_TYPES void*,                        \
    eilck_obj*, C_size, void*

#define _Intern__FMPbTiD6_eilck_obj_scaler_imp_VOID C_COUNT_nothing(C_FIRST(eilck_obj_scaler__Intern_defill_imp_TYPES))

#define eilck_obj_scaler_RETTYPE C_comma_if_else(_Intern__FMPbTiD6_eilck_obj_scaler_imp_VOID)(void)(C_FIRST(eilck_obj_scaler__Intern_defill_imp_TYPES))

#define eilck_obj_scaler_RETURN C_comma_unless(_Intern__FMPbTiD6_eilck_obj_scaler_imp_VOID)(return)

#if defined(__CMOD__) || defined(_DOXYGEN_)
#define _Intern__FMPbTiD6_eilck_obj_scaler_imp_PTYPES C_snippet_prototype_DROP(eilck_obj_scaler__Intern_defill_imp_TYPES)

#define _Intern__FMPbTiD6_eilck_obj_scaler_imp_EMPTY C_COUNT_nothing(_Intern__FMPbTiD6_eilck_obj_scaler_imp_PTYPES)

#define eilck_obj_scaler_PARAMTYPES C_comma_if_else(_Intern__FMPbTiD6_eilck_obj_scaler_imp_EMPTY)(void)(_Intern__FMPbTiD6_eilck_obj_scaler_imp_PTYPES)

typedef eilck_obj_scaler_RETTYPE eilck_obj_scaler(eilck_obj_scaler_PARAMTYPES);
#endif

enum eilck_obj_scaler_command {

    eilck_obj_scaler_clear,

    eilck_obj_scaler_map,

    eilck_obj_scaler_unmap,

    eilck_obj_scaler_open,

    eilck_obj_scaler_close,

    eilck_obj_scaler_create,

    eilck_obj_scaler_remove,

    eilck_obj_scaler_destroy,
};
typedef enum eilck_obj_scaler_command eilck_obj_scaler_command;

#define eilck_obj_scaler__Intern_defill_com_LIST eilck_obj_scaler_clear,        \
    eilck_obj_scaler_map, eilck_obj_scaler_unmap, eilck_obj_scaler_open, eilck_obj_scaler_close, eilck_obj_scaler_create, eilck_obj_scaler_remove, eilck_obj_scaler_destroy

static_assert(sizeof(int const[]) {
    eilck_obj_scaler__Intern_defill_com_LIST,
}, "LIST must expand to a list of valid constants, did you forget a declaration directive before a defill?");

#if defined(__CMOD__)
enum _Intern__FMPbTiD6_eilck_obj_scaler_com_dummy {

    _Intern__FMPbTiD6_eilck_obj_scaler_com_prelen = sizeof C_STRINGIFY0(eilck_obj_scaler) - 2,

    eilck_obj_scaler_command_MAX = C_comma_COUNT(eilck_obj_scaler__Intern_defill_com_LIST),
};
typedef enum _Intern__FMPbTiD6_eilck_obj_scaler_com_dummy _Intern__FMPbTiD6_eilck_obj_scaler_com_dummy;
#endif

extern C_attr_const
char const* eilck_obj_scaler_command_getname(unsigned x);

extern C_attr_pure
char const* eilck_obj_scaler_command_getshort(unsigned x);

extern C_attr_pure
unsigned eilck_obj_scaler_command_parse(char const s[]);

extern char const* restrict const _Intern__FMPbTiD6_eilck_obj_scaler_com_names[];

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

extern char const* restrict const* restrict const _Intern__FMPbTiD6_eilck_obj_scaler_com_shorts;

extern void _Intern__FMPbTiD6_eilck_obj_scaler_com_startup(void);

#ifndef eilck_obj_scaler_CLEAR
#define eilck_obj_scaler_CLEAR (((C_size)-eilck_obj_scaler_clear))
#endif

#ifndef eilck_obj_scaler_MAP
#define eilck_obj_scaler_MAP (((C_size)-eilck_obj_scaler_map))
#endif

#ifndef eilck_obj_scaler_UNMAP
#define eilck_obj_scaler_UNMAP (((C_size)-eilck_obj_scaler_unmap))
#endif

#ifndef eilck_obj_scaler_OPEN
#define eilck_obj_scaler_OPEN (((C_size)-eilck_obj_scaler_open))
#endif

#ifndef eilck_obj_scaler_CLOSE
#define eilck_obj_scaler_CLOSE (((C_size)-eilck_obj_scaler_close))
#endif

#ifndef eilck_obj_scaler_CREATE
#define eilck_obj_scaler_CREATE (((C_size)-eilck_obj_scaler_create))
#endif

#ifndef eilck_obj_scaler_REMOVE
#define eilck_obj_scaler_REMOVE (((C_size)-eilck_obj_scaler_remove))
#endif

#ifndef eilck_obj_scaler_DESTROY
#define eilck_obj_scaler_DESTROY (((C_size)-eilck_obj_scaler_destroy))
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E617A4F51766E6950464D5062546944362F65696C636B2D6F626A2D7363616C65722E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-obj-scaler */
