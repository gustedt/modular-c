/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_sem__GUARD
#define eilck_sem__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "eilck.h"
#include "eilck-ftx.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @def eilck_sem_MAX
 ** @brief Macro expansion computed at compile time from expression <code>1 << ((sizeof(int)* C_CHAR_BIT )/2)</code>
 **/

#define eilck_sem_MAX +65536
/**
 ** @module eilck_sem
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E52575452704F4C314B4A484A497250312F65696C636B2D73656D2E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E52575452704F4C314B4A484A497250312F65696C636B2D73656D2E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E52575452704F4C314B4A484A497250312F65696C636B2D73656D2E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E52575452704F4C314B4A484A497250312F65696C636B2D73656D2E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_sem_MODULE eilck_sem module internals
 ** @ingroup eilck_sem
 ** @{ */

#ifdef __CMOD_CONSTANT__
extern int _Intern_eilck_sem__Constant(int argc, char* argv[argc+1]);
#endif

typedef eilck_ftx eilck_sem;
#ifndef eilck_sem_MAX
#define eilck_sem_MAX (1 << ((sizeof(int)*C_CHAR_BIT)/2))
#endif

inline
int eilck_sem_twos_add(int _Intern__KJHJIrP1_eilck_sem_current, unsigned _Intern__KJHJIrP1_eilck_sem_value) {
    static_assert(C_int_MAX < C_unsigned_MAX, "sign bit shall be a value bit for unsigned");
    static_assert(true && C_int_MIN < -C_int_MAX, "-C_int_MAX-1 must be representable");
    _Intern__KJHJIrP1_eilck_sem_value += _Intern__KJHJIrP1_eilck_sem_current;
    if (_Intern__KJHJIrP1_eilck_sem_value <= C_int_MAX)
        return _Intern__KJHJIrP1_eilck_sem_value;
    else if (_Intern__KJHJIrP1_eilck_sem_value == C_int_MAX+1u)
        return C_int_MIN;
    else
        return -(int)(-_Intern__KJHJIrP1_eilck_sem_value);
}

inline
int _Intern__KJHJIrP1_eilck_sem_twos_sub(int _Intern__KJHJIrP1_eilck_sem_current, unsigned _Intern__KJHJIrP1_eilck_sem_value) {
    return eilck_sem_twos_add(_Intern__KJHJIrP1_eilck_sem_current, -_Intern__KJHJIrP1_eilck_sem_value);
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E52575452704F4C314B4A484A497250312F65696C636B2D73656D2E63_HEADER_INSTANTIATE
extern inline
int eilck_sem_twos_add(int _Intern__KJHJIrP1_eilck_sem_current, unsigned _Intern__KJHJIrP1_eilck_sem_value) ;

extern inline
int _Intern__KJHJIrP1_eilck_sem_twos_sub(int _Intern__KJHJIrP1_eilck_sem_current, unsigned _Intern__KJHJIrP1_eilck_sem_value) ;
#endif

extern void _Intern__KJHJIrP1_eilck_sem_slow(eilck_sem* _Intern__KJHJIrP1_eilck_sem_c);

inline
void eilck_sem_wait(eilck_sem _Intern__KJHJIrP1_eilck_sem_c[static 1]) {
    int _Intern__KJHJIrP1_eilck_sem_current = 1;
    do {
        if (eilck_ftx_cas(_Intern__KJHJIrP1_eilck_sem_c, &_Intern__KJHJIrP1_eilck_sem_current, _Intern__KJHJIrP1_eilck_sem_twos_sub(_Intern__KJHJIrP1_eilck_sem_current, 1))) return;
    } while ((_Intern__KJHJIrP1_eilck_sem_current+0U) % eilck_sem_MAX);
    _Intern__KJHJIrP1_eilck_sem_slow(_Intern__KJHJIrP1_eilck_sem_c);
}

inline
void eilck_sem_post(eilck_sem _Intern__KJHJIrP1_eilck_sem_c[static 1]) {
    int _Intern__KJHJIrP1_eilck_sem_current = eilck_ftx_fetch_add(_Intern__KJHJIrP1_eilck_sem_c, 1);

    if ((_Intern__KJHJIrP1_eilck_sem_current+0U) >= eilck_sem_MAX) eilck_ftx_signal(_Intern__KJHJIrP1_eilck_sem_c);
}

inline
int eilck_sem_getvalue(eilck_sem _Intern__KJHJIrP1_eilck_sem_c[static 1]) {
    return eilck_ftx_load(_Intern__KJHJIrP1_eilck_sem_c);
}

#define eilck_sem_init eilck_ftx_init

#define eilck_sem_INITIALIZER eilck_ftx_INITIALIZER

#ifdef CMOD_2F746D702F636D6F642D746D70642E52575452704F4C314B4A484A497250312F65696C636B2D73656D2E63_HEADER_INSTANTIATE
extern inline
void eilck_sem_wait(eilck_sem _Intern__KJHJIrP1_eilck_sem_c[static 1]) ;

extern inline
void eilck_sem_post(eilck_sem _Intern__KJHJIrP1_eilck_sem_c[static 1]) ;

extern inline
int eilck_sem_getvalue(eilck_sem _Intern__KJHJIrP1_eilck_sem_c[static 1]) ;
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E52575452704F4C314B4A484A497250312F65696C636B2D73656D2E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-sem */
