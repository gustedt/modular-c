#ifdef __GNUC__
#pragma implementation
#endif


#include <iostream>
#include <pthread.h>
#include "eilcktest++.hpp"

using namespace eilck;
using namespace eilck::hdl;

using std::cout;
using std::endl;

enum en { writers = 500, readers = 10, iterations = 80, };

typedef enum en en;

typedef ihdl< unsigned > ihdl10[readers];


static
_Atomic(unsigned) sum = { 0u };

static
void* reader(void* arg) {
  ihdl<unsigned>& h = *reinterpret_cast<ihdl< unsigned >* >(arg);
  ihdl<unsigned> h2[2];
  EILCK_HDL_SECTION(h) {
    sum += h[0];
    h.chain(h2[0]);
  }
  for (unsigned it = 0; it < iterations; ++it) {
    EILCK_HDL_SECTION(h2) {
      sum += map(h2)[0];
    }
  }
  drop(h2);
  return 0;
}

static
void* writer(void* arg) {
  ehdl<unsigned>& h = *reinterpret_cast<ehdl< unsigned >* >(arg);
  ehdl<unsigned> h2[2];
  EILCK_HDL_SECTION(h) {
    ++h[0];
    chain(h, h2[0]);
  }
  for (unsigned it = 0; it < iterations; ++it) {
    EILCK_HDL_SECTION(h2) {
      ++map(h2)[0];
    }
  }
  drop(h2);
  return 0;
}

static
obj<unsigned> object;

static
ehdl<unsigned>* eh;

static
ihdl10* ih;

static
void final(void) {
    // a last CS with exclusive access
    ehdl<unsigned> last(object);
    // use without argument to test exceptions
    // ehdl<unsigned> last;
    EILCK_HDL_SECTION(last) {
      cout << "last thread: we had " << last[0]
           << " writers, sum is " << sum.load()
           << ", length is " << last.length() << " elements"
           << endl;
    }
    delete[] eh;
    delete[] ih;
}

int main(int argc, char* argv[]) {
    atexit(final);
    unsigned writers = 10;
    if (argc > 1) writers = strtoull(argv[1], 0, 0);
    eh = new ehdl<unsigned>[writers];
    ih = new ihdl10[writers];
    ehdl<unsigned> first;
    first.req(object);

    for (unsigned i = 0; i < writers; ++i) {
        eh[i].req(object);
        for (unsigned j = 0; j < readers; ++j)
            ih[i][j].req(object);
    }

    printf("main: inserted all requests\n");

    for (unsigned i = 0; i < writers; ++i) {
        pthread_t id;
        pthread_create(&id, 0, writer, &eh[i]);
        pthread_detach(id);
    }

    printf("main: started all writer threads\n");

    for (unsigned i = 0; i < writers; ++i) {
        for (unsigned j = 0; j < readers; ++j) {
            pthread_t id;
            pthread_create(&id, 0, reader, &ih[i][j]);
            pthread_detach(id);
        }
    }

    printf("main: started all reader threads\n");

    // a first CS with write access
    EILCK_HDL_SECTION(first) {
      first[0] = 0;
    }

    printf("main: unleached threads, exiting\n");

    pthread_exit(0);
}
