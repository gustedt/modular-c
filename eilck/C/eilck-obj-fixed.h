/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_obj_fixed__GUARD
#define eilck_obj_fixed__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_obj_fixed
 ** @ingroup eilck_obj
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4D46547151584F756B724B746F377A502F65696C636B2D6F626A2D66697865642E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4D46547151584F756B724B746F377A502F65696C636B2D6F626A2D66697865642E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4D46547151584F756B724B746F377A502F65696C636B2D6F626A2D66697865642E63_HEADER
#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4D46547151584F756B724B746F377A502F65696C636B2D6F626A2D66697865642E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_obj_fixed_MODULE eilck_obj_fixed module internals
 ** @ingroup eilck_obj_fixed
 ** @{ */

#define eilck_obj_fixed_INITIALIZER(TABLEN,                                     \
        ...)                                                                    \
eilck_obj_INITIALIZER(TABLEN, eilck_obj_fixed, C_comma_if_else(__VA_ARGS__)(C_SECOND(__VA_ARGS__))(sizeof(__VA_ARGS__)),\
                               C_FIRST(__VA_ARGS__))

extern void* eilck_obj_fixed(eilck_obj* _Intern__krKto7zP_eilck_obj_fixed_ob, C_size _Intern__krKto7zP_eilck_obj_fixed_nsize, void* _Intern__krKto7zP_eilck_obj_fixed_cntx);

extern eilck_obj* eilck_obj_fixed_init(eilck_obj* _Intern__krKto7zP_eilck_obj_fixed_ob, C_size _Intern__krKto7zP_eilck_obj_fixed_tablen);
#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4D46547151584F756B724B746F377A502F65696C636B2D6F626A2D66697865642E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-obj-fixed */
