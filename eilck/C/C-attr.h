/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef C_attr__GUARD
#define C_attr__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module C_attr
 ** @ingroup C
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E6B7530546D635A41784159446249736C2F432D617474722E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E6B7530546D635A41784159446249736C2F432D617474722E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E6B7530546D635A41784159446249736C2F432D617474722E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E6B7530546D635A41784159446249736C2F432D617474722E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup C_attr_MODULE C_attr module internals
 ** @ingroup C_attr
 ** @{ */

#if C_has_gnu_inline
#define C_attr(...) __attribute__((__VA_ARGS__))

#define C_attr_alignas(X) C_attr(__aligned__(X))

#define C_attr_const C_attr(__const__)

#define C_attr_deprecated C_attr(__deprecated__)

#define C_attr_deprecate(TXT) C_attr(__deprecated__(TXT))

#define C_attr_leaf C_attr(__leaf__)

#define C_attr_noinline C_attr(__noinline__)

#define C_attr_nonnull C_attr(__nonnull__)

#define C_attr_nothrow C_attr(__nothrow__)

#define C_attr_optimize(X) C_attr(__optimize__(X))

#define C_attr_pure C_attr(__pure__)

#define C_attr_twice C_attr(__returns_twice__)

#define C_attr_returns_nonnull C_attr(__returns_nonnull__)

#define C_attr_used C_attr(__used__)

#define C_attr_maybe_unused C_attr(__unused__)

#define C_attr_nodiscard C_attr(__warn_unused_result__)
#else
#define C_attr(...)

#define C_attr_alignas(X) alignas(X)

#define C_attr_const

#define C_attr_deprecated

#define C_attr_deprecate(TXT)

#define C_attr_leaf

#define C_attr_noinline

#define C_attr_nonnull

#define C_attr_nothrow

#define C_attr_optimize(X)

#define C_attr_pure

#define C_attr_twice

#define C_attr_returns_nonnull

#define C_attr_used

#define C_attr_maybe_unused

#define C_attr_nodiscard
#endif
#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E6B7530546D635A41784159446249736C2F432D617474722E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* C-attr */
