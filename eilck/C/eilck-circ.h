/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_circ__GUARD
#define eilck_circ__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-snippet.h"
#include "C-attr.h"
#include "C-snippet-flexible.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "eilck-ftx.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_circ
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E736B4F786469784E7259736466584B552F65696C636B2D636972632E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E736B4F786469784E7259736466584B552F65696C636B2D636972632E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E736B4F786469784E7259736466584B552F65696C636B2D636972632E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E736B4F786469784E7259736466584B552F65696C636B2D636972632E63_HEADER
#ifndef C_bitset__Operator_eval
# define C_bitset__Operator_eval(...) __VA_ARGS__
#endif

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_circ_MODULE eilck_circ module internals
 ** @ingroup eilck_circ
 ** @{ */

#define eilck_circ__Intern_defill_FA_min 5

#define eilck_circ__Intern_defill_FA_INIT {                                     \
        [0] = eilck_ftx_INITIALIZER(eilck_state_locked),                        \
    }
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert(sizeof((eilck_ftx) {
    0
}), "T in C#snippet#flexible must be a complete type");
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert( !C_HAS2( 1) || (C_FIRST( 1) <= C_SECOND( 1)), "bounds inverted: " C_STRINGIFY(C_FIRST( 1)) " is > " C_STRINGIFY(C_SECOND( 1)));
static_assert(C_FIRST( 1) <= eilck_circ__Intern_defill_FA_min, "min is < " C_STRINGIFY(C_FIRST( 1)));
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_circ__MODULE_CON(I6_Block1C7snippet8flexible4INIT10_Snippet_3E)(void) {
    if (0)
        C_attr_maybe_unused eilck_ftx x[eilck_circ__Intern_defill_FA_min] = eilck_circ__Intern_defill_FA_INIT;
};
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_circ__MODULE_CON(I6_Block1C7snippet8flexible6OTHERS10_Snippet_3E)(void) {
    if (0) {
        C_attr_maybe_unused struct {
            C_size len;
            C_snippet_flexible_OTHERS
        } _Intern__rYsdfXKU_eilck_circ_FA_dummy = { .len = 67 };
    }
};
#endif

typedef struct {
    C_size len;
    C_snippet_flexible_OTHERS eilck_ftx tab[];
} eilck_circ_offset_type;

enum _Intern__rYsdfXKU_eilck_circ_FA_edummy {
    _Intern__rYsdfXKU_eilck_circ_FA_offset = __Cmod_offsetof(eilck_circ_offset_type, tab),
};
typedef enum _Intern__rYsdfXKU_eilck_circ_FA_edummy _Intern__rYsdfXKU_eilck_circ_FA_edummy;

typedef struct {
    C_size _Intern__rYsdfXKU_eilck_circ_FA_mutableLen;
    C_snippet_flexible_OTHERS
} _Intern__rYsdfXKU_eilck_circ_FA_header;

typedef union eilck_circ_fa eilck_circ_fa;
typedef union eilck_circ_fa eilck_circ_fa;

typedef union eilck_circ_fa eilck_circ_fa;
union eilck_circ_fa {

    struct {

        union {
            unsigned char _Intern__rYsdfXKU_eilck_circ_FA_chars[_Intern__rYsdfXKU_eilck_circ_FA_offset];
            _Intern__rYsdfXKU_eilck_circ_FA_header head;
        };
        eilck_ftx _Intern__rYsdfXKU_eilck_circ_FA_fixedTab[eilck_circ__Intern_defill_FA_min];
    } _Intern__rYsdfXKU_eilck_circ_FA_overlay;
    _Intern__rYsdfXKU_eilck_circ_FA_edummy _Intern__rYsdfXKU_eilck_circ_FA_dummy;

    struct {
        C_size const len;
        C_snippet_flexible_OTHERS
        eilck_ftx tab[];
    };
};

#define eilck_circ_COMPOUND(LEN)                                                \
    &((union {                                                                  \
        unsigned char _Intern__rYsdfXKU_eilck_circ_FA_maximum[_Intern__rYsdfXKU_eilck_circ_FA_offset+sizeof(eilck_ftx[LEN])];\
        struct {                                                                \
            union {                                                             \
                unsigned char _Intern__rYsdfXKU_eilck_circ_FA_chars[_Intern__rYsdfXKU_eilck_circ_FA_offset];\
                _Intern__rYsdfXKU_eilck_circ_FA_header head;                    \
            };                                                                  \
            eilck_ftx _Intern__rYsdfXKU_eilck_circ_FA_fixedTab[LEN];            \
        } _Intern__rYsdfXKU_eilck_circ_FA_overlay;                              \
        eilck_circ_fa _Intern__rYsdfXKU_eilck_circ_FA_dummy;                    \
    }){                                                                         \
        ._Intern__rYsdfXKU_eilck_circ_FA_overlay = {                            \
                                                            .head = {           \
                                                                     ._Intern__rYsdfXKU_eilck_circ_FA_mutableLen = (LEN),\
                                                                    }, ._Intern__rYsdfXKU_eilck_circ_FA_fixedTab = eilck_circ__Intern_defill_FA_INIT,\
                                                           }, })._Intern__rYsdfXKU_eilck_circ_FA_dummy

extern eilck_circ_fa* eilck_circ_alloc(C_size _Intern__rYsdfXKU_eilck_circ_FA_length);

typedef int __internal_dummy_type_to_be_ignored;

typedef eilck_circ_fa eilck_circ;

inline
int eilck_circ_require(eilck_circ* _Intern__rYsdfXKU_eilck_circ_c, C_size _Intern__rYsdfXKU_eilck_circ_pos) {
    _Intern__rYsdfXKU_eilck_circ_pos %= _Intern__rYsdfXKU_eilck_circ_c-> len;
    C_size pos1 = (_Intern__rYsdfXKU_eilck_circ_pos-1)%_Intern__rYsdfXKU_eilck_circ_c-> len;
    int prev = eilck_ftx_load(&_Intern__rYsdfXKU_eilck_circ_c->tab[pos1]);
    int act = eilck_ftx_add_fetch(&_Intern__rYsdfXKU_eilck_circ_c->tab[_Intern__rYsdfXKU_eilck_circ_pos], eilck_state_increment);
    if (act == eilck_state_increment) {
        bool immediate = eilck_state_released(prev);
        int contrib = (immediate ? eilck_state_locked : eilck_state_required);
        act = eilck_ftx_or_fetch(&_Intern__rYsdfXKU_eilck_circ_c->tab[_Intern__rYsdfXKU_eilck_circ_pos], contrib);
        if (immediate)
            eilck_ftx_store(&_Intern__rYsdfXKU_eilck_circ_c->tab[pos1], eilck_state_unlocked);
    }
    return act;
}

inline
int eilck_circ_acquire(eilck_circ* _Intern__rYsdfXKU_eilck_circ_c, C_size _Intern__rYsdfXKU_eilck_circ_pos) {
    _Intern__rYsdfXKU_eilck_circ_pos %= _Intern__rYsdfXKU_eilck_circ_c-> len;
    for (;;) {
        int act = eilck_ftx_load(&_Intern__rYsdfXKU_eilck_circ_c->tab[_Intern__rYsdfXKU_eilck_circ_pos]);
        if ( C_bitset__Operator_le(eilck_state_acquired, act ) )
            return act;
        eilck_ftx_wait(&_Intern__rYsdfXKU_eilck_circ_c->tab[_Intern__rYsdfXKU_eilck_circ_pos], act);
    }
}

inline
bool eilck_circ_test(eilck_circ* _Intern__rYsdfXKU_eilck_circ_c, C_size _Intern__rYsdfXKU_eilck_circ_pos) {
    _Intern__rYsdfXKU_eilck_circ_pos %= _Intern__rYsdfXKU_eilck_circ_c-> len;
    int act = eilck_ftx_load(&_Intern__rYsdfXKU_eilck_circ_c->tab[_Intern__rYsdfXKU_eilck_circ_pos]);
    return C_bitset__Operator_le(eilck_state_acquired, act ) ;
}

inline
int eilck_circ_release(eilck_circ* _Intern__rYsdfXKU_eilck_circ_c, C_size _Intern__rYsdfXKU_eilck_circ_pos, bool _Intern__rYsdfXKU_eilck_circ_tail) {
    _Intern__rYsdfXKU_eilck_circ_pos %= _Intern__rYsdfXKU_eilck_circ_c-> len;
    int act = eilck_ftx_sub_fetch(&_Intern__rYsdfXKU_eilck_circ_c->tab[_Intern__rYsdfXKU_eilck_circ_pos], eilck_state_increment);
    if (eilck_state_released(act) && !_Intern__rYsdfXKU_eilck_circ_tail) {
        C_size pos1 = (_Intern__rYsdfXKU_eilck_circ_pos+1)%_Intern__rYsdfXKU_eilck_circ_c-> len;
        eilck_ftx_or_fetch(&_Intern__rYsdfXKU_eilck_circ_c->tab[pos1], eilck_state_acquired);
        eilck_ftx_broadcast(&_Intern__rYsdfXKU_eilck_circ_c->tab[pos1]);
        act = 0;
        eilck_ftx_store(&_Intern__rYsdfXKU_eilck_circ_c->tab[_Intern__rYsdfXKU_eilck_circ_pos], 0);
    }
    return act;
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E736B4F786469784E7259736466584B552F65696C636B2D636972632E63_HEADER_INSTANTIATE
extern inline
int eilck_circ_require(eilck_circ* _Intern__rYsdfXKU_eilck_circ_c, C_size _Intern__rYsdfXKU_eilck_circ_pos) ;

extern inline
int eilck_circ_acquire(eilck_circ* _Intern__rYsdfXKU_eilck_circ_c, C_size _Intern__rYsdfXKU_eilck_circ_pos) ;

extern inline
bool eilck_circ_test(eilck_circ* _Intern__rYsdfXKU_eilck_circ_c, C_size _Intern__rYsdfXKU_eilck_circ_pos) ;

extern inline
int eilck_circ_release(eilck_circ* _Intern__rYsdfXKU_eilck_circ_c, C_size _Intern__rYsdfXKU_eilck_circ_pos, bool _Intern__rYsdfXKU_eilck_circ_tail) ;
#endif

typedef int __internal_dummy_type_to_be_ignored;

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E736B4F786469784E7259736466584B552F65696C636B2D636972632E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-circ */
