/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_unsigned__GUARD
#define eilck_unsigned__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"
#include "eilck-impl.h"
#include "eilck-impl-snippet.h"
#include "eilck-ehdl.h"
#include "eilck-ihdl.h"
#include "eilck-tmpl.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_unsigned
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E5970566D56774674724F3349397336772F65696C636B2D756E7369676E65642E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E5970566D56774674724F3349397336772F65696C636B2D756E7369676E65642E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E5970566D56774674724F3349397336772F65696C636B2D756E7369676E65642E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E5970566D56774674724F3349397336772F65696C636B2D756E7369676E65642E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_unsigned_MODULE eilck_unsigned module internals
 ** @ingroup eilck_unsigned
 ** @{ */

#if !defined(_DOXYGEN_) && !defined(__cplusplus)
typedef int __internal_dummy_type_to_be_ignored;

#endif

typedef C_unsigned _Intern__rO3I9s6w_eilck_unsigned_simpl_eT;

typedef C_unsigned const _Intern__rO3I9s6w_eilck_unsigned_simpl_iT;

typedef struct eilck_unsigned_obj eilck_unsigned_obj;
struct eilck_unsigned_obj {

    union {
        eilck_obj hide;
        C_unsigned* base;
    };
};

typedef struct eilck_unsigned_ehdl eilck_unsigned_ehdl;
struct eilck_unsigned_ehdl {

    union {
        eilck_ehdl hide;
        void* type;
        _Intern__rO3I9s6w_eilck_unsigned_simpl_eT* baze;

        struct {
            _Intern__rO3I9s6w_eilck_unsigned_simpl_eT* base;
            char _Intern__rO3I9s6w_eilck_unsigned_simpl_fill[sizeof(eilck_ehdl)-sizeof(_Intern__rO3I9s6w_eilck_unsigned_simpl_eT*)];
        };
    };
};

typedef struct eilck_unsigned_ihdl eilck_unsigned_ihdl;
struct eilck_unsigned_ihdl {

    union {
        eilck_ihdl hide;
        void const* type;
        _Intern__rO3I9s6w_eilck_unsigned_simpl_iT* baze;

        struct {
            _Intern__rO3I9s6w_eilck_unsigned_simpl_iT* base;
            char _Intern__rO3I9s6w_eilck_unsigned_simpl_fill[sizeof(eilck_ihdl)-sizeof(_Intern__rO3I9s6w_eilck_unsigned_simpl_iT*)];
        };
    };
};

#define eilck_unsigned_INITIALIZER(LEN)                                         \
    {                                                                           \
        .hide = eilck_obj_INITIALIZER(LEN),                                     \
    }

inline eilck_unsigned_obj* eilck_unsigned_init(eilck_unsigned_obj* _Intern__rO3I9s6w_eilck_unsigned_simpl_o, C_size _Intern__rO3I9s6w_eilck_unsigned_simpl_len) {
    if (_Intern__rO3I9s6w_eilck_unsigned_simpl_o) eilck_obj_init(&_Intern__rO3I9s6w_eilck_unsigned_simpl_o-> hide, _Intern__rO3I9s6w_eilck_unsigned_simpl_len);
    return _Intern__rO3I9s6w_eilck_unsigned_simpl_o;
}

inline void eilck_unsigned_destroy(eilck_unsigned_obj* _Intern__rO3I9s6w_eilck_unsigned_simpl_o) {
    if (_Intern__rO3I9s6w_eilck_unsigned_simpl_o) eilck_obj_destroy(&_Intern__rO3I9s6w_eilck_unsigned_simpl_o-> hide);
}

inline _Intern__rO3I9s6w_eilck_unsigned_simpl_eT* eilck_unsigned_emap(eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h) {
    return _Intern__rO3I9s6w_eilck_unsigned_simpl_h ? eilck_ehdl_map(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h-> hide) : 0;
}

inline int eilck_unsigned_ereq(eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h, eilck_unsigned_obj* _Intern__rO3I9s6w_eilck_unsigned_simpl_o) {
    return _Intern__rO3I9s6w_eilck_unsigned_simpl_h ? eilck_ehdl_req(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h-> hide, &_Intern__rO3I9s6w_eilck_unsigned_simpl_o-> hide) : 0;
}

inline C_size eilck_unsigned_elength(eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h) {
    return _Intern__rO3I9s6w_eilck_unsigned_simpl_h ? eilck_ehdl_length(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h-> hide)/sizeof(_Intern__rO3I9s6w_eilck_unsigned_simpl_h-> baze[0]) : 0;
}

inline _Intern__rO3I9s6w_eilck_unsigned_simpl_iT* eilck_unsigned_imap(eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h) {
    return _Intern__rO3I9s6w_eilck_unsigned_simpl_h ? eilck_ihdl_map(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h-> hide) : 0;
}

inline int eilck_unsigned_ireq(eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h, eilck_unsigned_obj* _Intern__rO3I9s6w_eilck_unsigned_simpl_o) {
    return _Intern__rO3I9s6w_eilck_unsigned_simpl_h ? eilck_ihdl_req(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h-> hide, &_Intern__rO3I9s6w_eilck_unsigned_simpl_o-> hide) : 0;
}

inline C_size eilck_unsigned_ilength(eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h) {
    return _Intern__rO3I9s6w_eilck_unsigned_simpl_h ? eilck_ihdl_length(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h-> hide)/sizeof(_Intern__rO3I9s6w_eilck_unsigned_simpl_h-> baze[0]) : 0;
}

inline _Intern__rO3I9s6w_eilck_unsigned_simpl_eT* eilck_unsigned_emap_2(eilck_unsigned_ehdl* restrict _Intern__rO3I9s6w_eilck_unsigned_simpl_h0, eilck_unsigned_ehdl* restrict _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) {
    return (_Intern__rO3I9s6w_eilck_unsigned_simpl_h0 && _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) ? eilck_ehdl_map_2(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h0-> hide, &_Intern__rO3I9s6w_eilck_unsigned_simpl_h1-> hide) : 0;
}

inline
C_size eilck_unsigned_elength_2(eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h0, eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) {
    return _Intern__rO3I9s6w_eilck_unsigned_simpl_h0 && _Intern__rO3I9s6w_eilck_unsigned_simpl_h1 ? eilck_ehdl_length_2(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h0-> hide, &_Intern__rO3I9s6w_eilck_unsigned_simpl_h1-> hide)/sizeof(_Intern__rO3I9s6w_eilck_unsigned_simpl_h0-> baze[0]) : 0;
}

inline _Intern__rO3I9s6w_eilck_unsigned_simpl_iT* eilck_unsigned_imap_2(eilck_unsigned_ihdl* restrict _Intern__rO3I9s6w_eilck_unsigned_simpl_h0, eilck_unsigned_ihdl* restrict _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) {
    return (_Intern__rO3I9s6w_eilck_unsigned_simpl_h0 && _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) ? eilck_ihdl_map_2(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h0-> hide, &_Intern__rO3I9s6w_eilck_unsigned_simpl_h1-> hide) : 0;
}

inline
C_size eilck_unsigned_ilength_2(eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h0, eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) {
    return _Intern__rO3I9s6w_eilck_unsigned_simpl_h0 && _Intern__rO3I9s6w_eilck_unsigned_simpl_h1 ? eilck_ihdl_length_2(&_Intern__rO3I9s6w_eilck_unsigned_simpl_h0-> hide, &_Intern__rO3I9s6w_eilck_unsigned_simpl_h1-> hide)/sizeof(_Intern__rO3I9s6w_eilck_unsigned_simpl_h0-> baze[0]) : 0;
}

#ifndef __cplusplus
#define eilck_unsigned_scale_3(H,                                               \
        N, C, ...)                                                              \
eilck_unsigned_scale_gen(H, N, C)

#define eilck_unsigned_scale(...)                                               \
    eilck_unsigned_scale_3(__VA_ARGS__,                                         \
                                      0, 0, 0, )

#define eilck_unsigned_scale2_3(H,                                              \
        N, C, ...)                                                              \
eilck_unsigned_scale2_gen(H,                                                    \
                                     N, C)

#define eilck_unsigned_scale_gen(H,                                             \
        N, C)                                                                   \
((C_unsigned*)(_Generic((char(*)[sizeof((H)[0])]){                              \
    0}, char(*)[sizeof(eilck_impl)]:                                            \
_Generic((H)[0].hide.hide,                                                      \
         eilck_impl:                                                            \
         _Generic((H)[0].base,                                                  \
                  C_unsigned*:                                                  \
                  _Generic((H)[0].type,                                         \
                           void* : eilck_ehdl_scale ))))(&((H)[0].hide),        \
                                   (N)*sizeof((H)[0].baze[0]), (C))))

#define eilck_unsigned_scale2(...)                                              \
    eilck_unsigned_scale2_3(__VA_ARGS__,                                        \
                                       0, 0, 0, )

#define eilck_unsigned_scale2_gen(H,                                            \
        N, C)                                                                   \
((C_unsigned*)(_Generic((char(*)[sizeof((H)[0])]){                              \
    0}, char(*)[sizeof(eilck_impl[2])]:                                         \
_Generic((H)[0][0].hide.hide,                                                   \
         eilck_impl:                                                            \
         _Generic((H)[0][0].base,                                               \
                  C_unsigned*:                                                  \
                  _Generic((H)[0][0].type,                                      \
                           void* : eilck_ehdl_scale2 ))))((void*)(H),           \
                                   (N)*sizeof((H)[0][0].baze[0]), (C))))

#define eilck_unsigned_map(H)                                                   \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].hide.hide,                                                  \
             eilck_impl:                                                        \
             _Generic((H)[0].base,                                              \
                      C_unsigned*:                                              \
                      _Generic((H)[0].type,                                     \
                               void* : eilck_unsigned_emap, void const* : eilck_unsigned_imap ))))((void*)H)

#define eilck_unsigned_map2(H)                                                  \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].hide.hide,                                               \
             eilck_impl:                                                        \
             _Generic((H)[0][0].base,                                           \
                      C_unsigned*:                                              \
                      _Generic((H)[0][0].type,                                  \
                               void* : eilck_unsigned_emap2, void const* : eilck_unsigned_imap2 ))))((void*)H)

#define eilck_unsigned_length(H)                                                \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].hide.hide,                                                  \
             eilck_impl:                                                        \
             _Generic((H)[0].base,                                              \
                      C_unsigned*:                                              \
                      _Generic((H)[0].type,                                     \
                               void* : eilck_unsigned_elength,                  \
                               void const* : eilck_unsigned_ilength ))))((void*)H)

#define eilck_unsigned_length2(H)                                               \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl[2])]:                                             \
    _Generic((H)[0][0].hide.hide,                                               \
             eilck_impl:                                                        \
             _Generic((H)[0][0].base,                                           \
                      C_unsigned*:                                              \
                      _Generic((H)[0][0].type,                                  \
                               void* : eilck_unsigned_elength2,                 \
                               void const* : eilck_unsigned_ilength2 ))))((void*)H)

#define eilck_unsigned_req(H, O)                                                \
    _Generic((char(*)[sizeof((H)[0])]){                                         \
        0},                                                                     \
    char(*)[sizeof(eilck_impl)]:                                                \
    _Generic((H)[0].hide.hide,                                                  \
             eilck_impl:                                                        \
             _Generic((O)[0].hide,                                              \
                      eilck_obj:                                                \
                      _Generic(*(H)[0].base,                                    \
                               C_unsigned:                                      \
                               _Generic(*(O)[0].base,                           \
                                        C_unsigned:                             \
                                        _Generic((H)[0].type,                   \
                                                void* : eilck_unsigned_ereq, void const* : eilck_unsigned_ireq ))))))((void*)(H),\
                                                        (void*)(O))

#define _Intern__rO3I9s6w_eilck_unsigned_simpl_SEQUENCE_11(O,                   \
        _0, _1, _2, _3, _4, _5, _6, _7, _8, _9)                                 \
do {                                                                            \
    eilck_unsigned_req((_0), (O));                                              \
    eilck_unsigned_req((_1), (O));                                              \
    eilck_unsigned_req((_2), (O));                                              \
    eilck_unsigned_req((_3), (O));                                              \
    eilck_unsigned_req((_4), (O));                                              \
    eilck_unsigned_req((_5), (O));                                              \
    eilck_unsigned_req((_6), (O));                                              \
    eilck_unsigned_req((_7), (O));                                              \
    eilck_unsigned_req((_8), (O));                                              \
    eilck_unsigned_req((_9), (O));                                              \
}                                                                               \
while (0)

#define _Intern__rO3I9s6w_eilck_unsigned_simpl_SEQUENCE_10(O,                   \
        _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, ...)                            \
_Intern__rO3I9s6w_eilck_unsigned_simpl_SEQUENCE_11(O,                           \
        _0, _1, _2, _3, _4, _5, _6, _7, _8, _9)

#define eilck_unsigned_nil ((eilck_unsigned_ehdl*)0)

#define eilck_unsigned_SEQUENCE(O,                                              \
                                        ...)                                    \
_Intern__rO3I9s6w_eilck_unsigned_simpl_SEQUENCE_10(O,                           \
        __VA_ARGS__, eilck_unsigned_nil, eilck_unsigned_nil, eilck_unsigned_nil, eilck_unsigned_nil, eilck_unsigned_nil, eilck_unsigned_nil, eilck_unsigned_nil, eilck_unsigned_nil, eilck_unsigned_nil,)

#endif

#ifdef CMOD_2F746D702F636D6F642D746D70642E5970566D56774674724F3349397336772F65696C636B2D756E7369676E65642E63_HEADER_INSTANTIATE
extern inline eilck_unsigned_obj* eilck_unsigned_init(eilck_unsigned_obj* _Intern__rO3I9s6w_eilck_unsigned_simpl_o, C_size _Intern__rO3I9s6w_eilck_unsigned_simpl_len) ;

extern inline void eilck_unsigned_destroy(eilck_unsigned_obj* _Intern__rO3I9s6w_eilck_unsigned_simpl_o) ;

extern inline _Intern__rO3I9s6w_eilck_unsigned_simpl_eT* eilck_unsigned_emap(eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h) ;

extern inline int eilck_unsigned_ereq(eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h, eilck_unsigned_obj* _Intern__rO3I9s6w_eilck_unsigned_simpl_o) ;

extern inline C_size eilck_unsigned_elength(eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h) ;

extern inline _Intern__rO3I9s6w_eilck_unsigned_simpl_iT* eilck_unsigned_imap(eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h) ;

extern inline int eilck_unsigned_ireq(eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h, eilck_unsigned_obj* _Intern__rO3I9s6w_eilck_unsigned_simpl_o) ;

extern inline C_size eilck_unsigned_ilength(eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h) ;

extern inline _Intern__rO3I9s6w_eilck_unsigned_simpl_eT* eilck_unsigned_emap_2(eilck_unsigned_ehdl* restrict _Intern__rO3I9s6w_eilck_unsigned_simpl_h0, eilck_unsigned_ehdl* restrict _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) ;

extern inline
C_size eilck_unsigned_elength_2(eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h0, eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) ;

extern inline _Intern__rO3I9s6w_eilck_unsigned_simpl_iT* eilck_unsigned_imap_2(eilck_unsigned_ihdl* restrict _Intern__rO3I9s6w_eilck_unsigned_simpl_h0, eilck_unsigned_ihdl* restrict _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) ;

extern inline
C_size eilck_unsigned_ilength_2(eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h0, eilck_unsigned_ihdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h1) ;
#endif

extern C_size eilck_unsigned_elength2(eilck_unsigned_ehdl (*_Intern__rO3I9s6w_eilck_unsigned_simpl_h)[2]);

extern _Intern__rO3I9s6w_eilck_unsigned_simpl_eT* eilck_unsigned_emap2(eilck_unsigned_ehdl (*_Intern__rO3I9s6w_eilck_unsigned_simpl_h)[2]);

extern C_size eilck_unsigned_ilength2(eilck_unsigned_ihdl (*_Intern__rO3I9s6w_eilck_unsigned_simpl_h)[2]);

extern _Intern__rO3I9s6w_eilck_unsigned_simpl_iT* eilck_unsigned_imap2(eilck_unsigned_ihdl (*_Intern__rO3I9s6w_eilck_unsigned_simpl_h)[2]);

extern C_unsigned* (eilck_unsigned_scale)(eilck_unsigned_ehdl* _Intern__rO3I9s6w_eilck_unsigned_simpl_h, C_size _Intern__rO3I9s6w_eilck_unsigned_simpl_nsize, void* cntx);

extern C_unsigned* (eilck_unsigned_scale2)(eilck_unsigned_ehdl (*_Intern__rO3I9s6w_eilck_unsigned_simpl_h)[2], C_size _Intern__rO3I9s6w_eilck_unsigned_simpl_nsize, void* cntx);

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E5970566D56774674724F3349397336772F65696C636B2D756E7369676E65642E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-unsigned */
