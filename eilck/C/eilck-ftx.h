/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_ftx__GUARD
#define eilck_ftx__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "eilck.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_ftx
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E7261476C6D7067527A456D44756B54702F65696C636B2D6674782E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E7261476C6D7067527A456D44756B54702F65696C636B2D6674782E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E7261476C6D7067527A456D44756B54702F65696C636B2D6674782E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E7261476C6D7067527A456D44756B54702F65696C636B2D6674782E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_ftx_MODULE eilck_ftx module internals
 ** @ingroup eilck_ftx
 ** @{ */

#ifndef NO_ATOMICS
extern void* _Intern__zEmDukTp_eilck_ftx_atomic_abi;

#endif

typedef struct eilck_ftx eilck_ftx;
struct eilck_ftx {
    _Atomic(int) _Intern__zEmDukTp_eilck_ftx_val;
};

typedef struct {
    C_size len;
    eilck_ftx tab[];
} eilck_ftx_offset_type;

enum _MODULE_tag_zEmDukTp_114 { eilck_ftx_offset = __Cmod_offsetof(eilck_ftx_offset_type, tab), };
typedef enum _MODULE_tag_zEmDukTp_114 _MODULE_tag_zEmDukTp_114;

#define eilck_ftx_INITIALIZER(X)                                                \
    {                                                                           \
        ._Intern__zEmDukTp_eilck_ftx_val = (X),                                 \
    }

#ifdef __CMOD__
inline
eilck_ftx* eilck_ftx_init(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    if (_Intern__zEmDukTp_eilck_ftx_c)
        *_Intern__zEmDukTp_eilck_ftx_c = (eilck_ftx)eilck_ftx_INITIALIZER(_Intern__zEmDukTp_eilck_ftx_val);
    return _Intern__zEmDukTp_eilck_ftx_c;
}

inline
int eilck_ftx_load(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) {
    return C_atomic_load_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, C_atomic_acquire);
}

inline
void eilck_ftx_store(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    C_atomic_store_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_val, C_atomic_release);
}

inline
void eilck_ftx_clear(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) {
    C_atomic_store_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, 0, C_atomic_release);
}

inline
int eilck_ftx_swap(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_desired) {
    return C_atomic_exchange_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_desired, C_atomic_acq_rel);
}

inline
bool eilck_ftx_cas(eilck_ftx* restrict _Intern__zEmDukTp_eilck_ftx_c, int* restrict _Intern__zEmDukTp_eilck_ftx_expected, int _Intern__zEmDukTp_eilck_ftx_desired) {
    return C_atomic_compare_exchange_strong_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_expected, _Intern__zEmDukTp_eilck_ftx_desired, C_atomic_acq_rel, C_atomic_acquire);
}

inline
int eilck_ftx_tas(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) {
    return C_atomic_exchange_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, 1, C_atomic_acq_rel);
}

#define eilck_ftx_add(A, B) ((A)+(B))

#define eilck_ftx_sub(A, B) ((A)-(B))

#define eilck_ftx_or(A, B) ((A)| (B))

#define eilck_ftx_and(A, B) ((A)& (B))

#define eilck_ftx_xor(A, B) ((A)^(B))

#define eilck_ftx_mul(A, B) ((A)*(B))

#define eilck_ftx_div(A, B) ((A)/(B))

#define eilck_ftx_mod(A, B) ((A)%(B))

#define eilck_ftx_lsh(A, B) ((A)<< (B))

#define eilck_ftx_rsh(A, B) ((A)>> (B))

#define eilck_ftx_min(A, B) ((A)<= (B)?(A):(B))

#define eilck_ftx_max(A, B) ((A)>= (B)?(A):(B))

inline
int eilck_ftx_fetch_mul(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_mul(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return exp;
}

inline
int eilck_ftx_mul_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_mul(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return des;
}

inline
int eilck_ftx_fetch_div(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_div(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return exp;
}

inline
int eilck_ftx_div_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_div(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return des;
}

inline
int eilck_ftx_fetch_mod(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_mod(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return exp;
}

inline
int eilck_ftx_mod_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_mod(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return des;
}

inline
int eilck_ftx_fetch_lsh(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_lsh(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return exp;
}

inline
int eilck_ftx_lsh_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_lsh(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return des;
}

inline
int eilck_ftx_fetch_rsh(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_rsh(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return exp;
}

inline
int eilck_ftx_rsh_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_rsh(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return des;
}

inline
int eilck_ftx_fetch_min(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_min(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return exp;
}

inline
int eilck_ftx_min_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_min(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return des;
}

inline
int eilck_ftx_fetch_max(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_max(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return exp;
}

inline
int eilck_ftx_max_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    int exp = eilck_ftx_load(_Intern__zEmDukTp_eilck_ftx_c);
    int des;
    do {
        des = eilck_ftx_max(exp, _Intern__zEmDukTp_eilck_ftx_val);
    } while (! eilck_ftx_cas(_Intern__zEmDukTp_eilck_ftx_c, &exp, des));
    return des;
}

inline
int eilck_ftx_fetch_add(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return C_atomic_fetch_add_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_val, C_atomic_acq_rel);
}

inline
int eilck_ftx_add_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return eilck_ftx_add(eilck_ftx_fetch_add(_Intern__zEmDukTp_eilck_ftx_c, _Intern__zEmDukTp_eilck_ftx_val), _Intern__zEmDukTp_eilck_ftx_val);
}

inline
int eilck_ftx_fetch_sub(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return C_atomic_fetch_sub_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_val, C_atomic_acq_rel);
}

inline
int eilck_ftx_sub_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return eilck_ftx_sub(eilck_ftx_fetch_sub(_Intern__zEmDukTp_eilck_ftx_c, _Intern__zEmDukTp_eilck_ftx_val), _Intern__zEmDukTp_eilck_ftx_val);
}

inline
int eilck_ftx_fetch_or(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return C_atomic_fetch_or_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_val, C_atomic_acq_rel);
}

inline
int eilck_ftx_or_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return eilck_ftx_or(eilck_ftx_fetch_or(_Intern__zEmDukTp_eilck_ftx_c, _Intern__zEmDukTp_eilck_ftx_val), _Intern__zEmDukTp_eilck_ftx_val);
}

inline
int eilck_ftx_fetch_and(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return C_atomic_fetch_and_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_val, C_atomic_acq_rel);
}

inline
int eilck_ftx_and_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return eilck_ftx_and(eilck_ftx_fetch_and(_Intern__zEmDukTp_eilck_ftx_c, _Intern__zEmDukTp_eilck_ftx_val), _Intern__zEmDukTp_eilck_ftx_val);
}

inline
int eilck_ftx_fetch_xor(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return C_atomic_fetch_xor_explicit(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_val, C_atomic_acq_rel);
}

inline
int eilck_ftx_xor_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return eilck_ftx_xor(eilck_ftx_fetch_xor(_Intern__zEmDukTp_eilck_ftx_c, _Intern__zEmDukTp_eilck_ftx_val), _Intern__zEmDukTp_eilck_ftx_val);
}

inline
int eilck_ftx_wait(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) {
    return Linux_futex_wait(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_val);
}

inline
int eilck_ftx_wake(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_cnt) {
    return Linux_futex_wake(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val, _Intern__zEmDukTp_eilck_ftx_cnt);
}

inline
int eilck_ftx_broadcast(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) {
    return Linux_futex_broadcast(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val);
}

inline
int eilck_ftx_signal(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) {
    return Linux_futex_signal(&_Intern__zEmDukTp_eilck_ftx_c-> _Intern__zEmDukTp_eilck_ftx_val);
}

#else
eilck_ftx* eilck_ftx_init(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
void eilck_ftx_store(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
bool eilck_ftx_cas(eilck_ftx* restrict _Intern__zEmDukTp_eilck_ftx_c, int* restrict _Intern__zEmDukTp_eilck_ftx_expected, int _Intern__zEmDukTp_eilck_ftx_desired);

int eilck_ftx_fetch_add(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_add_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_sub(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_sub_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_or(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_or_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_and(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_and_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_xor(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_xor_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_mul(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_mul_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_div(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_div_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_mod(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_mod_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_lsh(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_lsh_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_rsh(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_rsh_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_min(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_min_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_fetch_max(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);
int eilck_ftx_max_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_swap(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_wait(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_wake(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val);

int eilck_ftx_load(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c);

int eilck_ftx_clear(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c);

int eilck_ftx_tas(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c);

int eilck_ftx_broadcast(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c);

int eilck_ftx_signal(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c);

#endif

#ifdef CMOD_2F746D702F636D6F642D746D70642E7261476C6D7067527A456D44756B54702F65696C636B2D6674782E63_HEADER_INSTANTIATE
extern inline
eilck_ftx* eilck_ftx_init(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_load(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) ;

extern inline
void eilck_ftx_store(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
void eilck_ftx_clear(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) ;

extern inline
int eilck_ftx_swap(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_desired) ;

extern inline
bool eilck_ftx_cas(eilck_ftx* restrict _Intern__zEmDukTp_eilck_ftx_c, int* restrict _Intern__zEmDukTp_eilck_ftx_expected, int _Intern__zEmDukTp_eilck_ftx_desired) ;

extern inline
int eilck_ftx_tas(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) ;

extern inline
int eilck_ftx_fetch_mul(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_mul_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_div(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_div_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_mod(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_mod_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_lsh(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_lsh_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_rsh(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_rsh_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_min(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_min_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_max(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_max_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_add(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_add_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_sub(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_sub_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_or(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_or_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_and(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_and_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_fetch_xor(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_xor_fetch(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_wait(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_val) ;

extern inline
int eilck_ftx_wake(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c, int _Intern__zEmDukTp_eilck_ftx_cnt) ;

extern inline
int eilck_ftx_broadcast(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) ;

extern inline
int eilck_ftx_signal(eilck_ftx* _Intern__zEmDukTp_eilck_ftx_c) ;
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E7261476C6D7067527A456D44756B54702F65696C636B2D6674782E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-ftx */
