/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck__GUARD
#define eilck__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-snippet.h"
#include "C-attr.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @def eilck_FAILED
 ** @brief Macro expansion computed at compile time from expression <code> m_FAILED </code>
 **/

#define eilck_FAILED ((void*)0xffffffffffffffff)
/** @def eilck_R
 ** @brief Macro expansion computed at compile time from expression <code> m_READ </code>
 **/

#define eilck_R +1
/** @def eilck_RW
 ** @brief Macro expansion computed at compile time from expression <code>(R | W)</code>
 **/

#define eilck_RW +3
/** @def eilck_RWX
 ** @brief Macro expansion computed at compile time from expression <code>(R | W | X)</code>
 **/

#define eilck_RWX +7
/** @def eilck_RX
 ** @brief Macro expansion computed at compile time from expression <code>(R | X)</code>
 **/

#define eilck_RX +5
/** @def eilck_W
 ** @brief Macro expansion computed at compile time from expression <code> m_WRITE </code>
 **/

#define eilck_W +2
/** @def eilck_X
 ** @brief Macro expansion computed at compile time from expression <code> m_EXEC </code>
 **/

#define eilck_X +4
/**
 ** @module eilck
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E546F584A4D58757A4C74426A4A4B50512F65696C636B2E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E546F584A4D58757A4C74426A4A4B50512F65696C636B2E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E546F584A4D58757A4C74426A4A4B50512F65696C636B2E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E546F584A4D58757A4C74426A4A4B50512F65696C636B2E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_MODULE eilck module internals
 ** @ingroup eilck
 ** @{ */

#ifdef __CMOD_CONSTANT__
extern int _Intern_eilck__Constant(int argc, char* argv[argc+1]);
#endif

#ifndef eilck_FAILED
#define eilck_FAILED (Linux_m_FAILED)
#endif

#ifndef eilck_R
#define eilck_R (Linux_m_READ)
#endif

#ifndef eilck_RW
#define eilck_RW ((eilck_R | eilck_W))
#endif

#ifndef eilck_RWX
#define eilck_RWX ((eilck_R | eilck_W | eilck_X))
#endif

#ifndef eilck_RX
#define eilck_RX ((eilck_R | eilck_X))
#endif

#ifndef eilck_W
#define eilck_W (Linux_m_WRITE)
#endif

#ifndef eilck_X
#define eilck_X (Linux_m_EXEC)
#endif

enum _MODULE_tag_LtBjJKPQ_120 { eilck_min = 5, eilck_len = 1 << 10, };
typedef enum _MODULE_tag_LtBjJKPQ_120 _MODULE_tag_LtBjJKPQ_120;

typedef void eilck_writeHdl;

typedef void const eilck_readHdl;

typedef struct eilck_voidBase eilck_voidBase;
struct eilck_voidBase {
    unsigned char x;
};

#ifndef __cplusplus
#define eilck_exclusive(H)                                                      \
    _Generic((H),                                                               \
             void const*: false, void const volatile*: false,                   \
             default: true)
#else
#define eilck_exclusive __excl
#endif

static_assert(sizeof(eilck_voidBase) == 1, "void base must be 1");

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E546F584A4D58757A4C74426A4A4B50512F65696C636B2E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck */
