/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_lck__GUARD
#define eilck_lck__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "eilck.h"
#include "eilck-ftx.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @def eilck_lck_LOCKER
 ** @brief Macro expansion computed at compile time from expression <code>(-(int)(-1u>>1)-1)</code>
 **/

#define eilck_lck_LOCKER -2147483648
/**
 ** @module eilck_lck
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E5262557572656444754145444552304A2F65696C636B2D6C636B2E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E5262557572656444754145444552304A2F65696C636B2D6C636B2E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E5262557572656444754145444552304A2F65696C636B2D6C636B2E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E5262557572656444754145444552304A2F65696C636B2D6C636B2E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_lck_MODULE eilck_lck module internals
 ** @ingroup eilck_lck
 ** @{ */

#ifdef __CMOD_CONSTANT__
extern int _Intern_eilck_lck__Constant(int argc, char* argv[argc+1]);
#endif

typedef eilck_ftx eilck_lck;

#ifndef eilck_lck_LOCKER
#define eilck_lck_LOCKER ((-(int)(-1u>>1)-1))
#endif

extern void _Intern__uAEDER0J_eilck_lck_slow(eilck_lck* _Intern__uAEDER0J_eilck_lck_l, int _Intern__uAEDER0J_eilck_lck_current);

inline
int eilck_lck_lock(eilck_lck* _Intern__uAEDER0J_eilck_lck_c) {

    int _Intern__uAEDER0J_eilck_lck_current = 0;
    if (! eilck_ftx_cas(_Intern__uAEDER0J_eilck_lck_c, &_Intern__uAEDER0J_eilck_lck_current, eilck_lck_LOCKER + 1))
        _Intern__uAEDER0J_eilck_lck_slow(_Intern__uAEDER0J_eilck_lck_c, _Intern__uAEDER0J_eilck_lck_current);
    return 0;
}

inline
int eilck_lck_unlock(eilck_lck* _Intern__uAEDER0J_eilck_lck_c) {

    if ((eilck_ftx_load(_Intern__uAEDER0J_eilck_lck_c) < 0)
            && eilck_ftx_sub_fetch(_Intern__uAEDER0J_eilck_lck_c, eilck_lck_LOCKER + 1))
        eilck_ftx_broadcast(_Intern__uAEDER0J_eilck_lck_c);
    return 0;
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E5262557572656444754145444552304A2F65696C636B2D6C636B2E63_HEADER_INSTANTIATE
extern inline
int eilck_lck_lock(eilck_lck* _Intern__uAEDER0J_eilck_lck_c) ;

extern inline
int eilck_lck_unlock(eilck_lck* _Intern__uAEDER0J_eilck_lck_c) ;
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E5262557572656444754145444552304A2F65696C636B2D6C636B2E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-lck */
