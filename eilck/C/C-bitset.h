/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef C_bitset__GUARD
#define C_bitset__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module C_bitset
 ** @ingroup C
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E574E475A576D337845503731385265372F432D6269747365742E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E574E475A576D337845503731385265372F432D6269747365742E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E574E475A576D337845503731385265372F432D6269747365742E63_HEADER
#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E574E475A576D337845503731385265372F432D6269747365742E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup C_bitset_MODULE C_bitset module internals
 ** @ingroup C_bitset
 ** @{ */

#define C_bitset__Operator_eval(... )                                           \
    C_unsignedof(__VA_ARGS__ + 0)

enum C_bitset_constants {
    C_bitset__Operator_bor = -1,
    C_bitset__Operator_bnot = -1,
    C_bitset__Operator_bnotbnot = 0,
    C_bitset__Operator_add = 0,
    C_bitset__Operator_band = 0,
};
typedef enum C_bitset_constants C_bitset_constants;

#define C_bitset__Operator_bnotbnot(... )                                       \
    C_bitset__Operator_eval(__VA_ARGS__ )

#define C_bitset__Operator_bnot(... )                                           \
    ~ C_bitset__Operator_eval(__VA_ARGS__ )

#define C_bitset__Operator_bor(X ,                                              \
        Y )                                                                     \
( C_bitset__Operator_eval(X ) | C_bitset__Operator_eval(Y ) )

#define C_bitset__Operator_band(X ,                                             \
        Y )                                                                     \
( C_bitset__Operator_eval(X ) & C_bitset__Operator_eval(Y ) )

#define C_bitset__Operator_bxor(X ,                                             \
        Y )                                                                     \
( C_bitset__Operator_eval(X ) ^ C_bitset__Operator_eval(Y ) )

#define C_bitset__Operator_add(X ,                                              \
        Y )                                                                     \
C_bitset__Operator_bor(X , Y )

#define C_bitset__Operator_sub(X ,                                              \
        Y )                                                                     \
C_bitset__Operator_band(X , (C_bitset__Operator_bxor(X , Y)))

#define C_bitset__Operator_ls(X ,                                               \
        Y )                                                                     \
( C_bitset__Operator_eval(X ) << (Y))

#define C_bitset__Operator_rs(X ,                                               \
        Y )                                                                     \
( C_bitset__Operator_eval(X ) >> (Y))

#define C_bitset_setof(x)                                                       \
    C_bitset__Operator_ls(1ULL ,                                                \
                                     (x))

#define C_bitset__Operator_notnot(X )                                           \
    (!! C_bitset__Operator_eval(X ) )

#define C_bitset__Operator_eq(X ,                                               \
        Y )                                                                     \
(! C_bitset__Operator_bxor(Y ,                                                  \
                                      X ) )

#define C_bitset__Operator_ge(X ,                                               \
        Y )                                                                     \
(! C_bitset__Operator_sub(Y ,                                                   \
                                     X ) )

#define C_bitset__Operator_gt(X ,                                               \
        Y )                                                                     \
( C_bitset__Operator_ge(X , Y ) && !(C_bitset__Operator_eq((X ),                \
        ( Y ))) )

#define C_bitset__Operator_le(X ,                                               \
        Y )                                                                     \
(! C_bitset__Operator_sub(X ,                                                   \
                                     Y ) )

#define C_bitset__Operator_lt(X ,                                               \
        Y )                                                                     \
( C_bitset__Operator_le(X , Y ) && !(C_bitset__Operator_eq((X ),                \
        ( Y ))) )

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E574E475A576D337845503731385265372F432D6269747365742E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* C-bitset */
