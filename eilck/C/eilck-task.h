/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_task__GUARD
#define eilck_task__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-snippet.h"
#include "C-attr.h"
#include "C-snippet-flexible.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-fixed.h"
#include "eilck-sem.h"
#include "eilck-obj-blks.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"
#include "eilck-impl.h"
#include "eilck-impl-snippet.h"
#include "eilck-ehdl.h"
#include "eilck-ihdl.h"
#include "eilck-csv.h"
#include "eilck-impl-SECTION.h"
#include "eilck-hdl.h"
#include "eilck-hdl-group.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_task
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E34344168614E4A5A33534A32493368732F65696C636B2D7461736B2E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E34344168614E4A5A33534A32493368732F65696C636B2D7461736B2E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E34344168614E4A5A33534A32493368732F65696C636B2D7461736B2E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E34344168614E4A5A33534A32493368732F65696C636B2D7461736B2E63_HEADER
#ifndef C_attribute__Operator_eval
# define C_attribute__Operator_eval(...) __VA_ARGS__
#endif

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_task_MODULE eilck_task module internals
 ** @ingroup eilck_task
 ** @{ */

enum eilck_task_phase {
    eilck_task_counting,
    eilck_task_owning,
    eilck_task_subscribing,
    eilck_task_all,
};
typedef enum eilck_task_phase eilck_task_phase;

#define eilck_task__Intern_defill_ENUM_LIST eilck_task_counting,                \
    eilck_task_owning, eilck_task_subscribing, eilck_task_all

static_assert(sizeof(int const[]) {
    eilck_task__Intern_defill_ENUM_LIST,
}, "LIST must expand to a list of valid constants, did you forget a declaration directive before a defill?");

#if defined(__CMOD__)
enum _Intern__3SJ2I3hs_eilck_task_ENUM_dummy {

    _Intern__3SJ2I3hs_eilck_task_ENUM_prelen = sizeof C_STRINGIFY0(eilck_task) - 2,

    eilck_task_MAX = C_comma_COUNT(eilck_task__Intern_defill_ENUM_LIST),
};
typedef enum _Intern__3SJ2I3hs_eilck_task_ENUM_dummy _Intern__3SJ2I3hs_eilck_task_ENUM_dummy;
#endif

extern C_attr_const
char const* eilck_task_getname(unsigned x);

extern C_attr_pure
char const* eilck_task_getshort(unsigned x);

extern C_attr_pure
unsigned eilck_task_parse(char const s[]);

extern char const* restrict const _Intern__3SJ2I3hs_eilck_task_ENUM_names[];

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

extern char const* restrict const* restrict const _Intern__3SJ2I3hs_eilck_task_ENUM_shorts;

extern void _Intern__3SJ2I3hs_eilck_task_ENUM_startup(void);

enum eilck_task_files {
    eilck_task_rt = 0,
    eilck_task_fi = 0,
    eilck_task_gr = 0,
};
typedef enum eilck_task_files eilck_task_files;

enum eilck_task_cases {
    _Intern__3SJ2I3hs_eilck_task_inblock = 0,
};
typedef enum eilck_task_cases eilck_task_cases;

#define eilck_task__Intern_defill_FA_OTHERS C_size tot;                         \
    C_size grain;                                                               \
    C_size id;                                                                  \

#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert(sizeof((eilck_hdl_group) {
    0
}), "T in C#snippet#flexible must be a complete type");
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert( !C_HAS2( 1) || (C_FIRST( 1) <= C_SECOND( 1)), "bounds inverted: " C_STRINGIFY(C_FIRST( 1)) " is > " C_STRINGIFY(C_SECOND( 1)));
static_assert(C_FIRST( 1) <= C_snippet_flexible_min, "min is < " C_STRINGIFY(C_FIRST( 1)));
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_task__MODULE_CON(I6_Block1C7snippet8flexible4INIT10_Snippet_4E)(void) {
    if (0)
        C_attr_maybe_unused eilck_hdl_group x[C_snippet_flexible_min] = C_snippet_flexible_INIT;
};
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_task__MODULE_CON(I6_Block1C7snippet8flexible6OTHERS10_Snippet_4E)(void) {
    if (0) {
        C_attr_maybe_unused struct {
            C_size len;
            eilck_task__Intern_defill_FA_OTHERS
        } _Intern__3SJ2I3hs_eilck_task_FA_dummy = { .len = 67 };
    }
};
#endif

typedef struct {
    C_size len;
    eilck_task__Intern_defill_FA_OTHERS eilck_hdl_group tab[];
} eilck_task_FA_offset_type;

enum _Intern__3SJ2I3hs_eilck_task_FA_edummy {
    _Intern__3SJ2I3hs_eilck_task_FA_offset = __Cmod_offsetof(eilck_task_FA_offset_type, tab),
};
typedef enum _Intern__3SJ2I3hs_eilck_task_FA_edummy _Intern__3SJ2I3hs_eilck_task_FA_edummy;

typedef struct {
    C_size _Intern__3SJ2I3hs_eilck_task_FA_mutableLen;
    eilck_task__Intern_defill_FA_OTHERS
} _Intern__3SJ2I3hs_eilck_task_FA_header;

typedef union eilck_task_FA_fa eilck_task_FA_fa;
typedef union eilck_task_FA_fa eilck_task_FA_fa;

typedef union eilck_task_FA_fa eilck_task_FA_fa;
union eilck_task_FA_fa {

    struct {

        union {
            unsigned char _Intern__3SJ2I3hs_eilck_task_FA_chars[_Intern__3SJ2I3hs_eilck_task_FA_offset];
            _Intern__3SJ2I3hs_eilck_task_FA_header head;
        };
        eilck_hdl_group _Intern__3SJ2I3hs_eilck_task_FA_fixedTab[C_snippet_flexible_min];
    } _Intern__3SJ2I3hs_eilck_task_FA_overlay;
    _Intern__3SJ2I3hs_eilck_task_FA_edummy _Intern__3SJ2I3hs_eilck_task_FA_dummy;

    struct {
        C_size const len;
        eilck_task__Intern_defill_FA_OTHERS
        eilck_hdl_group tab[];
    };
};

#define eilck_task_FA_COMPOUND(LEN)                                             \
    &((union {                                                                  \
        unsigned char _Intern__3SJ2I3hs_eilck_task_FA_maximum[_Intern__3SJ2I3hs_eilck_task_FA_offset+sizeof(eilck_hdl_group[LEN])];\
        struct {                                                                \
            union {                                                             \
                unsigned char _Intern__3SJ2I3hs_eilck_task_FA_chars[_Intern__3SJ2I3hs_eilck_task_FA_offset];\
                _Intern__3SJ2I3hs_eilck_task_FA_header head;                    \
            };                                                                  \
            eilck_hdl_group _Intern__3SJ2I3hs_eilck_task_FA_fixedTab[LEN];      \
        } _Intern__3SJ2I3hs_eilck_task_FA_overlay;                              \
        eilck_task_FA_fa _Intern__3SJ2I3hs_eilck_task_FA_dummy;                 \
    }){                                                                         \
        ._Intern__3SJ2I3hs_eilck_task_FA_overlay = {                            \
                                                            .head = {           \
                                                                     ._Intern__3SJ2I3hs_eilck_task_FA_mutableLen = (LEN),\
                                                                    }, ._Intern__3SJ2I3hs_eilck_task_FA_fixedTab = C_snippet_flexible_INIT,\
                                                           }, })._Intern__3SJ2I3hs_eilck_task_FA_dummy

extern eilck_task_FA_fa* eilck_task_FA_alloc(C_size _Intern__3SJ2I3hs_eilck_task_FA_length);

typedef int __internal_dummy_type_to_be_ignored;

typedef eilck_task_FA_fa eilck_task;

#define eilck_task_sole alignas(eilck_obj_blks_grain_default)

#define eilck_task_ONCE                                                         \
    for (bool _Intern__3SJ2I3hs_eilck_task_flag = true;                         \
            _Intern__3SJ2I3hs_eilck_task_flag;                                  \
            _Intern__3SJ2I3hs_eilck_task_flag = false)

#define eilck_task_PRE(...)                                                     \
    for (bool _Intern__3SJ2I3hs_eilck_task_flag = ((void)(__VA_ARGS__),         \
            true);                                                              \
            _Intern__3SJ2I3hs_eilck_task_flag;                                  \
            _Intern__3SJ2I3hs_eilck_task_flag = false)

#define eilck_task_POST(...)                                                    \
    for (bool _Intern__3SJ2I3hs_eilck_task_flag = true;                         \
            _Intern__3SJ2I3hs_eilck_task_flag;                                  \
            (void)(__VA_ARGS__), _Intern__3SJ2I3hs_eilck_task_flag = false)

#define eilck_task_PRE_POST(PR,                                                 \
                                       ...)                                     \
for (bool _Intern__3SJ2I3hs_eilck_task_flag = ((void)(PR),                      \
        true);                                                                  \
        _Intern__3SJ2I3hs_eilck_task_flag;                                      \
        (void)(__VA_ARGS__), _Intern__3SJ2I3hs_eilck_task_flag = false)

#define eilck_task_DECL(...)                                                    \
    eilck_task_ONCE                                                             \
    for (__VA_ARGS__;                                                           \
            _Intern__3SJ2I3hs_eilck_task_flag;                                  \
            _Intern__3SJ2I3hs_eilck_task_flag = false)

#define eilck_task_micro() (eilck_task_cgroup+0)

#define eilck_task_iteration()                                                  \
    (_Intern__3SJ2I3hs_eilck_task_iter*eilck_task_itIncDup*eilck_task_itIncAlt+eilck_task_itOffDup*eilck_task_itIncAlt+eilck_task_itOffAlt)

#define eilck_task_step() (eilck_task_cstep+0)

#define eilck_task_ID()                                                         \
    (_Intern__3SJ2I3hs_eilck_task_p-> id+0)

#define eilck_task_SPLIT

#define eilck_task_SPLIT_seq eilck_task_ITERATE_TASK_seq() eilck_task_DECL(bool eilck_task_breaker = false) eilck_task_DECL(C_size eilck_task_cstep = 0,\
        eilck_task_inner = 0, eilck_task_cgroup = 0, _Intern__3SJ2I3hs_eilck_task_pgrain = _Intern__3SJ2I3hs_eilck_task_p-> grain,\
        eilck_task_plen = _Intern__3SJ2I3hs_eilck_task_p-> len,                 \
        _Intern__3SJ2I3hs_eilck_task_plen1 = eilck_task_plen-1) eilck_task_DECL(eilck_hdl_group*const restrict _Intern__3SJ2I3hs_eilck_task_ptab = _Intern__3SJ2I3hs_eilck_task_p-> tab)\
for (unsigned _Intern__3SJ2I3hs_eilck_task_twice = 0;                           \
        _Intern__3SJ2I3hs_eilck_task_twice < 2;                                 \
        ++_Intern__3SJ2I3hs_eilck_task_twice)                                   \
    if (_Intern__3SJ2I3hs_eilck_task_twice) {                                   \
                                                                                \
        switch (_Intern__3SJ2I3hs_eilck_task_iter) {                            \
                                                                                \
        case eilck_task_counting: _Intern__3SJ2I3hs_eilck_task_p-> tot = eilck_task_cstep;\
            _Intern__3SJ2I3hs_eilck_task_pgrain = eilck_task_cstep/_Intern__3SJ2I3hs_eilck_task_p-> len;\
            _Intern__3SJ2I3hs_eilck_task_p-> grain = _Intern__3SJ2I3hs_eilck_task_pgrain;\
            break;                                                              \
                                                                                \
        case eilck_task_owning:                                                 \
            for (unsigned _Intern__3SJ2I3hs_eilck_task_b = 1;                   \
                    _Intern__3SJ2I3hs_eilck_task_b < eilck_task_plen;           \
                    ++_Intern__3SJ2I3hs_eilck_task_b) {                         \
                eilck_task_dependinner(_Intern__3SJ2I3hs_eilck_task_p,          \
                                                _Intern__3SJ2I3hs_eilck_task_b, _Intern__3SJ2I3hs_eilck_task_ptab[_Intern__3SJ2I3hs_eilck_task_b-1].id);\
            } break;                                                            \
                                                                                \
        case eilck_task_subscribing: eilck_obj_blks_imark(_Intern__3SJ2I3hs_eilck_task_ptab[0].blk,\
                    &eilck_task_itend, sizeof eilck_task_itend);                \
                                                                                \
            if(eilck_task_inner) {                                              \
                eilck_hdl_group_req(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup],\
                                            eilck_task_all, &up-> tab[eilck_task_cgroup]);\
            } break;                                                            \
                                                                                \
        default:                                                                \
            for (;                                                              \
                    eilck_task_cgroup < eilck_task_plen;                        \
                    ++eilck_task_cgroup) {                                      \
                                                                                \
                if (_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_itend && eilck_task_iteration() < eilck_task_ittotal) {\
                    eilck_hdl_group_acq(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup]);\
                    eilck_hdl_group_rel(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup]);\
                }                                                               \
                else {                                                          \
                    eilck_hdl_group_destroy(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup]);\
                } } } }                                                         \
    else

#define eilck_task_SPLIT_omp eilck_task_ITERATE_TASK_omp() eilck_task_DECL(bool eilck_task_breaker = false) eilck_task_DECL(C_size eilck_task_cstep = 0,\
        eilck_task_inner = 0, eilck_task_cgroup = 0, _Intern__3SJ2I3hs_eilck_task_pgrain = _Intern__3SJ2I3hs_eilck_task_p-> grain,\
        eilck_task_plen = _Intern__3SJ2I3hs_eilck_task_p-> len,                 \
        _Intern__3SJ2I3hs_eilck_task_plen1 = eilck_task_plen-1) eilck_task_DECL(eilck_hdl_group*const restrict _Intern__3SJ2I3hs_eilck_task_ptab = _Intern__3SJ2I3hs_eilck_task_p-> tab)\
for (unsigned _Intern__3SJ2I3hs_eilck_task_twice = 0;                           \
        _Intern__3SJ2I3hs_eilck_task_twice < 2;                                 \
        ++_Intern__3SJ2I3hs_eilck_task_twice)                                   \
    if (_Intern__3SJ2I3hs_eilck_task_twice) {                                   \
                                                                                \
        switch (_Intern__3SJ2I3hs_eilck_task_iter) {                            \
                                                                                \
        case eilck_task_counting: _Intern__3SJ2I3hs_eilck_task_p-> tot = eilck_task_cstep;\
            _Intern__3SJ2I3hs_eilck_task_pgrain = eilck_task_cstep/_Intern__3SJ2I3hs_eilck_task_p-> len;\
            _Intern__3SJ2I3hs_eilck_task_p-> grain = _Intern__3SJ2I3hs_eilck_task_pgrain;\
            break;                                                              \
                                                                                \
        case eilck_task_owning:                                                 \
            for (unsigned _Intern__3SJ2I3hs_eilck_task_b = 1;                   \
                    _Intern__3SJ2I3hs_eilck_task_b < eilck_task_plen;           \
                    ++_Intern__3SJ2I3hs_eilck_task_b) {                         \
                eilck_task_dependinner(_Intern__3SJ2I3hs_eilck_task_p,          \
                                                _Intern__3SJ2I3hs_eilck_task_b, _Intern__3SJ2I3hs_eilck_task_ptab[_Intern__3SJ2I3hs_eilck_task_b-1].id);\
            } break;                                                            \
                                                                                \
        case eilck_task_subscribing: eilck_obj_blks_imark(_Intern__3SJ2I3hs_eilck_task_ptab[0].blk,\
                    &eilck_task_itend, sizeof eilck_task_itend);                \
                                                                                \
            if(eilck_task_inner) {                                              \
                eilck_hdl_group_req(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup],\
                                            eilck_task_all, &up-> tab[eilck_task_cgroup]);\
            } break;                                                            \
                                                                                \
        default:                                                                \
            for (;                                                              \
                    eilck_task_cgroup < eilck_task_plen;                        \
                    ++eilck_task_cgroup) {                                      \
                                                                                \
                if (_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_itend && eilck_task_iteration() < eilck_task_ittotal) {\
                    eilck_hdl_group_acq(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup]);\
                    eilck_hdl_group_rel(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup]);\
                }                                                               \
                else {                                                          \
                    eilck_hdl_group_destroy(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup]);\
                } } } }                                                         \
    else

#define eilck_task_STEP eilck_task_BREAKER eilck_task_POST((eilck_task_cgroup < _Intern__3SJ2I3hs_eilck_task_plen1) && ((eilck_task_inner < _Intern__3SJ2I3hs_eilck_task_pgrain) || (eilck_task_inner = 0,\
        (_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all ? 0 : eilck_hdl_group_rel(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup])),\
        ((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_subscribing) ? eilck_hdl_group_req(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup],\
                eilck_task_all, &up-> tab[eilck_task_cgroup]) : 0),             \
        ((_Intern__3SJ2I3hs_eilck_task_iter != eilck_task_counting) ? ++eilck_task_cgroup : 0)))) eilck_task_POST(++eilck_task_cstep) eilck_task_POST(++eilck_task_inner) eilck_task_PRE(eilck_task_inner || (_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all ? 0 : eilck_hdl_group_acq(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup])))\
if (!eilck_task_inner && (_Intern__3SJ2I3hs_eilck_task_iter >= eilck_task_itend)) {\
    eilck_task_breaker = true;                                                  \
    continue;                                                                   \
}                                                                               \
else

#define eilck_task_BREAKER                                                      \
    if (eilck_task_breaker) {                                                   \
        eilck_task_breaker = false;                                             \
        break;                                                                  \
    }                                                                           \
    else

#define eilck_task_break                                                        \
    switch ((_Intern__3SJ2I3hs_eilck_task_inblock ? (eilck_task_itend = _Intern__3SJ2I3hs_eilck_task_iter) : (eilck_task_breaker = true)))\
    case 0:                                                                     \
        case 1:                                                                 \
            default: continue

#define eilck_task_strict()                                                     \
    eilck_task_emark(eilck_task_itend)

#define eilck_task_continue                                                     \
    switch ((eilck_task_breaker = false))                                       \
    default: continue

#define eilck_task_for(...) for (__VA_ARGS__) eilck_task_STEP

#define eilck_task_while(...)                                                   \
    while (__VA_ARGS__) eilck_task_STEP

#define eilck_task_STEPS(TOTAL)

#define eilck_task_STEPS_seq(TOTAL)                                             \
    eilck_task_DECL(eilck_task*const _Intern__3SJ2I3hs_eilck_task_p = eilck_task_tasks[eilck_task_current],\
                            *const up = eilck_task_tasks[eilck_task_sibling]) eilck_task_DECL(C_size const eilck_task_plen = _Intern__3SJ2I3hs_eilck_task_p-> len) eilck_task_DECL(C_size _Intern__3SJ2I3hs_eilck_task_pgrain = _Intern__3SJ2I3hs_eilck_task_p-> grain) eilck_task_DECL(eilck_hdl_group*const restrict _Intern__3SJ2I3hs_eilck_task_ptab = _Intern__3SJ2I3hs_eilck_task_p-> tab) eilck_task_DECL(C_size const _Intern__3SJ2I3hs_eilck_task_total = (TOTAL)) eilck_task_ITERATE_TASK_INNER_seq(eilck_task_plen) eilck_task_DECL(C_size eilck_task_cstep = eilck_task_cgroup*_Intern__3SJ2I3hs_eilck_task_pgrain,\
                                    _Intern__3SJ2I3hs_eilck_task_rest = (_Intern__3SJ2I3hs_eilck_task_total > eilck_task_cstep) ? _Intern__3SJ2I3hs_eilck_task_total-eilck_task_cstep : 0,\
                                    stop = ((_Intern__3SJ2I3hs_eilck_task_rest < _Intern__3SJ2I3hs_eilck_task_pgrain)|| (eilck_task_cgroup == eilck_task_plen-1)) ? _Intern__3SJ2I3hs_eilck_task_rest : _Intern__3SJ2I3hs_eilck_task_pgrain)\
    if (_Intern__3SJ2I3hs_eilck_task_iter >= eilck_task_itend) {                \
    }                                                                           \
    else eilck_task_PRE_POST(((_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all) ? 0 : eilck_hdl_group_acq(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup])),\
                                            ((_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all) ? 0 : eilck_hdl_group_rel(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup]))) eilck_task_POST((void)((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_subscribing) && (eilck_obj_blks_imark(_Intern__3SJ2I3hs_eilck_task_ptab[0].blk,\
                                                    &eilck_task_itend, sizeof eilck_task_itend), eilck_hdl_group_req(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup],\
                                                            eilck_task_all, &up-> tab[eilck_task_cgroup])))) eilck_task_POST((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_counting) && (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all) && (_Intern__3SJ2I3hs_eilck_task_p-> tot = eilck_task_cstep) && (_Intern__3SJ2I3hs_eilck_task_pgrain = (_Intern__3SJ2I3hs_eilck_task_p-> grain = eilck_task_cstep/_Intern__3SJ2I3hs_eilck_task_p-> len)))\
        if (_Intern__3SJ2I3hs_eilck_task_iter >= eilck_task_itend) {            \
        }                                                                       \
        else for (C_size eilck_task_inner = 0;                                  \
                      eilck_task_inner < stop;                                  \
                      ++eilck_task_inner, ++eilck_task_cstep)

#define eilck_task_STEPS_omp(TOTAL)                                             \
    eilck_task_DECL(eilck_task*const _Intern__3SJ2I3hs_eilck_task_p = eilck_task_tasks[eilck_task_current],\
                            *const up = eilck_task_tasks[eilck_task_sibling]) eilck_task_DECL(C_size const eilck_task_plen = _Intern__3SJ2I3hs_eilck_task_p-> len) eilck_task_DECL(C_size _Intern__3SJ2I3hs_eilck_task_pgrain = _Intern__3SJ2I3hs_eilck_task_p-> grain) eilck_task_DECL(eilck_hdl_group*const restrict _Intern__3SJ2I3hs_eilck_task_ptab = _Intern__3SJ2I3hs_eilck_task_p-> tab) eilck_task_DECL(C_size const _Intern__3SJ2I3hs_eilck_task_total = (TOTAL)) eilck_task_ITERATE_TASK_INNER_omp(eilck_task_plen) eilck_task_DECL(C_size eilck_task_cstep = eilck_task_cgroup*_Intern__3SJ2I3hs_eilck_task_pgrain,\
                                    _Intern__3SJ2I3hs_eilck_task_rest = (_Intern__3SJ2I3hs_eilck_task_total > eilck_task_cstep) ? _Intern__3SJ2I3hs_eilck_task_total-eilck_task_cstep : 0,\
                                    stop = ((_Intern__3SJ2I3hs_eilck_task_rest < _Intern__3SJ2I3hs_eilck_task_pgrain)|| (eilck_task_cgroup == eilck_task_plen-1)) ? _Intern__3SJ2I3hs_eilck_task_rest : _Intern__3SJ2I3hs_eilck_task_pgrain)\
    if (_Intern__3SJ2I3hs_eilck_task_iter >= eilck_task_itend) {                \
    }                                                                           \
    else eilck_task_PRE_POST(((_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all) ? 0 : eilck_hdl_group_acq(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup])),\
                                            ((_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all) ? 0 : eilck_hdl_group_rel(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup]))) eilck_task_POST((void)((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_subscribing) && (eilck_obj_blks_imark(_Intern__3SJ2I3hs_eilck_task_ptab[0].blk,\
                                                    &eilck_task_itend, sizeof eilck_task_itend), eilck_hdl_group_req(&_Intern__3SJ2I3hs_eilck_task_ptab[eilck_task_cgroup],\
                                                            eilck_task_all, &up-> tab[eilck_task_cgroup])))) eilck_task_POST((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_counting) && (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all) && (_Intern__3SJ2I3hs_eilck_task_p-> tot = eilck_task_cstep) && (_Intern__3SJ2I3hs_eilck_task_pgrain = (_Intern__3SJ2I3hs_eilck_task_p-> grain = eilck_task_cstep/_Intern__3SJ2I3hs_eilck_task_p-> len)))\
        if (_Intern__3SJ2I3hs_eilck_task_iter >= eilck_task_itend) {            \
        }                                                                       \
        else for (C_size eilck_task_inner = 0;                                  \
                      eilck_task_inner < stop;                                  \
                      ++eilck_task_inner, ++eilck_task_cstep)

#define eilck_task_BLOCK_seq eilck_task_DECL(eilck_task*const _Intern__3SJ2I3hs_eilck_task_p = eilck_task_tasks[eilck_task_current],\
        *const up = eilck_task_tasks[eilck_task_sibling]) eilck_task_DECL(eilck_hdl_group*const restrict _Intern__3SJ2I3hs_eilck_task_ptab = _Intern__3SJ2I3hs_eilck_task_p-> tab) eilck_task_ITERATE_BLOCK_INNER_seq(1) eilck_task_PRE_POST(((_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all) ? 0 : eilck_hdl_group_acq(&_Intern__3SJ2I3hs_eilck_task_ptab[0])),\
                ((_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all) ? 0 : eilck_hdl_group_rel(&_Intern__3SJ2I3hs_eilck_task_ptab[0]))) eilck_task_POST((void)((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_subscribing) && (eilck_obj_blks_imark(_Intern__3SJ2I3hs_eilck_task_ptab[0].blk,\
                        &eilck_task_itend, sizeof eilck_task_itend), eilck_hdl_group_req(&_Intern__3SJ2I3hs_eilck_task_ptab[0],\
                                eilck_task_all, &up-> tab[0])))) eilck_task_POST((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_counting) && (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all) && (_Intern__3SJ2I3hs_eilck_task_p-> tot = 1))

#define eilck_task_BLOCK_omp eilck_task_DECL(eilck_task*const _Intern__3SJ2I3hs_eilck_task_p = eilck_task_tasks[eilck_task_current],\
        *const up = eilck_task_tasks[eilck_task_sibling]) eilck_task_DECL(eilck_hdl_group*const restrict _Intern__3SJ2I3hs_eilck_task_ptab = _Intern__3SJ2I3hs_eilck_task_p-> tab) eilck_task_ITERATE_BLOCK_INNER_omp(1) eilck_task_PRE_POST(((_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all) ? 0 : eilck_hdl_group_acq(&_Intern__3SJ2I3hs_eilck_task_ptab[0])),\
                ((_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all) ? 0 : eilck_hdl_group_rel(&_Intern__3SJ2I3hs_eilck_task_ptab[0]))) eilck_task_POST((void)((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_subscribing) && (eilck_obj_blks_imark(_Intern__3SJ2I3hs_eilck_task_ptab[0].blk,\
                        &eilck_task_itend, sizeof eilck_task_itend), eilck_hdl_group_req(&_Intern__3SJ2I3hs_eilck_task_ptab[0],\
                                eilck_task_all, &up-> tab[0])))) eilck_task_POST((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_counting) && (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all) && (_Intern__3SJ2I3hs_eilck_task_p-> tot = 1))

#define eilck_task_DEPENDS_seq(Id,                                              \
                                        Mi)                                     \
eilck_task_DECL(C_size _Intern__3SJ2I3hs_eilck_task_depId = (Id)%eilck_task_numtasks,\
                        eilck_task_depLen = eilck_task_tasks[_Intern__3SJ2I3hs_eilck_task_depId]->len,\
                        _Intern__3SJ2I3hs_eilck_task_depMi = (Mi)%(eilck_task_depLen)) eilck_task_PRE((_Intern__3SJ2I3hs_eilck_task_iter == eilck_task_subscribing) && (! eilck_task_inner) && eilck_task_depend(eilck_task_tasks[_Intern__3SJ2I3hs_eilck_task_depId]-> tab[_Intern__3SJ2I3hs_eilck_task_depMi].id))

#define eilck_task_DEPENDS_par(Id, Mi)

#define eilck_task_NUMTHREAD0(X)                                                \
    _Generic((X),                                                               \
             eilck_task*: ((eilck_task*)X)-> len,                               \
                                                                                \
             default: (X))

#define eilck_task__Intern_defill_number_SEP +

#ifdef eilck_task__Intern_defill_number_SEP
#define _Intern__3SJ2I3hs_eilck_task_number_COMBINE0(X,                         \
        ...)                                                                    \
X eilck_task__Intern_defill_number_SEP __VA_ARGS__

#define _Intern__3SJ2I3hs_eilck_task_number_COMBINE(...)                        \
    C_combine(_Intern__3SJ2I3hs_eilck_task_number_COMBINE0,                     \
                      __VA_ARGS__)

#define eilck_task_NUMTHREAD1(...)                                              \
    _Intern__3SJ2I3hs_eilck_task_number_COMBINE(C_snippet_map(eilck_task_NUMTHREAD0,\
            __VA_ARGS__))
#else
#define eilck_task_NUMTHREAD1(...)                                              \
    C_snippet_map(eilck_task_NUMTHREAD0,                                        \
                          __VA_ARGS__)
#endif

#define eilck_task_THREADS C_attribute__Operator_eval(std_maybe_unused ) C_size eilck_task_current = 0;\
    C_attribute__Operator_eval(std_maybe_unused ) C_size eilck_task_sibling = 0;\
    C_attribute__Operator_eval(std_maybe_unused ) C_size eilck_task_sibOffset = 0;\
    C_attribute__Operator_eval(std_maybe_unused ) C_size _Intern__3SJ2I3hs_eilck_task_blocks = 0

#define eilck_task_ITERATE_seq(START,                                           \
                                        NUMBER, ENUMBER)                        \
C_attribute__Operator_eval(std_maybe_unused ) register C_size const _Intern__3SJ2I3hs_eilck_task_start = (START);\
eilck_task_itend = (ENUMBER);                                                   \
eilck_task_ittotal = (NUMBER);                                                  \
                                                                                \
if (! eilck_task_ittotal) eilck_task_ittotal = eilck_task_itend*eilck_task_itIncDup*eilck_task_itIncAlt;\
                                                                                \
for(register C_size _Intern__3SJ2I3hs_eilck_task_iter = _Intern__3SJ2I3hs_eilck_task_start;\
        (_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_itend) && (eilck_task_iteration() < eilck_task_ittotal);\
        ++_Intern__3SJ2I3hs_eilck_task_iter) eilck_task_PRE(_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all && C_io_fprintf(C_io_err,\
                    "mctask: %s phase starts\t%g sec\n", eilck_task_getshort(_Intern__3SJ2I3hs_eilck_task_iter),\
                    eilck_csv_stamp()*1E-9)) eilck_task_POST(_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_all && C_io_fprintf(C_io_err,\
                            "mctask: %s phase ended\t%g sec\n", eilck_task_getshort(_Intern__3SJ2I3hs_eilck_task_iter),\
                            eilck_csv_stamp()*1E-9)) eilck_task_PRE(_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter == eilck_task_counting && (eilck_task_fifos = eilck_task_bind(eilck_task_qlength,\
                                    eilck_task_numtasks, eilck_task_tasks))) eilck_task_PRE(_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter == eilck_task_owning && (eilck_obj_blks_alloc_owner(eilck_task_fifos,\
                                            _Intern__3SJ2I3hs_eilck_task_blocks)))

#define eilck_task_ITERATE_TASK_seq()                                           \
    eilck_task_DECL(eilck_task*const _Intern__3SJ2I3hs_eilck_task_p = eilck_task_tasks[eilck_task_current],\
                            *const up = eilck_task_tasks[eilck_task_sibling]) eilck_task_ONCE

#define eilck_task_ONCE_TASKS_seq(...) eilck_task_ONCE

#define eilck_task_ONCE_TASK_seq eilck_task_ONCE

#define eilck_task_ITERATE_TASK_INNER_seq(PLEN)                                 \
    for(C_size eilck_task_cgroup = 0;                                           \
            eilck_task_cgroup < PLEN;                                           \
            ++eilck_task_cgroup)

#define eilck_task_ITERATE_BLOCK_INNER_seq(PLEN)                                \
    eilck_task_DECL( C_attribute__Operator_eval(std_maybe_unused ) bool eilck_task_breaker = false,\
                             _Intern__3SJ2I3hs_eilck_task_inblock = true)       \
    for(C_size eilck_task_cgroup = 0;                                           \
            eilck_task_cgroup < 1;                                              \
            ++eilck_task_cgroup)

#define eilck_task_ITERATE_omp(START,                                           \
                                        NUMBER, ENUMBER)                        \
C_attribute__Operator_eval(std_maybe_unused ) C_size const _Intern__3SJ2I3hs_eilck_task_start = (START);\
C_size const stime = eilck_csv_stamp();                                         \
eilck_task_itend = (ENUMBER);                                                   \
eilck_task_ittotal = (NUMBER);                                                  \
                                                                                \
if (! eilck_task_ittotal) eilck_task_ittotal = eilck_task_itend*eilck_task_itIncDup*eilck_task_itIncAlt;\
eilck_task_POST(C_io_printf(C_io_err,                                           \
                        "mctask: phase%8zu ended %g sec\n" "%zu iterations,     \
*eilck_task_itIncDup*eilck_task_itIncAlt;                                       \
                                                                                \
 < eilck_task_all && _Intern__3SJ2I3hs_eilck_task_iter == eilck_task_owning && (eilck_obj_blks_alloc_owner(eilck_task_fifos,\
]-> tab[_Intern__3SJ2I3hs_eilck_task_depMi].id))
  %zu steady state, %g seconds per iteration in steady state\n",                \
                        eilck_task_itend, eilck_csv_stamp()*1E-9, eilck_task_itend*eilck_task_itIncDupg*eilck_task_itIncAltg,\
                        ((eilck_task_itend-eilck_task_all)*eilck_task_itIncDupg*eilck_task_itIncAltg),\
                        (eilck_csv_stamp()-stime)*1E-9/((eilck_task_itend-eilck_task_all)*eilck_task_itIncDupg*eilck_task_itIncAltg))) omp_PARALLELIZE(eilck_task_numthreads)

#define eilck_task_ITERATE_TASK_omp()                                           \
    eilck_task_DECL(eilck_task*const _Intern__3SJ2I3hs_eilck_task_p = eilck_task_tasks[eilck_task_current],\
                            *const up = eilck_task_tasks[eilck_task_sibling]) omp_JOB eilck_task_POST(eilck_task_destroy(_Intern__3SJ2I3hs_eilck_task_p))\
    for(register C_size _Intern__3SJ2I3hs_eilck_task_iter = _Intern__3SJ2I3hs_eilck_task_start;\
            (_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_itend) && (eilck_task_iteration() < eilck_task_ittotal);\
            ++_Intern__3SJ2I3hs_eilck_task_iter)

#define eilck_task_ONCE_TASKS_omp(...)                                          \
    omp_PARALLELIZE(C_COUNT(__VA_ARGS__))

#define eilck_task_ONCE_TASK_omp omp_JOB

#define eilck_task_ITERATE_TASK_INNER_omp(PLEN)                                 \
    omp_JOBS(1)                                                                 \
    for(C_size eilck_task_cgroup = 0;                                           \
            eilck_task_cgroup < PLEN;                                           \
            ++eilck_task_cgroup) eilck_task_POST(eilck_hdl_group_destroy(&_Intern__3SJ2I3hs_eilck_task_p-> tab[eilck_task_cgroup]))\
        for(register C_size _Intern__3SJ2I3hs_eilck_task_iter = (_Intern__3SJ2I3hs_eilck_task_start <= eilck_task_all ? eilck_task_all : _Intern__3SJ2I3hs_eilck_task_start);\
                (_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_itend) && (eilck_task_iteration() < eilck_task_ittotal);\
                ++_Intern__3SJ2I3hs_eilck_task_iter)

#define eilck_task_ITERATE_BLOCK_INNER_omp(PLEN)                                \
    omp_JOBS(1)                                                                 \
    for(C_size eilck_task_cgroup = 0;                                           \
            eilck_task_cgroup < 1;                                              \
            ++eilck_task_cgroup) eilck_task_DECL( C_attribute__Operator_eval(std_maybe_unused ) bool eilck_task_breaker = false,\
                        _Intern__3SJ2I3hs_eilck_task_inblock = true) eilck_task_POST(eilck_hdl_group_destroy(&_Intern__3SJ2I3hs_eilck_task_p-> tab[eilck_task_cgroup]))\
        for(register C_size _Intern__3SJ2I3hs_eilck_task_iter = (_Intern__3SJ2I3hs_eilck_task_start <= eilck_task_all ? eilck_task_all : _Intern__3SJ2I3hs_eilck_task_start);\
                (_Intern__3SJ2I3hs_eilck_task_iter < eilck_task_itend) && (eilck_task_iteration() < eilck_task_ittotal);\
                ++_Intern__3SJ2I3hs_eilck_task_iter)

#ifndef _OPENMP
#define eilck_task_ITERATE_BLOCK_INNER_par eilck_task_ITERATE_BLOCK_INNER_seq

#define eilck_task_ITERATE_TASK_INNER_par eilck_task_ITERATE_TASK_INNER_seq

#define eilck_task_ITERATE_par eilck_task_ITERATE_seq

#define eilck_task_ONCE_TASKS_par eilck_task_ONCE_TASKS_seq

#define eilck_task_ONCE_TASK_par eilck_task_ONCE_TASK_seq

#define eilck_task_SPLIT_par eilck_task_SPLIT_seq

#define eilck_task_STEPS_par eilck_task_STEPS_seq

#define eilck_task_BLOCK_par eilck_task_BLOCK_seq
#else
#define eilck_task_ITERATE_BLOCK_INNER_par eilck_task_ITERATE_BLOCK_INNER_omp

#define eilck_task_ITERATE_TASK_INNER_par eilck_task_ITERATE_TASK_INNER_omp

#define eilck_task_ITERATE_par eilck_task_ITERATE_omp

#define eilck_task_ONCE_TASKS_par eilck_task_ONCE_TASKS_omp

#define eilck_task_ONCE_TASK_par eilck_task_ONCE_TASK_omp

#define eilck_task_SPLIT_par eilck_task_SPLIT_omp

#define eilck_task_STEPS_par eilck_task_STEPS_omp

#define eilck_task_BLOCK_par eilck_task_BLOCK_omp
#endif

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

extern eilck_task* eilck_task_init(eilck_task* _Intern__3SJ2I3hs_eilck_task_p);

extern eilck_task* eilck_task_destroy(eilck_task* _Intern__3SJ2I3hs_eilck_task_p);

extern eilck_obj_blks* eilck_task_bind(C_size qlen, C_size tlen, eilck_task* τ[tlen]);

extern void eilck_task_greedy(char const name[], C_size periods, C_size tlen, eilck_task* τ[tlen]);

inline
bool _Intern__3SJ2I3hs_eilck_task_eown(eilck_task const* restrict τ, void* addr, C_size size, C_size μ) {
    eilck_obj_blks_set(τ-> tab[μ].blk, addr, size, τ-> tab[μ].id);
    return true;
}

inline
void _Intern__3SJ2I3hs_eilck_task_iown(eilck_task const* restrict τ, void* addr, C_size size, C_size μ) {

}

inline
C_size _Intern__3SJ2I3hs_eilck_task_ecount(eilck_task const* restrict τ, void* addr, C_size size) {
    return eilck_obj_blks_count(τ-> tab[0].blk, addr, size);
}

inline
C_size _Intern__3SJ2I3hs_eilck_task_icount(eilck_task const* restrict τ, void* addr, C_size size) {
    return 0;
}

#ifdef _OPENMP
#define _Intern__3SJ2I3hs_eilck_task_imarkinner(τ,                         \
        addr, size, _Intern__3SJ2I3hs_eilck_task_start,                         \
        it, μ, blocksp)                                                    \
do {                                                                            \
                                                                                \
    if (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && it == eilck_task_counting) *blocksp += _Intern__3SJ2I3hs_eilck_task_icount(τ,\
                addr, size);                                                    \
                                                                                \
    if (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && it == eilck_task_owning) _Intern__3SJ2I3hs_eilck_task_iown(τ,\
                addr, size, μ);                                            \
                                                                                \
    if (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && it == eilck_task_subscribing) eilck_obj_blks_imark(τ-> tab[0].blk,\
                addr, size);                                                    \
}                                                                               \
while (false)

#define eilck_task_imark2(A, S)                                                 \
    _Intern__3SJ2I3hs_eilck_task_imarkinner(_Intern__3SJ2I3hs_eilck_task_p,     \
            (A), (S), _Intern__3SJ2I3hs_eilck_task_start,                       \
            _Intern__3SJ2I3hs_eilck_task_iter,                                  \
            eilck_task_micro(), &_Intern__3SJ2I3hs_eilck_task_blocks)

#define _Intern__3SJ2I3hs_eilck_task_emarkinner(τ,                         \
        addr, size, _Intern__3SJ2I3hs_eilck_task_start,                         \
        it, μ, blocksp)                                                    \
do {                                                                            \
                                                                                \
    if (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && it == eilck_task_counting) *blocksp += _Intern__3SJ2I3hs_eilck_task_ecount(τ,\
                addr, size);                                                    \
                                                                                \
    if (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && it == eilck_task_owning) _Intern__3SJ2I3hs_eilck_task_eown(τ,\
                addr, size, μ);                                            \
                                                                                \
    if (_Intern__3SJ2I3hs_eilck_task_start < eilck_task_all && it == eilck_task_subscribing) eilck_obj_blks_emark(τ-> tab[0].blk,\
                addr, size);                                                    \
}                                                                               \
while (false)

#define eilck_task_emark2(A, S)                                                 \
    _Intern__3SJ2I3hs_eilck_task_emarkinner(_Intern__3SJ2I3hs_eilck_task_p,     \
            (A), (S), _Intern__3SJ2I3hs_eilck_task_start,                       \
            _Intern__3SJ2I3hs_eilck_task_iter,                                  \
            eilck_task_micro(), &_Intern__3SJ2I3hs_eilck_task_blocks)

inline
bool eilck_task_dependinner(eilck_task const* restrict τ, C_size μ, C_size id) {
    τ-> tab[μ].deps[id] |= eilck_R;
    return true;
}

#else
#define eilck_task_imark2(A, S)                                                 \
    do {                                                                        \
        (void)_Intern__3SJ2I3hs_eilck_task_p;                                   \
        (void)(A);                                                              \
        (void)(S);                                                              \
        (void)_Intern__3SJ2I3hs_eilck_task_start;                               \
        (void)_Intern__3SJ2I3hs_eilck_task_iter;                                \
        (void)eilck_task_micro();                                               \
        (void)&_Intern__3SJ2I3hs_eilck_task_blocks;                             \
    }                                                                           \
    while (false)

#define eilck_task_emark2(A, S)                                                 \
    do {                                                                        \
        (void)_Intern__3SJ2I3hs_eilck_task_p;                                   \
        (void)(A);                                                              \
        (void)(S);                                                              \
        (void)_Intern__3SJ2I3hs_eilck_task_start;                               \
        (void)_Intern__3SJ2I3hs_eilck_task_iter;                                \
        (void)eilck_task_micro();                                               \
        (void)&_Intern__3SJ2I3hs_eilck_task_blocks;                             \
    }                                                                           \
    while (false)

inline
bool eilck_task_dependinner(eilck_task const* restrict τ, C_size μ, C_size id) {
    return true;
}

#endif

#define eilck_task_depend(X)                                                    \
    eilck_task_dependinner(_Intern__3SJ2I3hs_eilck_task_p,                      \
                                    eilck_task_micro(), (X))

#define eilck_task_imark(A)                                                     \
    eilck_task_imark2((void*)(&(A)),                                            \
                              sizeof(A))

#define eilck_task__Intern_defill_imarksImp_SEP ;
#ifdef CMOD_2F746D702F636D6F642D746D70642E34344168614E4A5A33534A32493368732F65696C636B2D7461736B2E63_HEADER_INSTANTIATE
extern inline
bool _Intern__3SJ2I3hs_eilck_task_eown(eilck_task const* restrict τ, void* addr, C_size size, C_size μ) ;

extern inline
void _Intern__3SJ2I3hs_eilck_task_iown(eilck_task const* restrict τ, void* addr, C_size size, C_size μ) ;

extern inline
C_size _Intern__3SJ2I3hs_eilck_task_ecount(eilck_task const* restrict τ, void* addr, C_size size) ;

extern inline
C_size _Intern__3SJ2I3hs_eilck_task_icount(eilck_task const* restrict τ, void* addr, C_size size) ;

extern inline
bool eilck_task_dependinner(eilck_task const* restrict τ, C_size μ, C_size id) ;

extern inline
bool eilck_task_dependinner(eilck_task const* restrict τ, C_size μ, C_size id) ;
#endif

#ifdef eilck_task__Intern_defill_imarksImp_SEP
#define _Intern__3SJ2I3hs_eilck_task_imarksImp_COMBINE0(X,                      \
        ...)                                                                    \
X eilck_task__Intern_defill_imarksImp_SEP __VA_ARGS__

#define _Intern__3SJ2I3hs_eilck_task_imarksImp_COMBINE(...)                     \
    C_combine(_Intern__3SJ2I3hs_eilck_task_imarksImp_COMBINE0,                  \
                      __VA_ARGS__)

#define eilck_task_imarks(...)                                                  \
    _Intern__3SJ2I3hs_eilck_task_imarksImp_COMBINE(C_snippet_map(eilck_task_imark,\
            __VA_ARGS__))
#else
#define eilck_task_imarks(...)                                                  \
    C_snippet_map(eilck_task_imark,                                             \
                          __VA_ARGS__)
#endif

#define eilck_task_emark(A)                                                     \
    eilck_task_emark2((void*)(&(A)),                                            \
                              sizeof(A))

#define eilck_task__Intern_defill_emarksImp_SEP ;

#ifdef eilck_task__Intern_defill_emarksImp_SEP
#define _Intern__3SJ2I3hs_eilck_task_emarksImp_COMBINE0(X,                      \
        ...)                                                                    \
X eilck_task__Intern_defill_emarksImp_SEP __VA_ARGS__

#define _Intern__3SJ2I3hs_eilck_task_emarksImp_COMBINE(...)                     \
    C_combine(_Intern__3SJ2I3hs_eilck_task_emarksImp_COMBINE0,                  \
                      __VA_ARGS__)

#define eilck_task_emarks(...)                                                  \
    _Intern__3SJ2I3hs_eilck_task_emarksImp_COMBINE(C_snippet_map(eilck_task_emark,\
            __VA_ARGS__))
#else
#define eilck_task_emarks(...)                                                  \
    C_snippet_map(eilck_task_emark,                                             \
                          __VA_ARGS__)
#endif

#define eilck_task_omark(...) ((void)0)

#define eilck_task_omarks(...) ((void)0)

inline
eilck_task* eilck_task_alloc(C_size _Intern__3SJ2I3hs_eilck_task_length) {
    return eilck_task_init(eilck_task_FA_alloc(_Intern__3SJ2I3hs_eilck_task_length));
}

inline
void eilck_task_free(eilck_task* _Intern__3SJ2I3hs_eilck_task_p) {
    C_lib_free(eilck_task_destroy(_Intern__3SJ2I3hs_eilck_task_p));
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E34344168614E4A5A33534A32493368732F65696C636B2D7461736B2E63_HEADER_INSTANTIATE
extern inline
eilck_task* eilck_task_alloc(C_size _Intern__3SJ2I3hs_eilck_task_length) ;

extern inline
void eilck_task_free(eilck_task* _Intern__3SJ2I3hs_eilck_task_p) ;
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E34344168614E4A5A33534A32493368732F65696C636B2D7461736B2E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-task */
