/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_ihdl__GUARD
#define eilck_ihdl__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"
#include "eilck-impl.h"
#include "eilck-impl-snippet.h"
#include "eilck-ehdl.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_ihdl
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E6D62385A6173684D79524579635347532F65696C636B2D6968646C2E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E6D62385A6173684D79524579635347532F65696C636B2D6968646C2E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E6D62385A6173684D79524579635347532F65696C636B2D6968646C2E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E6D62385A6173684D79524579635347532F65696C636B2D6968646C2E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_ihdl_MODULE eilck_ihdl module internals
 ** @ingroup eilck_ihdl
 ** @{ */

#if !defined(_DOXYGEN_) && !defined(__cplusplus)
typedef eilck_readHdl eilck_ihdl__MODULE_CON(I8_Typedef4TYPE2in5eilck4impl7snippet4must2be1a4type10_Snippet_3E);
#endif

typedef struct eilck_ihdl eilck_ihdl;
struct eilck_ihdl {

    union {
        eilck_impl hide;
        eilck_readHdl* type;
        eilck_voidBase* baze;

        struct {
            eilck_readHdl* base;
            char _Intern__yREycSGS_eilck_ihdl_impl_fill[sizeof(eilck_impl)-sizeof(eilck_readHdl*)];
        };
    };
};

inline int eilck_ihdl_acq(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) {
    return eilck_impl_acq(&_Intern__yREycSGS_eilck_ihdl_impl_h-> hide);
}

C_attr_pure

inline C_size eilck_ihdl_length(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) {
    return eilck_impl_length(&_Intern__yREycSGS_eilck_ihdl_impl_h-> hide);
}

inline bool eilck_ihdl_test(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) {
    return _Intern__yREycSGS_eilck_ihdl_impl_h ? eilck_impl_test(&_Intern__yREycSGS_eilck_ihdl_impl_h-> hide) : 0;
}

inline void eilck_ihdl_rel(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) {
    if(_Intern__yREycSGS_eilck_ihdl_impl_h) eilck_impl_rel(&_Intern__yREycSGS_eilck_ihdl_impl_h-> hide);
}

inline void eilck_ihdl_drop(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) {
    if (_Intern__yREycSGS_eilck_ihdl_impl_h) eilck_impl_drop(&_Intern__yREycSGS_eilck_ihdl_impl_h-> hide);
}

inline int eilck_ihdl_req(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h, eilck_obj* _Intern__yREycSGS_eilck_ihdl_impl_ob0) {
    return _Intern__yREycSGS_eilck_ihdl_impl_h ? eilck_impl_req(&_Intern__yREycSGS_eilck_ihdl_impl_h-> hide, _Intern__yREycSGS_eilck_ihdl_impl_ob0, eilck_impl_snippet_excl(_Intern__yREycSGS_eilck_ihdl_impl_h)) : 0;
}

inline
eilck_ihdl const* eilck_ihdl_chain(eilck_ihdl const* _Intern__yREycSGS_eilck_ihdl_impl_sooner, eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_later) {
    eilck_ihdl const* ret = _Intern__yREycSGS_eilck_ihdl_impl_sooner && _Intern__yREycSGS_eilck_ihdl_impl_later ? _Intern__yREycSGS_eilck_ihdl_impl_later : 0;
    if (ret)
        eilck_impl_chain(&_Intern__yREycSGS_eilck_ihdl_impl_sooner-> hide, &_Intern__yREycSGS_eilck_ihdl_impl_later-> hide, eilck_impl_snippet_excl(_Intern__yREycSGS_eilck_ihdl_impl_later));
    return ret;
}

inline
void eilck_ihdl_rel_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) {
    if (_Intern__yREycSGS_eilck_ihdl_impl_h0 && _Intern__yREycSGS_eilck_ihdl_impl_h1 && (eilck_impl_snippet_excl(_Intern__yREycSGS_eilck_ihdl_impl_h0) == eilck_impl_snippet_excl(_Intern__yREycSGS_eilck_ihdl_impl_h1)))
        eilck_impl_rel_2(&_Intern__yREycSGS_eilck_ihdl_impl_h0-> hide, &_Intern__yREycSGS_eilck_ihdl_impl_h1-> hide, eilck_impl_snippet_excl(_Intern__yREycSGS_eilck_ihdl_impl_h0));
}

inline int eilck_ihdl_acq_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) {
    return eilck_impl_acq_2(&_Intern__yREycSGS_eilck_ihdl_impl_h0-> hide, &_Intern__yREycSGS_eilck_ihdl_impl_h1-> hide);
}

C_attr_pure

inline C_size eilck_ihdl_length_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) {
    return eilck_impl_length_2(&_Intern__yREycSGS_eilck_ihdl_impl_h0-> hide, &_Intern__yREycSGS_eilck_ihdl_impl_h1-> hide);
}

inline bool eilck_ihdl_test_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) {
    return (_Intern__yREycSGS_eilck_ihdl_impl_h0 && _Intern__yREycSGS_eilck_ihdl_impl_h1) ? eilck_impl_test_2(&_Intern__yREycSGS_eilck_ihdl_impl_h0-> hide, &_Intern__yREycSGS_eilck_ihdl_impl_h1-> hide) : 0;
}

inline void eilck_ihdl_drop_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) {
    if (_Intern__yREycSGS_eilck_ihdl_impl_h0 && _Intern__yREycSGS_eilck_ihdl_impl_h1) eilck_impl_drop_2(&_Intern__yREycSGS_eilck_ihdl_impl_h0-> hide, &_Intern__yREycSGS_eilck_ihdl_impl_h1-> hide);
}

#ifndef __cplusplus
#define _Intern__yREycSGS_eilck_ihdl_impl_scale_3(H,                            \
        N, C, ...)                                                              \
eilck_ihdl_scale_gen(H, N, C)

#define eilck_ihdl_scale(...)                                                   \
    _Intern__yREycSGS_eilck_ihdl_impl_scale_3(__VA_ARGS__,                      \
            0, 0, 0, )

#define eilck_ihdl_scale_gen(H,                                                 \
                                        N, C)                                   \
_Generic((H)[0].type,                                                           \
         void*: eilck_impl_scale)(&(H)-> hide,                                  \
                 (N), (C))

#define _Intern__yREycSGS_eilck_ihdl_impl_scale2_3(H,                           \
        N, C, ...)                                                              \
eilck_ihdl_scale2_gen(H, N, C)

#define eilck_ihdl_scale2(...)                                                  \
    _Intern__yREycSGS_eilck_ihdl_impl_scale2_3(__VA_ARGS__,                     \
            0, 0, 0, )

#define eilck_ihdl_scale2_gen(H,                                                \
        N, C)                                                                   \
_Generic((H)[0][0].type,                                                        \
         void*: eilck_impl_scale2)((void*)(H),                                  \
                 (N), (C))
#endif

#ifdef CMOD_2F746D702F636D6F642D746D70642E6D62385A6173684D79524579635347532F65696C636B2D6968646C2E63_HEADER_INSTANTIATE
extern inline int eilck_ihdl_acq(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) ;

extern C_attr_pure

inline C_size eilck_ihdl_length(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) ;

extern inline bool eilck_ihdl_test(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) ;

extern inline void eilck_ihdl_rel(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) ;

extern inline void eilck_ihdl_drop(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h) ;

extern inline int eilck_ihdl_req(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h, eilck_obj* _Intern__yREycSGS_eilck_ihdl_impl_ob0) ;

extern inline
eilck_ihdl const* eilck_ihdl_chain(eilck_ihdl const* _Intern__yREycSGS_eilck_ihdl_impl_sooner, eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_later) ;

extern inline
void eilck_ihdl_rel_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) ;

extern inline int eilck_ihdl_acq_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) ;

extern C_attr_pure

inline C_size eilck_ihdl_length_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) ;

extern inline bool eilck_ihdl_test_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) ;

extern inline void eilck_ihdl_drop_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1) ;
#endif

extern eilck_readHdl* eilck_ihdl_map(eilck_ihdl* _Intern__yREycSGS_eilck_ihdl_impl_h);

extern eilck_readHdl* eilck_ihdl_map_2(eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h0, eilck_ihdl* restrict _Intern__yREycSGS_eilck_ihdl_impl_h1);

extern int eilck_ihdl_acq2(eilck_ihdl (*_Intern__yREycSGS_eilck_ihdl_impl_h)[2]);

extern C_attr_pure
C_size eilck_ihdl_length2(eilck_ihdl (*_Intern__yREycSGS_eilck_ihdl_impl_h)[2]);

extern bool eilck_ihdl_test2(eilck_ihdl (*_Intern__yREycSGS_eilck_ihdl_impl_h)[2]);

extern eilck_readHdl* eilck_ihdl_map2(eilck_ihdl (*_Intern__yREycSGS_eilck_ihdl_impl_h)[2]);

extern void eilck_ihdl_rel2(eilck_ihdl (*_Intern__yREycSGS_eilck_ihdl_impl_h)[2]);

extern void eilck_ihdl_drop2(eilck_ihdl (*_Intern__yREycSGS_eilck_ihdl_impl_h)[2]);

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E6D62385A6173684D79524579635347532F65696C636B2D6968646C2E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-ihdl */
