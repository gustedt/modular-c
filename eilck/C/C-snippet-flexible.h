/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef C_snippet_flexible__GUARD
#define C_snippet_flexible__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-snippet.h"
#include "C-attr.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module C_snippet_flexible
 ** @ingroup C_snippet
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E314834706735776B5A304F466A4544452F432D736E69707065742D666C657869626C652E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E314834706735776B5A304F466A4544452F432D736E69707065742D666C657869626C652E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E314834706735776B5A304F466A4544452F432D736E69707065742D666C657869626C652E63_HEADER
#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E314834706735776B5A304F466A4544452F432D736E69707065742D666C657869626C652E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup C_snippet_flexible_MODULE C_snippet_flexible module internals
 ** @ingroup C_snippet_flexible
 ** @{ */

enum _MODULE_tag_Z0OFjEDE_34 { C_snippet_flexible_min = 1, } ;
typedef enum _MODULE_tag_Z0OFjEDE_34 _MODULE_tag_Z0OFjEDE_34;

#define C_snippet_flexible_INIT { 0 }

#define C_snippet_flexible_OTHERS

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E314834706735776B5A304F466A4544452F432D736E69707065742D666C657869626C652E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* C-snippet-flexible */
