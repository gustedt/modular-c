/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_error__GUARD
#define eilck_error__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "eilck.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_error
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4838447238693938544341434931386B2F65696C636B2D6572726F722E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4838447238693938544341434931386B2F65696C636B2D6572726F722E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4838447238693938544341434931386B2F65696C636B2D6572726F722E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4838447238693938544341434931386B2F65696C636B2D6572726F722E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_error_MODULE eilck_error module internals
 ** @ingroup eilck_error
 ** @{ */

#ifdef __CMOD__
#define eilck_error_print_clear(NAME)                                           \
    do {                                                                        \
                                                                                \
        if (eilck_error) C_io_printf(eilck_error,                               \
                    "%s:%jd: %s failed: %s\n", __func__, C_LINENO, NAME, C_errno_tostr(C_errno));\
        C_errno = 0;                                                            \
    }                                                                           \
    while(false)
#else
#define C_io FILE
#endif

extern C_io* eilck_error;

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4838447238693938544341434931386B2F65696C636B2D6572726F722E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-error */
