/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_obj_file__GUARD
#define eilck_obj_file__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-error.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"
#include "eilck-obj-fd.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_obj_file
 ** @ingroup eilck_obj
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4C7761386A65457936416B707531796D2F65696C636B2D6F626A2D66696C652E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4C7761386A65457936416B707531796D2F65696C636B2D6F626A2D66696C652E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4C7761386A65457936416B707531796D2F65696C636B2D6F626A2D66696C652E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4C7761386A65457936416B707531796D2F65696C636B2D6F626A2D66696C652E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_obj_file_MODULE eilck_obj_file module internals
 ** @ingroup eilck_obj_file
 ** @{ */

#define eilck_obj_file__Intern_defill_fd_PREFIX ""
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static Linux_fd eilck_obj_file__MODULE_CON(I11_Compatible5eilck3obj2fd4OPEN10compatible10_Snippet_3E)( char const* x0, int x1, Linux_mode x2) {
    Linux_fd _Retval = eilck_obj_fd_OPEN(x0, x1, x2);
    return _Retval;
};
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static int eilck_obj_file__MODULE_CON(I11_Compatible5eilck3obj2fd6REMOVE10compatible10_Snippet_3E)( char const* x0) {
    int _Retval = eilck_obj_fd_REMOVE(x0);
    return _Retval;
};
#endif

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

extern void* eilck_obj_file(eilck_obj* _Intern__6Akpu1ym_eilck_obj_file_fd_ob0, C_size _Intern__6Akpu1ym_eilck_obj_file_fd_nsize, void* _Intern__6Akpu1ym_eilck_obj_file_fd_cntx);

extern eilck_obj* eilck_obj_file_init(eilck_obj* _Intern__6Akpu1ym_eilck_obj_file_fd_ob0, C_size _Intern__6Akpu1ym_eilck_obj_file_fd_tablen);

inline
void* eilck_obj_file_open(eilck_obj* _Intern__6Akpu1ym_eilck_obj_file_fd_ob0, char const* _Intern__6Akpu1ym_eilck_obj_file_fd_name0) {
    if (_Intern__6Akpu1ym_eilck_obj_file_fd_ob0-> data && eilck_obj_scale(_Intern__6Akpu1ym_eilck_obj_file_fd_ob0, eilck_obj_scaler_CLOSE, nullptr))
        return eilck_FAILED;
    _Intern__6Akpu1ym_eilck_obj_file_fd_ob0-> alloc = eilck_obj_file;
    return eilck_obj_file(_Intern__6Akpu1ym_eilck_obj_file_fd_ob0, ((C_size)-eilck_obj_scaler_open), (void*)_Intern__6Akpu1ym_eilck_obj_file_fd_name0);
}

inline
void* eilck_obj_file_create(eilck_obj* _Intern__6Akpu1ym_eilck_obj_file_fd_ob0, char const* _Intern__6Akpu1ym_eilck_obj_file_fd_name0) {
    if (_Intern__6Akpu1ym_eilck_obj_file_fd_ob0-> data && eilck_obj_scale(_Intern__6Akpu1ym_eilck_obj_file_fd_ob0, eilck_obj_scaler_CLOSE, nullptr))
        return eilck_FAILED;
    _Intern__6Akpu1ym_eilck_obj_file_fd_ob0-> alloc = eilck_obj_file;
    return eilck_obj_file(_Intern__6Akpu1ym_eilck_obj_file_fd_ob0, ((C_size)-eilck_obj_scaler_create), (void*)_Intern__6Akpu1ym_eilck_obj_file_fd_name0);
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E4C7761386A65457936416B707531796D2F65696C636B2D6F626A2D66696C652E63_HEADER_INSTANTIATE
extern inline
void* eilck_obj_file_open(eilck_obj* _Intern__6Akpu1ym_eilck_obj_file_fd_ob0, char const* _Intern__6Akpu1ym_eilck_obj_file_fd_name0) ;

extern inline
void* eilck_obj_file_create(eilck_obj* _Intern__6Akpu1ym_eilck_obj_file_fd_ob0, char const* _Intern__6Akpu1ym_eilck_obj_file_fd_name0) ;
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4C7761386A65457936416B707531796D2F65696C636B2D6F626A2D66696C652E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-obj-file */
