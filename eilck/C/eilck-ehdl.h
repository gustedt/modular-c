/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_ehdl__GUARD
#define eilck_ehdl__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"
#include "eilck-impl.h"
#include "eilck-impl-snippet.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_ehdl
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E467769794D635944704968793245396B2F65696C636B2D6568646C2E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E467769794D635944704968793245396B2F65696C636B2D6568646C2E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E467769794D635944704968793245396B2F65696C636B2D6568646C2E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E467769794D635944704968793245396B2F65696C636B2D6568646C2E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_ehdl_MODULE eilck_ehdl module internals
 ** @ingroup eilck_ehdl
 ** @{ */

#if !defined(_DOXYGEN_) && !defined(__cplusplus)
typedef int __internal_dummy_type_to_be_ignored;

#endif

typedef struct eilck_ehdl eilck_ehdl;
struct eilck_ehdl {

    union {
        eilck_impl hide;
        eilck_writeHdl* type;
        eilck_voidBase* baze;

        struct {
            eilck_writeHdl* base;
            char _Intern__pIhy2E9k_eilck_ehdl_simpl_fill[sizeof(eilck_impl)-sizeof(eilck_writeHdl*)];
        };
    };
};

inline int eilck_ehdl_acq(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) {
    return eilck_impl_acq(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h-> hide);
}

C_attr_pure

inline C_size eilck_ehdl_length(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) {
    return eilck_impl_length(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h-> hide);
}

inline bool eilck_ehdl_test(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) {
    return _Intern__pIhy2E9k_eilck_ehdl_simpl_h ? eilck_impl_test(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h-> hide) : 0;
}

inline void eilck_ehdl_rel(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) {
    if(_Intern__pIhy2E9k_eilck_ehdl_simpl_h) eilck_impl_rel(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h-> hide);
}

inline void eilck_ehdl_drop(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) {
    if (_Intern__pIhy2E9k_eilck_ehdl_simpl_h) eilck_impl_drop(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h-> hide);
}

inline int eilck_ehdl_req(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h, eilck_obj* _Intern__pIhy2E9k_eilck_ehdl_simpl_ob0) {
    return _Intern__pIhy2E9k_eilck_ehdl_simpl_h ? eilck_impl_req(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h-> hide, _Intern__pIhy2E9k_eilck_ehdl_simpl_ob0, eilck_impl_snippet_excl(_Intern__pIhy2E9k_eilck_ehdl_simpl_h)) : 0;
}

inline
eilck_ehdl const* eilck_ehdl_chain(eilck_ehdl const* _Intern__pIhy2E9k_eilck_ehdl_simpl_sooner, eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_later) {
    eilck_ehdl const* ret = _Intern__pIhy2E9k_eilck_ehdl_simpl_sooner && _Intern__pIhy2E9k_eilck_ehdl_simpl_later ? _Intern__pIhy2E9k_eilck_ehdl_simpl_later : 0;
    if (ret)
        eilck_impl_chain(&_Intern__pIhy2E9k_eilck_ehdl_simpl_sooner-> hide, &_Intern__pIhy2E9k_eilck_ehdl_simpl_later-> hide, eilck_impl_snippet_excl(_Intern__pIhy2E9k_eilck_ehdl_simpl_later));
    return ret;
}

inline
void eilck_ehdl_rel_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) {
    if (_Intern__pIhy2E9k_eilck_ehdl_simpl_h0 && _Intern__pIhy2E9k_eilck_ehdl_simpl_h1 && (eilck_impl_snippet_excl(_Intern__pIhy2E9k_eilck_ehdl_simpl_h0) == eilck_impl_snippet_excl(_Intern__pIhy2E9k_eilck_ehdl_simpl_h1)))
        eilck_impl_rel_2(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h0-> hide, &_Intern__pIhy2E9k_eilck_ehdl_simpl_h1-> hide, eilck_impl_snippet_excl(_Intern__pIhy2E9k_eilck_ehdl_simpl_h0));
}

inline int eilck_ehdl_acq_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) {
    return eilck_impl_acq_2(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h0-> hide, &_Intern__pIhy2E9k_eilck_ehdl_simpl_h1-> hide);
}

C_attr_pure

inline C_size eilck_ehdl_length_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) {
    return eilck_impl_length_2(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h0-> hide, &_Intern__pIhy2E9k_eilck_ehdl_simpl_h1-> hide);
}

inline bool eilck_ehdl_test_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) {
    return (_Intern__pIhy2E9k_eilck_ehdl_simpl_h0 && _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) ? eilck_impl_test_2(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h0-> hide, &_Intern__pIhy2E9k_eilck_ehdl_simpl_h1-> hide) : 0;
}

inline void eilck_ehdl_drop_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) {
    if (_Intern__pIhy2E9k_eilck_ehdl_simpl_h0 && _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) eilck_impl_drop_2(&_Intern__pIhy2E9k_eilck_ehdl_simpl_h0-> hide, &_Intern__pIhy2E9k_eilck_ehdl_simpl_h1-> hide);
}

#ifndef __cplusplus
#define _Intern__pIhy2E9k_eilck_ehdl_simpl_scale_3(H,                           \
        N, C, ...)                                                              \
eilck_ehdl_scale_gen(H, N, C)

#define eilck_ehdl_scale(...)                                                   \
    _Intern__pIhy2E9k_eilck_ehdl_simpl_scale_3(__VA_ARGS__,                     \
            0, 0, 0, )

#define eilck_ehdl_scale_gen(H,                                                 \
                                        N, C)                                   \
_Generic((H)[0].type,                                                           \
         void*: eilck_impl_scale)(&(H)-> hide,                                  \
                 (N), (C))

#define _Intern__pIhy2E9k_eilck_ehdl_simpl_scale2_3(H,                          \
        N, C, ...)                                                              \
eilck_ehdl_scale2_gen(H, N, C)

#define eilck_ehdl_scale2(...)                                                  \
    _Intern__pIhy2E9k_eilck_ehdl_simpl_scale2_3(__VA_ARGS__,                    \
            0, 0, 0, )

#define eilck_ehdl_scale2_gen(H,                                                \
        N, C)                                                                   \
_Generic((H)[0][0].type,                                                        \
         void*: eilck_impl_scale2)((void*)(H),                                  \
                 (N), (C))
#endif

#ifdef CMOD_2F746D702F636D6F642D746D70642E467769794D635944704968793245396B2F65696C636B2D6568646C2E63_HEADER_INSTANTIATE
extern inline int eilck_ehdl_acq(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) ;

extern C_attr_pure

inline C_size eilck_ehdl_length(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) ;

extern inline bool eilck_ehdl_test(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) ;

extern inline void eilck_ehdl_rel(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) ;

extern inline void eilck_ehdl_drop(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h) ;

extern inline int eilck_ehdl_req(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h, eilck_obj* _Intern__pIhy2E9k_eilck_ehdl_simpl_ob0) ;

extern inline
eilck_ehdl const* eilck_ehdl_chain(eilck_ehdl const* _Intern__pIhy2E9k_eilck_ehdl_simpl_sooner, eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_later) ;

extern inline
void eilck_ehdl_rel_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) ;

extern inline int eilck_ehdl_acq_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) ;

extern C_attr_pure

inline C_size eilck_ehdl_length_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) ;

extern inline bool eilck_ehdl_test_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) ;

extern inline void eilck_ehdl_drop_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1) ;
#endif

extern eilck_writeHdl* eilck_ehdl_map(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_simpl_h);

extern eilck_writeHdl* eilck_ehdl_map_2(eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h0, eilck_ehdl* restrict _Intern__pIhy2E9k_eilck_ehdl_simpl_h1);

extern int eilck_ehdl_acq2(eilck_ehdl (*_Intern__pIhy2E9k_eilck_ehdl_simpl_h)[2]);

extern C_attr_pure
C_size eilck_ehdl_length2(eilck_ehdl (*_Intern__pIhy2E9k_eilck_ehdl_simpl_h)[2]);

extern bool eilck_ehdl_test2(eilck_ehdl (*_Intern__pIhy2E9k_eilck_ehdl_simpl_h)[2]);

extern eilck_writeHdl* eilck_ehdl_map2(eilck_ehdl (*_Intern__pIhy2E9k_eilck_ehdl_simpl_h)[2]);

extern void eilck_ehdl_rel2(eilck_ehdl (*_Intern__pIhy2E9k_eilck_ehdl_simpl_h)[2]);

extern void eilck_ehdl_drop2(eilck_ehdl (*_Intern__pIhy2E9k_eilck_ehdl_simpl_h)[2]);

extern void* (eilck_ehdl_scale)(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h, C_size nsize, void* _Intern__pIhy2E9k_eilck_ehdl_cntx);

extern void* (eilck_ehdl_scale2)(eilck_ehdl (*_Intern__pIhy2E9k_eilck_ehdl_h)[2], C_size nsize, void* _Intern__pIhy2E9k_eilck_ehdl_cntx);

inline
void* eilck_ehdl_open(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h, void* _Intern__pIhy2E9k_eilck_ehdl_cntx) {
    return eilck_ehdl_scale(_Intern__pIhy2E9k_eilck_ehdl_h, ((C_size)-eilck_obj_scaler_open), _Intern__pIhy2E9k_eilck_ehdl_cntx);
}

inline
void* eilck_ehdl_create(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h, void* _Intern__pIhy2E9k_eilck_ehdl_cntx) {
    return eilck_ehdl_scale(_Intern__pIhy2E9k_eilck_ehdl_h, ((C_size)-eilck_obj_scaler_create), _Intern__pIhy2E9k_eilck_ehdl_cntx);
}

inline
void* eilck_ehdl_clear(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h) {
    return eilck_ehdl_scale(_Intern__pIhy2E9k_eilck_ehdl_h, ((C_size)-eilck_obj_scaler_clear), nullptr);
}

inline
void* eilck_ehdl_close(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h) {
    return eilck_ehdl_scale(_Intern__pIhy2E9k_eilck_ehdl_h, ((C_size)-eilck_obj_scaler_close), nullptr);
}

inline
void* eilck_ehdl_remove(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h) {
    return eilck_ehdl_scale(_Intern__pIhy2E9k_eilck_ehdl_h, ((C_size)-eilck_obj_scaler_remove), nullptr);
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E467769794D635944704968793245396B2F65696C636B2D6568646C2E63_HEADER_INSTANTIATE
extern inline
void* eilck_ehdl_open(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h, void* _Intern__pIhy2E9k_eilck_ehdl_cntx) ;

extern inline
void* eilck_ehdl_create(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h, void* _Intern__pIhy2E9k_eilck_ehdl_cntx) ;

extern inline
void* eilck_ehdl_clear(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h) ;

extern inline
void* eilck_ehdl_close(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h) ;

extern inline
void* eilck_ehdl_remove(eilck_ehdl* _Intern__pIhy2E9k_eilck_ehdl_h) ;
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E467769794D635944704968793245396B2F65696C636B2D6568646C2E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-ehdl */
