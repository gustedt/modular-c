/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_impl_snippet__GUARD
#define eilck_impl_snippet__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"
#include "eilck-impl.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_impl_snippet
 ** @ingroup eilck_impl
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4B4557554268514A4E6B3747464677762F65696C636B2D696D706C2D736E69707065742E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4B4557554268514A4E6B3747464677762F65696C636B2D696D706C2D736E69707065742E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4B4557554268514A4E6B3747464677762F65696C636B2D696D706C2D736E69707065742E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4B4557554268514A4E6B3747464677762F65696C636B2D696D706C2D736E69707065742E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_impl_snippet_MODULE eilck_impl_snippet module internals
 ** @ingroup eilck_impl_snippet
 ** @{ */

#ifndef __cplusplus
#define eilck_impl_snippet_excl(H)                                              \
    _Generic((H)-> type,                                                        \
             void const*: 0, void const volatile*: 0,                           \
             default: 1)
#else
#define eilck_impl_snippet_excl __excl
#endif

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4B4557554268514A4E6B3747464677762F65696C636B2D696D706C2D736E69707065742E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-impl-snippet */
