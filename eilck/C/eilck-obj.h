/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_obj__GUARD
#define eilck_obj__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-bitset.h"
#include "C-snippet.h"
#include "C-attr.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_obj
 ** @ingroup eilck
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4557435A6E555564716372613631584E2F65696C636B2D6F626A2E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4557435A6E555564716372613631584E2F65696C636B2D6F626A2E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4557435A6E555564716372613631584E2F65696C636B2D6F626A2E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4557435A6E555564716372613631584E2F65696C636B2D6F626A2E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_obj_MODULE eilck_obj module internals
 ** @ingroup eilck_obj
 ** @{ */

#ifndef NO_ATOMICS
extern void* _Intern__qcra61XN_eilck_obj_atomic_abi;

#endif

typedef struct eilck_obj eilck_obj;
struct eilck_obj;

typedef void* eilck_obj_scaler(eilck_obj*, C_size, void*);

extern void* eilck_obj_realloc(eilck_obj*, C_size, void*);

extern void* eilck_obj_scale(eilck_obj*, C_size, void*);

typedef struct eilck_obj eilck_obj;
struct eilck_obj {
    eilck_lck _Intern__qcra61XN_eilck_obj_mutex;
    _Atomic(C_size) _Intern__qcra61XN_eilck_obj_tail;
    C_size size;
    int prot;
    void* data;
    eilck_obj_scaler* alloc;
    eilck_circ* _Intern__qcra61XN_eilck_obj_table;
};

#define eilck_obj_INITIALIZER(...)                                              \
    eilck_obj_INITIALIZER_4(__VA_ARGS__,                                        \
                                        0, 0, 0, 0)

#define eilck_obj_DEFAULT ._Intern__qcra61XN_eilck_obj_tail = 1,                \
                                                                                \
        .prot = eilck_RW

#define eilck_obj_INITIALIZER_4(TABLEN,                                         \
        OALLOC, OSIZE, ODATA,...)                                               \
{                                                                               \
    eilck_obj_DEFAULT,                                                          \
    ._Intern__qcra61XN_eilck_obj_table = eilck_circ_COMPOUND(TABLEN),           \
                                                                                \
            .alloc = (OALLOC),                                                  \
                     .size = (OSIZE),                                           \
                             .data = (ODATA), }

inline
int eilck_obj_acq(eilck_obj* _Intern__qcra61XN_eilck_obj_ob, C_size _Intern__qcra61XN_eilck_obj_pos) {
    return eilck_circ_acquire(_Intern__qcra61XN_eilck_obj_ob-> _Intern__qcra61XN_eilck_obj_table, _Intern__qcra61XN_eilck_obj_pos);
}

inline
bool eilck_obj_test(eilck_obj* _Intern__qcra61XN_eilck_obj_ob, C_size _Intern__qcra61XN_eilck_obj_pos) {
    return _Intern__qcra61XN_eilck_obj_ob ? eilck_circ_test(_Intern__qcra61XN_eilck_obj_ob-> _Intern__qcra61XN_eilck_obj_table, _Intern__qcra61XN_eilck_obj_pos) : 0;
}

C_attr_pure

inline
C_size eilck_obj_length(eilck_obj* _Intern__qcra61XN_eilck_obj_ob) {
    return _Intern__qcra61XN_eilck_obj_ob ? _Intern__qcra61XN_eilck_obj_ob-> size : 0;
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E4557435A6E555564716372613631584E2F65696C636B2D6F626A2E63_HEADER_INSTANTIATE
extern inline
int eilck_obj_acq(eilck_obj* _Intern__qcra61XN_eilck_obj_ob, C_size _Intern__qcra61XN_eilck_obj_pos) ;

extern inline
bool eilck_obj_test(eilck_obj* _Intern__qcra61XN_eilck_obj_ob, C_size _Intern__qcra61XN_eilck_obj_pos) ;

extern C_attr_pure

inline
C_size eilck_obj_length(eilck_obj* _Intern__qcra61XN_eilck_obj_ob) ;
#endif

extern C_size eilck_obj_ireq(eilck_obj* _Intern__qcra61XN_eilck_obj_ob);

extern C_size eilck_obj_ereq(eilck_obj* _Intern__qcra61XN_eilck_obj_ob);

extern int eilck_obj_rel(eilck_obj* _Intern__qcra61XN_eilck_obj_ob, C_size _Intern__qcra61XN_eilck_obj_pos);

extern void* eilck_obj_map(eilck_obj* _Intern__qcra61XN_eilck_obj_ob);

extern void* eilck_obj_copy(eilck_obj* restrict target, eilck_obj* restrict source);

extern eilck_obj* eilck_obj_init(eilck_obj* _Intern__qcra61XN_eilck_obj_ob, C_size tablen);

extern eilck_obj* eilck_obj_destroy(eilck_obj* _Intern__qcra61XN_eilck_obj_ob);
#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4557435A6E555564716372613631584E2F65696C636B2D6F626A2E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-obj */
