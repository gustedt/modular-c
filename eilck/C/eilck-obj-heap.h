/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_obj_heap__GUARD
#define eilck_obj_heap__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_obj_heap
 ** @ingroup eilck_obj
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4C5538476958716A43636C4F377230612F65696C636B2D6F626A2D686561702E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4C5538476958716A43636C4F377230612F65696C636B2D6F626A2D686561702E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4C5538476958716A43636C4F377230612F65696C636B2D6F626A2D686561702E63_HEADER
#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E4C5538476958716A43636C4F377230612F65696C636B2D6F626A2D686561702E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_obj_heap_MODULE eilck_obj_heap module internals
 ** @ingroup eilck_obj_heap
 ** @{ */

extern void* eilck_obj_heap(eilck_obj* ob, C_size nsize, void* cntx);
#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E4C5538476958716A43636C4F377230612F65696C636B2D6F626A2D686561702E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-obj-heap */
