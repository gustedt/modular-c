#include "eilcktest.h"

static
unsigned _Atomic sum = 0;

static
void* reader(void* arg) {
    eilck_ihdl* h = arg;
    eilck_ihdl h2[2] = { 0 };
    eilck_hdl_SECTION(h) {
        unsigned const* u = eilck_hdl_map(h);
        sum += *u;
        eilck_hdl_chain(h, h2);
    }
    for (unsigned it = 0; it < iterations; ++it) {
        eilck_hdl_SECTION2(&h2) {
            unsigned const* u = eilck_hdl_map2(&h2);
            sum += *u;
        }
    }
    eilck_hdl_drop2(&h2);
    return 0;
}

static
void* writer(void* arg) {
    eilck_ehdl* h = arg;
    eilck_ehdl h2[2] = { 0 };
    eilck_hdl_SECTION(h) {
        unsigned* u = eilck_hdl_map(h);
        ++*u;
        eilck_hdl_chain(h, h2);
    }
    for (unsigned it = 0; it < iterations; ++it) {
        eilck_hdl_SECTION2(&h2) {
            unsigned* u = eilck_hdl_map2(&h2);
            ++*u;
        }
    }
    eilck_hdl_drop2(&h2);
    return 0;
}

static
eilck_obj object = eilck_obj_INITIALIZER(eilck_len);

static
eilck_ehdl* eh;

static
ihdl10* ih;

static
void final(void) {
    // a last CS with exclusive access
    eilck_ehdl last = { 0 };
    eilck_hdl_req(&last, &object);
    eilck_hdl_SECTION(&last) {
        unsigned* u = eilck_hdl_map(&last);
        printf("last thread: we had %u writers, sum is %u, length is %zu bytes\n",
               *u, sum, eilck_hdl_length(&last));
        eilck_ehdl_scale(&last, 0, 0);
    }
    free(eh);
    free(ih);
}

int main(int argc, char* argv[argc+1]) {
    atexit(final);
    unsigned writers = 10;
    if (argc > 1) writers = strtoull(argv[1], 0, 0);
    eh = malloc(sizeof(eilck_ehdl[writers]));
    ih = malloc(sizeof(ihdl10[writers]));
    eilck_ehdl first = { 0 };
    eilck_hdl_req(&first, &object);

    for (unsigned i = 0; i < writers; ++i) {
        eilck_hdl_req(&eh[i], &object);
        for (unsigned j = 0; j < readers; ++j)
            eilck_hdl_req(&ih[i][j], &object);
    }

    printf("main: inserted all requests\n");

    for (unsigned i = 0; i < writers; ++i) {
        pthread_t id;
        pthread_create(&id, 0, writer, &eh[i]);
        pthread_detach(id);
    }

    printf("main: started all writer threads\n");

    for (unsigned i = 0; i < writers; ++i) {
        for (unsigned j = 0; j < readers; ++j) {
            pthread_t id;
            pthread_create(&id, 0, reader, &ih[i][j]);
            pthread_detach(id);
        }
    }

    printf("main: started all reader threads\n");

    // a first CS with write access
    eilck_hdl_SECTION(&first) {
        eilck_ehdl_scale(&first, sizeof(unsigned), 0);
        unsigned* u = eilck_hdl_map(&first);
        *u = 0;
    }

    printf("main: unleached threads, exiting\n");

    pthread_exit(0);
}
