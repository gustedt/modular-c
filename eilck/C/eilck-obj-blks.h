/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_obj_blks__GUARD
#define eilck_obj_blks__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-snippet.h"
#include "C-attr.h"
#include "C-snippet-flexible.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-obj-fixed.h"
#include "eilck-sem.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_obj_blks
 ** @ingroup eilck_obj
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E626A4946354347393850386E536865652F65696C636B2D6F626A2D626C6B732E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E626A4946354347393850386E536865652F65696C636B2D6F626A2D626C6B732E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E626A4946354347393850386E536865652F65696C636B2D6F626A2D626C6B732E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E626A4946354347393850386E536865652F65696C636B2D6F626A2D626C6B732E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_obj_blks_MODULE eilck_obj_blks module internals
 ** @ingroup eilck_obj_blks
 ** @{ */

#if eilck_RW <= C_uchar_MAX
typedef unsigned char eilck_obj_blks_rw;
#elif eilck_RW <= C_ushort_MAX
typedef unsigned short eilck_obj_blks_rw;
#elif eilck_RW <= C_unsigned_MAX
typedef unsigned eilck_obj_blks_rw;
#elif eilck_RW <= C_ulong_MAX

typedef unsigned long eilck_obj_blks_rw;
#else

typedef unsigned long long eilck_obj_blks_rw;
#endif

#define eilck_obj_blks__Intern_defill_BLKS_min 5

#define eilck_obj_blks__Intern_defill_BLKS_OTHERS unsigned*restrict elast;      \
    unsigned*restrict                                                           \
    ifirst;                                                                     \
    unsigned*restrict ilast;                                                    \
    C_size grain;                                                               \
    eilck_sem processors;                                                       \
    C_hash12*restrict owner;                                                    \
    eilck_obj_blks_rw*restrict accs;                                            \

#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert(sizeof((eilck_obj) {
    0
}), "T in C#snippet#flexible must be a complete type");
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
static_assert( !C_HAS2( 1) || (C_FIRST( 1) <= C_SECOND( 1)), "bounds inverted: " C_STRINGIFY(C_FIRST( 1)) " is > " C_STRINGIFY(C_SECOND( 1)));
static_assert(C_FIRST( 1) <= eilck_obj_blks__Intern_defill_BLKS_min, "min is < " C_STRINGIFY(C_FIRST( 1)));
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_obj_blks__MODULE_CON(I6_Block1C7snippet8flexible4INIT10_Snippet_6E)(void) {
    if (0)
        C_attr_maybe_unused eilck_obj x[eilck_obj_blks__Intern_defill_BLKS_min] = C_snippet_flexible_INIT;
};
#endif
#if !defined(_DOXYGEN_) && !defined(__cplusplus)
C_attr_maybe_unused static void eilck_obj_blks__MODULE_CON(I6_Block1C7snippet8flexible6OTHERS10_Snippet_6E)(void) {
    if (0) {
        C_attr_maybe_unused struct {
            C_size len;
            eilck_obj_blks__Intern_defill_BLKS_OTHERS
        } _Intern__8P8nShee_eilck_obj_blks_BLKS_dummy = { .len = 67 };
    }
};
#endif

typedef struct {
    C_size len;
    eilck_obj_blks__Intern_defill_BLKS_OTHERS eilck_obj tab[];
} eilck_obj_blks_BLKS_offset_type;

enum _Intern__8P8nShee_eilck_obj_blks_BLKS_edummy {
    _Intern__8P8nShee_eilck_obj_blks_BLKS_offset = __Cmod_offsetof(eilck_obj_blks_BLKS_offset_type, tab),
};
typedef enum _Intern__8P8nShee_eilck_obj_blks_BLKS_edummy _Intern__8P8nShee_eilck_obj_blks_BLKS_edummy;

typedef struct {
    C_size _Intern__8P8nShee_eilck_obj_blks_BLKS_mutableLen;
    eilck_obj_blks__Intern_defill_BLKS_OTHERS
} _Intern__8P8nShee_eilck_obj_blks_BLKS_header;

typedef union eilck_obj_blks_BLKS_fa eilck_obj_blks_BLKS_fa;
typedef union eilck_obj_blks_BLKS_fa eilck_obj_blks_BLKS_fa;

typedef union eilck_obj_blks_BLKS_fa eilck_obj_blks_BLKS_fa;
union eilck_obj_blks_BLKS_fa {

    struct {

        union {
            unsigned char _Intern__8P8nShee_eilck_obj_blks_BLKS_chars[_Intern__8P8nShee_eilck_obj_blks_BLKS_offset];
            _Intern__8P8nShee_eilck_obj_blks_BLKS_header head;
        };
        eilck_obj _Intern__8P8nShee_eilck_obj_blks_BLKS_fixedTab[eilck_obj_blks__Intern_defill_BLKS_min];
    } _Intern__8P8nShee_eilck_obj_blks_BLKS_overlay;
    _Intern__8P8nShee_eilck_obj_blks_BLKS_edummy _Intern__8P8nShee_eilck_obj_blks_BLKS_dummy;

    struct {
        C_size const len;
        eilck_obj_blks__Intern_defill_BLKS_OTHERS
        eilck_obj tab[];
    };
};

#define eilck_obj_blks_BLKS_COMPOUND(LEN)                                       \
    &((union {                                                                  \
        unsigned char _Intern__8P8nShee_eilck_obj_blks_BLKS_maximum[_Intern__8P8nShee_eilck_obj_blks_BLKS_offset+sizeof(eilck_obj[LEN])];\
        struct {                                                                \
            union {                                                             \
                unsigned char _Intern__8P8nShee_eilck_obj_blks_BLKS_chars[_Intern__8P8nShee_eilck_obj_blks_BLKS_offset];\
                _Intern__8P8nShee_eilck_obj_blks_BLKS_header head;              \
            };                                                                  \
            eilck_obj _Intern__8P8nShee_eilck_obj_blks_BLKS_fixedTab[LEN];      \
        } _Intern__8P8nShee_eilck_obj_blks_BLKS_overlay;                        \
        eilck_obj_blks_BLKS_fa _Intern__8P8nShee_eilck_obj_blks_BLKS_dummy;     \
    }){                                                                         \
        ._Intern__8P8nShee_eilck_obj_blks_BLKS_overlay = {                      \
                                                                  .head = {     \
                                                                           ._Intern__8P8nShee_eilck_obj_blks_BLKS_mutableLen = (LEN),\
                                                                          }, ._Intern__8P8nShee_eilck_obj_blks_BLKS_fixedTab = C_snippet_flexible_INIT,\
                                                                 }, })._Intern__8P8nShee_eilck_obj_blks_BLKS_dummy

extern eilck_obj_blks_BLKS_fa* eilck_obj_blks_BLKS_alloc(C_size _Intern__8P8nShee_eilck_obj_blks_BLKS_length);

typedef int __internal_dummy_type_to_be_ignored;

typedef eilck_obj_blks_BLKS_fa eilck_obj_blks;

#define eilck_obj_blks_grain_default 16

extern eilck_obj_blks* eilck_obj_blks_init(eilck_obj_blks* _Intern__8P8nShee_eilck_obj_blks_b, C_size _Intern__8P8nShee_eilck_obj_blks_qlen);

extern eilck_obj_blks* eilck_obj_blks_destroy(eilck_obj_blks* _Intern__8P8nShee_eilck_obj_blks_b);

inline
eilck_obj_blks* eilck_obj_blks_alloc(C_size _Intern__8P8nShee_eilck_obj_blks_length, C_size _Intern__8P8nShee_eilck_obj_blks_qlen) {
    return eilck_obj_blks_init(eilck_obj_blks_BLKS_alloc(_Intern__8P8nShee_eilck_obj_blks_length), _Intern__8P8nShee_eilck_obj_blks_qlen);
}

inline
void eilck_obj_blks_free(eilck_obj_blks* _Intern__8P8nShee_eilck_obj_blks_b) {
    C_lib_free(eilck_obj_blks_destroy(_Intern__8P8nShee_eilck_obj_blks_b));
}

inline
C_size eilck_obj_blks_count(eilck_obj_blks const* restrict _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* restrict p, C_size size) {
    C_size const grain = _Intern__8P8nShee_eilck_obj_blks_b-> grain;

    C_size boffset = ((C_uintptr)p) % grain;
    size += boffset;
    return size / grain + !! (size % grain);
}

inline
void eilck_obj_blks_set(eilck_obj_blks* restrict _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* restrict p, C_size size, C_size τ) {
    C_hash12* restrict owner = _Intern__8P8nShee_eilck_obj_blks_b-> owner;
    C_size const grain = _Intern__8P8nShee_eilck_obj_blks_b-> grain;
    C_size const def = τ;
    unsigned const volatile char* by = p;

    C_size boffset = ((C_uintptr)p) % grain;
    by -= boffset;
    size += boffset;
    for (C_size blip = 0; blip < size; blip += grain)
        owner = C_hash12_insert(owner, (C_uintptr)(by+blip), def);
    _Intern__8P8nShee_eilck_obj_blks_b-> owner = owner;
}

inline
void eilck_obj_blks_mark(eilck_obj_blks const* restrict _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* restrict p, C_size size, bool excl) {
    C_hash12 const* restrict owner = _Intern__8P8nShee_eilck_obj_blks_b-> owner;
    eilck_obj_blks_rw* restrict accs = _Intern__8P8nShee_eilck_obj_blks_b-> accs;
    eilck_obj_blks_rw const flag = (! excl ? eilck_R : eilck_W);
    C_size const grain = _Intern__8P8nShee_eilck_obj_blks_b-> grain;
    unsigned const volatile char* by = p;

    C_size boffset = ((C_uintptr)p) % grain;
    by -= boffset;
    size += boffset;
    for (C_uintptr blip = (C_uintptr)by, stop = blip+size;;) {
        C_uintptr here = C_hash12_value(owner, blip);
        if (here != C_hash12_invalid) accs[here] |= flag;
        blip += grain;
        if (blip >= stop) break;
    }
}

inline
void eilck_obj_blks_imark(eilck_obj_blks const* _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* _Intern__8P8nShee_eilck_obj_blks_pos, C_size size) {
    eilck_obj_blks_mark(_Intern__8P8nShee_eilck_obj_blks_b, _Intern__8P8nShee_eilck_obj_blks_pos, size, false);
}

inline
void eilck_obj_blks_emark(eilck_obj_blks const* _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* _Intern__8P8nShee_eilck_obj_blks_pos, C_size size) {
    eilck_obj_blks_mark(_Intern__8P8nShee_eilck_obj_blks_b, _Intern__8P8nShee_eilck_obj_blks_pos, size, true);
}

#ifdef CMOD_2F746D702F636D6F642D746D70642E626A4946354347393850386E536865652F65696C636B2D6F626A2D626C6B732E63_HEADER_INSTANTIATE
extern inline
eilck_obj_blks* eilck_obj_blks_alloc(C_size _Intern__8P8nShee_eilck_obj_blks_length, C_size _Intern__8P8nShee_eilck_obj_blks_qlen) ;

extern inline
void eilck_obj_blks_free(eilck_obj_blks* _Intern__8P8nShee_eilck_obj_blks_b) ;

extern inline
C_size eilck_obj_blks_count(eilck_obj_blks const* restrict _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* restrict p, C_size size) ;

extern inline
void eilck_obj_blks_set(eilck_obj_blks* restrict _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* restrict p, C_size size, C_size τ) ;

extern inline
void eilck_obj_blks_mark(eilck_obj_blks const* restrict _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* restrict p, C_size size, bool excl) ;

extern inline
void eilck_obj_blks_imark(eilck_obj_blks const* _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* _Intern__8P8nShee_eilck_obj_blks_pos, C_size size) ;

extern inline
void eilck_obj_blks_emark(eilck_obj_blks const* _Intern__8P8nShee_eilck_obj_blks_b, void const volatile* _Intern__8P8nShee_eilck_obj_blks_pos, C_size size) ;
#endif

extern eilck_obj_blks* eilck_obj_blks_alloc_owner(eilck_obj_blks* _Intern__8P8nShee_eilck_obj_blks_b, C_size elems);

extern void eilck_obj_blks_free_owner(eilck_obj_blks* _Intern__8P8nShee_eilck_obj_blks_b);

typedef int __internal_dummy_type_to_be_ignored;

typedef int __internal_dummy_type_to_be_ignored;

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E626A4946354347393850386E536865652F65696C636B2D6F626A2D626C6B732E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-obj-blks */
