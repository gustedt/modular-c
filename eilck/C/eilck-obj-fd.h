/* This traditional -*- C -*- header file is automatically */
/* extracted for a Modular C project.                      */
/* DON'T MODIFY IT MANUALLY!                               */

#ifndef eilck_obj_fd__GUARD
#define eilck_obj_fd__GUARD 1

/* The automatically deduced dependencies: */
#include "C.h"
#include "C-attr.h"
#include "C-snippet.h"
#include "C-bitset.h"
#include "eilck.h"
#include "eilck-state.h"
#include "C-snippet-flexible.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-obj-scaler.h"
#include "eilck-error.h"
#include "eilck-obj-heap.h"
#include "eilck-obj-scale.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 ** @module eilck_obj_fd
 ** @ingroup eilck_obj
 ** @{ */
#ifdef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E45716F68706E4937314D54754A75626F2F65696C636B2D6F626A2D66642E63_HEADER
#error cyclic inclusion of interface specification
#endif

#define __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E45716F68706E4937314D54754A75626F2F65696C636B2D6F626A2D66642E63_HEADER
#ifndef __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E45716F68706E4937314D54754A75626F2F65696C636B2D6F626A2D66642E63_HEADER

#define __CMOD_INTERNAL_CMOD_2F746D702F636D6F642D746D70642E45716F68706E4937314D54754A75626F2F65696C636B2D6F626A2D66642E63_HEADER

typedef int __internal_dummy_type_to_be_ignored;

/** @} */

/**
 ** @defgroup eilck_obj_fd_MODULE eilck_obj_fd module internals
 ** @ingroup eilck_obj_fd
 ** @{ */

#ifndef __CMOD__
typedef int Linux_fd;
#endif

typedef struct eilck_obj_fd_info eilck_obj_fd_info;
struct eilck_obj_fd_info {
    Linux_fd desc;
    void* addr;
    char* name;
};

#define eilck_obj_fd_REMOVE Linux_fs_unlink

#define eilck_obj_fd_OPEN Linux_fd_open

extern void eilck_obj_fd_realpath(eilck_obj_fd_info* _Intern__1MTuJubo_eilck_obj_fd_inf);

extern void* eilck_obj_fd_cleanup(eilck_obj* _Intern__1MTuJubo_eilck_obj_fd_ob0, eilck_obj_fd_info* _Intern__1MTuJubo_eilck_obj_fd_inf, void* _Intern__1MTuJubo_eilck_obj_fd_ret);

extern void* eilck_obj_fd_mapit(eilck_obj* _Intern__1MTuJubo_eilck_obj_fd_ob0, eilck_obj_fd_info* _Intern__1MTuJubo_eilck_obj_fd_inf);

#endif
#undef __CMOD_CYCLIC_CMOD_2F746D702F636D6F642D746D70642E45716F68706E4937314D54754A75626F2F65696C636B2D6F626A2D66642E63_HEADER
/**
 ** @} */

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilck-obj-fd */
