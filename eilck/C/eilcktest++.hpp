#ifndef EILCKTEST_HPP
#define EILCKTEST_HPP 1

#ifdef __GNUC__
#pragma interface
#endif

#include "eilck++.hpp"

namespace eilck {
  template class obj< unsigned >;
  template class ehdl< unsigned >;
  template class ihdl< unsigned >;
}

#endif
