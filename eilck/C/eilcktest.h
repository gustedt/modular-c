/* This traditional -*- C -*- header file was automatically */
/* extracted for a Modular C project.                      */

#ifndef eilcktest__GUARD
#define eilcktest__GUARD 1

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

/* The automatically deduced dependencies: */
#include "C.h"
#include "eilck.h"
#include "eilck-state.h"
#include "eilck-ftx.h"
#include "eilck-circ.h"
#include "eilck-lck.h"
#include "eilck-obj.h"
#include "eilck-ehdl.h"
#include "eilck-ihdl.h"
#include "eilck-hdl.h"

#ifdef __cplusplus
extern "C" {
#endif

enum en { writers = 500, readers = 10, iterations = 80, };

typedef enum en en;

typedef eilck_ihdl ihdl10[readers];

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* eilcktest */
