/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module hdl =
#pragma CMOD separator  ∷
#pragma CMOD composer   —
#pragma CMOD import impl  = ..∷impl
#pragma CMOD import ehdl  = ..∷ehdl
#pragma CMOD import ihdl  = ..∷ihdl
#pragma CMOD import obj = ..∷obj

// protect some function parameters
#pragma CMOD intern ob
#pragma CMOD intern h

/**
 ** @module
 ** @brief Generic interfaces to deal with lock handles.
 **
 ** These interfaces are meant to handle @doxy eilck::ehdl and @doxy eilck::ihdl.
 ** The easiest to use are @doxy eilck::hdl::SECTION and @doxy eilck::hdl::SECTION2,
 ** to mark critical sections.
 ** @code
 eilck::ehdl ehd;
 eilck::ihdl ihd;
 ...
 eilck::hdl::SEQUENCE(ob, &ehd, &ihd);
 ...
 // writer thread
 eilck::hdl::SECTION(&hd) {
    double* a = hd->map();
    C::size l = hd->length();
    for (C::size i = 0; i < l; ++i)
       a[i] = bla(i);
 }
 ...
 // reader thread
 eilck::hdl::SECTION(&hd) {
    double* a = hd->map();
    C::size l = hd->length();
    for (C::size i = 0; i < l; ++i)
        C::io::printf("a[%zu] = %g\n", i, a[i]);
 }
 ** @endcode
 **/

/**
 ** @module
 ** @internal
 **
 ** Notes on the implementation:
 **
 ** To be able to handle "template" types, we suppose that the handle
 ** type has several dummy pointer members that overlay the initial
 ** bytes by use of a union.
 **
 ** This initial pointer member of the object is (mis)-used in an
 ** assignment statement to ensure that the return value of @c map and
 ** @c have the right type. Don't use the initial bytes of the object
 ** for any other purpose.
 **
 ** - @c type, should be either <code>void*</code> or <code>void
 **   const*</code> to indicate if this is an exclusive or inclusive
 **   handle.
 **
 ** - @c base has the pointer type to which @c map and @c scale should
 **   convert. This pointer is overwritten unexpectedly. Don't use it
 **   for other purposes.
 **
 ** - @c baze is almost the same as @c base, only that it should be a
 **   complete type. This difference is only relevant if @c base is
 **   <code>void*</code> or <code>void const*</code>. <code>sizeof
 **   *baze</code> is the size of individual elements of the array. @c
 **   length etc count in that size.
 **/


#pragma CMOD declaration

// These are generic base-type agnostic macros.
#pragma CMOD foreach MAC = acq test rel drop
#ifndef __cplusplus
/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC} and @doxy eilck::ihdl::${MAC}.
 **/
#define ${MAC}(H)                                              \
_Generic((char(*)[sizeof((H)[0])]){0},                         \
         char(*)[sizeof(impl)]:                                \
         _Generic((H)[0].type,                                 \
                  void*       : ehdl∷${MAC},                   \
                  void const* : ihdl∷${MAC})                   \
         )(H)
/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC}2 and
 ** @doxy eilck::ihdl::${MAC}2.
 **/
#define ${MAC}2(H)                                             \
_Generic((char(*)[sizeof((H)[0])]){0},                         \
         char(*)[sizeof(impl[2])]:                             \
         _Generic((H)[0][0].type,                              \
                  void*       : ehdl∷${MAC}2,                  \
                  void const* : ihdl∷${MAC}2)                  \
         )((void*)(H))
#endif
#pragma CMOD done

// These are base-type sensitive macros.
#pragma CMOD foreach MAC = map
#ifndef __cplusplus
/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC} and
 ** @doxy eilck::ihdl::${MAC}.
 **
 ** If the argument @a H is a template handle of base type @c T, the
 ** return is of type @c T*.
 **
 ** @remark Evaluates argument @a H twice.
 **/
#define ${MAC}(H)                                              \
/* This uses a dummy assigment to force an rvalue */           \
/* of the desired base type. */                                \
((H)[0].base =                                                 \
 _Generic((char(*)[sizeof((H)[0])]){0},                        \
          char(*)[sizeof(impl)]:                               \
          _Generic((H)[0].type,                                \
                   void*       : ehdl∷${MAC},                  \
                   void const* : ihdl∷${MAC}                   \
                   ))((void*)H))

/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC}2 and
 ** @doxy eilck::ihdl::${MAC}2.
 **
 ** If the argument @a H is a pair template handles of base type @c T,
 ** the return is of type @c T*.
 **
 ** @remark Evaluates argument @a H twice.
 **/
#define ${MAC}2(H)                                             \
/* This uses a dummy assigment to force an rvalue */           \
/* of the desired base type. */                                \
((H)[0]→base =                                                 \
 _Generic((char(*)[sizeof((H)[0])]){0},                        \
          char(*)[sizeof(impl[2])]:                            \
          _Generic((H)[0][0].type,                             \
                   void*       : ehdl∷${MAC}2,                 \
                   void const* : ihdl∷${MAC}2                  \
                   ))((void*)(H)))
#endif
#pragma CMOD done

// These are base-type sensitive macros.
#pragma CMOD foreach MAC = scale
#ifndef __cplusplus
/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC} such that
 ** the size @a N can be expressed in number of elements of the base
 ** type, and such the return type is a pointer to that type.
 **
 ** @remark Evaluates argument @a H twice.
 **/
#define ${MAC}(H, N, C)                                           \
/* This uses a dummy assigment to force an rvalue */              \
/* of the desired base type. */                                   \
((H)[0].base =                                                    \
 _Generic((char(*)[sizeof((H)[0])]){0},                           \
          char(*)[sizeof(impl)]:                                  \
          _Generic((H)[0].type,                                   \
                   void*       : ehdl∷${MAC}                      \
                   ))((void*)H, (N)*sizeof((H)[0].baze[0]), (C)))

/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC}2 such that
 ** the size @a N can be expressed in number of elements of the base
 ** type, and such the return type is a pointer to that type.
 **
 ** @remark Evaluates argument @a H twice.
 **/
#define ${MAC}2(H, N, C)                                            \
/* This uses a dummy assigment to force an rvalue */                \
/* of the desired base type. */                                     \
((H)[0]→base =                                                      \
 _Generic((char(*)[sizeof((H)[0])]){0},                             \
          char(*)[sizeof(impl[2])]:                                 \
          _Generic((H)[0][0].type,                                  \
                   void*       : ehdl∷${MAC}2                       \
                   ))((void*)(H), (N)*sizeof((H)[0]→baze[0]), (C)))
#endif
#pragma CMOD done

#pragma CMOD foreach MAC = length
#ifndef __cplusplus
/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC} and
 ** @doxy eilck::ihdl::${MAC}.
 **/
#define ${MAC}(H)                                              \
(_Generic((char(*)[sizeof((H)[0])]){0},                        \
          char(*)[sizeof(impl)]:                               \
          _Generic((H)[0].type,                                \
                   void*       : ehdl∷${MAC},                  \
                   void const* : ihdl∷${MAC}                   \
                   ))((void*)H)/sizeof((H)[0].baze[0]))
/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC}2 and
 ** @doxy eilck::ihdl::${MAC}2.
 **/
#define ${MAC}2(H)                                             \
(_Generic((char(*)[sizeof((H)[0][0])]){0},                     \
         char(*)[sizeof(impl[2])]:                             \
          _Generic((H)[0][0].type,                             \
                   void*       : ehdl∷${MAC}2,                 \
                   void const* : ihdl∷${MAC}2                  \
                   ))((void*)(H))/sizeof((H)[0][0].baze[0]))
#endif
#pragma CMOD done

#pragma CMOD foreach MAC = acq rel
#pragma CMOD intern ${MAC}—gen
#ifndef __cplusplus
#define ${MAC}—gen(H, V)                                       \
_Generic((char(*)[sizeof((H)[0])]){0},                         \
         char(*)[sizeof(impl)]:                                \
         _Generic((H)[0].type,                                 \
                  void*       : ehdl∷${MAC},                   \
                  void const* : ihdl∷${MAC})                   \
         )(V)
#pragma CMOD intern ${MAC}2—gen
#define ${MAC}2—gen(H, V)                                      \
_Generic((char(*)[sizeof((H)[0])]){0},                         \
         char(*)[sizeof(impl[2])]:                             \
         _Generic((H)[0][0].type,                              \
                  void*       : ehdl∷${MAC}2,                  \
                  void const* : ihdl∷${MAC}2)                  \
         )(V)
#endif
#pragma CMOD done

#pragma CMOD foreach MAC = chain
#ifndef __cplusplus
/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC} and
 ** @doxy eilck::ihdl::${MAC}.
 **/
#define ${MAC}(S, L) _Generic((S)→type, void*: ehdl∷${MAC},  void const*: ihdl∷${MAC})((void*)(S), (void*)(L))
#endif
#pragma CMOD done

#pragma CMOD intern qeri
inline
int qeri(obj* ob, ihdl* h) {
  return h ? ihdl∷req(h, ob) : 0;
}

#pragma CMOD intern qere
inline
int qere(obj* ob, ehdl* h) {
  return h ? ehdl∷req(h, ob) : 0;
}

#pragma CMOD foreach MAC = req
#ifndef __cplusplus
/**
 ** @brief A generic interface to @doxy eilck::ehdl::${MAC} and
 ** @doxy eilck::ihdl::${MAC}.
 **/
#define ${MAC}(_0, _1)                                         \
_Generic((_0)→type,                                            \
         void*       : ehdl∷${MAC},                            \
         void const* : ihdl∷${MAC})                            \
  ((void*)(_0), (_1))
#endif
#pragma CMOD done

#pragma CMOD intern after
#pragma CMOD intern point


/**
 ** @brief mark a critical section that uses a specific handle @a H
 **/
#pragma CMOD import se    = eilck∷impl∷SECTION
#pragma CMOD fill se∷SECTION  = SECTION
#pragma CMOD fill se∷ACQ    = acq—gen
#pragma CMOD fill se∷REL    = rel—gen

/**
 ** @brief mark a critical section that uses a specific pair of
 ** handles @a H.
 **
 ** @a H should be a vector of length two of handles. These handles
 ** are used in alternation. A FIFO for an object is used cyclicly
 ** with this: whenever the section is left, a request for the other
 ** handle is appended to the FIFO of the object.
 **/
#pragma CMOD import se2   = eilck∷impl∷SECTION
#pragma CMOD fill se2∷SECTION = SECTION2
#pragma CMOD fill se2∷ACQ   = acq2—gen
#pragma CMOD fill se2∷REL   = rel2—gen

#pragma CMOD intern SEQUENCE—11
#pragma CMOD intern SEQUENCE—10

#define SEQUENCE—11(O, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9) \
do {                                                           \
  req((_0), (O));                                              \
  req((_1), (O));                                              \
  req((_2), (O));                                              \
  req((_3), (O));                                              \
  req((_4), (O));                                              \
  req((_5), (O));                                              \
  req((_6), (O));                                              \
  req((_7), (O));                                              \
  req((_8), (O));                                              \
  req((_9), (O));                                              \
 } while (0)

#define SEQUENCE—10(O, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, ...) \
  SEQUENCE—11(O, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9)

#define nil ((ehdl*)0)

/**
 ** @brief Insert lock requests for a sequence of handles into the
 ** FIFO of object @a O.
 **
 ** The handles are requested in-order. By that we can prescribe
 ** exactly the order in which the handles visit the object.
 **
 ** Generally this macro should be used in an initial phase, most
 ** likely in @c main, to schedule threads at different positions in
 ** the FIFO.
 **/
#define SEQUENCE(O, ...) SEQUENCE—10(O, __VA_ARGS__, nil, nil, nil, nil, nil, nil, nil, nil, nil,)

#pragma CMOD definition
#pragma CMOD entry entry

obj object = obj∷INITIALIZER(10);

int entry(void) {
  ehdl h0;
  ihdl h1;
  ehdl h2;
  SEQUENCE(&object, &h0, &h1, &h2);
  // a first CS with write access
  SECTION(&h0) {
    ehdl∷scale(&h0, sizeof(unsigned));
    unsigned* u = map(&h0);
    *u = 42;
  }
  // a second CS with read access
  SECTION(&h1) {
    unsigned const* v = map(&h1);
    C∷io∷printf("%u\n", *v);
  }
  SECTION(&h2) {
    ehdl∷scale(&h2, 0);
  }
  return 0;
}
