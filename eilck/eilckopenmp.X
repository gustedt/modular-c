// -*- C -*-
#pragma CMOD separator	∷
#pragma CMOD composer 	—
#pragma CMOD import	hdl	= eilck∷hdl
#pragma CMOD import	ehdl	= eilck∷ehdl
#pragma CMOD import	ihdl	= eilck∷ihdl
#pragma CMOD import	obj	= eilck∷obj
#pragma CMOD import	fixed	= eilck∷obj∷fixed
#pragma CMOD import	shm	= eilck∷obj∷segment
#pragma CMOD import	file	= eilck∷obj∷file
#pragma CMOD import	scale	= eilck∷obj∷scale
#pragma CMOD import	OS	= Linux
#pragma CMOD import	thrd	= OS∷thrd

#pragma CMOD declaration

enum en { readers = 10, iterations = 80, };

typedef ihdl ihdl10[(en)readers];

#pragma CMOD definition
#pragma CMOD entry main
#pragma CMOD atexit final

static _Atomic(unsigned) sum = 0;

static
int reader(void* arg) {
  /* C∷io∷printf("started a reader thread: %d/%d\n", */
  /*             omp∷lib∷get_thread_num(), omp∷lib∷get_num_threads()); */
  ihdl* h = arg;
  ihdl h2[2] = { 0 };
  hdl∷SECTION(h) {
    unsigned const* u = hdl∷map(h);
    sum += *u;
    hdl∷chain(h, h2);
  }
  for (unsigned it = 0; it < iterations; ++it) {
    hdl∷SECTION2(&h2) {
      unsigned const* u = hdl∷map2(&h2);
      sum += *u;
    }
  }
  hdl∷drop2(&h2);
  return 0;
}

static
int writer(void* arg) {
  /* C∷io∷printf("started a writer thread: %d/%d\n", */
  /*             omp∷lib∷get_thread_num(), omp∷lib∷get_num_threads()); */
  ehdl* h = arg;
  ehdl h2[2] = { 0 };
  hdl∷SECTION(h) {
    unsigned* u = hdl∷map(h);
    ++*u;
    hdl∷chain(h, h2);
  }
  for (unsigned it = 0; it < iterations; ++it) {
    hdl∷SECTION2(&h2) {
      unsigned* u = hdl∷map2(&h2);
      ++*u;
    }
  }
  hdl∷drop2(&h2);
  return 0;
}

static obj object = obj∷INITIALIZER(eilck∷len);
static obj object—fixed = fixed∷INITIALIZER(eilck∷len, (unsigned[1]){ 0 });
static obj* ob = &object;

static ehdl* eh;
static ihdl10* ih;

void final(void){
  // a last CS with exclusive access
  ehdl last = { 0 };
  hdl∷req(&last, ob);
  if (hdl∷test(&last)) {
    unsigned* u = hdl∷map(&last);
    C∷io∷printf("thrd_exit we had %u writers, sum is %u, length is %zu bytes\n",
                *u, sum, hdl∷length(&last));
  } else {
    C∷io∷printf(C∷io∷err, "emergency thrd_exit, sum is %u, not all handles have been released\n",
                sum);
  }
  C∷lib∷free(eh);
  C∷lib∷free(ih);
}

int main(int argc, char* argv[argc+1]) {
  unsigned writers = 10;
  char const* namexcl = nullptr;
  char const* namincl = nullptr;
  char const* namcopy = nullptr;
  eilck∷error = C∷io∷err;
  /*
   * Command line arguments can be:
   * [ Nb of writers [ exclusive file or segment  [ inclusive file or segment [ final name of object ] ] ] ]
   */
  if (argc > 1) writers = C∷str∷toull(argv[1], 0, 0);
  if (argc > 2) namexcl = argv[2];
  if (argc > 3) namincl = argv[3];
  if (argc > 4) namcopy = argv[4];
  eh = C∷lib∷malloc(sizeof(ehdl[writers]));
  ih = C∷lib∷malloc(sizeof(ihdl10[writers]));
  ehdl first = { 0 };
  if (namexcl) {
    void* ret = file∷create(ob, namexcl);
    if (ret ≡ eilck∷FAILED) {
      C∷io∷printf(C∷io∷err, "main: creating object, %s failed.\n", namexcl);
      namexcl = nullptr;
      if (namincl) {
        void* ret = shm∷open(ob, namincl);
        if (ret ≡ eilck∷FAILED) {
          C∷io∷printf(C∷io∷err, "main: opening object, %s failed.\n", namincl);
          return C∷lib∷FAILURE;
        }
      }
    }
  } else {
    C∷io∷printf("using static object\n");
    ob = &object—fixed;
  }
  hdl∷req(&first, ob);

  for (unsigned i = 0; i < writers; ++i) {
    hdl∷req(&eh[i], ob);
    for (unsigned j = 0; j < readers; ++j) {
      hdl∷req(&ih[i][j], ob);
    }
  }

  C∷io∷printf("main: inserted all requests\n");
  omp∷PARALLELIZE(writers + readers*writers + 1)
    {
      omp∷JOBS()
        for (unsigned i = 0; i < writers; ++i)
          writer(&eh[i]);

      omp∷JOB
        C∷io∷printf("main: started all writer threads\n");

      omp∷JOBS(2)
        for (unsigned i = 0; i < writers; ++i)
          for (unsigned j = 0; j < readers; ++j)
            reader(&ih[i][j]);

      omp∷JOB
        C∷io∷printf("main: started all reader threads\n");

      // This job will only be launched, once the printf "jobs" have
      // terminated.
      omp∷JOB {
        // a first CS with write access
        hdl∷SECTION(&first) {
          unsigned* u = ehdl∷scale(&first, sizeof(unsigned));
          if (¬u) {
            C∷io∷printf(C∷io∷err, "main: could not scale object, hope for the best.\n");
          } else {
            C∷io∷printf(C∷io∷err, "main: address after scaling %p\n", u);
          }
          u = hdl∷map(&first);
          if (¬u) {
            C∷io∷printf(C∷io∷err, "main: could not map object, exiting.\n");
            C∷io∷flush(nullptr);
            C∷lib∷exit(C∷lib∷FAILURE);
          } else {
            C∷io∷printf(C∷io∷err, "main: address after mapping %p\n", u);
          }
          *u = 0;
          if (namexcl) ehdl∷remove(&first);
          if (namcopy) ehdl∷create(&first, (char*)namcopy);
          C∷io∷printf("main: unleached threads, exiting\n");
        }
      }
    }
  return 0;
}
