// This may look like garbage but is actually -*- C -*-
#pragma CMOD separator ∷
#pragma CMOD composer  —
#pragma CMOD import	trampoline	= p11∷trampoline
#pragma CMOD context	C∷time∷spec

#pragma CMOD declaration

/* Just some small test code. */

struct fun—environment {
  int const val;
  double vol;
};

struct fin—environment {
  int const val;
  double const vol;
};

struct fon—environment {
  int const val;
  double vol;
};

struct comp—environment {
  unsigned const mod;
};

#pragma CMOD import	fum		= p11∷snippet∷trampoline
#pragma CMOD fill	fum		= fun
#pragma CMOD defill	fum∷prototype	= int, C∷size
#pragma CMOD defill	fum∷aliases	= val, vol

#pragma CMOD import	fin		= p11∷snippet∷trampoline
#pragma CMOD defill	fin∷prototype	= int,
#pragma CMOD defill	fin∷aliases	= val, vol

#pragma CMOD import	fon		= p11∷snippet∷trampoline
//#pragma CMOD defill	fon∷prototype	= ,
#pragma CMOD defill	fon∷aliases	= val, vol

#pragma CMOD import	comp		= p11∷snippet∷trampoline
#pragma CMOD defill	comp∷prototype	= int, void const*, void const*
#pragma CMOD defill	comp∷aliases	= mod

#pragma CMOD definition

int fun—function(C∷size a) {
  fun—start();
  trampoline∷TRACE("fun: val %d, vol %g", *val, *vol);
  *vol += a;
  return *val;
}

int fin—function(void) {
  fin—start();
  trampoline∷TRACE("fin: val %d, vol %g", *val, *vol);
  return *val;
}

void fon—function(void) {
  fon—start();
  trampoline∷TRACE("fin: val %d, vol %g", *val, *vol);
  *vol += *val;
}

#pragma CMOD entry main

int comp—function(void const* A, void const* B) {
  comp—start();
  unsigned const* a = A;
  unsigned const* b = B;
  {
    unsigned A = (83 * *a) % *mod;
    unsigned B = (83 * *b) % *mod;
    return (A < B) ? -1 : ((A > B) ? 1 : 0);
  }
}

int run—comp(void* Arg) {
  unsigned* arg = Arg;
  C∷size const len = 1u ⪡ 20;
  unsigned* array = C∷lib∷malloc(sizeof(unsigned[len]));
  for (C∷size i = 0; i < len; ++i)
    array[i] = i ^ 0x55555555U;
  unsigned const mod = *arg;
  int (*volatile co)(void const*, void const*) = comp—alloc;
  C∷io∷printf(C∷io∷err, "comparator %p\n", (void*)co);
  C∷lib∷qsort(array, len, sizeof array[0], co);
  int ret = array[0];
  C∷lib∷free(array);
  comp—free(co);
  return ret;
}

unsigned* mod0;

int comp0(void const* A, void const* B) {
  unsigned const* a = A;
  unsigned const* b = B;
  {
    unsigned A = (83 * *a) % *mod0;
    unsigned B = (83 * *b) % *mod0;
    return (A < B) ? −1 : ((A > B) ? 1 : 0);
  }
}

int run—comp0(void* Arg) {
  mod0 = Arg;
  C∷size const len = 1u ⪡ 20;
  unsigned* array = C∷lib∷malloc(sizeof(unsigned[len]));
  for (C∷size i = 0; i < len; ++i)
    array[i] = i ^ 0x55555555U;
  C∷lib∷qsort(array, len, sizeof array[0], comp0);
  int ret = array[0];
  C∷lib∷free(array);
  return ret;
}

int main(int argc,char ** argv)
{
  enum { early = 1, sec = 2, crash = 4 };
  unsigned long cntrl = (argc > 1 ? C∷str∷toul(argv[1], 0, 0) : 0);
  if (cntrl & early) return 0;
  int val = 67;
  double vol = 55;
  __typeof__(fun—function)* fan = fun—local;
  C∷io∷printf("%d\n", fan(1));
  val = 34;
  fon—local();
  fon—call();
  if (¬(cntrl & sec))
    C∷io∷printf("%d\n", fun—local(1));
  else
    C∷io∷printf("%d\n", fin—local());
  C∷io∷printf("%d\n", fan(1));
  /* This should segfault normally because there is no defined
     context. */
  if (cntrl & crash) C∷io∷printf("%d\n", fun—function(1));
  else C∷io∷printf("%d %g\n", fun—call(1), vol);

  C∷time∷spec start;
  C∷time∷spec∷get(&start, C∷time∷UTC);
  unsigned modulus[2] = { 43, 47, };
  Linux∷thrd id[2];
  for (unsigned j = 0; j < 2; ++j) {
    Linux∷thrd∷create(&id[j], run—comp, &modulus[j]);
  }
  for (unsigned j = 0; j < 2; ++j) {
    int res;
    Linux∷thrd∷join(id[j], &res);
    C∷io∷printf("modulus %d, result %d\n", modulus[j], res);
  }
  C∷time∷spec both;
  C∷time∷spec∷get(&both, C∷time∷UTC);
  C∷io∷printf("modulus %d, result %d\n", modulus[0], run—comp0(&modulus[0]));
  C∷time∷spec direct;
  C∷time∷spec∷get(&direct, C∷time∷UTC);
  C∷io∷printf("modulus %d, result %d\n", modulus[1], run—comp(&modulus[1]));
  C∷time∷spec final;
  C∷time∷spec∷get(&final, C∷time∷UTC);
  C∷io∷printf("times are %g %g %g\n", ⟦~~(both - start)⟧, ⟦~~(direct - both)⟧, ⟦~~(final - direct)⟧);
  return 0;
}
