// This may look like garbage but is actually -*- C -*-
#pragma CMOD separator ¯
#pragma CMOD module		  p11¯tmpl¯lifo
#pragma CMOD import tp		= p11¯tp
#pragma CMOD import lib 	= C¯lib


#pragma CMOD snippet none
#pragma CMOD slot T	= complete
#pragma CMOD slot head	= typedef
#pragma CMOD slot next	= none

#pragma CMOD declaration

struct head {
  tp p;
};

#define INITIALIZER(VAL) { .p = tp¯INITIALIZER(VAL), }

inline
head* init(head* L, T* val) {
  if (L) {
    tp¯init(&L->p, val);
  }
  return L;
}

/**
 ** @brief Return a pointer to the top element of an atomic lifo @a L
 ** @see clear
 ** @see pop
 ** @see push
 **/
inline
T* top(head* L) {
  return tp¯load(&L->p);
}


/**
 ** @brief Push element @a el into an atomic lifo @a L
 ** @see clear
 ** @see pop
 ** @see top
 **/
inline
void push(head* L, T* el){
  tp state;
  tp¯store(&state, &L->p);
  do {
    el->next = tp¯load(&state);
  } while(¬tp¯compare_exchange_tp_vp(&L->p, &state, el));
}

/**
 ** @brief Pop the top element from an atomic lifo @a L
 **
 ** This implements a generic interface to an atomic lifo (Last In -
 ** First Out) data structure. To use it you just have do some
 ** preparatory declarations and add a field that holds the @c next in
 ** the list:
 **
 ** @code
 ** #pragma CMOD module data proj¯data
 ** #pragma CMOD import lib	= C¯lib
 ** #pragma CMOD import lifo	= p11¯tmpl¯lifo
 ** #pragma CMOD fill lifo¯T	= myData
 ** #pragma CMOD fill lifo¯head	= myDataHead
 ** #pragma CMOD fill lifo¯next	= myDataNext
 **
 ** #pragma CMOD declaration
 **
 ** struct myData {
 **   ...
 **   myData* forward;
 **   ...
 ** };
 **
 ** #define myDataNext forward
 **
 ** #pragma CMOD definition;
 **
 ** myDataHead head;
 ** @endcode
 **
 ** Now @c head can be used as the head of a lifo:
 **
 ** @code
 ** myData* el = init(lib¯malloc(sizeof(myData), \/\* your initializer arguments \*\/);
 ** push(&head, el);
 ** ...
 ** for (myData* el = pop(&head);
 **      el;
 **      el = pop(&head)) {
 **        // do something with el and then
 **        lib¯free(el);
 ** }
 ** @endcode
 **
 ** @warning This function may speculatively load a component of an
 ** already popped and then freed element. We suppose that such a load
 ** is always possible, i.e we suppose that the elements are not
 ** unmapped while we are working on the list. The information there
 ** is only used, in case the element is still valid.
 **
 ** @see clear
 ** @see lifo
 ** @see push
 **/

inline
T* pop(head* L){
  tp state;
  tp¯store(&state, &L->p);
  T* ret;
  T* nex;
  do {
    ret = tp¯load(&state);
    if (¬ret) return nullptr;
    /* This load is speculative and supposes that ret is always a
       valid address, even if the object was freed by someone else in
       the mean time. If so, that is if someone was quicker to pop
       this element from the list, the following cmpxchg will fail and
       the supposedly bogus value in "nex" will never be used. */
    nex = ret->next;
  } while(¬tp¯compare_exchange(&L->p, &state, nex));
  /* Delete all possible references to elements. */
  ret->next = nullptr;
  return ret;
}

inline
T* revert(T* el){
  T* h = el;
  T* t = nullptr;
  while (h) {
    T* n = h->next;
    h->next = t;
    t = h;
    h = n;
  }
  return t;
}

/**
 ** @brief Atomically clear an atomic lifo @a L and return a pointer
 ** to the start of the list that it previously contained
 **
 ** @see pop
 ** @see push
 ** @see top
 **/
inline
T* clear(head* L){
  tp state = tp¯INITIALIZER(0);
  while (¬tp¯compare_exchange(&L->p, &state, nullptr)) {
    /* busy loop */
  }
  return tp¯load(&state);
}

inline
T** tabulate(head* L) {
  C¯size cnt = 0;
  T* head = clear(L);
  for (T* e = head; e; e = e->next)
    ++cnt;
  T** ret = lib¯malloc(sizeof(T*[cnt+1]));
  for (T **t = ret, *e = head; e; e = e->next, ++t)
    *t = e;
  ret[cnt] = 0;
  return ret;
}

#pragma CMOD definition

void test(void) {
  T *e =  &(T){ 0 };
  e->next = e;
  //_Static_assert(sizeof((T){ .next = (T*)0, }), "lifo needs next pointer");
}
