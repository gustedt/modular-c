// This may look like garbage but is actually -*- C -*-
#pragma CMOD separator	∷
#pragma CMOD composer	—

/**
 ** @module
 ** @brief Add a separating line to the pretty print of the help text
 **
 **/

#pragma CMOD declaration

#define HEAD1 0
#define HEAD2 0
#define HEAD3 0

#pragma CMOD snippet	none
#pragma CMOD slot	HEAD1	= none
#pragma CMOD slot	HEAD2	= none
#pragma CMOD slot	HEAD3	= none
#pragma CMOD slot	local	= intern
#pragma CMOD slot	startup = init

void startup(void) {
  static opt local = {
    .t = HEAD1,
    .n = HEAD2,
    .d = HEAD3,
  };
  p11∷opt∷insert(&local);
}
