// This may look like garbage but is actually -*- C -*-
#pragma CMOD separator	∷
#pragma CMOD composer	—

/**
 ** @brief Create a variable @c NAME of type @c T and link it to
 ** command line option @c CHAR, long option @c ALIAS or environment
 ** variable @c ENV during initialization.
 **
 ** @c NAME is the name of a global object that created and
 ** initialized by this option. The interpretation of the option
 ** string depends on the type @c T (@c bool if omitted).
 **
 ** @c INIT is the static initializer for @c NAME. If omitted the
 ** generic initializer is used. This is the value of the variable if
 ** no command line option for this variable is given when invoking
 ** the program.
 **
 ** @c CHAR is the short option character that is used together with a
 ** @c - prefix. If @c 0 or omitted, no short option triggers this
 ** initialization.
 **
 ** @c ALIAS is an alternate long name for the option that is used
 ** with a @c -- prefix.  If @c 0 or omitted, no long option triggers
 ** this initialization. As long as they are unique, long options can
 ** be abbreviated.
 **
 ** @c ENV, if given, is an environment name for the option.
 **
 ** @remark One of @c CHAR, @c ALIAS or @c ENV must be given.
 **
 ** @c DOC is a documentation string, usually just one line.
 **/

#pragma CMOD declaration

#define INIT	{ 0 }
#define DOC	0
#define CHAR	0
#define ALIAS	0
#define ENV	0
#define T	bool

#pragma CMOD snippet	none
#pragma CMOD slot	T	= none
#pragma CMOD slot	NAME	= none
#pragma CMOD slot	INIT	= none
#pragma CMOD slot	CHAR	= none
#pragma CMOD slot	DOC	= none
#pragma CMOD slot	ALIAS	= none
#pragma CMOD slot	ENV	= none
#pragma CMOD slot	startup	= init

T NAME = INIT;

void startup(void) {
  _Static_assert(CHAR ∨ ALIAS ∨ ENV, C∷STRINGIFY(NAME) ": must give at least an option character, a long option or an environment variable");
  static opt el = {
    .c = CHAR,
    .l = p11∷opt∷LOCALE(NAME),
    .o = &NAME,
    .f = p11∷opt∷PROCESS(NAME),
    .p = p11∷opt∷SNPRINTF(NAME),
    .t = p11∷opt∷TYPE(NAME),
    .a = ALIAS,
    .e = ENV,
    .d = DOC,
    .n = C∷STRINGIFY(NAME),
  };
  p11∷opt∷insert(&el);
  if (ENV) p11∷opt∷env(&el);
}
