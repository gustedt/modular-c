// This may look like garbage but is actually -*- C -*-
#pragma CMOD module p11¯ufVoid
//#pragma CMOD import uf		= p11¯uf
#pragma CMOD entry test

/**
 ** @file
 **
 ** @brief Example usage of the uf2D snippet.
 **/

#pragma CMOD declaration

/**
 ** @brief A trivial oracle that doesn't use the data pointer @a d
 **
 ** This just fuses sets along thick diagonals.
 **/
inline
bool oracle(p11¯uf* UF, void* d, C¯size rows, C¯size columns, C¯size x0, C¯size y0, C¯size x1, C¯size y1) {
  return
    (y1 % 3) ≠ (x1 % 5)
    ? ((x0 ≡ x1-1) ∧ (y0 ≡ y1))
    : ((x0 ≡ x1) ∧ (y0 ≡ y1-1));
}

#pragma CMOD import uf2D	= p11¯snippet¯uf2D
#pragma CMOD fill uf2D		= p11¯ufVoid
#pragma CMOD defill uf2D¯Data	= void
#pragma CMOD fill uf2D¯oracle	= oracle

#pragma CMOD definition

int test(int argc, char* argv[argc+1]) {
  C¯size rows = argc < 2 ? 20 : C¯str¯toull(argv[1], 0, 0);
  C¯size columns = argc < 3 ? 16 : C¯str¯toull(argv[2], 0, 0);
  p11¯uf* UF = p11¯uf¯alloc(rows*columns);
  if (0) lineByLine(UF, 0, rows, columns);
  else divide(UF, 0, rows, columns);
  p11¯uf¯printBoard(rows, columns, UF);
  p11¯uf¯flatten(UF);
  p11¯uf¯printBoard(rows, columns, UF, true);
  C¯io¯printf("size of the diagonal %zu\n", p11¯uf¯size(UF, 0));
  p11¯uf¯free(UF);
  return C¯lib¯SUCCESS;
}
