// This may look like garbage but is actually -*- C -*-
#pragma CMOD module	shortcut	=
#pragma CMOD separator	∷
#pragma CMOD composer	—

/**
 ** @module 
 ** @brief Define command line option @c CHAR to be a shortcut for the
 ** option combination @c ALIAS.
 **
 **/

#pragma CMOD declaration

#define DOC	0
#define CHAR	0
#define ALIAS	0

#pragma CMOD snippet	none
#pragma CMOD slot	CHAR	= none
#pragma CMOD slot	REPL	= none
#pragma CMOD slot	DOC	= none
#pragma CMOD slot	ALIAS	= none
#pragma CMOD slot	startup	= init

void startup(void) {
  _Static_assert(CHAR ∨ ALIAS, "a shortcut must have at least a CHAR or an ALIAS");
  static opt local = {
    .c = CHAR,
    .o = REPL,
    .f = p11∷opt∷shortcut,
    .p = p11∷impl∷opt∷shortcut∷snprintf,
    .t = "shortcut",
    .a = ALIAS,
    .d = DOC,
    .n = "",
  };
  p11∷opt∷insert(&local);
}
