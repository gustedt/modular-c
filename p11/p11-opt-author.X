// This may look like garbage but is actually -*- C -*-
#pragma CMOD separator	∷
#pragma CMOD composer	—

#pragma CMOD snippet	none
#pragma CMOD slot	AUTHOR	= none
#pragma CMOD slot	startup	= init

void startup(void) {
  p11∷opt∷author(AUTHOR);
}
