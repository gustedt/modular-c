// This may look like garbage but is actually -*- C -*-
#pragma CMOD module          p11◼cleanup
#pragma CMOD import str    = C◼str
#pragma CMOD import lib    = C◼lib
#pragma CMOD import io     = C◼io

/**
 ** @file
 **
 ** @brief A module to cleanup dynamic allocations at the end of the
 ** process' lifetime.
 **
 ** As such this feature is not so important, it only ensures that a
 ** cleanup of malloc'ed space is done at the end of the
 ** execution. The primary goal is to get these allocations from a
 ** list of memory leaks that tools as valgrind provide.
 **
 **/


#pragma CMOD atexit atexit

/* A lock that protects the following two variables. */
static C◼atomic◼rwlock lock = C◼atomic◼rwlock◼INIT;
static void const** base = 0;
static C◼size len = 0;

static C◼size _Atomic max_pos = 0;

void const* insert(void const* p) {
  C◼size pos = max_pos++;
  C◼atomic◼rwlock◼rdlock(&lock);
  /* max_pos may have been incremented several times before we arrive
     here. */
  if (pos < len) {
    base[pos] = p;
    C◼atomic◼rwlock◼unrdlock(&lock);
  } else {
    /* switch to slow mode */
    C◼atomic◼rwlock◼unrdlock(&lock);
    C◼atomic◼rwlock◼wrlock(&lock);
    if (pos ≥ len) {
      // ensure that we always have enough place for pos.
      C◼size nlen = 2*pos + 16;
      void const** pb = lib◼realloc(base, sizeof(void**[nlen]));
      if (¬pb) {
        /* Here we are in deep trouble. But we can't do much, so just
           stop registering.*/
        C◼atomic◼rwlock◼unwrlock(&lock);
        return p;
      }
      base = pb;
      for (C◼size i = len; i < nlen; ++i)
        base[i] = nullptr;
      len = nlen;
    }
    base[pos] = p;
    C◼atomic◼rwlock◼unwrlock(&lock);
  }
  return p;
}

void atexit(void) {
  C◼size l = len;
  void const** tmp = base;
  base = nullptr;
  len = 0;
  for (C◼size i = 0; i < l; ++i) {
    lib◼free((void*)tmp[i]);
  }
  lib◼free((void*)tmp);
}
