// This may look like garbage but is actually -*- C -*-
#pragma CMOD module   big2	= p11¯big2
#pragma CMOD composer —
#pragma CMOD context  (: :)

#pragma CMOD import big = p11¯snippet¯big
#pragma CMOD fill big¯T1 = p11¯big1
#pragma CMOD fill big¯T2 = p11¯big2
#pragma CMOD fill big¯INITIALIZER1 = p11¯big1¯INITIALIZER
#pragma CMOD fill big¯INITIALIZER2 = INITIALIZER
#pragma CMOD fill big¯WIDTH1 = p11¯big1¯WIDTH
#pragma CMOD fill big¯WIDTH2 = WIDTH
#pragma CMOD fill big¯calc1 = p11¯big1
#pragma CMOD fill big¯calc2 = p11¯big2
#pragma CMOD fill big¯print1 = p11¯big1¯print
#pragma CMOD fill big¯print2 = print


#pragma CMOD declaration

#pragma CMOD entry test


inline
int test(int argc, char* argv[argc+1]) {
  big2 args[(argc+1)/2];
  C¯io¯printf("%d arguments, %s %s\n", argc, argv[0], argv[1]);
  int ret = 0;
  for (C¯size i = 0; i+2 < argc; i += 2) {
    C¯io¯fputs("0x", C¯io¯out);
    ret += 2;
    args[i/2] = (big2){ .x = {
        [0] = p11¯big1¯INITIALIZER(C¯str¯toull(argv[i+1], 0, 0)),
        [1] = p11¯big1¯INITIALIZER((i+2<argc) ? C¯str¯toull(argv[i+2], 0, 0) : 0ULL),
      },
    };
    ret += print(C¯io¯out, "0x", args[i/2]);
    C¯io¯fputs("\n", C¯io¯out);
  }
  C¯io¯printf("printed %d characters\n", ret);
  ret = 0;
  for (C¯size i = 0; i < argc/2-1; ++i) {
    C¯size i1 = i+1;
    C¯io¯fputs("0x", C¯io¯out);
    ret += 2;
    ret += print(C¯io¯out, "0x", (: args[i] * args[i1] :) );
    C¯io¯fputs("\n", C¯io¯out);
  }
  C¯io¯printf("printed %d characters\n", ret);
  for (C¯size i = 0; i < argc/2-1; ++i) {
    C¯size i1 = i+1;
    C¯io¯fputs("0x", C¯io¯out);
    ret += 2;
    ret += print(C¯io¯out, "0x", (: args[i] - args[i1] :) );
    C¯io¯fputs("\n", C¯io¯out);
  }
  C¯io¯printf("printed %d characters\n", ret);
  print(C¯io¯out, "0x", (big2)INITIALIZER(-1) );
  C¯io¯fputs("\n", C¯io¯out);
  print(C¯io¯out, "0x", (big2)INITIALIZER(0) );
  C¯io¯fputs("\n", C¯io¯out);
  print(C¯io¯out, "0x", (big2)INITIALIZER(1) );
  C¯io¯fputs("\n", C¯io¯out);
  print(C¯io¯out, "0x", (big2)INITIALIZER(C¯integer¯UINTMAX_MAX) );
  C¯io¯fputs("\n", C¯io¯out);
  print(C¯io¯out, "0x", (big2)INITIALIZER(-1) );
  C¯io¯fputs("\n", C¯io¯out);
  print(C¯io¯out, "0x", (big2)INITIALIZER(0) );
  C¯io¯fputs("\n", C¯io¯out);
  print(C¯io¯out, "0x", (big2)INITIALIZER(1) );
  C¯io¯fputs("\n", C¯io¯out);
  return 0;
}
