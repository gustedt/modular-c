// This may look like garbage but is actually -*- C -*-
#pragma CMOD module 		p11◼str
#pragma CMOD import str		= C◼str
#pragma CMOD import mem		= C◼mem
#pragma CMOD import malloc	= C◼lib◼malloc
#pragma CMOD import vsnprintf	= C◼io◼vsnprintf
#pragma CMOD import va		= C◼va
#pragma CMOD import cleanup	= p11◼cleanup

#pragma CMOD import STRTOLL   = C◼DEFARG
#pragma CMOD fill STRTOLL◼INNER= C◼str◼toll
#pragma CMOD fill STRTOLL◼OUTER= toll
#pragma CMOD fill STRTOLL◼LIST= p11◼strtoll◼INT_DEF

#pragma CMOD import STRTOL   = C◼DEFARG
#pragma CMOD fill STRTOL◼INNER= C◼str◼tol
#pragma CMOD fill STRTOL◼OUTER= tol
#pragma CMOD fill STRTOL◼LIST= p11◼strtoll◼INT_DEF

#pragma CMOD import STRTOULL   = C◼DEFARG
#pragma CMOD fill STRTOULL◼INNER= C◼str◼toull
#pragma CMOD fill STRTOULL◼OUTER= toull
#pragma CMOD fill STRTOULL◼LIST= p11◼str◼INT_DEF

#pragma CMOD import STRTOUL   = C◼DEFARG
#pragma CMOD fill STRTOUL◼INNER= C◼str◼toul
#pragma CMOD fill STRTOUL◼OUTER= toul
#pragma CMOD fill STRTOUL◼LIST= p11◼str◼INT_DEF

#pragma CMOD import STRTOUMAX   = C◼DEFARG
#pragma CMOD fill STRTOUMAX◼INNER= C◼str◼toumax
#pragma CMOD fill STRTOUMAX◼OUTER= toumax
#pragma CMOD fill STRTOUMAX◼LIST= p11◼str◼INT_DEF

#pragma CMOD import STRTOIMAX   = C◼DEFARG
#pragma CMOD fill STRTOIMAX◼INNER= C◼str◼toimax
#pragma CMOD fill STRTOIMAX◼OUTER= toimax
#pragma CMOD fill STRTOIMAX◼LIST= p11◼str◼INT_DEF

#pragma CMOD import STRTOLD   = C◼DEFARG
#pragma CMOD fill STRTOLD◼INNER= C◼str◼told
#pragma CMOD fill STRTOLD◼OUTER= told
#pragma CMOD fill STRTOLD◼LIST= p11◼str◼FLOAT_DEF

#pragma CMOD import STRTOD   = C◼DEFARG
#pragma CMOD fill STRTOD◼INNER= C◼str◼tod
#pragma CMOD fill STRTOD◼OUTER= tod
#pragma CMOD fill STRTOD◼LIST= p11◼str◼FLOAT_DEF

#pragma CMOD import STRTOF   = C◼DEFARG
#pragma CMOD fill STRTOF◼INNER= C◼str◼tof
#pragma CMOD fill STRTOF◼OUTER= tof
#pragma CMOD fill STRTOF◼LIST= p11◼str◼FLOAT_DEF

#pragma CMOD declaration

#define INT_DEF , 0, 0
#define FLOAT_DEF , 0

inline
char* dup(char const* s) {
  C◼size len = str◼len(s);
  char* ret = malloc(len+1);
  return ret ? mem◼cpy(ret, s, len+1) : nullptr;
}


#pragma CMOD definition

unsigned long long test0(void) {
  return toull("0890", 0, 2);
  return toull("0890",  , 3);
  return toull("0890", 0,  );
  return toull("0890", 0   );
  return toull("0890"      );
  return 0;
}

/**
 ** @brief Similar to a call to C◼lib◼vsnprintf, but returns the
 ** string to which the arguments are printed.
 **
 ** If one of @a size or @a s is @c 0, a suitable string is allocated
 ** with C◼lib◼malloc and returned.
 **/
char* vformat(C◼size size, char s[size], const char *form, va ap) {
  /* determine the size and allocate a buffer if necessary */
  if (¬size ∨ ¬s) {
    va bp;
    va◼copy(bp, ap);
    size = vsnprintf(0, 0, form, bp) + 1;
    s = malloc(size);
    va◼end(bp);
  }
  if (s) {
    vsnprintf(s, size, form, ap);
  }
  return s;
}

/**
 ** @brief Similar to a call to C◼lib◼snprintf, but returns the string
 ** to which the arguments are printed.
 **
 ** If one of @a size or @a s is @c 0, nothing is done an @c 0 is
 ** returned.
 **/
char* sformat(C◼size size, char s[size], const char *form, ...) {
  if (¬size ∨ ¬s) return nullptr;
  va ap;
  va◼start(ap, form);
  s = vformat(size, s, form, ap);
  va◼end(ap);
  return s;
}

/**
 ** @brief Similar to a call to C◼lib◼snprintf, but returns the string
 ** to which the arguments are printed.
 **
 ** The string that is returned is freshly allocated and should be
 ** freed with C◼lib◼free.
 **/
char* aformat(const char *form, ...) {
  char* s;
  va ap;
  va◼start(ap, form);
  s = vformat(0, 0, form, ap);
  va◼end(ap);
  return s;
}

/**
 ** @brief Similar to a call to C◼lib◼snprintf, but returns the string
 ** to which the arguments are printed.
 **
 ** The string that is returned is uniquely allocated. It should be
 ** considered immutable and persistent for the rest of the program
 ** execution.
 **
 ** @warning The returned string should @b not be freed with
 ** C◼lib◼free.
 **/
char const* cformat(const char *form, ...) {
  char* s;
  va ap;
  va◼start(ap, form);
  s = vformat(0, 0, form, ap);
  va◼end(ap);
  cleanup◼insert(s);
  return s;
}
