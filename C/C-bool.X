/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module B     = C◼bool

/**
 ** @module Features of the @c bool type.
 **/

#pragma CMOD declaration
#pragma CMOD mimic <stddef.h>
#define MIN 0
#define WIDTH 1
#pragma CMOD defexp RANK=CMOD_RANK((_Bool)+0)
#pragma CMOD defexp SIZE=sizeof(_Bool)

/**
 ** @brief this is just a dummy type such that we may produce the
 ** functions for the special type @c bool.
 **/

enum here {
  B◼false, B◼true,
};

#pragma CMOD defill ENUM◼LIST = B◼false, B◼true
#pragma CMOD fill   ENUM  = C◼bool

typedef bool C◼bool;

#pragma CMOD import ENUM  = C◼snippet◼ENUM

// FIXME: superflouous
#pragma CMOD declaration

C◼attr◼pure
inline
char const* get(bool x) {
  return getshort(x);
}

#pragma CMOD import C◼snippet◼equal
