/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module tz  = C∷time∷cal∷tz

#pragma CMOD init init

static int volatile tz_;
static C∷time∷cal volatile epoch_;

#pragma CMOD alias tz_    = C∷time∷cal∷tz
#pragma CMOD alias epoch_ = C∷time∷cal∷tz∷epoch

/**
 ** @brief The time zone difference in minutes from UTC.
 **
 ** This value is measured at application startup, and remains
 ** constant until exit.
 **/
extern int const tz;

/**
 ** @brief The starting point for time computations.
 **
 ** This value is measured at application startup, and remains
 ** constant until exit.
 **/
extern C∷time∷cal const epoch;

void init(void) {
  epoch_ = *C∷time∷local(&(C∷time) { 0 });
  C∷time∷cal gm = *C∷time∷gm(&C∷time∷startup);
  C∷time∷cal lo = *C∷time∷local(&C∷time∷startup);
  int tz = (gm.tm_hour*60 + gm.tm_min) - (lo.tm_hour*60 + lo.tm_min);
  if (tz > 720) {
    tz -= 1440;
  }
  if (tz < -720) {
    tz += 1440;
  }
  tz_ = tz;
}
