/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module pthread   =


/**
 ** @module An interface for thread functions
 ** @copydoc C::interface::pthread
 **/

#pragma CMOD fill imp   = pthread

#pragma CMOD declaration
#pragma CMOD defill imp∷TYPES = void*, void*
#pragma CMOD definition

#pragma CMOD import imp   = C∷snippet∷prototype

/**
 ** @typedef C::interface::pthread
 **
 ** @brief A function that can typically used in a separate POSIX
 ** thread.
 **
 ** It receives a <code>void*</code> for a generic argument. Caller
 ** (*invoker*) and callee must agree upon the interpretation of that
 ** argument, all type checking is off.
 **
 ** The return value is, again, a <code>void*</code>. Interpretation
 ** an possible allocation for such a returned pointer must be handled
 ** with care.
 **/

#pragma CMOD snippet none
#pragma CMOD slot   func    = none
#pragma CMOD slot RETTYPE   = global
#pragma CMOD slot PARAMTYPES  = global

/**
 ** @module
 ** @copydoc C::interface::pthread
 **/

#pragma CMOD definition

/**
 ** @copydoc C::interface::pthread
 **/
inline RETTYPE func(PARAMTYPES);
