/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module
#pragma CMOD separator ◼

#pragma CMOD alias cat  = wcscat
#pragma CMOD alias chr  = wcschr
#pragma CMOD alias cmp  = wcscmp
#pragma CMOD alias coll = wcscoll
#pragma CMOD alias cpy  = wcscpy
#pragma CMOD alias cspn = wcscspn
#pragma CMOD alias len  = wcslen
#pragma CMOD alias ncat = wcsncat
#pragma CMOD alias ncmp = wcsncmp
#pragma CMOD alias ncpy = wcsncpy
#pragma CMOD alias pbrk = wcspbrk
#pragma CMOD alias rchr = wcsrchr
#pragma CMOD alias spn  = wcsspn
#pragma CMOD alias str  = wcsstr
#pragma CMOD alias tod  = wcstod
#pragma CMOD alias tof  = wcstof
#pragma CMOD alias toimax = wcstoimax
#pragma CMOD alias tok  = wcstok
#pragma CMOD alias tol  = wcstol
#pragma CMOD alias told = wcstold
#pragma CMOD alias toll = wcstoll
#pragma CMOD alias toul = wcstoul
#pragma CMOD alias toull  = wcstoull
#pragma CMOD alias toumax = wcstoumax
#pragma CMOD alias xfrm = wcsxfrm
#pragma CMOD alias tob  = wctob
#pragma CMOD alias tomb = wctomb
#pragma CMOD alias tombs  = wcstombs

#pragma CMOD declaration

double tod(C◼wchar const[restrict 1], C◼wchar**restrict);
float tof(C◼wchar const[restrict 1], C◼wchar**restrict);
long double told(C◼wchar const[restrict 1], C◼wchar**restrict);
long int tol(C◼wchar const[restrict 1], C◼wchar ** restrict, int);
long long int toll(C◼wchar const[restrict 1], C◼wchar ** restrict, int);
C◼intmax toimax(C◼wchar const[restrict 1], C◼wchar ** restrict, int);
unsigned long int toul(C◼wchar const[restrict 1], C◼wchar ** restrict, int);
unsigned long long int toull(C◼wchar const[restrict 1], C◼wchar ** restrict, int);
C◼uintmax toumax(C◼wchar const[restrict 1], C◼wchar ** restrict, int);

C◼attr◼nonnull C◼wchar* cpy(C◼wchar*restrict, C◼wchar const*restrict);
C◼attr◼nonnull C◼wchar* ncpy(C◼wchar*restrict, C◼wchar const*restrict, C◼size);
C◼attr◼nonnull C◼wchar* cat(C◼wchar*restrict, C◼wchar const*restrict);
C◼attr◼nonnull C◼wchar* ncat(C◼wchar*restrict, C◼wchar const*restrict, C◼size);
C◼attr◼nonnull C◼attr◼pure int cmp(C◼wchar const*, C◼wchar const*);
C◼attr◼nonnull int coll(C◼wchar const*, C◼wchar const*);
C◼attr◼nonnull C◼attr◼pure int ncmp(C◼wchar const*, C◼wchar const*, C◼size);
C◼attr◼nonnull C◼size xfrm(C◼wchar*restrict, C◼wchar const*restrict, C◼size);
C◼attr◼nonnull C◼attr◼pure C◼wchar* chr(C◼wchar const*, C◼wchar);
C◼attr◼nonnull C◼attr◼pure C◼size cspn(C◼wchar const*, C◼wchar const*);
C◼attr◼nonnull C◼attr◼pure C◼wchar* pbrk(C◼wchar const*, C◼wchar const*);
C◼attr◼nonnull C◼attr◼pure C◼wchar* rchr(C◼wchar const*, C◼wchar);
C◼attr◼nonnull C◼attr◼pure C◼size spn(C◼wchar const*, C◼wchar const*);
C◼attr◼nonnull C◼attr◼pure C◼wchar* str(C◼wchar const*, C◼wchar const*);
C◼attr◼nonnull C◼wchar* tok(C◼wchar*restrict, C◼wchar const*restrict, C◼wchar**restrict);
C◼attr◼nonnull C◼attr◼pure C◼size len(C◼wchar const*);
C◼attr◼nonnull int tomb(char*, C◼wchar);
C◼attr◼const int tob(C◼wint);
C◼size tombs(char*restrict, C◼wchar const[restrict 1], C◼size);
