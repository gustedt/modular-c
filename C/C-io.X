/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module io    = C¯io

/**
 ** @module C's Input/Output primitives.
 **/

#pragma CMOD mimic <stdio.h>
#pragma CMOD mimic <stdalign.h>

#pragma CMOD typedef io   = FILE

#pragma CMOD defexp     EOF

#pragma CMOD alias in   = stdin
#pragma CMOD alias out    = stdout
#pragma CMOD alias err    = stderr

#pragma CMOD alias close  = fclose
#pragma CMOD alias flush  = fflush
#pragma CMOD alias      fgetc
#pragma CMOD alias      fgets
#pragma CMOD alias open   = fopen
#pragma CMOD alias      fprintf
#pragma CMOD alias      fputc
#pragma CMOD alias      fputs
#pragma CMOD alias fread
#pragma CMOD alias reopen = freopen
#pragma CMOD alias      fscanf
#pragma CMOD alias seek   = fseek
#pragma CMOD alias tell   = ftell
#pragma CMOD alias fwrite
#pragma CMOD alias      getc
#pragma CMOD alias      getchar
#pragma CMOD alias      perror
#pragma CMOD alias      printf
#pragma CMOD alias      putc
#pragma CMOD alias      putchar
#pragma CMOD alias      puts
#pragma CMOD alias      rewind
#pragma CMOD alias      scanf
#pragma CMOD alias      snprintf
#pragma CMOD alias      sprintf
#pragma CMOD alias      sscanf
#pragma CMOD alias tmp    = tmpfile
#pragma CMOD alias      ungetc
#pragma CMOD alias      vfprintf
#pragma CMOD alias      vfscanf
#pragma CMOD alias      vprintf
#pragma CMOD alias      vscanf
#pragma CMOD alias      vsnprintf
#pragma CMOD alias      vsprintf
#pragma CMOD alias      vsscanf

/* putc might be a macro with special properties. We want to use the
   one that the system provides, but add more features to it. */
#pragma CMOD define putc_internal = putc

#pragma CMOD declaration

typedef struct pos pos;

typedef C¯size size;

extern io * in;
extern io * out;
extern io * err;

C¯attr¯nodiscard
C¯attr¯nonnull
io *open(const char *restrict, const char *restrict);

C¯attr¯nodiscard
C¯attr¯nonnull
io *reopen(const char *restrict, const char *restrict, io *restrict);

C¯attr¯nonnull int close(io *);

int flush(io *);
C¯attr¯nonnull int seek(io *, long, int);
C¯attr¯nonnull long tell(io *);
C¯attr¯nonnull void rewind(io *);

C¯attr¯nonnull size fread(void *restrict, size, size, io *restrict);
C¯attr¯nonnull size fwrite(const void *restrict, size, size, io *restrict);

C¯attr¯nonnull int fgetc(io *);
C¯attr¯nonnull int getc(io *);
C¯attr¯nonnull int getchar(void);
C¯attr¯nonnull int ungetc(int, io *);

C¯attr¯nonnull int fputc(int, io *);
C¯attr¯nonnull int putc(int, io *);
int putchar(int);

C¯attr¯nonnull char *fgets(char *restrict, int, io *restrict);
C¯attr¯nonnull int fputs(const char *restrict, io *restrict);
C¯attr¯nonnull int puts(const char *);

C¯attr¯nonnull C¯attr¯format¯printf(1, 2) int (printf)(const char *restrict, ...);
C¯attr¯nonnull C¯attr¯format¯printf(2, 3) int fprintf(io *restrict, const char *restrict, ...);
C¯attr¯nonnull C¯attr¯format¯printf(2, 3) int sprintf(char *restrict, const char *restrict, ...);
C¯attr¯nonnull C¯attr¯format¯printf(3, 4) int snprintf(char *restrict, size, const char *restrict, ...);

C¯attr¯nonnull int vprintf(const char *restrict, C¯va);
C¯attr¯nonnull int vfprintf(io *restrict, const char *restrict, C¯va);
C¯attr¯nonnull int vsprintf(char *restrict, const char *restrict, C¯va);
C¯attr¯nonnull int vsnprintf(char *restrict, size, const char *restrict, C¯va);

C¯attr¯format¯scanf(1, 2) int (scanf)(const char *restrict, ...);
C¯attr¯format¯scanf(2, 3) int fscanf(io *restrict, const char *restrict, ...);
C¯attr¯format¯scanf(2, 3) int sscanf(const char *restrict, const char *restrict, ...);
C¯attr¯nonnull int vscanf(const char *restrict, C¯va);
C¯attr¯nonnull int vfscanf(io *restrict, const char *restrict, C¯va);
C¯attr¯nonnull int vsscanf(const char *restrict, const char *restrict, C¯va);

C¯attr¯nonnull void perror(const char *);

C¯attr¯nonnull
C¯attr¯nodiscard
io *tmp(void);

/** @brief The macro is a convenient interface to @c printf and @c fprintf. */
#define printf(...) _Generic(C¯FIRST(__VA_ARGS__), char const*: printf, char*: printf, io*: fprintf)(__VA_ARGS__)
/** @brief The macro is a convenient interface to @c scanf and @c fscanf. */
#define scanf(...) _Generic(C¯FIRST(__VA_ARGS__), char const*: scanf, char const*: scanf, io*: fscanf)(__VA_ARGS__)

#ifndef putc_internal
# define putc_internal fputc
#endif

/**
 ** @brief A convenient interface to @c putc and @c putchar.
 ***/
#define putc(...) C¯comma¯generic¯call((putchar, fputc), __VA_ARGS__)

/**
 ** @brief A convenient interface to @c getc and @c getchar.
 ***/
#define getc(...) C¯COUNT¯generic¯call((getchar, fgetc), __VA_ARGS__)

/**
 ** @brief A convenient interface to @c open and @c reopen.
 **/
#define open(...)  C¯comma¯generic¯call((open, open, reopen), __VA_ARGS__)

/**
 ** @def gets
 ** @brief Replaces the ancient gets function (which was removed), as
 ** a convenient interface to @c fgets with 2 or 3 arguments.
 **
 ** Reads from io¯in if the 3rd argument is omitted. */

#pragma CMOD fill igets¯OUTER  = gets
#pragma CMOD fill igets¯INNER  = fgets
#pragma CMOD defill igets¯LIST = , , in
#pragma CMOD import igets = C¯DEFARG

#pragma CMOD fill iungetc¯OUTER  = ungetc
#pragma CMOD fill iungetc¯INNER  = ungetc
#pragma CMOD defill iungetc¯LIST = , in
#pragma CMOD import iungetc = C¯DEFARG

#pragma CMOD declaration

#if C¯libc(C¯libc¯glibc)
#pragma CMOD define _IO_putc
#pragma CMOD alias _IO_putc
extern int _IO_putc(int, io *);
#endif

/**
 ** @brief read at most @a size bytes into buffer @a b
 **
 ** This is an extension of the current C library that is better in
 ** line with other IO functions, namely to have the stream as first
 ** argument and operating just on bytes. */
inline
C¯attr¯nonnull
C¯size read(io* s, void* b, C¯size size) {
  return fread(b, 1, size, s);
}

/**
 ** @brief write at most @a size bytes from buffer @a b
 **
 ** @see read
 ** */
inline
C¯attr¯nonnull
C¯size write(io* s, void const* b, C¯size size) {
  return fwrite(b, 1, size, s);
}

#pragma CMOD fill iflush¯OUTER = flush
#pragma CMOD fill iflush¯INNER = flush
#pragma CMOD defill iflush¯LIST  = nullptr
#pragma CMOD import iflush    = C¯DEFARG

// FIXME:
#pragma CMOD declaration

/**
 ** @brief Synchronize all buffered write data of IO streams.
 **/
inline
void sync(void) {
  flush();
}
