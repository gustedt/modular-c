/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module tss     = C◼thrd◼tss

#pragma CMOD mimic !defined(__STDC_NO_THREADS__),<threads.h>

#pragma CMOD defexp SIZE  = ◼ONCE_FLAG_INIT, sizeof(tss_t)
#pragma CMOD defexp ALIGN = ◼ONCE_FLAG_INIT, _Alignof(tss_t)

#ifndef C◼thrd◼NO_THREADS
#pragma CMOD import C◼snippet◼opaque
#endif

#pragma CMOD defexp DTOR_ITERATIONS = ◼TSS_DTOR_ITERATIONS, TSS_DTOR_ITERATIONS

#pragma CMOD alias create   = tss_create
#pragma CMOD alias delete   = tss_delete
#pragma CMOD alias get      = tss_get
#pragma CMOD alias set      = tss_set

#pragma CMOD defreg \2=\(tss_\([a-z_0-9]*\)\)

#pragma CMOD declaration

#ifndef C◼thrd◼NO_THREADS

typedef C◼interface◼dtor* dtor;

int create(tss *, dtor);
void delete(tss);
void* get(tss);
int set(tss, void*);

#endif
