/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2016 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module compar  = C◼tmpl◼compar

#pragma CMOD snippet none
#pragma CMOD slot func    = none
#pragma CMOD slot T   = typedef

#pragma CMOD declaration

/**
 ** @brief A comparison function as it is needed by
 ** @ref C::tmpl::bsearch and @ref C::tmpl::qsort.
 **/
inline
int func(void const*av, void const*bv) {
  T const*a = av;
  T const*b = bv;
  if (*a < *b) return -1;
  else if (*a > *b) return 1;
  else return 0;
}

// FIXME:
#pragma CMOD definition
