/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module rwlock =
#pragma CMOD separator ◼
#pragma CMOD composer  —
#pragma CMOD import ato   = ..
#pragma CMOD import io  = ..◼..◼io
#pragma CMOD import sig = ..◼..◼sig

/**
 ** @file
 **
 ** @brief Implement a simple rw lock that can be used thread safe.
 **
 ** @remark This function is implemented by means of a lock-free
 ** atomic, if the platform supports this.
 **
 ** If the platform doesn't support any lock-free atomic @c int, this
 ** data structure might not be lock-free, either. Operations
 ** tryrdlock() or trywrlock(), are guaranteed to have obtained the
 ** lock completely if they return @c true, so they are safe to use in
 ** the context of a signal handler or initialization function if they
 ** are undone by unrdlock() or unwrlock(), respectively, before
 ** returning from that function.
 **
 ** If they return @c false, there can be different reasons:
 **
 ** - The lock is already taken.
 ** - Somebody, maybe the same thread in case of a signal handler, is
 **   in the middle of modifying the object. This can only happen if
 **   not lock-free.
 ** - There are already more than unsigned◼MAX-1 readers.
 **
 ** In particular, in the not lock-free case if calls to tryrdlock()
 ** and trywrlock() are interlaced, both calls may fail and no lock
 ** will be taken.
 **
 ** @warning rdlock() and wrlock() are active waits.
 **
 ** In total this interface supplies eight different functions,
 ** {rd|wr}lock(), try{rd|wr}lock(), un{rd|wr}lock() and rdtowr() and
 ** wrtord(). Versions with @c rd are for read-only access, the others
 ** for exclusive write.
 **
 ** @remark This interface is somewhat unconventional as there is a
 ** distinction in two different unlock functions. This is intended to
 ** ease the implementation and to force the user of this to be always
 ** clear if this has been locked read-only or exclusive-write.
 **
 ** @remark Locks obtained by this are not attributed to any thread or
 ** other entity. It is completely up to the user to make that
 ** link.
 **
 ** If the macro ::C◼atomic◼rwlock◼LOCK_FREE evaluates to @c 2 this
 ** data structure is lock-free.
 **/


#pragma CMOD declaration

#ifndef ato◼NO_ATOMICS

#define LOCK_FREE ato◼INT_LOCK_FREE

# define UNLOCKED 0U
# define MAX—RD (C◼unsigned◼MAX-1U)
# define LOC—WR C◼unsigned◼MAX

struct rwlock {
  unsigned  volatile _Atomic lck;
};

#define INIT { ato◼INIT(0), }

inline bool trywrlock(rwlock* lck) {
  unsigned used = UNLOCKED;
  return ato◼compare_exchange_weak(&lck->lck, &used, LOC—WR);
}

inline void unwrlock(rwlock* lck) {
  unsigned used = LOC—WR;
  ato◼compare_exchange_weak(&lck->lck, &used, UNLOCKED);
}

inline void wrlock(rwlock* lck) {
  unsigned used = UNLOCKED;
  do { /* spin */ } while (!ato◼compare_exchange_weak(&lck->lck, &used, LOC—WR));
}

inline void wrtord(rwlock* lck) {
  unsigned used = LOC—WR;
  ato◼compare_exchange_weak(&lck->lck, &used, 1U);
}

inline void unrdlock(rwlock* lck) {
  ato◼fetch_sub(&lck->lck, 1U);
}

inline void rdtowr(rwlock* lck) {
  /* If we hold the only rdlock, deal it against a wrlock. */
  unsigned used = 1U;
  if (!ato◼compare_exchange_weak(&lck->lck, &used, LOC—WR)) {
    /* Otherwise unlock and relock. */
    unrdlock(lck);
    wrlock(lck);
  }
}

inline bool tryrdlock(rwlock* lck) {
  unsigned used = lck->lck;
  return (used < MAX—RD) && ato◼compare_exchange_weak(&lck->lck, &used, used+1U);
}

inline void rdlock(rwlock* lck) {
  unsigned used = UNLOCKED;
  do {
    if (used ≥ MAX—RD) used = MAX—RD - 1U;
  } while (!ato◼compare_exchange_weak(&lck->lck, &used, used+1U));
}

#endif
