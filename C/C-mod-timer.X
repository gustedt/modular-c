/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module timer =
#pragma CMOD import stats = ..¯stats
#pragma CMOD import spec  = C¯time¯spec
#pragma CMOD context C¯time¯spec
#pragma CMOD separator ¯
#pragma CMOD composer  —


/**
 ** @file
 ** @brief Collect some simple timing statistics online as we go.
 **
 **/

#pragma CMOD declaration

struct timer {
  timer*const sooner;
#ifdef C¯atomics¯NO_ATOMICS
  C¯time¯spec t;
  stats* stat;
#else
  C¯time¯spec _Atomic t;
  stats*_Atomic stat;
#endif
  char const*const desc;
};

#define INITIALIZER—0(DESC, SOONER, ...) {                     \
.sooner = (SOONER),                                            \
.desc = ""DESC"",                                              \
}

#define INITIALIZER(...) INITIALIZER—0(__VA_ARGS__ , 0, )

inline
bool startup(timer* t) {
  if (t ∧ t→sooner) {
    while (¬t→stat) {
      stats* s = C¯lib¯malloc(sizeof *s);
#ifndef C¯atomics¯NO_ATOMICS
      stats* expect = nullptr;
      if (¬C¯atomic¯compare_exchange_weak(&t→stat, &expect, s)) {
        C¯lib¯free(s);
        continue;
      }
#endif
      t→stat = stats¯init(s, t→desc);
    }
  }
  return t;
}

inline
void sample(timer* t) {
  if (startup(t)) {
    spec now;
    spec¯get(&now, C¯time¯UTC);
    if (t→sooner) {
      // .t may be atomic. Make sure that there is only one load.
      spec sooner = t→sooner→t;
      stats¯collect3(t→stat, ⟦~~(now-sooner)⟧);
    }
    // .t may be atomic. Make sure that there is only one store.
    t→t = now;
  }
}

#pragma CMOD definition

#pragma CMOD entry test

int test(int argc, char* argv[argc+1]) {
  static unsigned volatile s;
  static timer start = INITIALIZER("starting");
  for (unsigned probe = 0; probe < 5; ++probe) {
    sample(&start);
    for (unsigned volatile i = 0; i < -1; i++) {
    }
    static timer count_vol = INITIALIZER("counter volatile", &start);
    sample(&count_vol);
    for (unsigned i = 0; i < -1; i++) {
      s += i;
    }
    static timer store_vol = INITIALIZER("store volatile", &count_vol);
    sample(&store_vol);
  }
  stats¯print(C¯io¯out);
  return 0;
}
