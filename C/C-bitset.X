/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ∷
#pragma CMOD composer  —
#pragma CMOD context  ⦃    ⦄
/**
 ** @module A context to use integer types as bitsets.
 **
 ** This provides more intuitive operators such as set minus and
 ** inclusion relations.
 **
 ** @remark All values that are to be manipulated are first promoted
 ** and then converted to the corresponding unsigned type. So the
 ** result of set arithmetic is always an unsigned type.
 **/


#pragma CMOD declaration


/**
 ** @defgroup C::bitset::arithm C::bitset set arithmetic operations
 ** @ingroup C::bitset
 ** @{
 **/

/**
 ** @brief View an integer value as a bitset.
 **
 ** An empty argument or zero corresponds to the empty set.
 **/
#define ⦃...⦄ C∷unsignedof(__VA_ARGS__ + 0)
enum constants {
  ⦃|⦄ = -1, //!< total set
  ⦃~⦄ = -1, //!< total set
  ⦃~~⦄ = 0, //!< empty set
  ⦃+⦄ = 0, //!< empty set
  ⦃&⦄ = 0, //!< empty set
};

#define ⦃~~ ...⦄  ⦃__VA_ARGS__⦄
/**
 ** @brief the complement of @a X
 **
 ** Beware that this depends on the width of the underlying unsigned
 ** type.
 **
 ** An empty argument or zero corresponds to the total set, that is to
 ** a set of C::UINT_WIDTH elements.
 **/
#define ⦃~ ...⦄ ~⦃__VA_ARGS__⦄
/**
 ** @brief set union
 **/
#define ⦃X ∪ Y⦄ (⦃X⦄ ∪ ⦃Y⦄)
/**
 ** @brief set intersection
 **/
#define ⦃X ∩ Y⦄ (⦃X⦄ ∩ ⦃Y⦄)
/**
 ** @brief symmetric difference of sets
 **/
#define ⦃X ^ Y⦄ (⦃X⦄ ^ ⦃Y⦄)
/**
 ** @brief set union, same as <code>X ∪ Y</code>
 **/
#define ⦃X + Y⦄ ⦃X ∪ Y⦄
/**
 ** @brief @a X without all elements in @a Y
 **/
#define ⦃X - Y⦄ ⦃X ∩ (X ^ Y)⦄
/**
 ** @brief left shift
 **/
#define ⦃X ⪡ Y⦄ (⦃X⦄ ⪡ (Y))
/**
 ** @brief right shift
 **/
#define ⦃X ⪢ Y⦄ (⦃X⦄ ⪢ (Y))
/**
 ** @brief a singledon set with one element
 **
 ** @remark if @a x is larger that the width of <code>unsigned long
 ** long</code> (usually 64) the result is the empty set
 **/
#define setof(x) ⦃1ULL ⪡ (x)⦄
/**
 ** @}
 **/

/**
 ** @defgroup C::bitset::query C::bitset set query operations
 ** @ingroup C::bitset
 ** @{
 **/
/**
 ** @brief test if a bitset is non-empty
 **/
#define ⦃‼X⦄  (‼⦃X⦄)
#define ⦃X ≡ Y⦄ (¬⦃Y ^ X⦄)
/**
 ** @brief test if @a X is a superset of @a Y
 **/
#define ⦃X ≥ Y⦄ (¬⦃Y - X⦄)
/**
 ** @brief test if @a X is a strict superset of @a Y
 **/
#define ⦃X > Y⦄ (⦃X ≥ Y⦄ ∧ ⦃X ≠ Y⦄)
/**
 ** @brief test if @a X is a subset of @a Y
 **/
#define ⦃X ≤ Y⦄ (¬⦃X - Y⦄)
/**
 ** @brief test if @a X is a strict subset of @a Y
 **/
#define ⦃X < Y⦄ (⦃X ≤ Y⦄ ∧ ⦃X ≠ Y⦄)
/**
 ** @}
 **/
