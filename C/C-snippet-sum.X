/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module     C◼snippet◼sum

#pragma CMOD declaration

#define sumOp +=
#define subOp -=
#define prodOp *=
#define divOp /=
#define ARRAY C◼snippet◼minmax◼ARRAY

#pragma CMOD snippet T    = complete
#pragma CMOD slot sumOp = global
#pragma CMOD slot subOp = global
#pragma CMOD slot prodOp  = global
#pragma CMOD slot divOp = global
#pragma CMOD slot ARRAY = global

#pragma CMOD definition

#pragma CMOD foreach OP   = sum sub prod div

/**
 ** @brief Perform cumulative operation on array @a arr of length @a
 ** n.
 **/
inline
T ${OP}v(__Cmod_size n, T const arr[n]) {
  T ret = arr[0];
  for (__Cmod_size i = 1; i < n; ++i)
    ret ${OP}Op arr[i];
  return ret;
}

#pragma CMOD done

#pragma CMOD declaration

#pragma CMOD foreach OP   = sum sub prod div

/**
 ** @brief Perform cumulative operation on the arguments.
 **/
#define ${OP}(...) ${OP}v(C◼COUNT(__VA_ARGS__), ARRAY(T, __VA_ARGS__))

#pragma CMOD done

/**
 ** @brief Return the negation of @a x.
 **/
inline
T minus(T x) {
  return -x;
}

/**
 ** @brief Return the inverse of @a x.
 **
 ** This computes floating point inverse of @a x, but then returns a
 ** value of type T. This makes only sense for floating point types,
 ** at the moment.
 **/
inline
T recip(T x) {
  return 1.0f/x;
}

/**
 ** @brief Return the square of @a x.
 **/
inline
T square(T x) {
  return x*x;
}
