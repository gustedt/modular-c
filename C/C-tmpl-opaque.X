/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module opaque  = C¯tmpl¯opaque

#pragma CMOD snippet none
#pragma CMOD slot T     = none
#pragma CMOD slot SIZE    = size
#pragma CMOD slot ALIGN   = ice 1, (1U<<10)

#pragma CMOD declaration

/**
 ** @brief An opaque structure that is constructed with a specific
 ** size and alignment.
 **/
struct T {
  /* Sometimes gcc is capable to produce weird alignments. Fall back
     to its specifics, if we don't know how to do otherwise. */
#if ALIGN && (SIZE % ALIGN)
  C¯attr¯alignas(ALIGN)
#else
  alignas(ALIGN)
#endif
  char T[SIZE];
};

#pragma CMOD definition

static_assert(SIZE == sizeof(T), "import of opaque type failed: wrong size");
static_assert(ALIGN == alignof(T), "import of opaque type failed: wrong alignment");
