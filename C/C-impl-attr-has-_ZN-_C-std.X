/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module attr  =
#pragma CMOD separator ¯
#pragma CMOD composer —

/**
 ** @module Register the known "std" attributes
 **/

#pragma CMOD declaration

# define attr¯alignas(X) ,
# define attr¯const    ,
# define attr¯deprecated ,
# define attr¯deprecate(TXT) ,
# define attr¯leaf   ,
# define attr¯noinline   ,
# define attr¯nonnull    ,
# define attr¯nothrow    ,
# define attr¯optimize(X)  ,
# define attr¯pure   ,
# define attr¯twice    ,
# define attr¯returns_nonnull    ,
# define attr¯used   ,
# define attr¯maybe_unused ,
# define attr¯nodiscard    ,
