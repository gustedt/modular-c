/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ∷
#pragma CMOD composer —

/**
 ** @module A trivial context that replaces operator-functions back to their operator.
 **
 ** @remark This has to be compiled in a second phase of the Modular C
 ** bootstrap, because the context parser is itself written in Modular C.
 **/

#pragma CMOD snippet    = none
#pragma CMOD context context  = « »
#pragma CMOD slot    context  = none

#pragma CMOD declaration

#define « A ≡ B » ((A)==(B))
#define « A ≥ B » ((A)>=(B))
#define « A > B » ((A)>(B))
#define « A ≤ B » ((A)<=(B))
#define « A < B » ((A)<(B))
#define « ‼A »    (!!(A))
#define « ~A »    (~(A))
#define « ~~A »   (~~(A))
#define « A ^ B » ((A)^(B))
#define « A & B » ((A)&(B))
#define « A | B » ((A)|(B))
#define « A ⪢ B » ((A)⪢(B))
#define « A ⪡ B » ((A)⪡(B))
#define « A % B » ((A)%(B))
#define « A ÷ B »  ((A)/(B))
#define « A × B »  ((A)*(B))
#define « A - B » ((A)-(B))
#define « A + B » ((A)+(B))

// FIXME
#pragma CMOD definition
