/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module C◼symbols
#pragma CMOD composer —
#pragma CMOD snippet none

#pragma CMOD declaration

#ifndef I
# define I    C◼complex◼I
#endif
#ifndef ∞
# define ∞    C◼real◼INFINITY
#endif
#ifndef NAN
# define NAN    C◼real◼NAN
#endif
#ifndef ²√2
# define ²√2   C◼real◼double◼sqrt—2
#endif
#ifndef ²√π
# define ²√π    C◼real◼double◼sqrt—π
#endif
#ifndef π
# define π   C◼real◼double◼π
#endif
#ifndef ⅟—sqrt—2
# define ⅟—sqrt—2 C◼real◼double◼inv—sqrt—2
#endif
#ifndef e
# define e    C◼real◼double◼e
#endif
#ifndef log⏨ℯ
# define log⏨ℯ    C◼real◼double◼log10e
#endif
#ifndef log₂ℯ
# define log₂ℯ    C◼real◼double◼log2e
#endif
#ifndef ln2
# define ln2    C◼real◼double◼ln2
#endif
#ifndef ln10
# define ln10   C◼real◼double◼ln10
#endif
