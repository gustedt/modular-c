/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module char16  = C¯char16
#pragma CMOD import C¯snippet¯minmax
#pragma CMOD import C¯snippet¯sum
#pragma CMOD import C¯snippet¯avg
#pragma CMOD import C¯snippet¯equal

/**
 ** @module 16 bit character handling
 **/

#pragma CMOD mimic <limits.h>
#pragma CMOD mimic <stdalign.h>
#pragma CMOD mimic <stdint.h>
#pragma CMOD mimic <uchar.h>

/* make sure that these are promoted to the correct type */
#pragma CMOD defexp MAX=UINT_LEAST16_MAX
#pragma CMOD defexp MIN=+((uint_least16_t)+0)
#pragma CMOD defexp ALIGN=alignof(uint_least16_t)
#pragma CMOD defexp RANK=CMOD_RANK((uint_least16_t)+0)
#pragma CMOD defexp SIZE=sizeof(uint_least16_t)
#pragma CMOD defexp WIDTH=(int)cmod_msb(UINT_LEAST16_MAX)

#pragma CMOD declaration

#if C¯UTF_16
# define UTF_16 C¯UTF_16
#endif

#pragma CMOD alias mbsto = mbrtoc16
#pragma CMOD alias tombs = c16rtomb

C¯size mbsto (char16*restrict, char const*restrict, C¯size, C¯mbs¯state*restrict);
C¯size tombs (char*restrict, char16, C¯mbs¯state*restrict);

#pragma CMOD snippet none
#pragma CMOD declaration
typedef C¯char16 char16;
