/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module   𝔽—257 = C¯F257
#pragma CMOD composer —
#pragma CMOD context  ⟦    ⟧
#pragma CMOD import   C¯ullong

/**
 ** @module Implement some finite fields.
 **/



#pragma CMOD foreach P =                                               \
      2      3      5      7     11     13     17     19     23     29 \
     31     37     41     43     47     53     59     61     67     71 \
     73     79     83     89     97    101    103    107    109    113 \
    127    131    137    139    149    151    157    163    167    173 \
    179    181    191    193    197    199    211    223    227    229 \
    233    239    241    251    257    221

#pragma CMOD declaration

#pragma CMOD defill Z—${P}¯mod   = ${P}

#pragma CMOD definition

#pragma CMOD fill   Z—${P}¯context = 𝔽—${P}
#pragma CMOD fill   Z—${P}¯type    = 𝔽—${P}
#pragma CMOD fill   Z—${P}¯order = order—${P}
#pragma CMOD fill   Z—${P}¯generator = generator—${P}
#pragma CMOD fill   Z—${P}¯find    = find—${P}
#pragma CMOD import Z—${P}    = C¯snippet¯modulo

// FIXME:
#pragma CMOD declaration

#pragma CMOD constant gen—${P} = find—${P}(${P}+1)

#pragma CMOD done

#pragma CMOD definition

#pragma CMOD entry main

void main(int argc, char const* argv[argc+1]) {
  𝔽—257 x = ⟦ + C¯str¯toul(argv[1], 0, 0) ⟧;
  C¯io¯printf("digraph \"F257\" {\n");
  𝔽—257 y = ⟦ 1 ⟧;
  C¯io¯printf("y %u\n", y);
  for (𝔽—257 i = 1; i; ⟦ ++i ⟧) {
    C¯io¯printf("\t%u", y);
    ⟦ y *= x ⟧;
    C¯io¯printf(" -> %u [ label=\"%u**%u\" ] \n", y, x, i);
    if (⟦ y ≡ 1 ⟧) break;
  }
  C¯io¯printf("}\n");
}
