/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module       C◼snippet◼alloc
#pragma CMOD import     C◼snippet◼INITIALIZER
#pragma CMOD import       C◼def
#pragma CMOD import lib   = C◼lib
#pragma CMOD import mem   = C◼mem
#pragma CMOD import sig   = C◼sig
#pragma CMOD import mod   = C◼mod
#pragma CMOD context      C◼attribute

#pragma CMOD snippet T    = complete
/**
 ** @brief The default initializer is used if the corresponding slot
 ** is not filled by the importer.
 **/
#pragma CMOD slot INITIALIZER = compatible T,
#pragma CMOD slot helper  = intern

#pragma CMOD declaration

struct helper {
  C◼size len;
  void(*id)(void);
  T data[];
};

T* alloc(C◼size nlen, T*restrict prev) 〚std◼alloc〛;

#pragma CMOD definition

/* static snippet elements must still be made intern, since otherwise
   they may clash with names that the importer already uses. */
#pragma CMOD slot here    = intern
#pragma CMOD slot size    = intern
#pragma CMOD slot start   = intern
#pragma CMOD slot offsetof  = global

/* This is used to trace the identity of this module. */
static void (*const here)(void) = (void(*)(void))alloc;

/* Small static functions will be inlined by the compiler. No need to
   tell him. */
static C◼size size(C◼size n) 〚std◼pure〛;
static
C◼size size(C◼size n) {
  C◼size large = sizeof(T[n]) + offsetof(helper, data);
  C◼size small = sizeof(helper);
  return C◼math◼max(small, large);
}

static
helper* start(T* arr) {
  return (void*)((char*)arr-offsetof(helper, data));
}

/**
 ** @brief Allocate, reallocate or free an array of @a nlen elements
 ** of a given base type.
 **
 ** @warning This function should not be mixed with the C library
 ** allocation functions such as C::lib::malloc or C::lib::free.
 **
 ** - If @a prev is a null pointer and @a nlen is not zero as much
 **   elements are allocated, if possible.
 **
 ** - If @a prev is not a null pointer it must be a pointer that
 **   previously had been returned by the same function.
 **
 ** - If @a nlen is zero, the memory is freed and a null pointer is
 **   returned.
 **
 ** - If @a nlen is non zero, the array object is shrunk or expanded to
 **   the new size, and a pointer to the possibly different memory
 **   location is returned.
 **
 ** @remark Other than the standard C library allocation functions
 ** arrays that are allocated through this function are typed and
 ** cannot change their type.
 **
 ** @remark Newly allocated elements of the new array are default
 **   initialized.
 **
 ** @remark Array elements that are beyond @a nlen are lost and their
 ** contents is overwritten with zeros.
 **
 ** @remark If an array is freed (that is @a nlen is zero) all
 ** internal records of the previous type and length of the array is
 ** zeroed, too.
 **
 ** - If all went well, the now valid address of the array object is
 **   returned.
 **
 ** - If the array object previously had a different address, this
 **   should not be used, again.
 **
 ** - If the array could not be allocated a null pointer is returned
 **   to indicate the error.
 **
 ** - If the array could not be reallocated a null pointer is returned
 **   to indicate the error. The array remains accessible at its
 **   previous address and its length and contents are not changed.
 **/
〚std◼alloc〛
T* alloc(C◼size nlen, T*restrict prev) {
  helper*restrict old
    = prev
      ? start(prev)
      : nullptr;
  if (old ∧ old→id ≠ here) return nullptr;
  C◼size olen = old ? old→len : 0;
  if (¬nlen) {
    if (old) {
      /* Don't allow data to escape. */
      mem◼set(old, 0, size(olen));
      lib◼free(old);
      return nullptr;
    } else {
      nlen = 1;
    }
  }
  /* only realloc if sizes differ */
  if (olen ≡ nlen) return prev;
  helper* ret = lib◼realloc(old, size(nlen));
  if (¬ret) return nullptr;
  ret→len = nlen;
  ret→id = here;
  if (olen < nlen) {
    for (C◼size i = olen; i < nlen; ++i)
      ret→data[i] = (const T)INITIALIZER();
  } else {
    /* Don't allow data to escape. */
    mem◼set(ret→data+nlen, 0, sizeof(T[olen-nlen]));
  }
  return ret→data;
}
