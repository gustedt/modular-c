/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module C◼uintptr
#pragma CMOD mimic <stdint.h>

#pragma CMOD declaration
/* uintptr is optional*/
#pragma CMOD defexp MAX=◼UINTPTR_MAX
#pragma CMOD defexp ALIGN=◼UINTPTR_MAX,_Alignof(uintptr_t)
#pragma CMOD defexp RANK=◼UINTPTR_MAX,CMOD_RANK((uintptr_t)+0)
#pragma CMOD defexp WIDTH=◼UINTPTR_MAX,cmod_msb(UINTPTR_MAX)
#pragma CMOD defexp SIZE=◼UINTPTR_MAX,sizeof(uintptr_t)

#if MAX
#define MIN ((uintptr)+0)
#endif
