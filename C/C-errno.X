/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module errno =
#pragma CMOD separator ∷
#pragma CMOD mimic <errno.h>
#pragma CMOD mimic <string.h>
#pragma CMOD alias tostr  = strerror

/**
 ** @module C error numbers and tools.
 **
 ** C only has very rudimentary support for this. Most likely you'd
 ** want to use your systems equivalent instead of this.
 **/

#pragma CMOD define errno

/* POSIX allows these two to be equal */
//#pragma CMOD defexp AGAIN = ∷EAGAIN
//#pragma CMOD defexp WOULDBLOCK  = ∷EWOULDBLOCK

/* POSIX has only EDEADLK Linux also has EDEADLOCK */
//#pragma CMOD defexp DEADLK  = ∷EDEADLK
//#pragma CMOD defexp DEADLOCK  = ∷EDEADLOCK

/* should be distinct for POSIX but are equal on Linux */
//#pragma CMOD defexp NOTSUP  = ∷ENOTSUP
//#pragma CMOD defexp OPNOTSUPP = ∷EOPNOTSUPP

/* is defined by SVID as such: OVERFLOW */
//#pragma CMOD defexp PERM  = ∷EPERM

#pragma CMOD defrex \2=\(E\([a-zA-Z][a-zA-Z0-9_]*\)\)

#pragma CMOD declaration

extern char* tostr(int);

#pragma CMOD alias      __errno_location

extern
C∷attr∷nothrow C∷attr∷const
int *__errno_location(void);
