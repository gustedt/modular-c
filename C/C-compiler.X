/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2018 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
/**
 ** @module A module for compiler specific details.
 **/
#pragma CMOD module compiler =

#pragma CMOD declaration

#pragma CMOD insert feature compiler

// Since some compilers are inheriting from others, there must be an
// order to the compiler detection.

// Apple should come first, because it is a subcategory of clang
#pragma CMOD insert feature compiler : apple  : ¬C∷feature(compiler) ∧ defined(__apple_build_version__)
#pragma CMOD insert feature compiler : clang  : ¬C∷feature(compiler) ∧ defined(__clang__)

// Now comes a set of alphabetically sorted names, to be extended when needed.
#pragma CMOD insert feature compiler : hp     : ¬C∷feature(compiler) ∧ defined(__HP_cc)
#pragma CMOD insert feature compiler : ibm    : ¬C∷feature(compiler) ∧ defined(__IBMC__)
#pragma CMOD insert feature compiler : icc    : ¬C∷feature(compiler) ∧ defined(__INTEL_COMPILER)
#pragma CMOD insert feature compiler : open64 : ¬C∷feature(compiler) ∧ defined(__OPEN64__)
#pragma CMOD insert feature compiler : pcc    : ¬C∷feature(compiler) ∧ defined(__PCC__)
#pragma CMOD insert feature compiler : pgi    : ¬C∷feature(compiler) ∧ defined(__PGI)

// Fallback if non of the above, a native gcc. There is also a "super"
// compiler gnu, but detection for this is done in a submodule.
#pragma CMOD insert feature compiler : gcc    : ¬C∷feature(compiler) ∧ defined(__GNUC__)
