/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2018 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
/**
 ** @module A module for libc specific details.
 **/
#pragma CMOD module libc =

#pragma CMOD declaration

#pragma CMOD insert feature libc

#pragma CMOD insert feature libc : bionic : defined(__BIONIC__)
#pragma CMOD insert feature libc : glibc  : defined(__GLIBC__)
#pragma CMOD insert feature libc : klibc  : defined(__KLIBC__)
#pragma CMOD insert feature libc : uclibc : defined(__UCLIBC__)
#pragma CMOD insert feature libc : vms    : defined(__CRTL_VER)
#pragma CMOD insert feature libc : zos    : defined(__LIBREL__)

// Unix systems are derivatives that define a lot of extensions to the
// C library. Try to capture such systems.

// Attention many systems also define the non-conforming macro `unix`,
// which really causes headaches. Therefore we use the uppercase
// version, here.
#pragma CMOD insert feature libc : UNIX : defined(__unix__)

#pragma CMOD mimic defined(__unix__),<unistd.h>

#pragma CMOD defexp POSIX_VERSION = ∷_POSIX_VERSION,_POSIX_VERSION

#pragma CMOD insert feature libc : POSIX     : defined(POSIX_VERSION)
#pragma CMOD insert feature libc : android   : defined(__ANDROID__)
#pragma CMOD insert feature libc : OSX       : defined(__APPLE__)
#pragma CMOD insert feature libc : sysv      : defined(__sysv__)
#pragma CMOD insert feature libc : ultrix    : defined(__ultrix__)

#pragma CMOD insert feature libc : FreeBSD   : defined(__FreeBSD__)
#pragma CMOD insert feature libc : NetBSD    : defined(__NetBSD__)
#pragma CMOD insert feature libc : OpenBSD   : defined(__OpenBSD__)
#pragma CMOD insert feature libc : DragonFly : defined(__DragonFly__)
#pragma CMOD insert feature libc : BSD       : libc(FreeBSD) ∨ libc(NetBSD) ∨ libc(OpenBSD) ∨ libc(DragonFly)

// Cygwin is a hybrid.
#pragma CMOD insert feature libc : cygwin  : defined(__CYGWIN__)

#pragma CMOD insert feature libc : OS400   : defined(__OS400__)

// Windows systems are not POSIX but have a lot of extensions and
// particularities of their own.

#pragma CMOD insert feature libc : msdos   : defined(__MSDOS__)
#pragma CMOD insert feature libc : win16   : defined(_WIN16)
#pragma CMOD insert feature libc : win64   : defined(_WIN64)
#pragma CMOD insert feature libc : win32   : ¬libc(win64) ∧ defined(_WIN32)
#pragma CMOD insert feature libc : windows : defined(__WINDOWS__) ∨ libc(win16) ∨ libc(win32) ∨ libc(win64)

enum test {
  GNU   = libc(glibc),
  BIONIC= libc(bionic),
  UN  = libc(UNIX),
  POS = libc(POSIX),
  bsd = libc(BSD),
};
