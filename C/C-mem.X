/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module C◼mem

#pragma CMOD mimic <string.h>
#pragma CMOD mimic <wchar.h>

/**
 ** @file
 **
 ** @brief The interface to the memory manipulating functions
 **
 ** @warning This file is special because dependencies to it can be
 ** produced automatically during code generation.
 **
 ** The only imports that this module may have are
 **
 **  - C
 **  - C::attr
 **
 ** Be careful with attributes that have their own module, these could
 ** imply a cyclic import.
 **
 ** Because of these restrictions the semantic types that are used
 ** here are the internals @c __Cmod_size and @c __Cmod_wchar instead of C::size
 ** and C::wchar. Otherwise we would include a whole chain of
 ** dependencies to other modules that could cause problems.
 **/

#pragma CMOD alias chr=memchr
#pragma CMOD alias cmp=memcmp
#pragma CMOD alias cpy=memcpy
#pragma CMOD alias move=memmove
#pragma CMOD alias set=memset

#pragma CMOD alias wchr=wmemchr
#pragma CMOD alias wcmp=wmemcmp
#pragma CMOD alias wcpy=wmemcpy
#pragma CMOD alias wmove=wmemmove
#pragma CMOD alias wset=wmemset

#pragma CMOD declaration

C◼attr◼nonnull void* cpy(void* restrict, const void* restrict, __Cmod_size);
C◼attr◼nonnull void* move(void*, const void*, __Cmod_size);
C◼attr◼nonnull C◼attr◼pure int cmp(const void*, const void*, __Cmod_size);
C◼attr◼nonnull C◼attr◼pure void* chr(const void*, int, __Cmod_size);
C◼attr◼nonnull void* set(void*, int, __Cmod_size);

C◼attr◼nonnull __Cmod_wchar* wcpy(__Cmod_wchar*restrict, __Cmod_wchar const*restrict, __Cmod_size);
C◼attr◼nonnull __Cmod_wchar* wmove(__Cmod_wchar*, __Cmod_wchar const*, __Cmod_size);
C◼attr◼nonnull C◼attr◼pure int wcmp(__Cmod_wchar const*, __Cmod_wchar const*, __Cmod_size);
C◼attr◼nonnull C◼attr◼pure __Cmod_wchar* wchr(__Cmod_wchar const*, __Cmod_wchar, __Cmod_size);
C◼attr◼nonnull __Cmod_wchar* wset(__Cmod_wchar*, __Cmod_wchar, __Cmod_size);
