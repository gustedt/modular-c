/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
/**
 ** @module
 ** @brief A list data structure.
 **
 ** In the first part of this, this implements a common data structure
 ** that is called @ref C::tmpl::list at the outside and @c tlist at the
 ** inside. In the second part it then provides template data
 ** structure with a snippet.
 **
 ** */
#pragma CMOD module here    = C◼tmpl◼list

//#pragma CMOD fill in◼INITIALIZER  = INITIALIZER
#pragma CMOD import is    = C◼is

/* We also need some basic integer stuff. */
#pragma CMOD import       C◼integer
#pragma CMOD import       C◼str
#pragma CMOD import       C◼uintpr

#pragma CMOD declaration

#define here tlist

/**
 ** @brief The code that is shared between different instantiations of
 ** this template.
 **/

struct tlist {
  tlist* car;
  void* cdr;
};


/* This imports an identifier C◼tmpl◼list◼INITIALIZER */
#pragma CMOD import         C◼snippet◼INITIALIZER
/* This imports an identifier C◼tmpl◼list◼alloc */
#pragma CMOD import         C◼snippet◼alloc
/* This imports an identifier C◼tmpl◼list◼init. */
#pragma CMOD import       C◼snippet◼init
#pragma CMOD import       C◼interface◼init

// FIXME:
#pragma CMOD declaration

/** @memberof C::tmpl::list */
inline
void* top(tlist* l) {
  return l ? l→cdr : nullptr;
}

/** @memberof C::tmpl::list */
inline
tlist* push(tlist* l, void* data) {
  tlist* n = alloc(1, 0);
  if (n) {
    *n = (tlist) { .car=l, .cdr=data, };
  }
  return n;
}

/** @memberof C::tmpl::list */
inline
tlist* drop(tlist* l) {
  tlist* ret = 0;
  if (l) {
    ret = l→car;
    alloc(0, l);
  }
  return ret;
}

/** @memberof C::tmpl::list */
inline
tlist* next(tlist* l) {
  return l ? l→car : nullptr;
}

/**************************************************/
/* Now comes the part that is inserted into the importers. There is no
   restriction about the importers themselves but they have to fill
   two slots, T identifier of a base type and instance an identifier
   for the newly created list type. */

#pragma CMOD snippet none
/* T is the base type for the list. */
#pragma CMOD slot T   = complete 0
/* This will translate the identifier C◼tmpl◼list◼instance to whatever
   the importer wants to call it. */
#pragma CMOD slot instance  = typedef
#pragma CMOD slot tlist   = global

#pragma CMOD declaration

/**
 ** @brief The specialized type is just aliased.
 **/
typedef tlist instance;

inline
T top(instance* l) {
  // FIXME:
  /* _Static_assert(sizeof(T) ≤ sizeof(void*), */
  /*                "tmpl#list only can handle types that fit into void*"); */
  if (is◼POINTER(T)) {
    return (T)here◼top(l);
  } else if (is◼SIGNED(T)) {
    C◼intptr ret = (C◼intptr)here◼top(l);
    return (T)ret;
  } else {
    C◼uintptr ret = (C◼uintptr)here◼top(l);
    return (T)ret;
  }
}

inline
instance* push(instance* l, T data) {
  if (is◼POINTER(T)) {
    return here◼push(l, (void*)data);
  } else if (is◼SIGNED(T)) {
    return here◼push(l, (void*)(C◼intptr)data);
  } else {
    return here◼push(l, (void*)(C◼uintptr)data);
  }
}

inline
instance* drop(instance* l) {
  return here◼drop(l);
}

inline
instance* next(instance* l) {
  return here◼next(l);
}
