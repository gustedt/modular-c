/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ¯
#pragma CMOD composer —
#pragma CMOD import    top  = ..
#pragma CMOD import    table  = top¯table
#pragma CMOD import    is = top¯is

#pragma CMOD declaration

#define isnotupper is¯lower
#define isnotlower is¯upper


// ++++++++++++++++++++++++++++++++++++
#pragma CMOD foreach T = lower upper

/**
 ** @brief Character class transformation to${T} in the C locale.
 **
 ** In some locales character classification might be changed. If you
 ** need such locale specific behavior, use C::impl::char::to${T},
 ** instead.
 **/
inline
int ${T}(int tok) {
  if (isnot${T}(tok)) {
    if (table¯to${T}.difference) {
      return tok-table¯to${T}.difference;
    } else {
      return table¯to${T}.other[tok];
    }
  }
  return tok;
}


#pragma CMOD done
// ++++++++++++++++++++++++++++++++++++
