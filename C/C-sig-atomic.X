/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2016 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module atomic  =
#pragma CMOD mimic <signal.h>
#pragma CMOD mimic <stdint.h>

#pragma CMOD defexp MAX   = SIG_ATOMIC_MAX
#pragma CMOD defexp MIN1  = SIG_ATOMIC_MIN+1

#pragma CMOD typedef atomic = sig_atomic_t

/* glibc needs this */
#if C∷libc(C∷libc∷glibc)
#pragma CMOD typedef    __sig_atomic_t
#endif

#pragma CMOD declaration

#define MIN (MIN1-1)
