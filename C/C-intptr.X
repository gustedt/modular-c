/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module C◼intptr
#pragma CMOD mimic <stdint.h>

#pragma CMOD declaration

/* intptr is optional*/
#pragma CMOD defexp MAX=◼INTPTR_MAX,INTPTR_MAX
#pragma CMOD defexp MIN1=◼INTPTR_MAX,(INTPTR_MIN+1)
#pragma CMOD defexp ALIGN=◼INTPTR_MAX,_Alignof(intptr_t)
#pragma CMOD defexp RANK=◼INTPTR_MAX,CMOD_RANK((intptr_t)+0)
#pragma CMOD defexp WIDTH=◼INTPTR_MAX,(cmod_msb(INTPTR_MAX)+(INTPTR_MAX < UINTPTR_MAX))
#pragma CMOD defexp SIZE=◼INTPTR_MAX,sizeof(intptr_t)

#ifdef MIN1
# define MIN (MIN1-1)
#endif

#pragma CMOD snippet none
#pragma CMOD declaration

typedef C◼intptr intptr;
