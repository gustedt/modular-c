/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module equal = C¯equal
#pragma CMOD context C¯attribute

/**
 ** @module Provide a general framework for equality.
 **/

#pragma CMOD declaration

#pragma CMOD intern equal3
#pragma CMOD intern ptr
#pragma CMOD intern unknown
#pragma CMOD intern unknown_equal

#define equal3(A, B, …)                                        \
  _Generic(true ? (A) : (B),                                   \
         unsigned: C¯unsigned¯equal,                           \
         signed: C¯int¯equal,                                  \
         unsigned long: C¯ulong¯equal,                         \
         signed long: C¯long¯equal,                            \
         unsigned long long: C¯ullong¯equal,                   \
         signed long long: C¯llong¯equal,                      \
         float: C¯real¯float¯equal,                            \
         double: C¯real¯double¯equal,                          \
         long double: C¯real¯ldouble¯equal,                    \
         complex float: C¯complex¯float¯equal,                 \
         complex double: C¯complex¯double¯equal,               \
         complex long double: C¯complex¯ldouble¯equal,         \
         __VA_ARGS__                                           \
         )((A), (B))

typedef struct unknown unknown;

bool unknown_equal(unknown* a, unknown* b);

#define equal(A, …) equal3((A), __VA_ARGS__, unknown*: unknown_equal)

bool ptr(void const volatile* a, void const volatile* b) 〚std¯const〛;

bool test(long a, int b) 〚std¯const〛;

bool testp(long const* a, long const* b) 〚std¯const〛;

bool testb(bool a, bool b) 〚std¯const〛;


#pragma CMOD definition

bool ptr(void const volatile* a, void const volatile* b) {
  return a ≡ b;
}

bool test(long a, int b) {
  return equal(a, b, void*: ptr);
}

bool testp(long const* a, long const* b) {
  return equal(a, b, long*: ptr, long const*: ptr, void*: ptr);
}

bool testb(bool a, bool b) {
  return equal(a, b, long*: ptr, long const*: ptr, void*: ptr);
}
