/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module fenv  = C◼fenv
#pragma CMOD import     C◼snippet◼opaque

/**
 ** @module The floating point environment.
 **/

#pragma CMOD mimic      <fenv.h>
#pragma CMOD link     "-lm"

#pragma CMOD defexp SIZE  = sizeof(fenv_t)
#pragma CMOD defexp ALIGN = _Alignof(fenv_t)

#pragma CMOD defreg \2=\(FE_\([A-Z_0-9]*\)\)

#pragma CMOD alias get    = fegetenv
#pragma CMOD alias set    = fesetenv
#pragma CMOD alias update = feupdateenv
#pragma CMOD alias hold   = feholdexcept
#pragma CMOD alias raise  = feraiseexcept
#pragma CMOD alias test   = fetestexcept
#pragma CMOD alias clear  = feclearexcept

#pragma CMOD declaration

typedef struct fenv fenv_t;

int get(fenv *envp);
int hold(fenv *envp);
int set(const fenv *envp);
int update(const fenv *envp);
int clear(int excepts);
int raise(int excepts);
int test(int excepts);
