/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ∷
#pragma CMOD composer  —

#pragma CMOD declaration

#define PARENTHESIS(...)  (__VA_ARGS__)
#define IGNORE(...)
#define CASE(...) case __VA_ARGS__
#define ZERO(...) 0
#define ONE(...)  1
#define BIT(...)  (1ULL<<(__VA_ARGS__))
#define TWICE(...)  __VA_ARGS__, __VA_ARGS__

#define STRINGIFY(...)  C∷STRINGIFY(C∷combine∷nothing(__VA_ARGS__))

#pragma CMOD fill   cases∷OUTER = CASES
#pragma CMOD fill   cases∷INNER = CASE
#pragma CMOD defill cases∷SEP = :
#pragma CMOD import cases = ..∷snippet∷map

#pragma CMOD fill   commas∷OUTER  = COMMAS
#pragma CMOD fill   commas∷INNER  = IGNORE
#pragma CMOD import commas    = ..∷snippet∷map

#pragma CMOD fill   colons∷OUTER  = COLONS
#pragma CMOD fill   colons∷INNER  = IGNORE
#pragma CMOD defill colons∷SEP    = :
#pragma CMOD import colons    = ..∷snippet∷map

#pragma CMOD fill   semicolons∷OUTER  = SEMICOLONS
#pragma CMOD fill   semicolons∷INNER  = IGNORE
#pragma CMOD defill semicolons∷SEP  = ;
#pragma CMOD import semicolons    = ..∷snippet∷map

#pragma CMOD fill   blanks∷OUTER  = BLANKS
#pragma CMOD fill   blanks∷INNER  = IGNORE
#pragma CMOD defill blanks∷SEP    = " "
#pragma CMOD import blanks    = ..∷snippet∷map

#pragma CMOD fill   tabs∷OUTER    = TABS
#pragma CMOD fill   tabs∷INNER    = IGNORE
#pragma CMOD defill tabs∷SEP    = "\t"
#pragma CMOD import tabs    = ..∷snippet∷map

#pragma CMOD fill   opens∷OUTER   = OPENS
#pragma CMOD fill   opens∷INNER   = IGNORE
#pragma CMOD defill opens∷SEP   = (
#pragma CMOD import opens   = ..∷snippet∷map

#pragma CMOD fill   closes∷OUTER  = CLOSES
#pragma CMOD fill   closes∷INNER  = IGNORE
#pragma CMOD defill closes∷SEP    = )
#pragma CMOD import closes    = ..∷snippet∷map

#pragma CMOD fill   not∷OUTER   = NOTS
#pragma CMOD fill   not∷INNER   = IGNORE
#pragma CMOD defill not∷SEP   = ¬
#pragma CMOD import not     = ..∷snippet∷map

#pragma CMOD fill   zero∷OUTER    = ZEROS
#pragma CMOD fill   zero∷INNER    = ZERO
#pragma CMOD import zero    = ..∷snippet∷map

#pragma CMOD fill   one∷OUTER   = ONES
#pragma CMOD fill   one∷INNER   = ONE
#pragma CMOD import one     = ..∷snippet∷map

#pragma CMOD fill   bit∷OUTER   = BITS
#pragma CMOD fill   bit∷INNER   = BIT
#pragma CMOD import bit     = ..∷snippet∷map

#pragma CMOD fill   duplicate∷OUTER = DUPLICATE
#pragma CMOD fill   duplicate∷INNER = TWICE
#pragma CMOD import duplicate   = ..∷snippet∷map

#define SET(...)  (C∷combine∷union(BITS(__VA_ARGS__)))

#define ISIN(X, ...)  (‼(BIT(X) & SET(__VA_ARGS__)))

#pragma CMOD definition

/** @cond DOXYGEN **/
static C∷attr∷maybe_unused char const test[] = STRINGIFY(alle, Jahre, wieder);
// FIXME: makeheaders chokes on this
//static C∷attr∷maybe_unused unsigned const ZZZZ = ISIN(1, 4, 7, 10, 3);
/** @endcond **/
