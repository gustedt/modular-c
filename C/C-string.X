/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017-2018 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module   string—64 = C¯string
#pragma CMOD composer —
#pragma CMOD context  «    »
#pragma CMOD import   C¯ullong

#pragma CMOD do L = 16 512 *2

#pragma CMOD declaration

#pragma CMOD defill   S—${L}¯len = ${L}
#pragma CMOD fill   S—${L}¯context = string—${L}
#pragma CMOD import S—${L}    = C¯snippet¯string

#pragma CMOD done

#pragma CMOD declaration

enum { len = 32, };
typedef char str32[len];

#pragma CMOD defill once¯type    = char const
#pragma CMOD defill once¯buffer  = str32
#pragma CMOD defill once¯context = char const*
#pragma CMOD fill once¯once    = once
#pragma CMOD import once         = C¯snippet¯once

#pragma CMOD definition

#pragma CMOD entry main

static
void initialize(str32* s, char const* r) {
  C¯io¯printf("%s\n", C¯mod¯demangle¯FUNC);
  for (C¯size i = 0; i < len-1; ++i)
    (*s)[i] = r[0]+i;
  (*s)[len-1] = 0;
}

static once local = once—INITIALIZER(initialize);

#define set(X, C) once—set((X), (C))

void main(int argc, char const* argv[argc+1]) {
  for (C¯size i = 1; i < argc; ++i) {
    C¯io¯printf("%s\n", argv[i]);
    C¯io¯printf("%s\n", «  "x" * (~~argv[i]) »);
    C¯io¯printf("%s\n", « argv[0] + '-' + argv[i] »);
    C¯io¯printf("%s %s %s\n", argv[i], « argv[i] > "rrrr" » ? ">" : "≤", "rrrr");
    C¯io¯printf("%s\n", « "Oh" ∪ ((argv[i] * i) ⪡ 4 ⪢ 4) + '|' »);
  }
  C¯io¯printf("%s, address %p, repeated %p\n", set(&local, "A"), set(&local, "A"), set(&local, "A"));
}
