/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module dbl   =
#pragma CMOD separator ∷
#pragma CMOD composer —
#pragma CMOD context C∷attribute

#pragma CMOD mimic <math.h>
#pragma CMOD mimic <float.h>

#pragma CMOD defexp SIZE=sizeof(double)
#pragma CMOD defexp ALIGN=_Alignof(double)
#pragma CMOD defexp HAS_SUBNORM=∷DBL_HAS_SUBNORM
#pragma CMOD defexp MANT_DIG=∷DBL_MANT_DIG
#pragma CMOD defexp DECIMAL_DIG=∷DBL_DECIMAL_DIG
#pragma CMOD defexp DIG=∷DBL_DIG
#pragma CMOD define HUGE=HUGE_VAL
#pragma CMOD defexp MIN_EXP=∷DBL_MIN_EXP
#pragma CMOD defexp MIN_10_EXP=∷DBL_MIN_10_EXP
#pragma CMOD defexp MAX_EXP=∷DBL_MAX_EXP
#pragma CMOD defexp MAX_10_EXP=∷DBL_MAX_10_EXP
#pragma CMOD defexp MAX=∷DBL_MAX
#pragma CMOD defexp MIN=∷DBL_MIN
#pragma CMOD defexp TRUE_MIN=∷DBL_TRUE_MIN
#pragma CMOD defexp EPSILON=DBL_EPSILON
#pragma CMOD defexp FAST_FMA=∷FP_FAST_FMA

#pragma CMOD alias acos
#pragma CMOD alias acosh
#pragma CMOD alias asin
#pragma CMOD alias asinh
#pragma CMOD alias atan
#pragma CMOD alias atan2
#pragma CMOD alias atanh
#pragma CMOD alias ceil
#pragma CMOD alias cbrt
#pragma CMOD alias copysign
#pragma CMOD alias cos
#pragma CMOD alias cosh
#pragma CMOD alias erf
#pragma CMOD alias erfc
#pragma CMOD alias exp
#pragma CMOD alias exp2
#pragma CMOD alias expm1
#pragma CMOD alias fabs
#pragma CMOD alias fdim
#pragma CMOD alias floor
#pragma CMOD alias fma—platform = fma
#pragma CMOD alias fmax
#pragma CMOD alias fmin
#pragma CMOD alias fmod
#pragma CMOD alias frexp
#pragma CMOD alias hypot
#pragma CMOD alias ilogb
#pragma CMOD alias ldexp
#pragma CMOD alias lgamma
#pragma CMOD alias llrint
#pragma CMOD alias llround
#pragma CMOD alias log
#pragma CMOD alias log10
#pragma CMOD alias log1p
#pragma CMOD alias log2
#pragma CMOD alias logb
#pragma CMOD alias lrint
#pragma CMOD alias lround
#pragma CMOD alias modf
#pragma CMOD alias nan
#pragma CMOD alias nearbyint
#pragma CMOD alias nextafter
#pragma CMOD alias nexttoward
#pragma CMOD alias pow
#pragma CMOD alias remainder
#pragma CMOD alias remquo
#pragma CMOD alias rint
#pragma CMOD alias round
#pragma CMOD alias scalbln
#pragma CMOD alias scalbn
#pragma CMOD alias sin
#pragma CMOD alias sinh
#pragma CMOD alias sqrt
#pragma CMOD alias tan
#pragma CMOD alias tanh
#pragma CMOD alias tgamma
#pragma CMOD alias trunc

#pragma CMOD define acos
#pragma CMOD define acosh
#pragma CMOD define asin
#pragma CMOD define asinh
#pragma CMOD define atan
#pragma CMOD define atan2
#pragma CMOD define atanh
#pragma CMOD define ceil
#pragma CMOD define cbrt
#pragma CMOD define copysign
#pragma CMOD define cos
#pragma CMOD define cosh
#pragma CMOD define erf
#pragma CMOD define erfc
#pragma CMOD define exp
#pragma CMOD define exp2
#pragma CMOD define expm1
#pragma CMOD define fabs
#pragma CMOD define fdim
#pragma CMOD define floor
#pragma CMOD define fma—platform = fma
#pragma CMOD define fmax
#pragma CMOD define fmin
#pragma CMOD define fmod
#pragma CMOD define frexp
#pragma CMOD define hypot
#pragma CMOD define ilogb
#pragma CMOD define ldexp
#pragma CMOD define lgamma
#pragma CMOD define llrint
#pragma CMOD define llround
#pragma CMOD define log
#pragma CMOD define log10
#pragma CMOD define log1p
#pragma CMOD define log2
#pragma CMOD define logb
#pragma CMOD define lrint
#pragma CMOD define lround
#pragma CMOD define modf
#pragma CMOD define nan
#pragma CMOD define nearbyint
#pragma CMOD define nextafter
#pragma CMOD define nexttoward
#pragma CMOD define pow
#pragma CMOD define remainder
#pragma CMOD define remquo
#pragma CMOD define rint
#pragma CMOD define round
#pragma CMOD define scalbln
#pragma CMOD define scalbn
#pragma CMOD define sin
#pragma CMOD define sinh
#pragma CMOD define sqrt
#pragma CMOD define tan
#pragma CMOD define tanh
#pragma CMOD define tgamma
#pragma CMOD define trunc

#pragma CMOD typedef eval=double_t
#pragma CMOD declaration
typedef double dbl;

#pragma CMOD import C∷snippet∷minmax
#pragma CMOD import C∷snippet∷sum
#pragma CMOD import C∷snippet∷avg
#pragma CMOD import C∷snippet∷equal

// FIXME:
#pragma CMOD declaration

〚std∷const〛 double (acos)(double);
〚std∷const〛 double (acosh)(double);
〚std∷const〛 double (asin)(double);
〚std∷const〛 double (asinh)(double);
〚std∷const〛 double (atan)(double);
〚std∷const〛 double (atan2)(double, double);
〚std∷const〛 double (atanh)(double);
〚std∷const〛 double (cbrt)(double);
〚std∷const〛 double (copysign)(double, double);
〚std∷const〛 double (cos)(double);
〚std∷const〛 double (cosh)(double);
〚std∷const〛 double (erf)(double);
〚std∷const〛 double (erfc)(double);
〚std∷const〛 double (exp)(double);
〚std∷const〛 double (exp2)(double);
〚std∷const〛 double (expm1)(double);
〚std∷const〛 double (fabs)(double);
〚std∷const〛 double (fdim)(double, double);
〚std∷const〛 double (ceil)(double);
〚std∷const〛 double (floor)(double);
〚std∷const〛 double (fma—platform)(double, double, double);
〚std∷const〛 double (fmax)(double, double);
〚std∷const〛 double (fmin)(double, double);
〚std∷const〛 double (fmod)(double, double);
〚std∷pure〛  double (frexp)(double, int*);
〚std∷const〛 double (hypot)(double, double);
〚std∷const〛 double (ldexp)(double, int);
〚std∷const〛 double (lgamma)(double);
〚std∷const〛 double (log)(double);
〚std∷const〛 double (log10)(double);
〚std∷const〛 double (log1p)(double);
〚std∷const〛 double (log2)(double);
〚std∷const〛 double (logb)(double);
〚std∷pure〛 double (modf)(double, double[1]);
〚std∷pure〛 double (nan)(const char[1]);
〚std∷const〛 double (nearbyint)(double);
〚std∷const〛 double (nextafter)(double, double);
〚std∷const〛 double (nexttoward)(double, long double);
〚std∷const〛 double (pow)(double, double);
〚std∷const〛 double (remainder)(double, double);
〚std∷pure〛  double (remquo)(double, double, int[1]);
〚std∷const〛 double (rint)(double);
〚std∷const〛 double (round)(double);
〚std∷const〛 double (scalbln)(double, long int);
〚std∷const〛 double (scalbn)(double, int);
〚std∷const〛 double (sin)(double);
〚std∷const〛 double (sinh)(double);
〚std∷const〛 double (sqrt)(double);
〚std∷const〛 double (tan)(double);
〚std∷const〛 double (tanh)(double);
〚std∷const〛 double (tgamma)(double);
〚std∷const〛 double (trunc)(double);
〚std∷const〛 int (ilogb)(double);
〚std∷const〛 long int (lrint)(double);
〚std∷const〛 long int (lround)(double);
〚std∷const〛 long long int (llrint)(double);
〚std∷const〛 long long int (llround)(double);

#define ε    EPSILON
#define π    PI
#define two—π    two—PI
#define ⅟—π    inv—PI
#define ⅟—2—π    inv—two—PI
#define ⅟—sqrt—π inv—sqrt—PI
#define sqrt—π   sqrt—PI
#define two—⅟—sqrt—π two—inv—sqrt—PI
#define ⅟—sqrt—2  inv—sqrt—2

#define Γ    tgamma
#define ln—Γ   lgamma

#if FAST_FMA ∧ ¬defined(fma—builtin) ∧ C∷builtin(C∷builtin∷fma)
# define fma—builtin(X, Y, Z) C∷builtin∷fma((X), (Y), (Z))
#endif

#ifndef fma—builtin
#define fma—builtin(X, Y, Z) fma—platform((X), (Y), (Z))
#endif

inline
〚std∷const〛
double fma(double x, double y, double z) {
  // makeheaders misses this one
  extern 〚std∷const〛 double fma—platform(double, double, double);
  return fma—builtin(x, y, z);
}

inline
〚std∷const〛
double fma—fast(double x, double y, double z) {
#if FAST_FMA
  return fma—builtin(x, y, z);
#else
  return x*y + z;
#endif
}

#pragma CMOD constant PI      = 4*atan(1)
#pragma CMOD constant two—PI    = 2*atan(1)
#pragma CMOD constant inv—PI    = 0.25/atan(1)
#pragma CMOD constant inv—two—PI    = 0.5/atan(1)
#pragma CMOD constant sqrt—PI   = 2*sqrt(atan(1))
#pragma CMOD constant inv—sqrt—PI   = 0.5/sqrt(atan(1))
#pragma CMOD constant two—inv—sqrt—PI = 1/sqrt(atan(1))
#pragma CMOD constant sqrt—2    = sqrt(2)
#pragma CMOD constant inv—sqrt—2    = 1/sqrt(2)
#pragma CMOD constant e     = exp(1)
#pragma CMOD constant log2e   = (double)(1/log(2))
#pragma CMOD constant log10e    = (double)(1/log(10))
#pragma CMOD constant ln2     = log(2)
#pragma CMOD constant ln10    = log(10)

