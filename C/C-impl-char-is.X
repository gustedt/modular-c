/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ¯
#pragma CMOD composer —
#pragma CMOD import    top  = ..
#pragma CMOD import    table  = top¯table

#pragma CMOD declaration

// ++++++++++++++++++++++++++++++++++++
#pragma CMOD foreach T = alnum alpha blank cntrl digit graph lower print punct space upper xdigit

/**
 ** @brief Character class ${T} in the C locale.
 **
 ** In some locales character classification might be changed. If you
 ** need such locale specific behavior, use C::impl::char::is${T},
 ** instead.
 **/
inline
bool ${T}(int tok) {
  unsigned t = tok;
  // If the class is contiguous, the test may just use unsigned magic
  // to test if tok is inside the interval.
  if (table¯${T}—max ≡ table¯${T}—min + table¯${T}—amount - 1) {
    return (t-table¯${T}—min < table¯${T}—amount);
  } else {
    unsigned pos = t/64;
    unsigned bit = t%64;
    table¯ufast64 val = 1ULL << bit;
    switch (pos) {
      // We unroll to force const propagation of each of the 64bit words
      // in PUNCT
#pragma CMOD do POS = 16
#if ((table¯MAX+1)/64) > ${POS}
    case ${POS}:  return val ∩ table¯${T}.b[${POS}];
#endif
#pragma CMOD done
    //
    default: return false;
    }
  }
}


#pragma CMOD done
// ++++++++++++++++++++++++++++++++++++

#pragma CMOD definition

int test—uA(void) {
  return upper('A');
}

int test—lz(void) {
  return lower('z');
}

int test—ua(void) {
  return upper('a');
}

int test—lZ(void) {
  return lower('Z');
}

// All the following should be false.

int test—uA1(void) {
  return upper('A'-1);
}

int test—lz1(void) {
  return lower('z'+1);
}

int test—ua1(void) {
  return upper('a'-1);
}

int test—lZ1(void) {
  return lower('Z'+1);
}
