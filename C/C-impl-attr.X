/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ∷

/**
 ** @module
 ** @brief This is an artificial module hierarchy that implements the
 ** "namespace" for attributes.
 **
 ** There are two other empty levels in this hierarchy @c _ZN∷_C that
 ** are just there to help the name composition of
 ** attributes. Underneath is the @c std hierarchy that just links to
 ** @c C∷attr. Thereby we only maintain the @c C∷attr hierarchy and @c
 ** C∷impl∷attr∷std follows automatically.
 **/
