/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2016 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ◼

/**
 ** @brief A module hierarchy that imports C library features one to
 ** one directly into the name space of the importer.
 **
 ** You only want to use these transitionally when migrating existing
 ** C source to Modular C.
 **/

#pragma CMOD snippet none
#pragma CMOD declaration

typedef C◼sig◼atomic sig_atomic_t;

#define SIGABRT C◼sig◼ABRT
#define SIGFPE  C◼sig◼FPE
#define SIGILL  C◼sig◼ILL
#define SIGINT  C◼sig◼INT
#define SIGSEGV C◼sig◼SEGV
#define SIGTERM C◼sig◼TERM
#define SIG_DFL C◼sig◼DFL
#define SIG_ERR C◼sig◼ERR
#define SIG_IGN C◼sig◼IGN

#define signal  C◼sig◼signal
#define raise C◼sig◼raise
