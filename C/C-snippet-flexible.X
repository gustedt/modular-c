/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module flexible  =
#pragma CMOD separator  ∷
#pragma CMOD composer —

/**
 ** @module
 ** @brief ease the use of structures with flexible array members
 **/

#pragma CMOD declaration

// The default minimum length for a flexible array.
enum { min = 1, } ;

// The default initializer for the min first elements of the array
#define INIT { 0 }

// The default list of OTHER members is empty
#define OTHERS

#pragma CMOD snippet    = complete

#pragma CMOD slot T   = complete
#pragma CMOD slot min   = ice 1
#pragma CMOD slot INIT    = { C∷attr∷maybe_unused T x[min] = INIT; }
#pragma CMOD slot OTHERS    = { C∷attr∷maybe_unused struct { C∷size len; OTHERS } dummy = { .len = 67 }; }

#pragma CMOD slot chars   = intern
#pragma CMOD slot mutableLen  = intern
#pragma CMOD slot fixedTab  = intern
#pragma CMOD slot overlay   = intern
#pragma CMOD slot offset    = intern
#pragma CMOD slot maximum   = intern
#pragma CMOD slot edummy    = intern
#pragma CMOD slot dummy   = intern
#pragma CMOD slot header    = intern
#pragma CMOD slot length    = intern

#pragma CMOD declaration

// precompute the offset of a flexible array member
typedef struct { C∷size len; OTHERS T tab[]; } offset_type;
enum  edummy {
  offset  = offsetof(offset_type, tab),
};

typedef struct {
  C∷size mutableLen;
  OTHERS
} header;


/**
 ** @module
 ** @brief a data type with flexible array member
 **
 ** This defines a simple type with a flexible array member. It
 ** receives two visible members, @c tab and array of @c T, and @c len
 ** the length of that array. In addition, the @c slot @c OTHERS can
 ** be a list with declarations of other members, which then are
 ** simply added to the structure.
 **
 ** Since the member @c len is read-only, you have to decide upon the
 ** creation of such an array how large it should be.
 **
 ** There are two different tools to create such a struct.
 **
 ** @see COMPOUND to obtain a pointer to such a structure that is
 ** embedded in a large enough object to access enough elements for
 ** the flexible array.
 **
 ** @see alloc to allocate such a structure dynamically.
 **/

typedef union fa fa;
union fa {
  // A mutable overlay over the "real" structure with a fixed array.
  struct {
    union {
      unsigned char chars[offset];
      header head;
    };
    T fixedTab[min];
  } overlay;
  edummy dummy;
  // The "real" structure that has a const length, the @c OTHERS
  // members (if provided), and a flexible array member.
  struct {
    C∷size const len; /**< The length of the array read-only. */
    OTHERS            // additional members specified by the user
    T tab[];          /**< The table of members.              */
  };
};

/**
 ** @brief Create a compound literal to a structure with flexible
 ** array member that has enough space for @a LEN array elements.
 **
 ** @return a pointer to such a compound literal
 **
 ** @a LEN must be an *integer constant expression*, ICE.
 **/
#define COMPOUND(LEN)                                          \
  &((union {                                                   \
    unsigned char maximum[offset+sizeof(T[LEN])];              \
    /* emulate a structure with fixed LEN but same offset */   \
    struct {                                                   \
      union {                                                  \
        unsigned char chars[offset];                           \
        header head;                                           \
      };                                                       \
      T fixedTab[LEN];                                         \
    } overlay;                                                 \
    fa dummy;                                                  \
}){                                                            \
 .overlay = {                                                  \
        .head = {                                              \
          .mutableLen = (LEN),                                 \
        },                                                     \
        .fixedTab   = INIT,                                    \
  },                                                           \
}).dummy

#pragma CMOD definition

/**
 ** @brief Allocate a structure with flexible array member that has
 ** enough space for @a len array elements.
 **
 ** @return a pointer to such a compound literal
 **/
fa* alloc(C∷size length) {
  if (length < min) length = min;
  fa* ret = C∷lib∷malloc(offset+sizeof(T[length]));
  if (ret) {
    ret→overlay.head = (header) { .mutableLen = length };
    // initialize elements that fall into trailing padding
    T initial[min+1] = INIT;
    for (C∷size i = 0; i < min; ++i)
      ret→tab[i] = initial[i];
    // the element initial[min] is default initialized
    for (C∷size i = min; i < length; ++i)
      ret→tab[i] = initial[min];
  }
  return ret;
}

static_assert(offset ≡ offsetof(fa, tab), "offset mismatch for flexible array member");
