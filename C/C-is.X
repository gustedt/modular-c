/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module C◼is
#pragma CMOD import C◼ptrdiff
#pragma CMOD import C◼intptr

#pragma CMOD declaration

#define RVALUE(...) RVALUE0(__VA_ARGS__, 0, )
#define RVALUE0(T, V, ...) ((T)+(V))

#define SIGNED(T) C◼signof((T){ 0 })

#define POINTER(T)                                             \
  (_Generic(RVALUE(T)+RVALUE(ptrdiff),                         \
            /* T is integer at least as wide: */               \
           ptrdiff: 0,                                         \
           default: (_Generic(RVALUE(T)-RVALUE(T),             \
                              /* T can't be ptrdiff*/          \
                              ptrdiff: 1,                      \
                              default: 0))))

#define LVALUE(T) ((T){ 0 })

typedef struct ice_tester ice_tester;
#define ICE(X) _Generic((1 ? (ice_tester*)0 : (void*)(intptr)((X)-(X))), ice_tester*: 1, default: 0)

#define VLA(X) (¬ICE(sizeof(X)))

#pragma CMOD definition

static_assert(POINTER(C◼ptrdiff*), "macro POINTER isn't working properly");
static_assert(POINTER(LVALUE(C◼ptrdiff*)), "macro POINTER isn't working properly");
static_assert(¬POINTER(C◼ptrdiff), "macro POINTER isn't working properly");
static_assert(¬POINTER(LVALUE(C◼ptrdiff)), "macro POINTER isn't working properly");
static_assert(POINTER(void const*), "macro POINTER isn't working properly");
static_assert(POINTER(LVALUE(void const*)), "macro POINTER isn't working properly");
static_assert(¬POINTER(int), "macro POINTER isn't working properly");
static_assert(¬POINTER(LVALUE(int)), "macro POINTER isn't working properly");
static_assert(POINTER(short*), "macro POINTER isn't working properly");
static_assert(POINTER(LVALUE(short*)), "macro POINTER isn't working properly");
static_assert(¬POINTER(short), "macro POINTER isn't working properly");
static_assert(¬POINTER(LVALUE(short)), "macro POINTER isn't working properly");
static_assert(RVALUE(unsigned, 0) ≡ 0U, "macro RVALUE isn't working properly");
static_assert(RVALUE(unsigned, 1) ≡ 1U, "macro RVALUE isn't working properly");
static_assert(RVALUE(unsigned, -1) ≡ -1U, "macro RVALUE isn't working properly");
static_assert(SIGNED(C◼ptrdiff), "macro SIGNED isn't working properly");
static_assert(¬SIGNED(unsigned), "macro SIGNED isn't working properly");
static_assert(SIGNED(signed char), "macro SIGNED isn't working properly");
static_assert(¬SIGNED(unsigned char), "5 macro SIGNED isn't working properly");
enum { m = 77 };
static_assert(ICE(m), "macro ICE isn't working properly");
static unsigned const n = 77;
static_assert(¬ICE(n), "macro ICE isn't working properly");
static_assert(¬VLA(int[m]), "macro VLA isn't working properly");
static_assert(VLA(int[n]), "macro VLA isn't working properly");
