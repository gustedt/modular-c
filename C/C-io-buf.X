/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD separator ¯
#pragma CMOD import io = ..
#pragma CMOD import top = ..¯..

/** @module Interfaces to handle stream buffers.
 **
 ** @see C::io::buf::set is the principal interface of this module
 **/

#pragma CMOD mimic <stdio.h>

#pragma CMOD defexp SIZE  = BUFSIZ
#pragma CMOD defrex \2    = \(_IO\([A-Z][A-Z0-9]*\)\)
#pragma CMOD alias  set2  = setbuf
#pragma CMOD alias  set4  = setvbuf

#pragma CMOD declaration

/**
 ** @brief Set buffering of @a s according to @a buf with fixed size.
 **
 ** If @a buf is a null pointer, buffering is switched off.  If it is
 ** not a null pointer, it must have a size of at least io¯buf¯SIZE
 ** bytes.
 **/
void set2(io *restrict s, char *restrict buf);

/**
 ** @brief Set buffering of @a s according to @a buf with dynamic @a
 ** size and specific @a mode.
 **
 ** If @a buf is a null pointer, buffering is switched off.
 **/
int set4(io *restrict s, char *restrict buf, int mode, C¯size size);

/**
 ** @brief A generic interface to set the buffering properties of an
 ** IO stream.
 **
 ** Its behavior depends on the number of arguments that it receives,
 ** according that number it calls @c C::io::set1, @c C::io::set2, @c
 ** C::io::set3 or @c C::io::set4, respectively.
 **/
#define set(...) C¯comma¯generic¯call((set1, set2, set3, set4), __VA_ARGS__)

/**
 ** @brief Set buffering of @a s according to @a buf with dynamic @a
 ** size.
 **
 ** If @a buf is a null pointer, buffering is switched off.  If it is
 ** not a null pointer, it must have a size of at least @a size bytes.
 **/
inline
void set3(io*restrict s, char*restrict buf, top¯size size) {
  set4(s, buf, buf ? FBF : NBF, size);
}

/**
 ** @brief Set buffering of @a s to line buffering.
 **/
inline
void set1(io*restrict s) {
  set4(s, nullptr, LBF, SIZE);
}
