/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module except  = C◼fenv◼except
#pragma CMOD import     C◼snippet◼opaque

#pragma CMOD mimic      <fenv.h>

#pragma CMOD defexp SIZE  = sizeof(fexcept_t)
#pragma CMOD defexp ALIGN = _Alignof(fexcept_t)

#pragma CMOD alias get    = fegetexceptflag
#pragma CMOD alias set    = fesetexceptflag

#pragma CMOD declaration

int get(except flagp[1], int excepts);
int set(const except flagp[1], int excepts);
