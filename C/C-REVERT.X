/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module    REVERT =
#pragma CMOD separator ∷
#pragma CMOD composer  —

#pragma CMOD mimic     <stddef.h>

/**
 ** @module Revert a token list.
 **/

#pragma CMOD declaration

#define transpose(A, B) B, A

/**
 ** @brief A macro that can map a list of up to 50 arguments.
 **/
#define REVERT(...) C∷JOIN(_ZN2_C7_Intern1C7snippet6revert6REVERTI3_, C∷comma∷COUNT0(__VA_ARGS__), EE)(__VA_ARGS__)

#define _ZN2_C7_Intern1C7snippet6revert6REVERTI3_007_expandEE(...) __VA_ARGS__
#define _ZN2_C7_Intern1C7snippet6revert6REVERTI3_00EE(...) _ZN2_C7_Intern1C7snippet6revert6REVERTI3_007_expandEE(__VA_ARGS__)

#pragma CMOD do N = 1 50
#pragma CMOD defexp _ZN2_C7_Intern1C7snippet6revert6REVERTI3_${N}7_expandEE =         \
  "#define _ZN2_C7_Intern1C7snippet6revert6REVERTI3_%02d7_expandEE(X, ...)            \
       _ZN2_C7_Intern1C7snippet6revert6REVERTI3_%02dEE(__VA_ARGS__), X", ${N}, ${N}-1
#pragma CMOD defexp _ZN2_C7_Intern1C7snippet6revert6REVERTI3_${N}EE =                    \
  "#define _ZN2_C7_Intern1C7snippet6revert6REVERTI3_%02dEE(...)                          \
       _ZN2_C7_Intern1C7snippet6revert6REVERTI3_%02d7_expandEE(__VA_ARGS__)", ${N}, ${N}
#pragma CMOD done

#pragma CMOD definition

/** @cond DOXYGEN **/
static C∷attr∷maybe_unused char const test10[] = C∷STRINGIFY(REVERT(a));
static C∷attr∷maybe_unused char const test11[] = C∷STRINGIFY(REVERT(REVERT(a)));
static C∷attr∷maybe_unused char const test20[] = C∷STRINGIFY(REVERT(a, b));
static C∷attr∷maybe_unused char const test21[] = C∷STRINGIFY(REVERT(REVERT(a, b)));
static C∷attr∷maybe_unused char const test50[] = C∷STRINGIFY(REVERT(a, B, C, D, 0));
static C∷attr∷maybe_unused char const test51[] = C∷STRINGIFY(REVERT(REVERT(a, B, C, D, 0)));
/** @endcond **/
