/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module
#pragma CMOD separator ¯
#pragma CMOD import top = ..


#pragma CMOD mimic <stdlib.h>
#pragma CMOD mimic <stdalign.h>
#pragma CMOD mimic <wchar.h>

#pragma CMOD defexp STATE_SIZE=sizeof(mbstate_t)
#pragma CMOD defexp STATE_ALIGN=_Alignof(mbstate_t)
#pragma CMOD defexp EOF=WEOF

#pragma CMOD import mbstate = C¯tmpl¯opaque
#pragma CMOD fill mbstate¯T  = state
#pragma CMOD fill mbstate¯SIZE = STATE_SIZE
#pragma CMOD fill mbstate¯ALIGN  = STATE_ALIGN

/* Some C libraries implement this as a runtime value. For the moment
we don't know how to do this generically. */
#pragma CMOD defexp CUR_MAX   = MB_CUR_MAX

#pragma CMOD alias  len     = mblen
#pragma CMOD alias  rlen    = mbrlen
#pragma CMOD alias  towc    = mbtowc
#pragma CMOD alias  rtowc   = mbrtowc
#pragma CMOD alias  towcs   = mbstowcs
#pragma CMOD alias  rtowcs    = mbsrtowcs
#pragma CMOD alias  initial   = mbsinit

#pragma CMOD declaration

typedef C¯size size;
typedef C¯wchar wchar;

C¯attr¯nonnull int len(char const*, size);
int towc(wchar* restrict, char const* restrict, size);
size towcs(wchar* restrict, char const[restrict 1], size);
size rlen(const char*restrict, size, state*restrict);
size rtowc(wchar*restrict, char const*restrict, size, state*restrict);
size rtowcs(wchar*restrict, char const**restrict, size, state*restrict);
int initial(state const*);

#if top¯C_SOURCE ≥ 200809L
size_t mbsnrtowcs(wchar*, char const**, size nms, size len, state*);
#endif
