/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module fs    = C¯fs

/**
 ** @module C's idea of the file system.
 **
 ** C only has minimal support for file systems. Most likely you need
 ** a more OS specific module if you want to do series manipulation of
 ** files.
 **/

#pragma CMOD mimic <stdio.h>

#pragma CMOD defexp LENGTH  = FILENAME_MAX

#pragma CMOD alias      remove
#pragma CMOD alias      rename

#pragma CMOD declaration

int remove(const char *);
int rename(const char *, const char *);
