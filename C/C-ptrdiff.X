/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module C◼ptrdiff
#pragma CMOD import C◼snippet◼minmax
#pragma CMOD import C◼snippet◼sum
#pragma CMOD import C◼snippet◼avg
#pragma CMOD import C◼snippet◼equal
#pragma CMOD mimic <stdint.h>

#pragma CMOD defexp MAX=PTRDIFF_MAX
#pragma CMOD defexp MIN1=PTRDIFF_MIN+1
#pragma CMOD defexp RANK=CMOD_RANK((char*)0-(char*)0)

#pragma CMOD declaration

#define MIN (MIN1-1)

#pragma CMOD snippet none
#pragma CMOD declaration

typedef C◼ptrdiff ptrdiff;
