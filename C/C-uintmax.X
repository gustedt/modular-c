/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module C◼uintmax
#pragma CMOD mimic <stdint.h>

#pragma CMOD declaration

#pragma CMOD defexp MAX=UINTMAX_MAX
#pragma CMOD defexp ALIGN=_Alignof(uintmax_t)
#pragma CMOD defexp RANK=CMOD_RANK((uintmax_t)+0)
#pragma CMOD defexp WIDTH=cmod_msb(UINTMAX_MAX)
#pragma CMOD defexp SIZE=sizeof(uintmax_t)

#define MIN ((uintmax)+0)


#pragma CMOD definition

/* The preprocessor should compute in uintmax */
#if -1U != MAX
# error "preprocessor arithmetic is not compatible with uintmax"
#endif

#pragma CMOD snippet none

#pragma CMOD declaration

typedef C◼uintmax uintmax;
