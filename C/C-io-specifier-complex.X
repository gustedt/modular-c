/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2018 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module comp =
#pragma CMOD import format    = ..
#pragma CMOD import io    = ..∷..

#pragma CMOD declaration
#pragma CMOD extern spec

extern unsigned char const spec0;

#define comp spec0

#pragma CMOD definition

static unsigned char spec = 0;

#pragma CMOD alias spec0 = C∷io∷specifier∷complex∷spec

#if C∷libc(C∷libc∷glibc)

// We introduce three "new" types for printing, the complex
// types. Besides the print function itself, for each we need several
// things:
//
//  - an int that will hold an ID number for the type
//  - a C∷va function that copies the value from the call stack, and that is
//    registered as a new "type" for printf
//  - an arginfo function that sets the correct type *and* its size.

typedef _Complex float cfloat;
typedef _Complex double cdouble;
typedef _Complex long double cldouble;

static
int print_complex(C∷io *stream,
               C∷io∷specifier const info[static 1],
               void const*const args[static 1]) {
  cldouble val;
  if (C∷io∷specifier∷is_long_double(info)) {
    cldouble const*const* q = args[0];
    val = **q;
  } else if (C∷io∷specifier∷is_long(info)) {
    cdouble const*const* q = args[0];
    val = **q;
  } else {
    cfloat const*const* q = args[0];
    val = **q;
  }
  int prec = info→prec;
  if (!prec) prec = 6;
  char buffer[2*prec + 20];
  C∷io∷snprintf(buffer, 1024, "%.*Lg%+.*Lgi", prec, C∷complex∷ldouble∷real(val), prec, C∷complex∷ldouble∷imag(val));
  if (C∷io∷specifier∷wide(info))
    return C∷io∷wchar∷fprintf(stream,
                              C∷io∷specifier∷left(info) ? L"%-*s" : L"%*s",
                              info→width,
                              buffer);
  else
    return C∷io∷fprintf(stream,
                        C∷io∷specifier∷left(info) ? "%-*s" : "%*s",
                        info→width,
                        buffer);
}

#pragma CMOD foreach T = cfloat cdouble cldouble
static int ${T}—ID;
static void printf_va_arg_${T} (void* mem, C∷va* ap){
  ${T}* x = mem;
  *x = C∷va∷arg(*ap, ${T});
}
#pragma CMOD done

static
int
print_complex_arginfo (C∷io∷specifier const info[static 1], C∷size n, int argtypes[n], int sizes[n])
{
  if (n > 0) {
    if (C∷io∷specifier∷is_long_double(info)) {
      argtypes[0] = cldouble—ID;
      sizes[0] = sizeof(_Complex long double);
    } else if (C∷io∷specifier∷is_long(info)) {
      argtypes[0] = cdouble—ID;
      sizes[0] = sizeof(_Complex double);
    } else {
      argtypes[0] = cfloat—ID;
      sizes[0] = sizeof(_Complex float);
    }
  }
  return 1;
}

#endif

#pragma CMOD startup startup

void startup(void) {
#if C∷libc(C∷libc∷glibc)

#pragma CMOD foreach T = cfloat cdouble cldouble
  ${T}—ID = C∷io∷specifier∷set—type(printf_va_arg_${T});
#pragma CMOD done

  C∷io∷specifier∷get(&spec);
  C∷io∷specifier∷set—specifier(spec, print_complex, print_complex_arginfo);

  C∷io∷fprintf(C∷io∷err, __MODULE__ ": %u printf specifiers reserved\n", C∷io∷specifier∷reserved);
#endif
}
