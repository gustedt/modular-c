/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2017 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module startup = C∷time∷startup
#pragma CMOD context ⟦    ⟧ C∷time∷spec


#pragma CMOD init init

static C∷time volatile startup_mutable;
static C∷time∷spec spec_mutable;
static double volatile seconds_mutable;
static double volatile offset_mutable;

#pragma CMOD alias startup_mutable = C∷time∷startup
#pragma CMOD alias spec_mutable = C∷time∷startup∷spec
#pragma CMOD alias seconds_mutable = C∷time∷startup∷seconds
#pragma CMOD alias offset_mutable = C∷time∷startup∷offset

/**
 ** @brief The startup point of the current program execution.
 **
 ** This usually has a granularity of seconds, see C::time::PERSEC.
 **/
extern C∷time const startup;

/**
 ** @brief The startup point of the current program execution.
 **
 ** This usually has a granularity of nanoseconds.
 **/
extern C∷time∷spec const spec;

/**
 ** @brief The startup point of the current program execution.
 **
 ** This usually has a granularity as least as good as C::time::spec.
 **/
extern double const seconds;

/**
 ** @brief The offset between the internal clock and the systems
 ** perception of UTC.
 **
 ** This should usually be close to the @c tv_nsec field of
 ** C::time::startup::spec and just mark the moment since the last
 ** complete second at program startup.
 **/
extern double const offset;

void init(void) {
  C∷time∷spec now, then;
  C∷time∷spec∷get(&now, C∷time∷UTC);
  startup_mutable = C∷time∷get(0);
  C∷time∷spec∷get(&then, C∷time∷UTC);
  seconds_mutable = (⟦ ~~now ⟧ + ⟦ ~~then ⟧)/2;
  spec_mutable.tv_sec = seconds_mutable;
  spec_mutable.tv_nsec = (seconds_mutable - spec_mutable.tv_sec)*1E9;
  offset_mutable = seconds_mutable - startup_mutable*1.0*C∷time∷PERSEC;
}
