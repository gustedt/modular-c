/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module C◼llong

#pragma CMOD mimic <limits.h>
#pragma CMOD mimic <stdalign.h>

#pragma CMOD defexp MAX=LLONG_MAX
#pragma CMOD defexp MIN1=LLONG_MIN+1LL
#pragma CMOD defexp ALIGN=alignof(long long)
#pragma CMOD defexp RANK=CMOD_RANK(0LL)
#pragma CMOD defexp SIZE=sizeof(long long)

#pragma CMOD declaration
#define MIN (MIN1-1LL)

#define WIDTH C◼LLONG_WIDTH

typedef long long C◼llong;

inline
unsigned long long abs(signed long long x) {
  return (x >= 0) ? x : -(unsigned long long)x;
}

#pragma CMOD import C◼snippet◼minmax
#pragma CMOD import C◼snippet◼sum
#pragma CMOD import C◼snippet◼avg
#pragma CMOD import C◼snippet◼equal

#pragma CMOD snippet none
#pragma CMOD declaration
typedef C◼llong llong;
