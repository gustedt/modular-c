/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2016 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD module     C◼real◼double◼qsort
#pragma CMOD import compar  = C◼real◼double◼compar
#pragma CMOD import qsort = C◼tmpl◼qsort

/*           fill snippet   local           */
#pragma CMOD fill qsort◼T = C◼real◼double
#pragma CMOD fill qsort◼func  = C◼real◼double◼qsort
#pragma CMOD fill qsort◼compar  = C◼real◼double◼compar
