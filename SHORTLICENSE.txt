
This file is free software; it is part of the Modular C project.

You can redistribute it and/or modify it under the terms of the BSD
License as given in the file LICENSE.txt. It is distributed without
any warranty; without even the implied warranty of merchantability or
fitness for a particular purpose.
