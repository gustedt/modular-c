// equality
#define «A==B» equal((A), (B))
// comparison
#define «A<B» less((A), (B))
#define «A>B» greater((A), (B))
#define «A<=B» less((A), (B))
#define «A>=B» greater((A), (B))
// arithmetic:
// rewriting rules assume that + and - operators are such that
// A - A == 0
// A + 0 == 0 + A == 0
// ((A + x) - x) == (A + (x - x)) == (A + 0) == A
// ((A - x) + x) == (A + (-x + x)) == (A + ((0-x) + x)) == A
#define «A+B» plus((A), (B))
#define «A-B» minus((A), (B))
// rewriting rules assume that *, / and % operators are such that
// A*0 == 0*A == (T){ 0 }
// A * 1 == 1 * A == A
// for x with y such that (x*y)*A = A*(x*y) = A for all A
// A/x = A*y
// ((A * x) / x) == (A * (x / x)) == (A * (x*y)) == A
// (A % x) == A - ((A / x) * x)
#define «A*B» mult((A), (B))
#define «A/B» div((A), (B))
#define «A%B» mod((A), (B))
// bitwise operations
#define «A|B» bor((A), (B))
#define «A&B» band((A), (B))
#define «A^B» bxor((A), (B))
#define «~B» bnot(B)
#define «~~B» bnotbnot(B)
#define «A<<B» lshift(A, B)
#define «A>>B» rshift(B)
// logical operation
#define «!!B» notnot(B)
// Examples that may not be overloaded
// assigment is always elementary
#define «A=B» assign((A), (B))
// memory operators are always native
#define «*A»  deref(A)
#define «&A»  addr(A)
// In addition the following will add evaluation contexts and require the !! operator
#define «A?B:C» tern((A), (B), (C))
#define «A&&B» land((A), (B))
#define «A||B» lor((A), (B))
#define «!A» not(A)
// Examples that may not be overloaded because they are always rewritten
#define «A!=B» unequal((A), (B))       // !(A==B)
#define «+B» plus(B)                   // 0 + B
#define «-B» minus(B)                  // 0 - B
// operations that require an lvalue are rewritten as assignment
#define «++B» plusplus(B)                 // B = B + 1
#define «--B» minusminus(B)               // B = B - 1
#define «A++» fetchadd(A, 1)              // ((A = A + 1) - 1)
#define «A--» fetchsub(A, 1)              // ((A = A - 1) + 1)
#define «A+=B» pluseq(A, B)               // A = A + B
#define «A-=B» minuseq(A, B)              // A = A - B
#define «A*=B» timeseq(A, B)              // A = A * B
#define «A/=B» diveq(A, B)                // A = A / B
#define «A%=B» modeq(A, B)                // A = A % B
#define «A|=B» boreq(A, B)                // A = A | B
#define «A&=B» bandeq(A, B)               // A = A & B
#define «A^=B» bxoreq(A, B)               // A = A ^ B
#define «A<<=B» lshifteq(A, B)            // A = A << B
#define «A>>=B» rshifteq(A, B)            // A = A >> B

unsigned «unsigned a[] + signed b» {
  return a[0] + b;
}

_Bool «!!unsigned a» {
  return a>8;
}
unsigned short «~~unsigned a» {
  unsigned short ret = a*5;
  return «~~ret» + «!!a» + «!a»;
}
