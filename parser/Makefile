.NOTPARALLEL :

SOURCES =					\
	parser.X				\
	parser-ops.X				\
	parser-node.X

SOURCES := $(abspath ${SOURCES})
OBJECTS = ${SOURCES:.X=.a}

TARGET = parser-node libparser.a

CMODDIR=../bin
CMODLIB=../lib
CMOD=${CMODDIR}/Cmod
MAKEHEADER=${CMODDIR}/makeheader

TOOLS = ${CMOD} ${MAKEHEADER}

CMODEP = ${CMOD} -M

export CMOD_LEGSEP ?= _

CFLAGS	?= -std=c11 -Wall -O3 -march=native -fno-common -fdata-sections -ffunction-sections ${COPTS}
LDFLAGS ?= -static -Wl,--gc-sections

ifeq (${CMOD_STATIC},)
TARGET   += libparser.so
CFLAGS   += -fPIC
LDSHARED ?= -shared -Bdynamic -lrt -lgcc
else
LDFLAGS += -static
endif

ifeq (${DEBUG},)
	LDFLAGS := ${LDFLAGS} -s
endif

ifneq (${STDATOMIC},)
	CFLAGS := ${CFLAGS} -I- -I ${STDATOMIC}
	LDFLAGS := -L ${STDATOMIC} -l atomic ${LDFLAGS}
endif

%.dep : %.X Makefile ${CMODLIB}/libC.a
	${CMODEP} $*.X -o $*.dep

%.a : %.dep %.X ${CMOD}
	${CMOD} -c ${CFLAGS} ${LDFLAGS} $*.X

% : %.dep %.a ${CMOD} libparser.a
	${CMOD}  $*.a -o $* ${CFLAGS} ${LDFLAGS}

target : ${TARGET}

libparser.a : ${OBJECTS}
	${CMOD}_archive -o $@ $^

libparser.so : ${OBJECTS}
	${CMOD}_archive -o $@ $^

libparser.dep : ${DEPS}
	cat ${DEPS} > libparser.dep


all : depend ${OBJECTS}

clean :
	rm -f ${SOURCES:.X=.dep} ${OBJECTS} ${TARGET}

depend : ${SOURCES:.X=.dep}

-include ${SOURCES:.X=.dep} $${DEPENDCIES}
