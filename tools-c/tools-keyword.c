/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

/**
 ** @file
 **
 ** @brief Tools to handle the keywords that are needed for
 ** header/body classification.
 **/

#line 0 "tools-keyword.X" // preprocessor
#define CMOD_746F6F6C732D6B6579776F72642E58_HEADER_INSTANTIATE
#line 0 "tools-keyword.X" // preprocessor
#include "tools-keyword.h"
#line 10 "tools-keyword.X" // preprocessor
#line 10 "tools-keyword.X" // preprocessor
/* #pragma CMOD declaration */
#line 119 "tools-keyword.X" // preprocessor
/* #pragma CMOD definition */
#line 120 "tools-keyword.X" // preprocessor
#include <limits.h>

#line 122 "tools-keyword.X" // preprocessor
#undef KW_ENTRY
#line 123 "tools-keyword.X" // preprocessor
#define KW_ENTRY(X) [kw_ ## X] = #X

/**
 ** @brief a lexicographic list of all keywords as strings
 **/
#line 128 "tools-keyword.X" // symbol
kw_string const kw_words[] = {
  KW_KEYWORDS,
} ;


#line 132 "tools-keyword.X" // symbol
static_assert(kw_max == (sizeof kw_words / sizeof kw_words[0]), "kw_max and number of elements don't coincide");


#line 134 "tools-keyword.X" // symbol
extern inline properties kw_control(properties k);


/* don't add extern to such declarations */
#line 140 "tools-keyword.X" // symbol
extern inline properties   kw_noextern(properties k);


#line 145 "tools-keyword.X" // symbol
extern inline properties kw_cmod(properties k);


#line 150 "tools-keyword.X" // symbol
extern inline properties kw_internal(properties k);


#line 155 "tools-keyword.X" // symbol
extern inline properties kw_typename(properties k);


#line 160 "tools-keyword.X" // symbol
extern inline properties kw_wide(properties k);


#line 165 "tools-keyword.X" // symbol
extern inline properties kw_narrow(properties k);


#line 170 "tools-keyword.X" // symbol
extern inline properties kw_integer(properties k);


#line 176 "tools-keyword.X" // symbol
extern inline properties kw_floating(properties k);


#line 181 "tools-keyword.X" // symbol
extern inline properties kw_basetype(properties k);


#line 188 "tools-keyword.X" // symbol
extern inline
kw_string const* kw_get(keyword k);


#line 193 "tools-keyword.X" // symbol
extern inline
int kw_compar(void const* a, void const* b);


#line 200 "tools-keyword.X" // symbol
extern inline
kw_string const* kw_bsearch(kw_string const s[static 1],
                            size_t nmemb,
                            kw_string const arr[static nmemb]);


#line 207 "tools-keyword.X" // symbol
extern inline
keyword kw_find(kw_string const w[static 1]);


#line 213 "tools-keyword.X" // symbol
void kw_print(FILE* restrict out, properties prop) {
  char const*restrict format = "%s";
  for (unsigned kw = 0; prop; ++kw, prop >>=1) {
    if (prop % 2) {
      fprintf(out, format, kw_words[kw]);
      format = ", %s";
    }
  }
}

