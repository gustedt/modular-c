/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2018 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

/**
 ** @file
 **
 ** @brief test case for header production and list of symbols
 **
 ** This tests several bad corner cases for parsing C. In most parts,
 ** this is not meant as an example how to program in C, but in the
 ** contrary, how to *not* program in C. In particular, defining
 ** struct, union or enum types inside variable or function
 ** declarations or definitions is really bad practice.
 **
 ** Some of the cases will *not* work with the automatic header
 ** generation. These are only conditionally exposed when the macro
 ** "EXPOSE" is defined. If you compile this with this macro defined
 ** (usually -DEXPOSE on the command line of your compiler) this
 ** should produce meaningful errors and give you line numbers that
 ** correspond to the exact problem location.
 **/

#ifdef __CMOD_CYCLIC_CMOD_TESTCASE_HEADER
#error cyclic inclusion of interface specification
#endif
#define __CMOD_CYCLIC_CMOD_TESTCASE_HEADER
#ifndef __CMOD_INTERNAL_CMOD_TESTCASE_HEADER
#line 1 "<start interface>"
#define __CMOD_INTERNAL_CMOD_TESTCASE_HEADER
#line 34 "testcase.X" // preprocessor
#ifdef EXPOSE
/**
 ** These variables cannot be exposed to the outer world, because an
 ** enumeration type cannot be forward declared.
 **/
// Here we have a private enum type.
// Since enum types can't even be forward declared, this can never work to declare
// external symbol that is based on it.
// You should consider declaring such a symbol static.
#line 39 "testcase.X" // symbol
typedef int __internal_dummy_type_to_be_ignored; // private enum
#line 42 "testcase.X" // symbol
extern *ota,                                     /* continuation */
#line 42 "testcase.X" // symbol
       *itat;

#line 43 "testcase.X" // preprocessor
#endif

/**
 ** A private enum type *alone* should work. It will just not be
 ** exported to the .h file
 **/
// Here we have a private enum type.
// Since enum types can't even be forward declared, this can never work to declare
// external symbol that is based on it.
// You should consider declaring such a symbol static.
#line 49 "testcase.X" // symbol
typedef int __internal_dummy_type_to_be_ignored; // private enum

#line 51 "testcase.X" // preprocessor
#ifdef EXPOSE
/**
 ** A private enum type that declares a variable will not work. Such a
 ** declaration should be static.
 **/
// Here we have a private enum type.
// Since enum types can't even be forward declared, this can never work to declare
// external symbol that is based on it.
// You should consider declaring such a symbol static.
#line 56 "testcase.X" // symbol
typedef int __internal_dummy_type_to_be_ignored; // private enum
#line 56 "testcase.X" // symbol
extern var;

#line 57 "testcase.X" // preprocessor
#endif

/**
 ** But a similar with a typedef should work. All typedef are
 ** internal, unless placed in a "declaration" section.
 **/
typedef int __internal_dummy_type_to_be_ignored; // hiding local typedef


/**
 ** These variables can only be exposed to the outer world, because a
 ** struct type can be forward declared *and* this only declares
 ** pointers to this type. Hiding such declarations as second part of
 ** the struct declaration is really bad style, avoid it.
 **/
// We are typedefing a struct type, such that we can declare the following symbol(s).
// To be exported, such a symbol should be a pointer to tutu.
// If not, you should consider declaring such a symbol static.
typedef struct tutu tutu;
#line 80 "testcase.X" // symbol
extern tutu *tail,                                    /* continuation */
#line 80 "testcase.X" // symbol
       *head;


/**
 ** A static variables or functions should never cause problems and
 ** just not be exported to the header. Good.
 **/
typedef int __internal_dummy_type_to_be_ignored; // hiding static symbol

typedef int __internal_dummy_type_to_be_ignored; // hiding static tagtype
typedef int __internal_dummy_type_to_be_ignored; // hiding static tagtype


/**
 ** A declaration section starts at such a pragma and ends at the next
 ** "definition" pragma.
 **
 ** Generally, it should only contain type or macro definitions, all
 ** the rest is taken care of by the tool.
 **
 ** If you think you must you could also place variable or function
 ** *declarations* here. But normally these be generated from their
 ** definitions.
 **
 ** inline functions are treated special they may appear in both,
 ** declaration or definition sections. If they appear in a definition
 ** section they will be instantiated automatically in the .c file
 ** produced by tools-def. If it is in the declaration section as
 ** here, you'd have to define a special macro for that case. For this
 ** example this would be CMOD_TESTCASE_HEADER_INSTANTIATE.
 **/

#line 112 "testcase.X" // preprocessor
#if EXPOSE
#line 113 "testcase.X" // preprocessor
/* #pragma CMOD declaration */
#error mismatch of "declaration" directive at #else, consider adding "definition" directive before this.
#else
#endif

#pragma CMOD declaration

#if 1
#line 120 "testcase.X" // preprocessor
/* #pragma CMOD definition */
#line 121 "testcase.X" // preprocessor
/* #pragma CMOD declaration */
#endif

typedef struct obob obob; struct obob {
  int r;
};

enum usable {
  one,
  two,
}; typedef enum usable usable; 

inline
usable bimbam(void) {
  return two;
}

#if EXPOSE
#line 1 "<CMOD_TESTCASE_HEADER inline collect>"
#ifdef CMOD_TESTCASE_HEADER_INSTANTIATE
extern inline
usable bimbam(void) ;
#endif

#line 139 "testcase.X" // preprocessor
/* #pragma CMOD definition */
#error mismatch of "definition" directive at #endif, consider adding "declaration" directive before this.
#line 140 "testcase.X" // preprocessor
#endif

#line 142 "testcase.X" // preprocessor
/* #pragma CMOD definition */

/**
 ** Testing different forms of function declarations. Such function
 ** declarations should be produced automatically by the tool, so we
 ** don't expose them.
 **
 ** Syntactically challenging are declarations that also protect the
 ** identifier that is declared against macro protection by using
 ** additional parenthesis.
 **/
#line 153 "testcase.X" // preprocessor
#ifndef EXPOSE
// function pointer with basetype
#line 155 "testcase.X" // symbol
extern void (*const ttttt)(void);


// function pointer with named type
#line 158 "testcase.X" // symbol
extern tutu* (*tstttt)(int);


// function pointer with named type
#line 161 "testcase.X" // symbol
extern tutu* (*tsttii)(tutu);


// function name, basetype, macro protected
#line 164 "testcase.X" // symbol
extern void (ttttfff)(void);


// function name, named type, macro protected
#line 167 "testcase.X" // symbol
extern tutu* (ttttggg)(void);


// function name, basetype, macro protected
#line 170 "testcase.X" // symbol
extern int (ttttfffr)(int);


// function name, named type, macro protected
#line 173 "testcase.X" // symbol
extern tutu* (ttttgggr)(double);


// function definition, basetype, macro protected
#line 176 "testcase.X" // symbol
extern void (ttttfff)(void);
#line 179 "testcase.X" // preprocessor
#endif

/**
 ** Testing different forms of function definitions.
 **
 ** Syntactically challenging are definitions that also protect the
 ** identifier that is declared against macro protection by using
 ** additional parenthesis.
 **/

// function definition, named type, macro protected
#line 190 "testcase.X" // symbol
extern tutu* (ttttggg)(void);

// function definition, basetype, macro protected
#line 195 "testcase.X" // symbol
extern int (ttttfffr)(int a);

// function definition, named type, macro protected
#line 200 "testcase.X" // symbol
extern tutu* (ttttgggr)(double b);

// function definition, struct type
#line 205 "testcase.X" // symbol
extern struct tutu ttttgggs(double c);

// function definition, struct type, macro protected
#line 210 "testcase.X" // symbol
extern struct tutu (ttttgggsp)(double d);

// function definition, named type, macro protected, inline
#line 215 "testcase.X" // symbol
inline tutu* (ttttgggi)(void) {
  return 0;
}

// function definition, basetype, macro protected, inline
#line 220 "testcase.X" // symbol
inline int (ttttfffri)(int a) {
  return a;
}

// function definition, named type, macro protected, inline
#line 225 "testcase.X" // symbol
inline tutu* (ttttgggri)(double b) {
  return 0;
}

// function definition, struct type, inline
#line 230 "testcase.X" // symbol
inline struct obob ttttgggsi(double c) {
  return (obob){ 0 };
}

// function definition, struct type, macro protected, inline
#line 235 "testcase.X" // symbol
inline struct obob (ttttgggspi)(double d) {
  return (obob){ 0 };
}

// function definition, struct type
// really bad style
// We are typedefing a struct type, such that we can declare the following symbol(s).
// To be exported, such a symbol should be a pointer to op.
// If not, you should consider declaring such a symbol static.
typedef struct op op;
#line 243 "testcase.X" // symbol
extern op *ttttgggss(double e);

// function definition, struct type, macro protected
// really bad style
// We are typedefing a struct type, such that we can declare the following symbol(s).
// To be exported, such a symbol should be a pointer to ap.
// If not, you should consider declaring such a symbol static.
typedef struct ap ap;
#line 251 "testcase.X" // symbol
extern ap * (ttttgggssp)(double f);

#line 255 "testcase.X" // preprocessor
#ifdef EXPOSE
// function definition, anonymous struct type
//
// Because the return type is anonymous, nobody can call this function
// without external knowledge about the exact definition of the return
// type.
// We can't typedef an anonymous struct.
// Consider declaring symbols of that type static.
#line 261 "testcase.X" // symbol
#error external symbol with internal type, consider declaring it static
#line 263 "testcase.X" // symbol
extern *ttttgggsa(double g);
#line 266 "testcase.X" // preprocessor
#endif

#line 268 "testcase.X" // preprocessor
#ifdef EXPOSE
// function definition, anonymous struct type, macro protected
//
// Because the return type is anonymous, nobody can call this function
// without external knowledge about the exact definition of the return
// type.
// We can't typedef an anonymous struct.
// Consider declaring symbols of that type static.
#line 274 "testcase.X" // symbol
#error external symbol with internal type, consider declaring it static
#line 276 "testcase.X" // symbol
extern *(ttttgggspa)(double h);
#line 279 "testcase.X" // preprocessor
#endif

#line 281 "testcase.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control

#line 283 "testcase.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control

#line 285 "testcase.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control

#endif /* __CMOD_INTERNAL_CMOD_TESTCASE_HEADER */
#undef __CMOD_CYCLIC_CMOD_TESTCASE_HEADER
