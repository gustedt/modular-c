/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2018 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

/**
 ** @file
 **
 ** @brief test case for header production and list of symbols
 **
 ** This tests several bad corner cases for parsing C. In most parts,
 ** this is not meant as an example how to program in C, but in the
 ** contrary, how to *not* program in C. In particular, defining
 ** struct, union or enum types inside variable or function
 ** declarations or definitions is really bad practice.
 **
 ** Some of the cases will *not* work with the automatic header
 ** generation. These are only conditionally exposed when the macro
 ** "EXPOSE" is defined. If you compile this with this macro defined
 ** (usually -DEXPOSE on the command line of your compiler) this
 ** should produce meaningful errors and give you line numbers that
 ** correspond to the exact problem location.
 **/

#ifdef EXPOSE
/**
 ** These variables cannot be exposed to the outer world, because an
 ** enumeration type cannot be forward declared.
 **/
enum toto {
  a,
  b = sizeof(int),
  c = 7 } *ota = 0, *itat;
#endif

/**
 ** A private enum type *alone* should work. It will just not be
 ** exported to the .h file
 **/
enum { another0 };

#ifdef EXPOSE
/**
 ** A private enum type that declares a variable will not work. Such a
 ** declaration should be static.
 **/
enum { yetanother0withvariable } var;
#endif

/**
 ** But a similar with a typedef should work. All typedef are
 ** internal, unless placed in a "declaration" section.
 **/
typedef enum hoho {
  dddd,
  oooo,
} ohoh;

/**
 ** These variables can only be exposed to the outer world, because a
 ** struct type can be forward declared *and* this only declares
 ** pointers to this type. Hiding such declarations as second part of
 ** the struct declaration is really bad style, avoid it.
 **/
struct tutu {
  // this is only possible if we have the typedef from the
  // automatically produced header, otherwise we would have to use
  // "struct tutu"
  tutu* next;
  int ui;
} *tail = { 0 }, *head = 0;

/**
 ** A static variables or functions should never cause problems and
 ** just not be exported to the header. Good.
 **/
static int poiu;

static union hihi {
  int dd;
  unsigned r;
} sthihi = { .dd = 1, };

/**
 ** A declaration section starts at such a pragma and ends at the next
 ** "definition" pragma.
 **
 ** Generally, it should only contain type or macro definitions, all
 ** the rest is taken care of by the tool.
 **
 ** If you think you must you could also place variable or function
 ** *declarations* here. But normally these be generated from their
 ** definitions.
 **
 ** inline functions are treated special they may appear in both,
 ** declaration or definition sections. If they appear in a definition
 ** section they will be instantiated automatically in the .c file
 ** produced by tools-def. If it is in the declaration section as
 ** here, you'd have to define a special macro for that case. For this
 ** example this would be CMOD_TESTCASE_HEADER_INSTANTIATE.
 **/

#if EXPOSE
#pragma CMOD declaration
#else
#endif

#pragma CMOD declaration

#if 1
#pragma CMOD definition
#pragma CMOD declaration
#endif

struct obob {
  int r;
};

enum usable {
  one,
  two,
};

inline
usable bimbam(void) {
  return two;
}

#if EXPOSE
#pragma CMOD definition
#endif

#pragma CMOD definition

/**
 ** Testing different forms of function declarations. Such function
 ** declarations should be produced automatically by the tool, so we
 ** don't expose them.
 **
 ** Syntactically challenging are declarations that also protect the
 ** identifier that is declared against macro protection by using
 ** additional parenthesis.
 **/
#ifndef EXPOSE
// function pointer with basetype
void (*const ttttt)(void);

// function pointer with named type
tutu* (*tstttt)(int);

// function pointer with named type
tutu* (*tsttii)(tutu);

// function name, basetype, macro protected
void (ttttfff)(void);

// function name, named type, macro protected
tutu* (ttttggg)(void);

// function name, basetype, macro protected
int (ttttfffr)(int);

// function name, named type, macro protected
tutu* (ttttgggr)(double);

// function definition, basetype, macro protected
void (ttttfff)(void) {
  // nothing
}
#endif

/**
 ** Testing different forms of function definitions.
 **
 ** Syntactically challenging are definitions that also protect the
 ** identifier that is declared against macro protection by using
 ** additional parenthesis.
 **/

// function definition, named type, macro protected
tutu* (ttttggg)(void) {
  return 0;
}

// function definition, basetype, macro protected
int (ttttfffr)(int a) {
  return a + poiu;
}

// function definition, named type, macro protected
tutu* (ttttgggr)(double b) {
  return 0;
}

// function definition, struct type
struct tutu ttttgggs(double c) {
  return (tutu){ 0 };
}

// function definition, struct type, macro protected
struct tutu (ttttgggsp)(double d) {
  return (tutu){ 0 };
}

// function definition, named type, macro protected, inline
inline tutu* (ttttgggi)(void) {
  return 0;
}

// function definition, basetype, macro protected, inline
inline int (ttttfffri)(int a) {
  return a;
}

// function definition, named type, macro protected, inline
inline tutu* (ttttgggri)(double b) {
  return 0;
}

// function definition, struct type, inline
inline struct obob ttttgggsi(double c) {
  return (obob){ 0 };
}

// function definition, struct type, macro protected, inline
inline struct obob (ttttgggspi)(double d) {
  return (obob){ 0 };
}

// function definition, struct type
// really bad style
struct op {
  int o;
} *ttttgggss(double e) {
  return 0;
}

// function definition, struct type, macro protected
// really bad style
struct ap {
  int o;
} * (ttttgggssp)(double f) {
  return 0;
}

#ifdef EXPOSE
// function definition, anonymous struct type
//
// Because the return type is anonymous, nobody can call this function
// without external knowledge about the exact definition of the return
// type.
struct {
  int o;
} *ttttgggsa(double g) {
  return 0;
}
#endif

#ifdef EXPOSE
// function definition, anonymous struct type, macro protected
//
// Because the return type is anonymous, nobody can call this function
// without external knowledge about the exact definition of the return
// type.
struct {
  int o;
} *(ttttgggspa)(double h) {
  return 0;
}
#endif

# 1 @S454578@
# define substance

# 1 "testerline"
# define functionlike(A)

#line 1 "testline"
# define functionunlike (A)

