/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

/**
 ** @file
 **
 ** @brief Tools to handle the keywords that are needed for
 ** header/body classification.
 **/

#ifdef __CMOD_CYCLIC_CMOD_746F6F6C732D6B6579776F72642E58_HEADER
#error cyclic inclusion of interface specification
#endif
#define __CMOD_CYCLIC_CMOD_746F6F6C732D6B6579776F72642E58_HEADER
#ifndef __CMOD_INTERNAL_CMOD_746F6F6C732D6B6579776F72642E58_HEADER
#line 1 "<start interface>"
#define __CMOD_INTERNAL_CMOD_746F6F6C732D6B6579776F72642E58_HEADER
#line 10 "tools-keyword.X" // preprocessor
/* #pragma CMOD declaration */
#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools-bs.h"

/**
 ** @brief handle sets of keywords
 **/
typedef bs properties;

/**
 ** @brief a list of keywords that we need in three different places
 **
 ** @remark this list must remain sorted
 **/
#define KW_KEYWORDS                             \
  KW_ENTRY(CMOD),                               \
  KW_ENTRY(_Alignas),                           \
  KW_ENTRY(_Atomic),                            \
  KW_ENTRY(_Bool),                              \
  KW_ENTRY(_Comma),                             \
  KW_ENTRY(_Complex),                           \
  KW_ENTRY(_Imaginary),                         \
  KW_ENTRY(_Noreturn),                          \
  KW_ENTRY(_Static_assert),                     \
  KW_ENTRY(_Thread_local),                      \
  KW_ENTRY(alignas),                            \
  KW_ENTRY(auto),                               \
  KW_ENTRY(bool),                               \
  KW_ENTRY(char),                               \
  KW_ENTRY(complex),                            \
  KW_ENTRY(const),                              \
  KW_ENTRY(declaration),                        \
  KW_ENTRY(define),                             \
  KW_ENTRY(definition),                         \
  KW_ENTRY(double),                             \
  KW_ENTRY(elif),                               \
  KW_ENTRY(else),                               \
  KW_ENTRY(endif),                              \
  KW_ENTRY(enum),                               \
  KW_ENTRY(extern),                             \
  KW_ENTRY(float),                              \
  KW_ENTRY(if),                                 \
  KW_ENTRY(ifdef),                              \
  KW_ENTRY(ifndef),                             \
  KW_ENTRY(imaginary),                          \
  KW_ENTRY(inline),                             \
  KW_ENTRY(int),                                \
  KW_ENTRY(line),                               \
  KW_ENTRY(long),                               \
  KW_ENTRY(noreturn),                           \
  KW_ENTRY(pragma),                             \
  KW_ENTRY(register),                           \
  KW_ENTRY(restrict),                           \
  KW_ENTRY(short),                              \
  KW_ENTRY(signed),                             \
  KW_ENTRY(static),                             \
  KW_ENTRY(static_assert),                      \
  KW_ENTRY(struct),                             \
  KW_ENTRY(thread_local),                       \
  KW_ENTRY(typedef),                            \
  KW_ENTRY(union),                              \
  KW_ENTRY(unsigned),                           \
  KW_ENTRY(void),                               \
  KW_ENTRY(volatile)

#undef KW_ENTRY
#define KW_ENTRY(X) kw_ ## X

/**
 ** @brief a lexicographic list of all keywords that we use
 **
 ** These numbers will be used as bit numbers to summarize properties
 ** of blobs of code.
 **
 ** Added at the end are some of the combinations that we use
 ** hereafter.
 **/
enum keyword {
  KW_KEYWORDS,
  KW_ENTRY(max),
}; typedef enum keyword keyword; 

static_assert(kw_max <= 64, "too many elements in enum keyword");

/* determine the longest keyword */
#undef KW_ENTRY
#define KW_ENTRY(X) [(sizeof #X) - 1] = 0
static char const kw_longest[] = {
  KW_KEYWORDS,
};

enum {
  /**
   ** @brief the size of the longest keyword string
   **/
  kw_size = sizeof kw_longest,
};

/**
 ** @brief Used internally to hold identifiers that could be keywords.
 **/
typedef char kw_string[kw_size];


#line 119 "tools-keyword.X" // preprocessor
/* #pragma CMOD definition */
#line 120 "tools-keyword.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control

#line 122 "tools-keyword.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control
#line 123 "tools-keyword.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control

/**
 ** @brief a lexicographic list of all keywords as strings
 **/
#line 128 "tools-keyword.X" // symbol
extern kw_string const kw_words[];

typedef int __internal_dummy_type_to_be_ignored; // hiding static symbol

#line 134 "tools-keyword.X" // symbol
inline properties kw_control(properties k) {
  properties const kw_all = BS_SET(kw_if, kw_elif, kw_else, kw_ifdef, kw_ifndef, kw_endif, kw_pragma);
  return bs_intersect(k, kw_all);
}

/* don't add extern to such declarations */
#line 140 "tools-keyword.X" // symbol
inline properties   kw_noextern(properties k) {
  properties const kw_allextern = BS_SET(kw_extern, kw_inline);
  return bs_intersect(k, kw_allextern);
}

#line 145 "tools-keyword.X" // symbol
inline properties kw_cmod(properties k) {
  properties const kw_allcmod = BS_SET(kw_pragma, kw_CMOD, kw_declaration, kw_definition);
  return bs_intersect(k, kw_allcmod);
}

#line 150 "tools-keyword.X" // symbol
inline properties kw_internal(properties k) {
  properties const kw_intern	= BS_SET(kw_typedef, kw_static, kw_static_assert, kw__Static_assert);
  return bs_intersect(k, kw_intern);
}

#line 155 "tools-keyword.X" // symbol
inline properties kw_typename(properties k) {
  properties const kw_tagger	= BS_SET(kw_enum, kw_struct, kw_union);
  return bs_intersect(k, kw_tagger);
}

#line 160 "tools-keyword.X" // symbol
inline properties kw_wide(properties k) {
  properties const kw_allwide	= BS_SET(kw_int, kw_unsigned, kw_signed, kw_long);
  return bs_intersect(k, kw_allwide);
}

#line 165 "tools-keyword.X" // symbol
inline properties kw_narrow(properties k) {
  properties const kw_allnarrow	= BS_SET(kw_short, kw_char, kw_bool, kw__Bool);
  return bs_intersect(k, kw_allnarrow);
}

#line 170 "tools-keyword.X" // symbol
inline properties kw_integer(properties k) {
  properties const kw_all	= BS_SET(kw_int, kw_unsigned, kw_signed, kw_long,
                                         kw_short, kw_char, kw_bool, kw__Bool);
  return bs_intersect(k, kw_all);
}

#line 176 "tools-keyword.X" // symbol
inline properties kw_floating(properties k) {
  properties const kw_all	= BS_SET(kw_double, kw_float);
  return bs_intersect(k, kw_all);
}

#line 181 "tools-keyword.X" // symbol
inline properties kw_basetype(properties k) {
  properties const kw_all	= BS_SET(kw_int, kw_unsigned, kw_signed, kw_long,
                                         kw_short, kw_char, kw_bool, kw__Bool,
                                         kw_double, kw_float, kw_void);
  return bs_intersect(k, kw_all);
}

#line 188 "tools-keyword.X" // symbol
inline
kw_string const* kw_get(keyword k) {
  return (k < kw_max) ? &kw_words[k] : 0;
}

#line 193 "tools-keyword.X" // symbol
inline
int kw_compar(void const* a, void const* b) {
  kw_string const* A = a;
  kw_string const* B = b;
  return strcmp(*A, *B);
}

#line 200 "tools-keyword.X" // symbol
inline
kw_string const* kw_bsearch(kw_string const s[static 1],
                            size_t nmemb,
                            kw_string const arr[static nmemb]) {
  return bsearch(s, arr, nmemb, sizeof arr[0], kw_compar);
}

#line 207 "tools-keyword.X" // symbol
inline
keyword kw_find(kw_string const w[static 1]) {
  kw_string const* ret = kw_bsearch(w, kw_max, kw_words);
  return ret ? ret - &kw_words[0] : -1;
}

#line 213 "tools-keyword.X" // symbol
extern void kw_print(FILE* restrict out, properties prop);
#endif /* __CMOD_INTERNAL_CMOD_746F6F6C732D6B6579776F72642E58_HEADER */
#undef __CMOD_CYCLIC_CMOD_746F6F6C732D6B6579776F72642E58_HEADER
