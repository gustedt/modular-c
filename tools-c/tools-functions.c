/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#line 0 "tools-functions.X" // preprocessor
#define CMOD_746F6F6C732D66756E6374696F6E732E58_HEADER_INSTANTIATE
#line 0 "tools-functions.X" // preprocessor
#include "tools-functions.h"
#line 14 "tools-functions.X" // preprocessor
#line 14 "tools-functions.X" // preprocessor
/* #pragma CMOD declaration */
#line 23 "tools-functions.X" // preprocessor
/* #pragma CMOD definition */
#line 24 "tools-functions.X" // preprocessor
#include <errno.h>
#line 25 "tools-functions.X" // preprocessor
#include <stdlib.h>

/**
 ** @file
 **
 ** @brief An adhoc utility to hide characters, strings and comments.
 **
 ** All of these are transformed into sequences of the form
 ** @code
 ** @ X nm nm nm nm nm nm .... @
 ** @endcode
 **
 ** Where @c nm are two character hex numbers for character code and
 ** @c X is one character with the following significance:
 **
 ** - @c C C comment
 ** - @c L @c "wide string literal"
 ** - @c M @c 'wide character'
 ** - @c P C++ comment
 ** - @c S @c "normal string literal"
 ** - @c T @c 'normal character'
 ** - @c U @c U"bla"
 ** - @c V @c U'bla'
 ** - @c u @c u"bla"
 ** - @c u8 @c u8"bla"
 ** - @c v @c u'bla'
 **
 ** Multiline comments are split at the end of each line to ensure
 ** that the line number count remains correct.
 **/

#line 56 "tools-functions.X" // symbol
extern inline
char* chunk(FILE*restrict in, size_t size, char buffer[static restrict size]);


/**
 ** @brief Return the hex character for the lower half byte @a c.
 **/
#line 67 "tools-functions.X" // symbol
extern inline
char xdigit(unsigned c);


#line 76 "tools-functions.X" // symbol
extern inline
unsigned udigit(unsigned c);


#line 104 "tools-functions.X" // symbol
extern inline
size_t skip(char const buf[static 1]);


#line 109 "tools-functions.X" // symbol
extern inline
bs setbit(unsigned x, unsigned level);


#line 115 "tools-functions.X" // symbol
extern inline
bs idchar(unsigned L);


#line 182 "tools-functions.X" // symbol
extern inline
bool isid(unsigned char c);


#line 189 "tools-functions.X" // symbol
extern inline
size_t word(char const buf[static 1]);


#line 197 "tools-functions.X" // symbol
extern inline
size_t skipat2(char const buf[static 1]);


#line 211 "tools-functions.X" // symbol
char const* decode(FILE*restrict out, FILE*restrict in, char const*p, size_t size, char buffer[static restrict size], bool show) {
  while (*p != '@') {
    if (!p[0]) goto RELOAD;
    if (*p == '\n') {
      putc('\n', out);
      fflush(out);
      ++p;
    }
    if (!p[0]) goto RELOAD;
    if (!p[1]) {
      ungetc(p[0], in);
      goto RELOAD;
    }
    if (show) {
      unsigned c = (udigit(p[0])<<(CHAR_BIT/2)) | udigit(p[1]);
      putc(c, out);
    }
    p += 2;
    continue;
RELOAD:
    p = chunk(in, size, buffer);
    if (!p) return 0;
  }
  return p;
}


#line 237 "tools-functions.X" // symbol
flex const* readin(FILE*restrict in) {
  size_t len = 1024;
  flex* ret = resize(0, len);
  while (flex_fgets(ret, in)) {
    if (full(ret)) {
      size_t nlen = 2*total(ret);
      ret = resize(ret, nlen);
      // we are not able to allocate, so better stop the loop.
      if (ret->total != nlen) break;
    }
  }
  return ret;
}


#line 251 "tools-functions.X" // symbol
void writeout(FILE *restrict out, size_t len, char const buf[static restrict len]) {
  errno = 0;
  while (len) {
    int l = len > INT_MAX ? INT_MAX : len;
    int k = fprintf(out, "%.*s", l, buf);
    if (k < 0) {
      perror("writeout");
      return;
    }
    len -= k;
    buf += k;
  }
}


#line 265 "tools-functions.X" // symbol
void lineout(FILE *restrict out, size_t len, char const buf[static restrict len]) {
  errno = 0;
  for (char const* end = buf + len; buf < end; ++buf) {
    if (buf[0] != '\n')
      putc(buf[0], out);
    else
      putc(' ', out);
  }
}


#line 275 "tools-functions.X" // symbol
extern inline
void wordout(FILE*restrict out, char const buf[static restrict 1]);


#line 281 "tools-functions.X" // symbol
size_t skipat(char const buf[static 1]) {
  size_t pos = 0, len;
 RESTART:
  len = skip(buf+pos);
  len += skipat2(buf+pos+len);
  pos += len;
  if (len) goto RESTART;
  return pos;
}


#line 291 "tools-functions.X" // symbol
size_t count(size_t len, char const buf[static len], char needle) {
  size_t ret = 0;
  while (len) {
    char const* nbuf = memchr(buf, needle, len);
    if (!nbuf) break;
    ++ret;
    ++nbuf;
    len -= (nbuf-buf);
    buf = nbuf;
  }
  return ret;
}


#line 304 "tools-functions.X" // symbol
flex* uniq(char const* name, char const* suffix){
  flex* ret = 0;
  size_t len = strlen(name);
  char buf[2*len + 1];
  for (size_t i = 0; i < len; ++i) {
    snprintf(buf+2*i, 3, "%.2X", name[i]);
  }
  ret = cpy(ret, "CMOD_");
  ret = cat(ret, buf);
  ret = cat(ret, suffix);
  return ret;
}


#line 317 "tools-functions.X" // symbol
flex* hbtob(flex* ret, size_t len, char const code[len]) {
  ret = resize(ret, len/2 + 1);
  char* d = data(ret);
  for (size_t i = 0; i < len; i += 2) {
    d[i/2] = (udigit(code[i]) << CHAR_BIT/2) | udigit(code[i+1]);
  }
  d[len/2] = 0;
  ret->actual = len;
  return ret;
}


#line 328 "tools-functions.X" // symbol
flex* get_linenumber(flex* currentf, char const b[static 1], size_t line[static 1]) {
  char* end = 0;
  size_t nl = strtoull(b, &end, 10);
  if (end != b) {
    char const* sfile = end + skip(end);
    switch (sfile[0]) {
    case '@': ;
    case '"': ;
      line[0] = nl;
      end = strchr(sfile+1, sfile[0]);
      size_t eol = end-sfile;
      switch (sfile[0]) {
      case '@':
        eol -= 2;
        sfile += 2;
        return hbtob(currentf, eol, sfile);
      case '"':
        eol -= 1;
        sfile += 1;
        return cpy(currentf, eol, sfile);
      }
    }
  }
  return 0;
}

