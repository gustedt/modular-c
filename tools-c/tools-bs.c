/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

#line 0 "tools-bs.X" // preprocessor
#define CMOD_746F6F6C732D62732E58_HEADER_INSTANTIATE
#line 0 "tools-bs.X" // preprocessor
#include "tools-bs.h"
#line 3 "tools-bs.X" // preprocessor
#line 3 "tools-bs.X" // preprocessor
/* #pragma CMOD declaration */
#line 23 "tools-bs.X" // preprocessor
/* #pragma CMOD definition */

#line 26 "tools-bs.X" // symbol
extern inline
bs bs_intersect(bs a, bs b);


#line 31 "tools-bs.X" // symbol
extern inline
bs bs_isin(unsigned x, bs set);


#line 36 "tools-bs.X" // symbol
extern inline
bs bs_union(bs a, bs b);


#line 41 "tools-bs.X" // symbol
extern inline
bs bs_complement(bs a);


#line 46 "tools-bs.X" // symbol
extern inline
bs bs_minus(bs a, bs b);


#line 51 "tools-bs.X" // symbol
extern inline
bs bs_difference(bs a, bs b);


#line 58 "tools-bs.X" // symbol
extern inline
unsigned bs_min(size_t len, unsigned const a[static restrict len]);


#line 71 "tools-bs.X" // symbol
extern inline
unsigned bs_max(size_t len, unsigned const a[static restrict len]);


/**
 ** @brief Generate the bitset of all members of the array.
 **
 ** This will usually not be used directly, but through the #BS_SET
 ** macro.
 **
 ** If it is called with an array that is filled with values that are
 ** known at compile time, this should be inlined in place by the
 ** caller. It should lead to the compile time computation of a bit
 ** mask and a simple bitwise and against this bit mask.
 **
 ** The implementation is recursive to trick the compiler into
 ** considering each of the branches to return a compile time result.
 **/
#line 98 "tools-bs.X" // symbol
extern inline
bs bs_set(size_t len, unsigned const a[static restrict len]);

