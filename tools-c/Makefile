.SUFFIXES : .o .h .c .X .a .so .prepro

.PRECIOUS : %.c %.h

SOURCES =					\
	tools-decl.c				\
	tools-reduce.c				\
	tools-def.c				\
	tools-dump8.c				\
	tools-flex.c				\
	tools-functions.c			\
	tools-grammar.c				\
	tools-hide.c				\
	tools-keyword.c				\
	tools-show.c				\
	tools-tag.c				\
	tools-undump8.c				\

OBJECTS = ${SOURCES:.c=.o}

LOBJS =						\
	tools-bs.o				\
	tools-flex.o				\
	tools-functions.o			\
	tools-grammar.o				\
	tools-keyword.o

CMODDIR=../bin
LICENSE = $(abspath ../SHORTLICENSE.txt)
AUTHORS = $(abspath ../AUTHORS.txt)
TIDY = ${CMODDIR}/findAuthors --scm git --type c --authors=${AUTHORS} --lice ${LICENSE} --ofile
SPLIT2 = ${CMODDIR}/Cmod_split2

TOOLS = ./makeheaders ./tools-decl ./tools-def ./tools-reduce ./tools-tag ./tools-dump8 ./tools-hide ./tools-show ./tools-undump8

COPTS ?= -O3 -march=native

CFLAGS	?= -fPIC -std=c11 -Wall  -Wno-unknown-pragmas -fno-common -fdata-sections -ffunction-sections ${COPTS}
LDFLAGS ?= -Wl,--gc-sections
LDLIBS  ?= -L . -ltoolc

TARGET :=  libtoolc.a ${TOOLS}

target :

ifeq (${CMOD_STATIC},)
TARGET += libtoolc.so
LDSHARED ?= -shared -Bdynamic
${TOOLS} : libtoolc.so
LDLIBS += -Wl,-rpath,$(abspath .)
else
LDFLAGS += -static
endif

target : ${TARGET}

tools : ${TOOLS}

${TOOLS} : libtoolc.a

libtoolc.a : ${LOBJS}
	${AR} sr $@ $^

libtoolc.so : ${LOBJS}
	${CC} ${LDSHARED} -o $@ $^

%.c %.h : %.X
	${SPLIT2} $*.X

%.o : %.c %.h %.X
	${CC} ${CFLAGS} -c -o $*.o $*.c

split : ${LOBJS:.o=.h} ${LOBJS:.o=.c}

depend :
	${CC} ${CFLAGS} -M ${SOURCES} > Makefile.inc

clean :
	rm -f ${OBJECTS} ${TARGET}

tidy :
	for f in ${SOURCES} ; do  ${TIDY} $$f ; done

-include Makefile.inc
