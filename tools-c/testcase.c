/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2018 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

/**
 ** @file
 **
 ** @brief test case for header production and list of symbols
 **
 ** This tests several bad corner cases for parsing C. In most parts,
 ** this is not meant as an example how to program in C, but in the
 ** contrary, how to *not* program in C. In particular, defining
 ** struct, union or enum types inside variable or function
 ** declarations or definitions is really bad practice.
 **
 ** Some of the cases will *not* work with the automatic header
 ** generation. These are only conditionally exposed when the macro
 ** "EXPOSE" is defined. If you compile this with this macro defined
 ** (usually -DEXPOSE on the command line of your compiler) this
 ** should produce meaningful errors and give you line numbers that
 ** correspond to the exact problem location.
 **/

#line 0 "testcase.X" // preprocessor
#define CMOD_TESTCASE_HEADER_INSTANTIATE
#line 0 "testcase.X" // preprocessor
#include "testcase.h"
#line 34 "testcase.X" // preprocessor
#line 34 "testcase.X" // preprocessor
#ifdef EXPOSE
/**
 ** These variables cannot be exposed to the outer world, because an
 ** enumeration type cannot be forward declared.
 **/
#line 39 "testcase.X" // symbol
enum toto {
  a,
  b = sizeof(int),
  c = 7 } 
#line 42 "testcase.X" // symbol
*ota = 0,
#line 42 "testcase.X" // symbol
*itat;


#line 43 "testcase.X" // preprocessor
#endif

/**
 ** A private enum type *alone* should work. It will just not be
 ** exported to the .h file
 **/
#line 49 "testcase.X" // symbol
enum { another0 };

#line 51 "testcase.X" // preprocessor
#ifdef EXPOSE
/**
 ** A private enum type that declares a variable will not work. Such a
 ** declaration should be static.
 **/
#line 56 "testcase.X" // symbol
enum { yetanother0withvariable } 
#line 56 "testcase.X" // symbol
var;


#line 57 "testcase.X" // preprocessor
#endif

/**
 ** But a similar with a typedef should work. All typedef are
 ** internal, unless placed in a "declaration" section.
 **/
#line 63 "testcase.X" // symbol
typedef enum hoho {
  dddd,
  oooo,
} 
#line 66 "testcase.X" // symbol
ohoh;



/**
 ** These variables can only be exposed to the outer world, because a
 ** struct type can be forward declared *and* this only declares
 ** pointers to this type. Hiding such declarations as second part of
 ** the struct declaration is really bad style, avoid it.
 **/
#line 74 "testcase.X" // symbol
struct tutu {
  // this is only possible if we have the typedef from the
  // automatically produced header, otherwise we would have to use
  // "struct tutu"
  tutu* next;
  int ui;
} 
#line 80 "testcase.X" // symbol
*tail = { 0 },
#line 80 "testcase.X" // symbol
*head = 0;



/**
 ** A static variables or functions should never cause problems and
 ** just not be exported to the header. Good.
 **/
#line 86 "testcase.X" // symbol
static int poiu;


#line 88 "testcase.X" // symbol
static union hihi {
  int dd;
  unsigned r;
} 
#line 91 "testcase.X" // symbol
sthihi = { .dd = 1, };



/**
 ** A declaration section starts at such a pragma and ends at the next
 ** "definition" pragma.
 **
 ** Generally, it should only contain type or macro definitions, all
 ** the rest is taken care of by the tool.
 **
 ** If you think you must you could also place variable or function
 ** *declarations* here. But normally these be generated from their
 ** definitions.
 **
 ** inline functions are treated special they may appear in both,
 ** declaration or definition sections. If they appear in a definition
 ** section they will be instantiated automatically in the .c file
 ** produced by tools-def. If it is in the declaration section as
 ** here, you'd have to define a special macro for that case. For this
 ** example this would be CMOD_TESTCASE_HEADER_INSTANTIATE.
 **/

#line 112 "testcase.X" // preprocessor
#if EXPOSE
#line 113 "testcase.X" // preprocessor
/* #pragma CMOD declaration */
#line 120 "testcase.X" // preprocessor
/* #pragma CMOD definition */
#line 121 "testcase.X" // preprocessor
/* #pragma CMOD declaration */
#line 139 "testcase.X" // preprocessor
/* #pragma CMOD definition */
#line 140 "testcase.X" // preprocessor
#endif

#line 142 "testcase.X" // preprocessor
/* #pragma CMOD definition */

/**
 ** Testing different forms of function declarations. Such function
 ** declarations should be produced automatically by the tool, so we
 ** don't expose them.
 **
 ** Syntactically challenging are declarations that also protect the
 ** identifier that is declared against macro protection by using
 ** additional parenthesis.
 **/
#line 153 "testcase.X" // preprocessor
#ifndef EXPOSE
// function pointer with basetype
#line 155 "testcase.X" // symbol
void (*const ttttt)(void);



// function pointer with named type
#line 158 "testcase.X" // symbol
tutu* (*tstttt)(int);



// function pointer with named type
#line 161 "testcase.X" // symbol
tutu* (*tsttii)(tutu);



// function name, basetype, macro protected
#line 164 "testcase.X" // symbol
void (ttttfff)(void);



// function name, named type, macro protected
#line 167 "testcase.X" // symbol
tutu* (ttttggg)(void);



// function name, basetype, macro protected
#line 170 "testcase.X" // symbol
int (ttttfffr)(int);



// function name, named type, macro protected
#line 173 "testcase.X" // symbol
tutu* (ttttgggr)(double);



// function definition, basetype, macro protected
#line 176 "testcase.X" // symbol
void (ttttfff)(void) {
  // nothing
}

#line 179 "testcase.X" // preprocessor
#endif

/**
 ** Testing different forms of function definitions.
 **
 ** Syntactically challenging are definitions that also protect the
 ** identifier that is declared against macro protection by using
 ** additional parenthesis.
 **/

// function definition, named type, macro protected
#line 190 "testcase.X" // symbol
tutu* (ttttggg)(void) {
  return 0;
}


// function definition, basetype, macro protected
#line 195 "testcase.X" // symbol
int (ttttfffr)(int a) {
  return a + poiu;
}


// function definition, named type, macro protected
#line 200 "testcase.X" // symbol
tutu* (ttttgggr)(double b) {
  return 0;
}


// function definition, struct type
#line 205 "testcase.X" // symbol
struct tutu ttttgggs(double c) {
  return (tutu){ 0 };
}


// function definition, struct type, macro protected
#line 210 "testcase.X" // symbol
struct tutu (ttttgggsp)(double d) {
  return (tutu){ 0 };
}


// function definition, named type, macro protected, inline
#line 215 "testcase.X" // symbol
extern inline tutu* (ttttgggi)(void);


// function definition, basetype, macro protected, inline
#line 220 "testcase.X" // symbol
extern inline int (ttttfffri)(int a);


// function definition, named type, macro protected, inline
#line 225 "testcase.X" // symbol
extern inline tutu* (ttttgggri)(double b);


// function definition, struct type, inline
#line 230 "testcase.X" // symbol
extern inline struct obob ttttgggsi(double c);


// function definition, struct type, macro protected, inline
#line 235 "testcase.X" // symbol
extern inline struct obob (ttttgggspi)(double d);


// function definition, struct type
// really bad style
#line 241 "testcase.X" // symbol
struct op {
  int o;
} 
#line 243 "testcase.X" // symbol
*ttttgggss(double e) {
  return 0;
}


// function definition, struct type, macro protected
// really bad style
#line 249 "testcase.X" // symbol
struct ap {
  int o;
} 
#line 251 "testcase.X" // symbol
* (ttttgggssp)(double f) {
  return 0;
}


#line 255 "testcase.X" // preprocessor
#ifdef EXPOSE
// function definition, anonymous struct type
//
// Because the return type is anonymous, nobody can call this function
// without external knowledge about the exact definition of the return
// type.
#line 261 "testcase.X" // symbol
struct {
  int o;
} 
#line 263 "testcase.X" // symbol
*ttttgggsa(double g) {
  return 0;
}

#line 266 "testcase.X" // preprocessor
#endif

#line 268 "testcase.X" // preprocessor
#ifdef EXPOSE
// function definition, anonymous struct type, macro protected
//
// Because the return type is anonymous, nobody can call this function
// without external knowledge about the exact definition of the return
// type.
#line 274 "testcase.X" // symbol
struct {
  int o;
} 
#line 276 "testcase.X" // symbol
*(ttttgggspa)(double h) {
  return 0;
}

#line 279 "testcase.X" // preprocessor
#endif

#line 281 "testcase.X" // preprocessor
# define substance

#line 283 "testcase.X" // preprocessor
# define functionlike(A)

#line 285 "testcase.X" // preprocessor
# define functionunlike (A)

