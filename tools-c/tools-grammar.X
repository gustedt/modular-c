/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/**
 ** @file
 **
 ** Get basic structuring of C code at the file level.
 **/

#pragma CMOD declaration
#include <stdbool.h>
#include <stddef.h>
#include <ctype.h>
#include "tools-keyword.h"
#include "tools-functions.h"
#include "tools-bs.h"
#pragma CMOD definition
#include <string.h>


/**
 ** @brief Counts the number of newlines in the buffer.
 **/
inline size_t grammar_lines(size_t len, char const buf[static len]){
  return count(len, buf, '\n');
}

/**
 ** @brief Find the end of the next C declaration.
 **
 ** Ends at an initialization, declaration block or function block.
 **/
size_t grammar_decl(char const buf[static 1]) {
  size_t para = 0;
  size_t pos = 0;
  for (; buf[pos]; ++pos) {
    switch (buf[pos]) {
    case '[' : ;
    case '(' : ++para; break;
    case ']' : ;
    case ')' : --para; break;
    default: {
      if (para) continue;
      switch (buf[pos]) {
      case '#' : return pos;
      case '{' : return pos;
      case '=' : return pos;
      case ',' : return pos;
      case ';' : return pos;
      case 0   : return pos;
      }
    }
    }
  }
  return pos;
}

/**
 ** @brief Find the end of the next C block.
 **
 ** Supposes that the first character is a "{". Skips all text until a
 ** corresponding "}" is found.
 **/
size_t grammar_block(char const buf[static 1]) {
  if (buf[0] && buf[0] != '{') return 0;
  size_t pos = 0;
  size_t level = 1;
  ++pos;
  for (;level && buf[pos]; ++pos) {
    switch (buf[pos]) {
    case '{' : ++level; break;
    case '}' : --level; break;
    case 0 : break;
    }
  }
  // If a block is followed by a ";" this must be the end of a type
  // declaration.
  size_t sk = skip(buf+pos);
  if (buf[pos+sk] == ';') pos += sk;
  return pos;
}

/**
 ** @brief Find the end of the next C initialization.
 **/
size_t grammar_init(char const buf[static 1]) {
  size_t para = 0;
  size_t pos = 0;
  for (; buf[pos]; ++pos) {
    switch (buf[pos]) {
    case '[' : ;
    case '(' : ++para; break;
    case ']' : ;
    case ')' : --para; break;
    default: {
      if (para) continue;
      switch (buf[pos]) {
      case '{' : {
        size_t b = pos + grammar_block(buf+pos);
        pos = b+strcspn(buf+b, ";,");
        return pos;
      }
      case ',' : return pos;
      case ';' : return pos;
      case 0   : return pos;
      }
    }
    }
  }
  return pos;
}

/**
 ** @brief Get the properties of the all words in the buffer that are
 ** not protected by () or [].
 **/
properties grammar_prop(size_t len, char const buf[static len]) {
  if (len > kw_size) return 0;
  kw_string word = { 0 };
  strncpy(word, buf, len);
  keyword kw = kw_find(&word);
  return kw < kw_max ? BS_OF(kw) : 0;
}

/**
 ** @brief Get the properties of the all words in the buffer that are
 ** not protected by () or [].
 **/
properties grammar_props(size_t end, char const buf[static end], size_t positions[2]) {
  if (!positions) positions = (size_t[2]){ -1, -1, };
  size_t id = 0;
  properties ret = 0;
  size_t para = 0;
  size_t brac = 0;
  // we keep track of the first parenthesis that we encounter
  size_t fpara = -1;
  for (size_t pos = 0; pos < end; ++pos) {
    switch (buf[pos]) {
    case '(' : ++para; if (fpara == -1) fpara = pos; break;
    case ')' : --para; break;
    case '[' : ++brac; break;
    case ']' : --brac; break;
    case ' ' : break;
    case '\t' : break;
    case '\n' : break;
    default: {
      if (!brac && para == 1 && buf[pos] == ',') {
        ret |= BS_OF(kw__Comma);
      }
      if (para || brac) continue;
      size_t w = word(buf+pos);
      if (!w) continue;
      properties prop = grammar_prop(w, buf+pos);
      ret |= prop;
      if (!prop && id < 2 && (isalpha(buf[pos]) || buf[pos] == '_')) {
        positions[id] = pos;
        ++id;
      }
      pos += w-1;
    }
    }
  }
  // If we have not yet collected enough information to find the name
  // of this declaration and we have encountered a parenthesis, the
  // first identifier inside the parenthesis might be what we are
  // looking for.
  if (!((positions[0] < -1) && (kw_basetype(ret) || (positions[1] < -1)))) {
    // Advance fpara until we encounter an identifier token.
    while (fpara < end) {
      size_t l = 1;
      if (isid(buf[fpara])) {
        l = word(buf+fpara);
        // If the word is an keyword, it is not an identifier
        if (!grammar_prop(l, buf+fpara)) {
          if (positions[0] == -1)
            positions[0] = fpara;
          else
            positions[1] = fpara;
          break;
        }
      }
      fpara += l;
    }
  }
  return ret;
}

/**
 ** @brief Search for the end of a preprocessing line, including
 ** hidden comments.
 **/
size_t grammar_prepro(char const buf[static 1], size_t line[static 1]) {
  char const* en = buf;
  for (;;) {
    en = strchr(en, '\n');
    if (!en || en[-1] != '\\') break;
    ++en;
  }
  size_t l = (en ? en-buf : 0) + 1;
  /* An odd number of @ characters means that this originates from a
     comment at the end of a #define or so. */
  if (count(l, buf, '@') % 2) {
    // find the end of the comment
    en = strchr(&buf[l], '@');
    if (en) {
      l += (en-&buf[l]) + 1;
    }
  }
  *line += grammar_lines(l, buf);
  return l;
}

properties grammar_plevel(size_t len, char const buf[len]) {
  size_t pos = skip(buf);
  if (pos > len) return 0;
  buf += pos;
  len -= pos;
  pos = word(buf);
  if (pos > len) return 0;
  return grammar_prop(pos, buf);
}


/**
 ** @brief forward all lines as long as we are in a declaration
 ** section
 **/
size_t grammar_skip(FILE* restrict out, char const f[static restrict 1], size_t line[static restrict 1]) {
  size_t pos = 0;
  while (f[pos]) {
    size_t l = strcspn(f+pos, "\n")+1;
    size_t s = strspn(f+pos, " \t");
    // Only try to find the end of the section if we encounter a
    // preprocessor line.
    if (f[pos+s] == '#') {
      size_t e = pos+s;
      ++s;
      s += strspn(f+pos+s, " \t");
      size_t w = word(f+pos+s);
      properties kw = grammar_prop(w, f+pos+s);
      // try to see if this is a pragma
      if (bs_isin(kw_pragma, kw)) {
        kw = grammar_props(l-s, f+pos+s, 0);
        /*   // declaration and definition sections are removed */
        switch (kw_cmod(kw)) {
        case BS_OF(kw_declaration)|BS_OF(kw_pragma)|BS_OF(kw_CMOD):
          if (out) fputs("// redundant declaration section\n", out);
          pos += l;
          ++*line;
          continue;
          // A definition section ends the declaration section.
        case BS_OF(kw_definition)|BS_OF(kw_pragma)|BS_OF(kw_CMOD):
          --*line;
          while (f[e] != '\n') --e;
          return e;
        }
      }
    }
    if (out) writeout(out, l-1, f+pos);
    ++*line;
    pos += l-1;
    if (f[pos] == '\n') {
      if (out) fputc('\n', out);
      ++pos;
    }
  }
  return pos;
}

/**
 ** @brief Determine if buf starts a tagtype, that is, is of the form
 ** "[enum|struct|union] {name}"
 **/
bool grammar_tagtype(size_t e, char const buf[static e]) {
  // type names don't have parenthesis
  if (memchr(buf, '(', e)) return false;
  size_t pos = skip(buf);
  size_t w = word(buf+pos);
  if (!w) return false;
  // First word must be struct, union or enum
  if (kw_typename(grammar_prop(w, buf+pos))) {
    // then there is a optional tagname
    pos += w;
    pos += skip(buf+pos);
    w = word(buf+pos);
    pos += w;
    pos += skip(buf+pos);
    // If we are now at the end, this is a type definition or forward
    // declaration.
    return pos == e;
  }
  return false;
}

