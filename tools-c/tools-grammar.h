/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/**
 ** @file
 **
 ** Get basic structuring of C code at the file level.
 **/

#ifdef __CMOD_CYCLIC_CMOD_746F6F6C732D6772616D6D61722E58_HEADER
#error cyclic inclusion of interface specification
#endif
#define __CMOD_CYCLIC_CMOD_746F6F6C732D6772616D6D61722E58_HEADER
#ifndef __CMOD_INTERNAL_CMOD_746F6F6C732D6772616D6D61722E58_HEADER
#line 1 "<start interface>"
#define __CMOD_INTERNAL_CMOD_746F6F6C732D6772616D6D61722E58_HEADER
#line 8 "tools-grammar.X" // preprocessor
/* #pragma CMOD declaration */
#include <stdbool.h>
#include <stddef.h>
#include <ctype.h>
#include "tools-keyword.h"
#include "tools-functions.h"
#include "tools-bs.h"
#line 15 "tools-grammar.X" // preprocessor
/* #pragma CMOD definition */
#line 16 "tools-grammar.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control


/**
 ** @brief Counts the number of newlines in the buffer.
 **/
#line 22 "tools-grammar.X" // symbol
inline size_t grammar_lines(size_t len, char const buf[static len]){
  return count(len, buf, '\n');
}

/**
 ** @brief Find the end of the next C declaration.
 **
 ** Ends at an initialization, declaration block or function block.
 **/
#line 31 "tools-grammar.X" // symbol
extern size_t grammar_decl(char const buf[static 1]);

/**
 ** @brief Find the end of the next C block.
 **
 ** Supposes that the first character is a "{". Skips all text until a
 ** corresponding "}" is found.
 **/
#line 62 "tools-grammar.X" // symbol
extern size_t grammar_block(char const buf[static 1]);

/**
 ** @brief Find the end of the next C initialization.
 **/
#line 84 "tools-grammar.X" // symbol
extern size_t grammar_init(char const buf[static 1]);

/**
 ** @brief Get the properties of the all words in the buffer that are
 ** not protected by () or [].
 **/
#line 115 "tools-grammar.X" // symbol
extern properties grammar_prop(size_t len, char const buf[static len]);

/**
 ** @brief Get the properties of the all words in the buffer that are
 ** not protected by () or [].
 **/
#line 127 "tools-grammar.X" // symbol
extern properties grammar_props(size_t end, char const buf[static end], size_t positions[2]);

/**
 ** @brief Search for the end of a preprocessing line, including
 ** hidden comments.
 **/
#line 190 "tools-grammar.X" // symbol
extern size_t grammar_prepro(char const buf[static 1], size_t line[static 1]);

#line 211 "tools-grammar.X" // symbol
extern properties grammar_plevel(size_t len, char const buf[len]);


/**
 ** @brief forward all lines as long as we are in a declaration
 ** section
 **/
#line 226 "tools-grammar.X" // symbol
extern size_t grammar_skip(FILE* restrict out, char const f[static restrict 1], size_t line[static restrict 1]);

/**
 ** @brief Determine if buf starts a tagtype, that is, is of the form
 ** "[enum|struct|union] {name}"
 **/
#line 272 "tools-grammar.X" // symbol
extern bool grammar_tagtype(size_t e, char const buf[static e]);

#endif /* __CMOD_INTERNAL_CMOD_746F6F6C732D6772616D6D61722E58_HEADER */
#undef __CMOD_CYCLIC_CMOD_746F6F6C732D6772616D6D61722E58_HEADER
