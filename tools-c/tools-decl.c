#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools-keyword.h"
#include "tools-flex.h"
#include "tools-functions.h"
#include "tools-grammar.h"

/**
 ** @file
 **
 ** This implements a rudimentary parser that reads a C file and spits
 ** out a header file with all declarations of symbols with external
 ** linkage.
 **
 ** This has an important restriction that comes with Modular
 ** C. Definitions that contain a type *and* a symbol are not allowed
 ** in a definition section, unless they are just pointers to the
 ** type.  This is because types are not external by default, and
 ** symbols that use this type cannot be external because they can't
 ** be declared.
 **
 ** For these symbols the same rule applies as for symbols that use a
 ** hidden type: you'd have to declare it "static". But you did that
 ** anyhow, didn't you?
 **
 ** (For declaration sections see below.)
 **
 ** This is not a standalone tool, but relies on the other tools found
 ** in this directory. Namely it assumes that all comments are hidden
 ** and can easily be skipped over.
 **
 ** The idea for the implementation of this utility is simple. In file
 ** scope there are only a few types of blobs:
 **
 ** - preprocessor directives
 ** - static assertions
 ** - declarations that may also happen to be definitions
 **
 ** A declaration is also a definition, if
 **
 ** - it is an object declaration that is followed by a "=" token and
 **   an initializer.
 **
 ** - it is a function, struct, union or enum declaration that is also followed
 **   by a {} block.
 **
 ** So we can decide that full declaration ends when we meet a "=",
 ** ";", "," or "{" character that is fully exposed, that is not
 ** protected by () or [] parenthesis.
 **
 ** (The comma operator cannot appear in an top level initialization.)
 **
 ** Once we have a declaration, we can decide if it has external
 ** linkage. All but typedef, static, struct, union, and enum have
 ** external linkage.
 **
 ** Internal definitions and static assertions are skipped, unless
 ** they are found in a declaration section, see below, but for struct
 ** or union types with a tagname, a forward declaration as a typedef
 ** is produced.
 **
 ** Preprocessor conditionals, line numbers and pragmas are kept.
 **
 ** External definitions have their initialization or block removed,
 ** to form a declaration, only. Only inline functions that are not
 ** static keep their block.
 **
 ** Such external declarations are then prefixed with "extern", unless
 ** they are already marked extern or inline.
 **
 ** CMOD declaration and definition pragmas play a special role. Lines
 ** that are found after a declaration directive and before any other
 ** definition directive are mostly passed through as they are. The
 ** only addition are struct, union or enum typedef, that introduce
 ** the tagname as an identifier.
 **
 ** Some effort is made to keep a count of the line numbers. All
 ** identified particles are annotated with #line directives such that
 ** compilers that error out have chances to give the line number
 ** where the error occured.
 **/

// The actual level of nestedness of preprocessing conditionals.
static signed plevel = 0;

enum { maxlevel = 64, };
static bool state[maxlevel] = { false, };

static bool check(FILE* out, properties change, bool decl) {
  if (plevel < maxlevel) {
    char const* kw = "else";
    switch (change) {
    case BS_OF(kw_if): ;
    case BS_OF(kw_ifdef): ;
    case BS_OF(kw_ifndef): ;
      state[plevel] = decl;
      ++plevel;
      break;
    case BS_OF(kw_elif): kw = "elif";
    case BS_OF(kw_else):
      // If there is a mismatch at an else or elif, we just reset the
      // state to the current, such that we don't that error over and
      // over again.
      if ((plevel > 0) && (state[plevel-1] != decl)) {
        fprintf(out, "#error mismatch of \"%s\" directive at #%s, consider adding \"%s\" directive before this.\n",
                decl ? "declaration" : "definition",
                kw,
                !decl ? "declaration" : "definition");
        state[plevel-1] = decl;
        return false;
      }
      break;
    case BS_OF(kw_endif):
      if ((plevel > 0) && (state[plevel-1] != decl)) {
        fprintf(out, "#error mismatch of \"%s\" directive at #endif, consider adding \"%s\" directive before this.\n",
                decl ? "declaration" : "definition",
                !decl ? "declaration" : "definition");
        return false;
      }
      --plevel;
      break;
    }
  }
  return true;
}

/**
 ** @brief Print a declaration section that is presented in the buffer.
 **
 ** Mostly, this passes the contents of f trough, without modifying
 ** it. There are only three exceptions of this: struct, union and
 ** enum type definitions.
 **
 ** struct and union definitions are prefixed with a typedef of the
 ** form
 ** @code
 ** typedef [struct|union] name name;
 ** @endcode
 **
 ** enum definitions are *postfixed*  with a typedef of the
 ** form
 ** @code
 ** typedef enum name name;
 ** @endcode
 **
 ** This is done, such that the line structure of the code does not
 ** change.  In particular, if the line number information had been
 ** correct before this is written, it should be correct for every
 ** code line that is printed by this function.
 **/
void printdeclaration(FILE* out, size_t len, char const f[static len], char const nameuniq[static 1]){
  size_t pos = 0;
  flex* collect = 0;
  for (;pos < len;++pos) {
    // go over initial comments and empty lines
    {
      size_t len = skipat(f+pos);
      if (len) {
        writeout(out, len, f+pos);
        pos += len-1;
        continue;
      }
    }
    size_t s = pos, e = s + grammar_decl(f + s), ie = e, b = e;
    size_t positions[2] = { -1, -1, };
    properties prop = grammar_props(e-s, f+s, positions);
    if (!f[e]) {
      pos = e;
    } else {
      // Handle preprocessor directives specially.
      if (f[e] == '#' ) {
        size_t l = grammar_prepro(f+e+1, &(size_t){ 0 });
        {
          properties kw = grammar_plevel(l-1, f+e+1);
          if (bs_isin(kw_pragma ,kw)) {
            kw = grammar_props(l-1, f+e+1, 0);
            switch (kw_cmod(kw)) {
            case BS_OF(kw_declaration)|BS_OF(kw_pragma)|BS_OF(kw_CMOD):
              //fprintf(stderr, "printdeclaration: %.20s\n", f+e);
              s += l;
            }
          } else {
            check(out, kw, true);
          }
        }
        writeout(out, l+(e-s), f+s);
        pos = e+l;
        if (pos < len)
          fputc(f[pos], out);
        continue;
      }
      // Capture the grammatical structure of the next declaration or
      // definition.
      if (f[e] == '=') {
        ie = grammar_init(f+e+1) + e + 1;
      }
      if (f[ie] != '{') {
        pos = ie;
      } else {
        b = ie + grammar_block(f+ie);
        pos = b;
      }
    }
    // Enum, struct and union definitions need special care.
    char const* tagname = 0;              // Remember the name of a detected tag type.
    properties kwset = kw_typename(prop);
    if (kwset && positions[0] < -1) {
      tagname = f+s+positions[0];
      // typedef for enum can only be generated at the end.
      if (!bs_isin(kw_enum, kwset)) {
        char const* type = 0;
        switch (kwset) {
        case BS_OF(kw_struct): type = "struct"; break;
        case BS_OF(kw_union): type = "union"; break;
        }
        fprintf(out, "typedef %s ", type);
        wordout(out, tagname);
        fputc(' ', out);
        wordout(out, tagname);
        fputs("; ", out);
        tagname = 0;
      }
    }
    // Inline definitions are collected for later use.
    if (bs_isin(kw_inline, prop) && !bs_isin(kw_static, prop)) {
      collect = cat(collect, "extern ");
      collect = cat(collect, e-s, f+s);
      collect = cat(collect, ";\n");
    }
    // Now just echo the text
    writeout(out, pos-s, f+s);
    if (pos < len) {
      fputc(f[pos], out);
      // If this was terminating an enum definition output the
      // typedef.
      if (f[pos] == ';' && tagname) {
        fprintf(out, " typedef enum ");
        wordout(out, tagname);
        fputc(' ', out);
        wordout(out, tagname);
        fputs("; ", out);
      }
    }
  }
  // If there have been any inline definitions, output
  // "instantiations" for them. These are protected by a macro, such
  // that they are only effective when included by the corresponding
  // .c file.
  if (collect) {
    fprintf(out, "\n#line 1 \"<%s inline collect>\"\n", nameuniq);
    fprintf(out, "#ifdef %s_INSTANTIATE\n", nameuniq);
    fputs(data(collect), out);
    fprintf(out, "#endif\n");
    flex_free(collect);
  }
}


int main(int argc, char* argv[static restrict argc+1]) {
  FILE* out = stdout;
  flex const* formatpre = 0;
  flex const* formatsym = 0;
  flex const* nameuniq = 0;
  flex* currentf = 0;
  if (argc > 1) {
    formatpre = flex_printf(0, "#line %%zu \"%%s\" // preprocessor\n");
    formatsym = flex_printf(0, "#line %%zu \"%%s\" // symbol\n");
    nameuniq = uniq(argv[1], "_HEADER");
    currentf = cpy(0, argv[1]);
  } else {
    fprintf(stderr, "%s: we need the source file name as first argument", argv[0]);
    return EXIT_FAILURE;
  }
  char const* run = nameuniq->data;
  // Suck all the input into one large array.
  flex const* fl = readin(stdin);
  if (!fl) return EXIT_FAILURE;
  char const*const restrict f  = data(fl);
  size_t const flen  = actual(fl);
  // The actual position in the buffer.
  size_t pos = 0;
  // The collected state of the current declaration or definition. May
  // be accumulated over several iterations.
  properties prop = 0;
  // The name of the current type, in case we have to split forward
  // declaration of a type and continuations.
  char const* currenttype = 0;
  char const* conti = "";
  // skip initial comment in the file
  size_t len = skipat(f+pos);
  // the current line number
  size_t line = 1 + grammar_lines(len, f+pos);
  if (memchr(f+pos, '@', len)) {
    writeout(out, len, f+pos);
    pos += len;
  }
  // Output include guards. These come in two levels, the proper
  // include exclusion, __CMOD_INTERNAL_<filename>_HEADER, and a
  // supplementary detection of cyclic inclusion,
  // __CMOD_CYCLIC_<filename>_HEADER.
  fprintf(out,
          "#ifdef __CMOD_CYCLIC_%s\n"
          "#error cyclic inclusion of interface specification\n"
          "#endif\n"
          "#define __CMOD_CYCLIC_%s\n"
          "#ifndef __CMOD_INTERNAL_%s\n"
          "#line 1 \"<start interface>\"\n"
          "#define __CMOD_INTERNAL_%s\n",
          run, run, run, run);
  for (;pos < flen;++pos) {
    // Skip initial white space, but only if it doesn't contain
    // comments.
    {
      size_t len = skipat(f+pos);
      size_t nl = grammar_lines(len, f+pos);
      if (memchr(f+pos, '@', len)) {
        writeout(out, len, f+pos);
      } else if (nl) {
        fputc('\n', out);
      }
      line += nl;
      pos += len;
      if (!f[pos]) break;
    }
    // These will hold the positions of the first two identifiers of a
    // declaration, if any.
    size_t positions[2] = { -1, -1, };
    size_t s = pos,                  // start of the declaration
      e = s + grammar_decl(f + s),   // end of the declaration, proper
      ie = e,                        // end of an initializer
      b = e;                         // end of an associated { } block
    // Start a new declaration, eventually.
    if (!prop) {
      prop = grammar_props(e-s, f+s, positions);
      currenttype = 0;
    }
    if (f[e] == '#' ) {
      if (s < e) {
        line += grammar_lines(e-s, f+s);
        writeout(out, e-s, f+s);
      }
      size_t before = line;
      bool declaration = false;
      // find the keyword after the #
      size_t l = grammar_prepro(f+e, &line);
      size_t first = skip(f+e+1);
      size_t w = word(f+e+1+first);
      properties kw = grammar_prop(w, f+e+1+first);
      // Only preprocessing directives that control conditional
      // execution will be forwarded.
      if (kw_control(kw)) {
        check(out, kw, false);
        // If this is a pragma, check if it is for us.
        if (bs_isin(kw_pragma, kw)) {
          kw = grammar_props(l, f+e, 0);
          switch (kw_cmod(kw)) {
          case BS_OF(kw_declaration)|BS_OF(kw_pragma)|BS_OF(kw_CMOD):
            declaration = true;
            fprintf(out, data(formatpre), before, data(currentf));
            fputs("/* #pragma CMOD declaration */", out);
            break;
          case BS_OF(kw_definition)|BS_OF(kw_pragma)|BS_OF(kw_CMOD):
            fprintf(out, data(formatpre), before, data(currentf));
            fputs("/* #pragma CMOD definition */\n", out);
            pos = e+l-1;
            continue;
          }
        }
        // If we detected the start of a declaration section we
        // process it separately.
        if (declaration) {
          pos = e+l-1;
          pos += strcspn(f+pos, "\n")+1;
          size_t declend = grammar_skip(0, f+pos, &line);
          printdeclaration(out, declend, f+pos, data(nameuniq));
          fputc('\n', out);
          /* fprintf(out, data(formatpre), line); */
          /* fputs("/\* #pragma CMOD definition *\/\n", out); */
          pos += declend;
          continue;
        } else {
          fprintf(out, data(formatpre), before, data(currentf));
          writeout(out, l, f+e);
        }
      } else {
        if (bs_isin(kw_line, kw) || isdigit(f[e+1+first])) {
          if (isdigit(f[e+1+first])) w = 0;
          flex* tmp = get_linenumber(currentf, f+e+1+first+w, &line);
          if (tmp) {
            currentf = tmp;
            fprintf(out, "#line %zu \"%s\" // detected\n", line, data(currentf));
          }
        } else {
          // Other preprocessing directives are replaced by a dummy
          // typedef such that documentation comments will not get out
          // of order and are not attached to an arbitrary, following
          // declaration.
          fprintf(out, data(formatpre), line-1, data(currentf));
          fputs("typedef int __internal_dummy_type_to_be_ignored; // control\n", out);
        }
      }
      pos = e+l-1;
      continue;
    } else {
      // Now we know we have identified the next declaration or
      // definition.
      if (f[e] == '=') {
        ie = grammar_init(f+e+1) + e + 1;
      }
      if (f[ie] != '{') {
        pos = ie;
      } else {
        if (f[ie]) {
          b = ie + grammar_block(f+ie);
          pos = b;
        } else {
          pos = ie;
        }
      }
    }
    // A chunk is internal if
    // - it is static or typedef
    // - it is a struct, union or enum definition
    if (!kw_internal(prop) && !grammar_tagtype(e-s, f+s)) {
      fprintf(out, data(formatsym), line, data(currentf));
      // Certain declarations must be prefixed with extern.
      if (!kw_noextern(prop)) {
        // This is a continuation that also needs the typename in
        // addition.
        if (currenttype) {
          int l = word(currenttype);
          fprintf(out, "extern %.*s ", l, currenttype);
          currenttype = 0;
        } else {
          /*    "1234567"    */
          fputs("extern ", out);
        }
        prop |= BS_OF(kw_extern);
      }
      // An inline function has a block and the inline keyword.
      if ((b > e) && bs_isin(kw_inline, prop)) {
        // For this kind of (regular) inline function we don't have to
        // take provisions for an instantiation. This is taken care of
        // in the .c file.
        writeout(out, b-s, f+s);
        prop = 0;
      } else {
        // Otherwise, we must only copy the declaration part.  What
        // follows is some uninteresting magic to make continuations
        // look nicer.
        fputs(conti, out);
        // Retract while we see spaces.
        while (isspace(f[e-1])) --e;
        writeout(out, e-s, f+s);
        static char const blanks[] = "                                        ";
        int pad = sizeof blanks;
        if (e-s < pad) pad -= e-s+1;
        else pad = 0;
        switch (f[ie]) {
        case ',' :
          fprintf(out, ", %.*s/* continuation */", pad, blanks);
          /*      "1234567"    */
          conti = "       ";
          break;
        default  : fprintf(out, "/* strange %c */", f[ie]);
        case '{' : ;
        case ';' :
          fputc(';', out);
          prop = 0;
          conti = "";
          break;
        }
      }
      fputc('\n', out);
    } else {
      // A typedef always expects a terminating ";"
      if (bs_isin(kw_typedef, prop)) {
        if (f[pos] == ';') {
          prop = 0;
          fputs("typedef int __internal_dummy_type_to_be_ignored; // hiding local typedef\n", out);
        }
      } else {
        properties kwset = kw_typename(prop);
        if (kwset) {
          if (bs_isin(kw_static, prop)) {
            fputs("typedef int __internal_dummy_type_to_be_ignored; // hiding static tagtype\n", out);
            if (f[pos] == ';') prop = 0;
          } else {
            // Handling tagtypes is somewhat delicate. They can hide
            // symbol definitions that come as continuations. Not all
            // of them are valid for our approach. In particular,
            // exporting a symbol with a private type makes not much
            // sense, and cannot be handled at this level.
            char const* type = "struct";
            switch (kwset) {
            case BS_OF(kw_enum):
              fputs("// Here we have a private enum type.\n"
                    "// Since enum types can't even be forward declared, this can never work to declare\n"
                    "// external symbol that is based on it.\n"
                    "// You should consider declaring such a symbol static.\n",
                    out);
              fprintf(out, data(formatsym), line, data(currentf));
              fputs("typedef int __internal_dummy_type_to_be_ignored; // private enum\n", out);
              break;
            case BS_OF(kw_union): type = "union";
              // fall through
            case BS_OF(kw_struct):
              if (positions[0] < -1) {
                currenttype = f+s+positions[0];
                int l = word(currenttype);
                fprintf(out,
                        "// We are typedefing a %s type, such that we can declare the following symbol(s).\n"
                        "// To be exported, such a symbol should be a pointer to %.*s.\n"
                        "// If not, you should consider declaring such a symbol static.\n"
                        "typedef %s %.*s %.*s;\n",
                        type, l, currenttype, type, l, currenttype, l, currenttype);
              } else {
                fprintf(out,
                        "// We can't typedef an anonymous %s.\n"
                        "// Consider declaring symbols of that type static.\n",
                        type);
                fprintf(out, data(formatsym), line, data(currentf));
                fputs("#error external symbol with internal type, consider declaring it static\n",
                      out);
              }
            }
            if (f[pos] == ';') {
              prop = 0;
              currenttype = 0;
            }
          }
          // So the only possible continuation is that it is not a tag
          // type definition and thus a static symbol. A static function
          // never has a continuation and thus also never has a comma
          // following it. Now, the last case is a static object, and
          // that can only have a continuation, if it is followed by a
          // comma.
        } else if (f[pos] != ',') {
          prop = 0;
          fputs("typedef int __internal_dummy_type_to_be_ignored; // hiding static symbol\n", out);
        }
      }
    }
    if (f[b] == '\n') ++line;
    if (b < ie) line += grammar_lines(ie-s, f+s);
    else line += grammar_lines(b-s, f+s);
  }
  fprintf(out,
          "#endif /* __CMOD_INTERNAL_%s */\n"
          "#undef __CMOD_CYCLIC_%s\n",
          run, run);
  flex_free(fl);
  flex_free(formatsym);
  flex_free(formatpre);
  flex_free(nameuniq);
  return EXIT_SUCCESS;
}
