#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools-keyword.h"
#include "tools-flex.h"
#include "tools-functions.h"
#include "tools-grammar.h"
#include "tools-bs.h"

/**
 ** @file
 **
 ** This implements a rudimentary parser that reads a C file and spits
 ** out an annotated list of all global identifiers.
 **
 **/

void printenumeration(FILE*restrict out, size_t slen, char const f[static restrict slen]) {
  for (size_t pos = 0; pos < slen; ++pos) {
  RETRY:
    pos += skipat(f+pos);
    if (f[pos] == '#' ) {
      pos += grammar_prepro(f+pos, &(size_t){ 0 });
      goto RETRY;
    }
    size_t s = pos, e = s + grammar_decl(f + s), ie = e;
    if (f[e] == '=') {
      ie = grammar_init(f+e+1) + e + 1;
    }
    size_t positions[2] = { -1, -1, };
    grammar_props(e-s, f+s, positions);
    if (positions[0] < -1) {
      fputs("enumconstant: ", out);
      wordout(out, f+s+positions[0]);
      fputc('\n', out);
      pos = ie + 1;
    } else {
      break;
    }
  }
}


#define TYPECASE(X)                                                     \
  case BS_OF(kw_ ## X): kw = kw_ ## X


int main(int argc, char* argv[static restrict argc+1]) {
  FILE*restrict out = stdout;
  flex const* fl = readin(stdin);
  if (!fl) return EXIT_FAILURE;
  char const*const restrict f  = data(fl);
  size_t pos = 0;
  properties prop = 0;
  size_t len = skipat(f+pos);
  if (memchr(f+pos, '@', len)) {
    lineout(out, len, f+pos);
    pos += len;
  }
  for (;f[pos];++pos) {
    {
      size_t len = skipat(f+pos);
      if (len) {
        fputs("// skipped |", out);
        lineout(out, len, f+pos);
        fputs("|\n", out);
        pos += len;
        //fprintf(out, "// found %.6s\n", f+pos);
      }
    }
    size_t s = pos, e = s + grammar_decl(f + s), ie = e, b = e;
    size_t positions[2] = { -1, -1, };
    prop |= grammar_props(e-s, f+s, positions);
    if (!f[e]) break;
    if (f[e] == '#' ) {
      size_t l = grammar_prepro(f+e, &(size_t){ 0 });
      size_t first = skip(f+e+1);
      size_t wl = word(f+e+1+first);
      properties kw = grammar_prop(wl, f+e+1+first);
      if (bs_isin(kw_define, kw)) {
        first += wl;
        first += skip(f+e+1+first);
        size_t l = word(f+e+1+first);
        if (f[e+1+first+l] == '(')
          fputs("function macro: ", out);
        else
          fputs("object macro: ", out);
        lineout(out, l, f+e+1+first);
        fputc('\n', out);
      }
      pos = e+l-1;
      continue;
    } else {
      if (f[e] == '=') {
        ie = grammar_init(f+e+1) + e + 1;
      }
      if (f[ie] != '{') {
        pos = ie;
      } else {
        b = ie + grammar_block(f+ie);
        pos = b;
      }
    }
    if (!kw_internal(prop) && !grammar_tagtype(e-s, f+s)) {
      if (!kw_noextern(prop)) {
        prop |= BS_OF(kw_extern);
      }
      if ((positions[0] < -1) && (kw_basetype(prop) || (positions[1] < -1))) {
        if (positions[1] < -1) {
          fputs("two ids: ", out);
          wordout(out, f+s+positions[1]);
        } else {
          fputs("basetype: ", out);
          wordout(out, f+s+positions[0]);
        }
      } else {
        // a symbol
        if (positions[0] < -1) {
          kw_print(out, prop);
          fputs(" symbol: ", out);
          wordout(out, f+s+positions[0]);
        } else {
          fputs("// empty ", out);
        }
      }
      switch (f[ie]) {
      case ',' :
      case '}' :
        break;
      case '{' : ;
      default  : ;
      case ';' : ;
        prop = 0;
        break;
      }
      fputc('\n', out);
    } else {
      if (!bs_isin(kw_static_assert, prop)&&!bs_isin(kw__Static_assert, prop)) {
        if (bs_isin(kw_typedef, prop)) {
          if (f[pos] == ';' || f[pos] == ',') {
            fputs("typedef: ", out);
            if (positions[1] < -1) {
              wordout(out, f+s+positions[1]);
            } else {
              wordout(out, f+s+positions[0]);
            }
            fputc('\n', out);
            if (f[pos] == ';')
              prop = 0;
            pos = ie;
            continue;
          }
        }
        properties typename = kw_typename(prop);
        keyword kw = 0;
        if (typename) {
          switch(typename){
            TYPECASE(struct); pos = b-1;     prop = bs_minus(prop, BS_OF(kw_struct)); break;
            TYPECASE(union);  pos = b-1;     prop = bs_minus(prop, BS_OF(kw_union));  break;
            TYPECASE(enum);
            if (positions[0] < -1) {
              fprintf(out, "%s: ", kw_words[kw]);
              wordout(out, f+s+positions[0]);
            } else {
              fprintf(out, "// anonymous %s type ", kw_words[kw]);
            }
            fputc('\n', out);
            printenumeration(out, b-e, f+e+1);
            prop = bs_minus(prop, BS_OF(kw_enum));
            pos = b-1;
            continue;
          }
          if (positions[0] < -1) {
            fprintf(out, "%s: ", kw_words[kw]);
            wordout(out, f+s+positions[0]);
          } else {
            fprintf(out, "// anonymous %s type ", kw_words[kw]);
          }
          fputc('\n', out);
          continue;
        } else if ((positions[0] < -1) && (kw_basetype(prop) || (positions[1] < -1))) {
          size_t pp = (positions[1] < -1) ? positions[1] : positions[0];
          fputs("symbol: ", out);
          wordout(out, f+s+pp);
          fputc('\n', out);
        } else {
          kw_print(out, prop);
          fputs(" others: ", out);
          lineout(out, e-s, f+s);
          fputc('\n', out);
        }
      }
      if (f[pos] != ',') {
        prop = 0;
      }
    }
  }
  flex_free(fl);
  return EXIT_SUCCESS;
}
