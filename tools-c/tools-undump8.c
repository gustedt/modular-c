/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
// This lists all includes explicitly, such that this later on also
// compiles as Modular C.
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"
#include "tools-functions.h"

/**
 ** @file
 **
 ** @brief An adhoc utility to transform code points of the form
 ** <code>\uXXXX</code> or <code>\UXXXXXXX</code> into UTF-8.
 **/

int main(int argc, char* argv[argc+1]) {
  FILE* fout = stdout;
  FILE* in = stdin;
  size_t f = 1;
  // Loop over all command line arguments and interpret them as
  // filenames. If there is none or the first argument is a single -,
  // use stdin.
  if (argc < 2 || !strcmp("-", argv[1]))
    goto ONCE;
  for (; f < argc; ++f) {
    if (!freopen(argv[f], "r", in)) goto FREOPEN;
ONCE:;
    char buffer[bsize];
    while (chunk(in, bsize, buffer)) {
      char const* p = buffer;
      while (*p) {
        char const* next = strchr(p, '\\');
        if (!next) {
          fputs(p, fout);
          break;
        } else {
          writeout(fout, next-p, p);
          p = next + 1;
        }
        // Here we know that we have seen a backslash.
        int len = 4;
        switch (*p) {
        case 0: ungetc('\\', in); goto RETRY;
        default: writeout(fout, 2, p-1); ++p; break;
        case 'U': len = 8;
        case 'u': {
          char const* zero = memchr(p, 0, len+1);
          // If the buffer ends too early, unwind it and retry.
          if (zero) {
            while (zero != p) ungetc(*--zero, in);
            ungetc('\\', in);
            goto RETRY;
          }
          // Now we know that there are enough characters in the
          // buffer.
          uint64_t val = 0;
          int ret;
          if (len > 4)
            ret = sscanf(p+1, "%8" SCNx64, &val);
          else
            ret = sscanf(p+1, "%4" SCNx64, &val);
          if (ret < 1 || val-1 > 0x10FFFF) {
            // sscanf failed or the code is not valid. Just print the
            // input pattern.
            writeout(fout, 1, p-1);
            len = 0;
          } else {
            char out[4];
            int olen = 1;
            if (val <= 0x007F) {
              // one byte encoding, 7 bit in 8 bit
              out[0] = val;
            } else if (val <= 0x07FF) {
              // two byte encoding, 5 and 6 bit = 11 bit in 16 bit
              olen = 2;
              out[0] = 0xC0 | (val >> 6);
              out[1] = 0x80 | (val & 0x3F);
            } else if (val <= 0xFFFF) {
              // three byte encoding, 4, 6 and 6 bit = 16 bit in 24 bit
              olen = 3;
              out[0] = 0xE0 | (val >> 12);
              out[1] = 0x80 | ((val >> 6) & 0x3F);
              out[2] = 0x80 | (val & 0x3F);
            } else {
              // four byte encoding, 3, 6, 6 and 6 bit = 21 bit in 32 bit
              olen = 4;
              out[0] = 0xF0 | (val >> 18);
              out[1] = 0x80 | ((val >> 12) & 0x3F);
              out[2] = 0x80 | ((val >> 6) & 0x3F);
              out[3] = 0x80 | (val & 0x3F);
            }
            writeout(fout, olen, out);
          }
          p += len+1;
        }
        }
      }
RETRY:;
    }
  }
  return EXIT_SUCCESS;
FREOPEN:
  perror(argv[0]);
  if (argc > 1)
    fprintf(stderr, "\twhen trying to open \"%s\"\n", argv[f]);
  return EXIT_FAILURE;
}
