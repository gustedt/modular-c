/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2018 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#line 0 "tools-flex.X" // preprocessor
#define CMOD_746F6F6C732D666C65782E58_HEADER_INSTANTIATE
#line 0 "tools-flex.X" // preprocessor
#include "tools-flex.h"
#line 14 "tools-flex.X" // preprocessor
/**
 ** @file
 **
 ** @brief A simple flexible string type à la C.
 **
 ** This is a type that extends usual C string properties (null
 ** termination) with the possibility to have instant access to the
 ** length and the end of the string, and to have such strings grow
 ** dynamically if needed.
 **
 ** They are used through pointers, in fact it is almost never a good
 ** idea to have a <code>flex</code> allocated on the stack or even as static.
 **
 ** Most functions working on <code>flex*</code> return also such a
 ** pointer. The idea is that this gives the function the possibility
 ** to reallocate the object at a different place in memory if its
 ** needs to, and to return the new position.
 **
 ** All <code>flex</code> functions can safely receive a null pointer
 ** as their <code>flex*</code> argument. They will then simply
 ** allocate space and return the newly created object.
 **/

#line 38 "tools-flex.X" // preprocessor
/* #pragma CMOD declaration */
#line 85 "tools-flex.X" // preprocessor
/* #pragma CMOD definition */

#line 87 "tools-flex.X" // preprocessor
#include <errno.h>
#line 88 "tools-flex.X" // preprocessor
#include <stdarg.h>

#line 90 "tools-flex.X" // symbol
extern inline size_t actual(flex const* buf);


#line 94 "tools-flex.X" // symbol
extern inline size_t total(flex const* buf);


#line 98 "tools-flex.X" // symbol
extern inline char* flex_data_mutable(flex* buf);


#line 102 "tools-flex.X" // symbol
extern inline char const* flex_data_const(flex const* buf);


#line 106 "tools-flex.X" // symbol
extern inline size_t remaining(flex const* buf);


#line 110 "tools-flex.X" // symbol
extern inline char* flex_next_mutable(flex* buf);


#line 116 "tools-flex.X" // symbol
extern inline char const* flex_next_const(flex const* buf);


#line 122 "tools-flex.X" // symbol
extern inline flex* cut(flex* buf, size_t s);


/**
 ** @brief Enlarge or shrink the flex to maximum size @a s, including
 ** the @c 0 termination.
 **
 ** This doesn't provide data if @a s is bigger than the actual size,
 ** and so the contents of the string will remain the same.
 **
 ** If on the other hand @a s is smaller, the string will be cut at
 ** that character, so the actual string length will be
 ** <code>s-1</code>.
 **
 ** As as special case, @a s of @c 0 will completely deallocate the
 ** whole string and return a null pointer.
 **/
#line 146 "tools-flex.X" // symbol
extern inline flex* resize(flex* buf, size_t s);


#line 162 "tools-flex.X" // symbol
extern inline void flex_free(flex const* buf);


#line 166 "tools-flex.X" // symbol
extern inline flex* flex_strncat_char_inner(flex*restrict buf, size_t len, char const txt[static restrict len]);


#line 179 "tools-flex.X" // symbol
extern inline flex* flex_strncat_char(flex*restrict buf, size_t len, char const txt[static restrict len]);


#line 185 "tools-flex.X" // symbol
extern inline flex* flex_strncat_flex(flex*restrict buf, size_t len,  flex const*restrict src);


#line 191 "tools-flex.X" // symbol
extern inline flex* flex_strncpy_char(flex*restrict buf, size_t len, char const txt[static restrict len]);


#line 196 "tools-flex.X" // symbol
extern inline flex* flex_strncpy_flex(flex*restrict buf, size_t len,  flex const*restrict src);


#line 201 "tools-flex.X" // symbol
extern inline flex* flex_strcat_char(flex*restrict buf, char const txt[static restrict 1]);


#line 206 "tools-flex.X" // symbol
extern inline flex* flex_strcat_char3(flex*restrict buf, char const txt[static restrict 1], int _ignore);


#line 210 "tools-flex.X" // symbol
extern inline flex* flex_strcat_flex(flex*restrict buf, flex const*restrict src);


#line 216 "tools-flex.X" // symbol
extern inline flex* flex_strcat_flex3(flex*restrict buf, flex const*restrict src, int _ignore);


#line 220 "tools-flex.X" // symbol
extern inline flex* flex_strcpy_char(flex*restrict buf, char const txt[static restrict 1]);


#line 225 "tools-flex.X" // symbol
extern inline flex* flex_strcpy_flex(flex*restrict buf, flex const*restrict src);


#line 230 "tools-flex.X" // symbol
extern inline flex* flex_strcpy_char3(flex*restrict buf, char const txt[static restrict 1], int _ignore);


#line 234 "tools-flex.X" // symbol
extern inline flex* flex_strcpy_flex3(flex*restrict buf, flex const*restrict src, int _ignore);


#line 238 "tools-flex.X" // symbol
extern inline int flex_fputs(flex const*restrict buf, FILE* out);


#line 244 "tools-flex.X" // symbol
extern inline bool full(flex const* buf);


#line 248 "tools-flex.X" // symbol
size_t flex_fgets(flex*restrict buf, FILE* in) {
  if (!fgets(next(buf), remaining(buf)+1, in))
    return 0;
  size_t found = strlen(buf->data+(buf->actual));
  buf->actual += found;
  return found;
}


#line 256 "tools-flex.X" // symbol
flex* flex_printf(flex*restrict buf, char const format[static restrict 1], ...) {
  int avail  = total(buf)-actual(buf);
  va_list ap;
  va_start(ap, format);
  int size = vsnprintf(data(buf), avail, format, ap);
  va_end(ap);

  if (size > avail) {
    size_t needed = actual(buf) + size + 1;
    if (needed < 2*total(buf)) needed = 2*total(buf);
    buf = resize(buf, needed);
    if (buf) {
      va_start(ap, format);
      size = vsnprintf(data(buf), size + 1, format, ap);
      va_end(ap);
    }
  }
  if (buf && size > 0) {
    buf->actual += size;
  }
  return buf;
}

