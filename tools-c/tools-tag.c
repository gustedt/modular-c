#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools-keyword.h"
#include "tools-flex.h"
#include "tools-functions.h"
#include "tools-grammar.h"

/**
 ** @file
 **
 ** This implements a simple augmentation of C code by adding @c typedef
 ** to tagged type definitions. For a structure type
 ** @code
 ** struct toto {
 **   struct toto* next;
 **   double val;
 ** };
 ** @endcode
 ** this would just look like
 ** @code
 ** typedef struct toto toto; struct toto {
 **   struct toto* next;
 **   double val;
 ** };
 ** @endcode
 **
 ** that is the structure definition is prefixed by a typedef with the
 ** same identifier as the tag name. When using this, you can then
 ** systematically drop the use of the @c struct keyword in all other
 ** places than the definition.
 ** @code
 ** typedef struct toto toto; struct toto {
 **   toto* next;
 **   double val;
 ** };
 ** @endcode
 **
 ** The rules for @c union types are completely analogous.
 **
 ** For @c enum there is a difference: the @c typedef comes *after*
 ** the definition:
 ** @code
 ** enum twins {
 **   romulus,
 **   remus,
 ** }; typedef enum twins twins;
 **
 ** The reason that we put it on the same line, is that we don't want
 ** to mess up the line structure of the code such that compiler
 ** diagnosis can always point us to the correct line if there are
 ** problems.
 **/

#define TYPECASE(X)                                                     \
  case BS_OF(kw_ ## X): kw = kw_ ## X; len = sizeof #X - 1; needle = #X


int main(int argc, char* argv[static restrict argc+1]) {
  FILE* out = stdout;
  char const*const filename = (argc > 1)
    ? argv[1]
    :"unknown file";
  flex* il = flex_printf(0,
                         "#ifdef __CMOD_INTERNAL_COMPILATION\n"
                         "/* Collected instantiations for inline functions. */\n"
                         "#line 1 \"instantiation in %s\"\n",
                         filename);
  size_t const li = actual(il);
  flex const* fl = readin(stdin);
  if (!fl) return EXIT_FAILURE;
  char const*const restrict f  = data(fl);
  size_t line = 1;
  size_t pos = 0;
  properties prop = 0;
  int len = 0;
  char const* enumname = 0;
  for (;f[pos];++pos) {
    {
      size_t len = skipat(f+pos);
      line += grammar_lines(len, f+pos);
      writeout(out, len, f+pos);
      pos += len;
    }
    size_t s = pos, e = s + grammar_decl(f + s), ie = e, b = e;
    if (!f[e]) break;
    if (f[e] == '#' ) {
      if (s < e) {
        line += grammar_lines(e-s, f+s);
        writeout(out, e-s, f+s);
      }
      size_t l = grammar_prepro(f+e, &line);
      writeout(out, l, f+e);
      pos = e+l-1;
      continue;
    }
    if (!prop) prop = grammar_props(e-s, f+s, 0);
    if (f[e] == '=') {
      ie = grammar_init(f+e+1) + e + 1;
    }
    if (f[ie] != '{') {
      pos = ie;
    } else {
      b = ie + grammar_block(f+ie);
      pos = b;
    }
    if (b < ie) {
      writeout(out, ie-s, f+s);
      line += grammar_lines(ie-s, f+s);
    } else {
      if (!kw_internal(prop)) {
        if (kw_typename(prop)) {
          keyword kw = -1;
          char const* needle = 0;
          switch(kw_typename(prop)){
            TYPECASE(struct); break;
            TYPECASE(union);  break;
            TYPECASE(enum);   break;
          }
          if (needle) {
            char const*start = strstr(f+s, needle);
            if (start) {
              start += len;
              start += skip(start);
              len = word(start);
              if (len) {
                if (kw != kw_enum) fprintf(out, "typedef %s %.*s %.*s; ", needle, len, start, len, start);
                else enumname = start;
              }
            }
          }
        } else if (bs_isin(kw_inline, prop)) {
          il = cat(il, "extern ");
          il = cat(il, e-s, f+s);
          il = cat(il, ";\n");
        }
      }
      writeout(out, b-s, f+s);
      line += grammar_lines(b-s, f+s);
    }
    switch (f[pos]) {
    case ';' :;
      fputc(f[pos], out);
      if (enumname) fprintf(out, " typedef enum %.*s %.*s;", len, enumname, len, enumname);
      enumname = 0;
      break;
    case ',' :;
      fputc(f[pos], out);
      break;
    default: --pos;
    }
    if (f[b] == '\n') ++line;
    // A typedef always expects a terminating ";"
    if (bs_isin(kw_typedef, prop)) {
      if (f[pos] == ';') prop = 0;
    } else {
      // If it is not a typedef but a tag type definition,
      // continuations are forbidden, see the introduction.
      //
      // So the only possible continuation is that it is not a tag
      // type definition and thus a static symbol. A static function
      // never has a continuation and thus also never has a comma
      // following it. Now, the last case is a static object, and
      // that can only have a continuation, if it is followed by a
      // comma.
      if (f[pos] != ',') prop = 0;
    }
  }
  if (actual(il) > li) {
    il = cat(il, "#endif\n");
    flex_puts(il);
  }
  flex_free(il);
  flex_free(fl);
  return EXIT_SUCCESS;
}
