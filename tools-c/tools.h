#ifndef BOGUS_GUARD_H      // nasty comment
# define BOGUS_GUARD_H 1

#include <stdlib.h>

/* Use this file to test the include guard deactivation */
#define MAKES_A_DIFFERENCE 1
/* This should be detected as useless */
#ifndef DOESNT_CHANGE_ANYTHING

#endif

/* This should be detected as needed and not deactivated */
#ifndef MAKES_A_DIFFERENCE
inline
void should_never_appear(void) {

}
#endif

/* This should be detected as useless */
#if !DOESNT_CHANGE_ANYTHING

#endif

/* This should be detected as needed and not deactivated */
#if !MAKES_A_DIFFERENCE
inline
void should_never_appear_either(void) {

}
#endif

enum sizes {
  /**
   ** @brief The buffer size.
   **/
  bsize = 1024,
};

#endif
