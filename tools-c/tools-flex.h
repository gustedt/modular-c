/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2018 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#ifdef __CMOD_CYCLIC_CMOD_746F6F6C732D666C65782E58_HEADER
#error cyclic inclusion of interface specification
#endif
#define __CMOD_CYCLIC_CMOD_746F6F6C732D666C65782E58_HEADER
#ifndef __CMOD_INTERNAL_CMOD_746F6F6C732D666C65782E58_HEADER
#line 1 "<start interface>"
#define __CMOD_INTERNAL_CMOD_746F6F6C732D666C65782E58_HEADER
#line 14 "tools-flex.X" // preprocessor
#pragma CMOD module flex =
/**
 ** @file
 **
 ** @brief A simple flexible string type à la C.
 **
 ** This is a type that extends usual C string properties (null
 ** termination) with the possibility to have instant access to the
 ** length and the end of the string, and to have such strings grow
 ** dynamically if needed.
 **
 ** They are used through pointers, in fact it is almost never a good
 ** idea to have a <code>flex</code> allocated on the stack or even as static.
 **
 ** Most functions working on <code>flex*</code> return also such a
 ** pointer. The idea is that this gives the function the possibility
 ** to reallocate the object at a different place in memory if its
 ** needs to, and to return the new position.
 **
 ** All <code>flex</code> functions can safely receive a null pointer
 ** as their <code>flex*</code> argument. They will then simply
 ** allocate space and return the newly created object.
 **/

#line 38 "tools-flex.X" // preprocessor
/* #pragma CMOD declaration */
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct flex flex; struct flex {
  size_t actual;
  size_t total;
  char data[];
};

#define next(B) _Generic((B), flex*: flex_next_mutable, default: flex_next_const)(B)
#define data(B) _Generic((B), flex*: flex_data_mutable, default: flex_data_const)(B)

#define flex_cat3(_0, _1, _2, ...)                                      \
  _Generic((_1),                                                        \
           char*: flex_strcat_char3,                                    \
           char const*: flex_strcat_char3,                              \
           flex*: flex_strcat_flex3,                                    \
           flex const*: flex_strcat_flex3,                              \
  default: _Generic((_2),                                               \
                    char*: flex_strncat_char,                           \
                    char const*: flex_strncat_char,                     \
           default: flex_strncat_flex                                   \
                    ))((_0), (_1), (_2))

#define cat(...) flex_cat3(__VA_ARGS__, 0, 0)

#define flex_cpy3(_0, _1, _2, ...)                                      \
  _Generic((_1),                                                        \
           char*: flex_strcpy_char3,                                    \
           char const*: flex_strcpy_char3,                              \
           flex*: flex_strcpy_flex3,                                    \
           flex const*: flex_strcpy_flex3,                              \
  default: _Generic((_2),                                               \
                    char*: flex_strncpy_char,                           \
                    char const*: flex_strncpy_char,                     \
           default: flex_strncpy_flex                                   \
                    ))((_0), (_1), (_2))

#define cpy(...) flex_cpy3(__VA_ARGS__, 0, 0)

#define flex_gets(B) flex_fgets((B), stdin)
#define flex_puts(B) flex_fputs((B), stdout)

#line 85 "tools-flex.X" // preprocessor
/* #pragma CMOD definition */

#line 87 "tools-flex.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control
#line 88 "tools-flex.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control

#line 90 "tools-flex.X" // symbol
inline size_t actual(flex const* buf) {
  return (buf ? buf->actual : 0);
}

#line 94 "tools-flex.X" // symbol
inline size_t total(flex const* buf) {
  return (buf ? buf->total : 0);
}

#line 98 "tools-flex.X" // symbol
inline char* flex_data_mutable(flex* buf) {
  return (buf ? buf->data : 0);
}

#line 102 "tools-flex.X" // symbol
inline char const* flex_data_const(flex const* buf) {
  return (buf ? buf->data : 0);
}

#line 106 "tools-flex.X" // symbol
inline size_t remaining(flex const* buf) {
  return buf ? (total(buf)-actual(buf)-1) : 0;
}

#line 110 "tools-flex.X" // symbol
inline char* flex_next_mutable(flex* buf) {
  return remaining(buf)
    ? buf->data + buf->actual
    : 0;
}

#line 116 "tools-flex.X" // symbol
inline char const* flex_next_const(flex const* buf) {
  return remaining(buf)
    ? buf->data + buf->actual
    : 0;
}

#line 122 "tools-flex.X" // symbol
inline flex* cut(flex* buf, size_t s) {
  if (buf) {
    if (s < buf->actual) {
      buf->actual = s;
      buf->data[s] = 0;
    }
  }
  return buf;
}

/**
 ** @brief Enlarge or shrink the flex to maximum size @a s, including
 ** the @c 0 termination.
 **
 ** This doesn't provide data if @a s is bigger than the actual size,
 ** and so the contents of the string will remain the same.
 **
 ** If on the other hand @a s is smaller, the string will be cut at
 ** that character, so the actual string length will be
 ** <code>s-1</code>.
 **
 ** As as special case, @a s of @c 0 will completely deallocate the
 ** whole string and return a null pointer.
 **/
#line 146 "tools-flex.X" // symbol
inline flex* resize(flex* buf, size_t s) {
  if (!s) {
    free(buf);
    return 0;
  }
  flex* nbuf = realloc(buf, offsetof(flex, data) + s);
  if (!nbuf) return buf;
  if (buf) {
    cut(nbuf, s-1);
  } else {
    memset(nbuf, 0, offsetof(flex, data)+1);
  }
  nbuf->total = s;
  return nbuf;
}

#line 162 "tools-flex.X" // symbol
inline void flex_free(flex const* buf) {
  free((void*)buf);
}

#line 166 "tools-flex.X" // symbol
inline flex* flex_strncat_char_inner(flex*restrict buf, size_t len, char const txt[static restrict len]) {
  size_t needed = actual(buf) + len + 1;
  if (needed > total(buf)) {
    buf = resize(buf, 2*needed);
  }
  if (needed <= total(buf)) {
    memcpy(next(buf), txt, len);
    buf->actual += len;
    buf->data[buf->actual] = 0;
  }
  return buf;
}

#line 179 "tools-flex.X" // symbol
inline flex* flex_strncat_char(flex*restrict buf, size_t len, char const txt[static restrict len]) {
  char const* etxt = memchr(txt, 0, len);
  if (etxt) len = etxt-txt;
  return flex_strncat_char_inner(buf, len, txt);
}

#line 185 "tools-flex.X" // symbol
inline flex* flex_strncat_flex(flex*restrict buf, size_t len,  flex const*restrict src) {
  if (!src) return buf;
  if (len > actual(src)) len = actual(src);
  return flex_strncat_char_inner(buf, len, data(src));
}

#line 191 "tools-flex.X" // symbol
inline flex* flex_strncpy_char(flex*restrict buf, size_t len, char const txt[static restrict len]) {
  cut(buf, 0);
  return flex_strncat_char(buf, len, txt);
}

#line 196 "tools-flex.X" // symbol
inline flex* flex_strncpy_flex(flex*restrict buf, size_t len,  flex const*restrict src) {
  cut(buf, 0);
  return flex_strncat_flex(buf, len, src);
}

#line 201 "tools-flex.X" // symbol
inline flex* flex_strcat_char(flex*restrict buf, char const txt[static restrict 1]) {
  size_t len = strlen(txt);
  return flex_strncat_char_inner(buf, len, txt);
}

#line 206 "tools-flex.X" // symbol
inline flex* flex_strcat_char3(flex*restrict buf, char const txt[static restrict 1], int _ignore) {
  return flex_strcat_char(buf, txt);
}

#line 210 "tools-flex.X" // symbol
inline flex* flex_strcat_flex(flex*restrict buf, flex const*restrict src) {
  if (!src) return buf;
  size_t len = actual(src);
  return flex_strncat_char_inner(buf, len, data(src));
}

#line 216 "tools-flex.X" // symbol
inline flex* flex_strcat_flex3(flex*restrict buf, flex const*restrict src, int _ignore) {
  return flex_strcat_flex(buf, src);
}

#line 220 "tools-flex.X" // symbol
inline flex* flex_strcpy_char(flex*restrict buf, char const txt[static restrict 1]) {
  cut(buf, 0);
  return flex_strcat_char(buf, txt);
}

#line 225 "tools-flex.X" // symbol
inline flex* flex_strcpy_flex(flex*restrict buf, flex const*restrict src) {
  cut(buf, 0);
  return flex_strcat_flex(buf, src);
}

#line 230 "tools-flex.X" // symbol
inline flex* flex_strcpy_char3(flex*restrict buf, char const txt[static restrict 1], int _ignore) {
  return flex_strcpy_char(buf, txt);
}

#line 234 "tools-flex.X" // symbol
inline flex* flex_strcpy_flex3(flex*restrict buf, flex const*restrict src, int _ignore) {
  return flex_strcpy_flex(buf, src);
}

#line 238 "tools-flex.X" // symbol
inline int flex_fputs(flex const*restrict buf, FILE* out) {
  return buf
    ? fputs(buf->data, out)
    : EOF;
}

#line 244 "tools-flex.X" // symbol
inline bool full(flex const* buf) {
  return actual(buf)+1 >= total(buf);
}

#line 248 "tools-flex.X" // symbol
extern size_t flex_fgets(flex*restrict buf, FILE* in);

#line 256 "tools-flex.X" // symbol
extern flex* flex_printf(flex*restrict buf, char const format[static restrict 1], ...);
#endif /* __CMOD_INTERNAL_CMOD_746F6F6C732D666C65782E58_HEADER */
#undef __CMOD_CYCLIC_CMOD_746F6F6C732D666C65782E58_HEADER
