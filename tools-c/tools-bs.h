/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

#ifdef __CMOD_CYCLIC_CMOD_746F6F6C732D62732E58_HEADER
#error cyclic inclusion of interface specification
#endif
#define __CMOD_CYCLIC_CMOD_746F6F6C732D62732E58_HEADER
#ifndef __CMOD_INTERNAL_CMOD_746F6F6C732D62732E58_HEADER
#line 1 "<start interface>"
#define __CMOD_INTERNAL_CMOD_746F6F6C732D62732E58_HEADER
#line 3 "tools-bs.X" // preprocessor
/* #pragma CMOD declaration */
#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/**
 ** @brief handle bit sets
 **/
typedef uint64_t bs;

#define BS_OF(X)  (UINT64_C(1)<<(X))

#define BS_ARRAY(T, ...) ((T const[]){ __VA_ARGS__ })
#define BS_ALEN(T, ...) (sizeof(BS_ARRAY(T, __VA_ARGS__))/sizeof(T))

#define BS_SET(...) bs_set(BS_ALEN(unsigned, __VA_ARGS__), BS_ARRAY(unsigned, __VA_ARGS__))

#line 23 "tools-bs.X" // preprocessor
/* #pragma CMOD definition */

#line 26 "tools-bs.X" // symbol
inline
bs bs_intersect(bs a, bs b) {
  return a & b;
}

#line 31 "tools-bs.X" // symbol
inline
bs bs_isin(unsigned x, bs set) {
  return bs_intersect(BS_OF(x), set);
}

#line 36 "tools-bs.X" // symbol
inline
bs bs_union(bs a, bs b) {
  return a | b;
}

#line 41 "tools-bs.X" // symbol
inline
bs bs_complement(bs a) {
  return ~a;
}

#line 46 "tools-bs.X" // symbol
inline
bs bs_minus(bs a, bs b) {
  return bs_intersect(a, bs_complement(b));
}

#line 51 "tools-bs.X" // symbol
inline
bs bs_difference(bs a, bs b) {
  return bs_minus(bs_union(a, b), bs_intersect(a, b));
}

#line 58 "tools-bs.X" // symbol
inline
unsigned bs_min(size_t len, unsigned const a[static restrict len]) {
  switch (len) {
  case 0: return -1;
  case 1: return a[0];
  }
  size_t const len2 = len/2;
  size_t const len1 = len-len2;
  unsigned const left = bs_min(len1, a);
  unsigned const right= bs_min(len2, a+len1);
  return (left < right) ? left : right;
}

#line 71 "tools-bs.X" // symbol
inline
unsigned bs_max(size_t len, unsigned const a[static restrict len]) {
  switch (len) {
  case 0: return 0;
  case 1: return a[0];
  }
  size_t const len2 = len/2;
  size_t const len1 = len-len2;
  unsigned const left = bs_max(len1, a);
  unsigned const right= bs_max(len2, a+len1);
  return (left > right) ? left : right;
}

/**
 ** @brief Generate the bitset of all members of the array.
 **
 ** This will usually not be used directly, but through the #BS_SET
 ** macro.
 **
 ** If it is called with an array that is filled with values that are
 ** known at compile time, this should be inlined in place by the
 ** caller. It should lead to the compile time computation of a bit
 ** mask and a simple bitwise and against this bit mask.
 **
 ** The implementation is recursive to trick the compiler into
 ** considering each of the branches to return a compile time result.
 **/
#line 98 "tools-bs.X" // symbol
inline
bs bs_set(size_t len, unsigned const a[static restrict len]) {
  switch (len) {
  case 0: return 0;
  case 1: return BS_OF(a[0]);
  }
  size_t const len2 = len/2;
  size_t const len1 = len-len2;
  return bs_union(bs_set(len1, a), bs_set(len2, a+len1));
}
#endif /* __CMOD_INTERNAL_CMOD_746F6F6C732D62732E58_HEADER */
#undef __CMOD_CYCLIC_CMOD_746F6F6C732D62732E58_HEADER
