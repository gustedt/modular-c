/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#ifdef __CMOD_CYCLIC_CMOD_746F6F6C732D66756E6374696F6E732E58_HEADER
#error cyclic inclusion of interface specification
#endif
#define __CMOD_CYCLIC_CMOD_746F6F6C732D66756E6374696F6E732E58_HEADER
#ifndef __CMOD_INTERNAL_CMOD_746F6F6C732D66756E6374696F6E732E58_HEADER
#line 1 "<start interface>"
#define __CMOD_INTERNAL_CMOD_746F6F6C732D66756E6374696F6E732E58_HEADER
#line 14 "tools-functions.X" // preprocessor
/* #pragma CMOD declaration */
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "tools-bs.h"
#include "tools-flex.h"
#line 23 "tools-functions.X" // preprocessor
/* #pragma CMOD definition */
#line 24 "tools-functions.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control
#line 25 "tools-functions.X" // preprocessor
typedef int __internal_dummy_type_to_be_ignored; // control

/**
 ** @file
 **
 ** @brief An adhoc utility to hide characters, strings and comments.
 **
 ** All of these are transformed into sequences of the form
 ** @code
 ** @ X nm nm nm nm nm nm .... @
 ** @endcode
 **
 ** Where @c nm are two character hex numbers for character code and
 ** @c X is one character with the following significance:
 **
 ** - @c C C comment
 ** - @c L @c "wide string literal"
 ** - @c M @c 'wide character'
 ** - @c P C++ comment
 ** - @c S @c "normal string literal"
 ** - @c T @c 'normal character'
 ** - @c U @c U"bla"
 ** - @c V @c U'bla'
 ** - @c u @c u"bla"
 ** - @c u8 @c u8"bla"
 ** - @c v @c u'bla'
 **
 ** Multiline comments are split at the end of each line to ensure
 ** that the line number count remains correct.
 **/

#line 56 "tools-functions.X" // symbol
inline
char* chunk(FILE*restrict in, size_t size, char buffer[static restrict size]) {
  char* ret = 0;
  if (!feof(in))
    ret = fgets(buffer, size, in);
  return ret;
}

/**
 ** @brief Return the hex character for the lower half byte @a c.
 **/
#line 67 "tools-functions.X" // symbol
inline
char xdigit(unsigned c) {
  c &= 0xF;
  // this works for ASCII and EBDIC, since for both the characters
  // 'A'...'F' are consecutive.
  signed const A10 = 'A' - 10;
  return c + (c < 10 ? '0' : A10);
}

#line 76 "tools-functions.X" // symbol
inline
unsigned udigit(unsigned c) {
  switch (c) {
  default : return 0x0;
  case '1': return 0x1;
  case '2': return 0x2;
  case '3': return 0x3;
  case '4': return 0x4;
  case '5': return 0x5;
  case '6': return 0x6;
  case '7': return 0x7;
  case '8': return 0x8;
  case '9': return 0x9;
  case 'a': return 0xa;
  case 'b': return 0xb;
  case 'c': return 0xc;
  case 'd': return 0xd;
  case 'e': return 0xe;
  case 'f': return 0xf;
  case 'A': return 0XA;
  case 'B': return 0XB;
  case 'C': return 0XC;
  case 'D': return 0XD;
  case 'E': return 0XE;
  case 'F': return 0XF;
  }
}

#line 104 "tools-functions.X" // symbol
inline
size_t skip(char const buf[static 1]) {
  return strspn(buf, " \t\n");
}

#line 109 "tools-functions.X" // symbol
inline
bs setbit(unsigned x, unsigned level) {
  unsigned l = x / 64;
  return (l == level) ? BS_OF(x%64) : 0;
}

#line 115 "tools-functions.X" // symbol
inline
bs idchar(unsigned L) {
  return (setbit('a', L)
          | setbit('b', L)
          | setbit('c', L)
          | setbit('d', L)
          | setbit('e', L)
          | setbit('f', L)
          | setbit('g', L)
          | setbit('h', L)
          | setbit('i', L)
          | setbit('j', L)
          | setbit('k', L)
          | setbit('l', L)
          | setbit('m', L)
          | setbit('n', L)
          | setbit('o', L)
          | setbit('p', L)
          | setbit('q', L)
          | setbit('r', L)
          | setbit('s', L)
          | setbit('t', L)
          | setbit('u', L)
          | setbit('v', L)
          | setbit('w', L)
          | setbit('x', L)
          | setbit('y', L)
          | setbit('z', L)
          | setbit('A', L)
          | setbit('B', L)
          | setbit('C', L)
          | setbit('D', L)
          | setbit('E', L)
          | setbit('F', L)
          | setbit('G', L)
          | setbit('H', L)
          | setbit('I', L)
          | setbit('J', L)
          | setbit('K', L)
          | setbit('L', L)
          | setbit('M', L)
          | setbit('N', L)
          | setbit('O', L)
          | setbit('P', L)
          | setbit('Q', L)
          | setbit('R', L)
          | setbit('S', L)
          | setbit('T', L)
          | setbit('U', L)
          | setbit('V', L)
          | setbit('W', L)
          | setbit('X', L)
          | setbit('Y', L)
          | setbit('Z', L)
          | setbit('_', L)
          | setbit('0', L)
          | setbit('1', L)
          | setbit('2', L)
          | setbit('3', L)
          | setbit('4', L)
          | setbit('5', L)
          | setbit('6', L)
          | setbit('7', L)
          | setbit('8', L)
          | setbit('9', L));
}

#line 182 "tools-functions.X" // symbol
inline
bool isid(unsigned char c) {
  unsigned const l = c/64U;
  unsigned const m = c%64U;
  return bs_isin(m, idchar(l));
}

#line 189 "tools-functions.X" // symbol
inline
size_t word(char const buf[static 1]) {
  for (size_t ret = 0;;++ret) {
    if (!isid(buf[ret]))
      return ret;
  }
}

#line 197 "tools-functions.X" // symbol
inline
size_t skipat2(char const buf[static 1]) {
  size_t ret = 0;
  if (buf[0] == '@') {
    {
      char const* at = strchr(&buf[1], '@');
      if (at) {
        ret = (at-&buf[1]) + 2;
      }
    }
  }
  return ret;
}

#line 211 "tools-functions.X" // symbol
extern char const* decode(FILE*restrict out, FILE*restrict in, char const*p, size_t size, char buffer[static restrict size], bool show);

#line 237 "tools-functions.X" // symbol
extern flex const* readin(FILE*restrict in);

#line 251 "tools-functions.X" // symbol
extern void writeout(FILE *restrict out, size_t len, char const buf[static restrict len]);

#line 265 "tools-functions.X" // symbol
extern void lineout(FILE *restrict out, size_t len, char const buf[static restrict len]);

#line 275 "tools-functions.X" // symbol
inline
void wordout(FILE*restrict out, char const buf[static restrict 1]) {
  for (; isid(*buf); ++buf)
    putc(*buf, out);
}

#line 281 "tools-functions.X" // symbol
extern size_t skipat(char const buf[static 1]);

#line 291 "tools-functions.X" // symbol
extern size_t count(size_t len, char const buf[static len], char needle);

#line 304 "tools-functions.X" // symbol
extern flex* uniq(char const* name, char const* suffix);

#line 317 "tools-functions.X" // symbol
extern flex* hbtob(flex* ret, size_t len, char const code[len]);

#line 328 "tools-functions.X" // symbol
extern flex* get_linenumber(flex* currentf, char const b[static 1], size_t line[static 1]);
#endif /* __CMOD_INTERNAL_CMOD_746F6F6C732D66756E6374696F6E732E58_HEADER */
#undef __CMOD_CYCLIC_CMOD_746F6F6C732D66756E6374696F6E732E58_HEADER
