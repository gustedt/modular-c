/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
// This lists all includes explicitly, such that this later on also
// compiles as Modular C.
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"
#include "tools-functions.h"

/**
 ** @file
 **
 ** @brief An adhoc utility to hide characters, strings and comments.
 **
 ** All of these are transformed into sequences of the form
 ** @code
 ** @ X nm nm nm nm nm nm .... @
 ** @endcode
 **
 ** Where @c nm are two character hex numbers for character code and
 ** @c X is one character with the following significance:
 **
 ** - @c C C comment
 ** - @c L @c "wide string literal"
 ** - @c M @c 'wide character'
 ** - @c P C++ comment
 ** - @c S @c "normal string literal"
 ** - @c T @c 'normal character'
 ** - @c U @c U"bla"
 ** - @c V @c U'bla'
 ** - @c u @c u"bla"
 ** - @c u8 @c u8"bla"
 ** - @c v @c u'bla'
 **
 ** EOL characters in multiline comments are not encoded but printed
 ** such that the line number count remains correct.
 **/

int main(int argc, char* argv[argc+1]) {
  FILE* out = stdout;
  FILE* in = stdin;
  size_t f = 1;
  // Whether or not to remove comments
  bool remove = getenv("CMOD_REMOVE_COMMENTS");
  // Loop over all command line arguments and interpret them as
  // filenames. If there is none or the first argument is a single -,
  // use stdin.
  if (argc < 2 || !strcmp("-", argv[1]))
    goto ONCE;
  for (; f < argc; ++f) {
    if (!freopen(argv[f], "r", in)) goto FREOPEN;
ONCE:;
    char buffer[bsize];
    char* p = 0;
    char k = 0;
    while (chunk(in, bsize, buffer)) {
      for (p = buffer; *p; ++p) {
RESTART:
        switch (*p) {
        default  :        fputc(*p, out);                      break;
        case '\'':   ++p; fputc('@', out); fputc('T', out); goto CHAR_LITERAL;
        case '"' :   ++p; fputc('@', out); fputc('S', out); goto STRING_LITERAL;
        case 'u' :;
        case 'L' :;
        case 'U' :;
          k = *p;
          ++p;
          switch (*p) {
          case '\'': ++p; fputc('@', out); fputc(k+1, out); goto CHAR_LITERAL;
          case '"' : ++p; fputc('@', out); fputc(k, out);   goto STRING_LITERAL;
          default:                            fputc(k, out);   goto RESTART;
          /* the weird case of UTF8 string literals */
          case '8':
            ++p;
            switch (*p) {
            case '"':
              if (k == 'u') {
                ++p; fputc('@', out); fputc('8', out); goto STRING_LITERAL;
              }
            /* fall through */
            default :     fputc(k, out);   fputc('8', out); goto RESTART;
            case 0  :     ungetc('8', in);
              /* fall through */
            }
          case 0:         ungetc(k, in);                       goto RETRY;
          }
        case '/' :
          ++p;
          switch (*p) {
          case '/':
            ++p;
            if (!remove) {
              fputc('@', out);
              fputc('P', out);
            }
            goto EOL_COMMENT;
          case '*':
            ++p;
            if (!remove) {
              fputc('@', out);
              fputc('C', out);
            }
            goto C_COMMENT;
          default :       fputc('/', out);                      goto RESTART;
          case 0  :       ungetc('/', in);
            /* fall through */
          }
        case 0   : goto RETRY;
        }
      }
RETRY:;
    }
    return 0;
CHAR_LITERAL:
    do for (; *p; ++p) {
        while (*p == '\\') {
          ++p;
          if (!*p) {
            ungetc('\\', in);
            break;
          }
          fprintf(out, "%02hhX%02hhX", '\\', *p);
          ++p;
          if (!*p) break;
        }
        if (*p == '\'') {
          fputc('@', out);
          ++p;
          if (!*p) goto RETRY;
          else goto RESTART;
        }
        fprintf(out, "%02hhX", *p);
      } while ((p = chunk(in, bsize, buffer)));
    if (!*p) goto RETRY;
    else goto RESTART;
    goto RETRY;
STRING_LITERAL:
    do for (; *p; ++p) {
        while (*p == '\\') {
          ++p;
          if (!*p) {
            ungetc('\\', in);
            break;
          }
          fprintf(out, "%02hhX%02hhX", '\\', *p);
          ++p;
          if (!*p) break;
        }
        if (*p == '"') {
          fputc('@', out);
          ++p;
          if (!*p) goto RETRY;
          else goto RESTART;
        }
        fprintf(out, "%02hhX", *p);
      } while ((p = chunk(in, bsize, buffer)));
    if (!*p) goto RETRY;
    else goto RESTART;
EOL_COMMENT:
    do for (; *p; ++p) {
        if (*p == '\n') {
          if (!remove) fputc('@', out);
          fputc('\n', out);
          ++p;
          if (!*p) goto RETRY;
          else goto RESTART;
        }
        if (!remove) fprintf(out, "%02hhX", *p);
      } while ((p = chunk(in, bsize, buffer)));
    if (!*p) goto RETRY;
    else goto RESTART;
C_COMMENT:
    do {
      for (; *p; ++p) {
        while (*p == '*') {
          ++p;
          switch (*p) {
          case 0:
            ungetc('*', in);
            goto CONTINUE;
          case '/':
            if (!remove) fputc('@', out);
            ++p;
            if (!*p) goto RETRY;
            else goto RESTART;
          }
          if (!remove) fprintf(out, "%02hhX", '*');
        }
        if (*p == '\n') fputc('\n', out);
        else if (!remove) fprintf(out, "%02hhX", *p);
      }
CONTINUE:;
    } while ((p = chunk(in, bsize, buffer)));

    if (!*p) goto RETRY;
    else goto RESTART;
  }
  return EXIT_SUCCESS;
FREOPEN:
  perror(argv[0]);
  if (argc > 1)
    fprintf(stderr, "\twhen trying to open \"%s\"\n", argv[f]);
  return EXIT_FAILURE;
}
