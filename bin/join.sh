#!/bin/sh -f

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2017

# Print all words on a separate line
slines () {
    local IFS="
"
    echo "$*"
}

# Print all words separated by spaces
splitter () {
    local IFS=' '
    echo "$*"
}

# Join the arguments in list $2 $3 ... by the string in $1 and store
# into variable named joinRet
join () {
    case $# in
        (0)
            joinRet=
            ;;
        (1)
            joinRet="$1"
            ;;
        (*)
            sj1sep="$1"
            shift
            if [ ${#sj1sep} -eq 1 ] ; then
                local IFS="${sj1sep}"
                joinRet="$*"
            else
                joinRet="$1"
                shift
                local i
                for i in $*; do
                    joinRet="${joinRet}${sj1sep}${i}"
                done
            fi
        ;;
    esac
}

alias join2=join

# Split the arguments starting at $2 according to the character in $1.
split () {
    local IFS=$1
    shift 1
    splitter $*
}

# Split the arguments starting at $3 according to the character in $1,
# and rejoin them together with the string in $2.
sj () {
    local IFS=$1
    shift 1
    join $*
}
