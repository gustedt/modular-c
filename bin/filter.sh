#!/bin/sh -f

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2017

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"
import tmpd

MKNOD=${MKNOD:=mknod}
FILT=${FILT:=${EXEDIR}/Cmod_filt}

export CHAR=${CHAR:-"∷"}
export COMP=${COMP:-"—"}

endPreamble $*

filter () {
    local filt="$*"
    if [ -z "${filt}" ] ; then
        filt="${FILT}"
    fi
    # If ${filt} works, we spawn a filter pipeline.
    if [ -x "${filt}" ] ; then
        if [ -n "${CMOD_VERBOSE}" ] ; then
            echo "switching output through pipe ${filt}" 1>&2
        fi
        newTmp pipe pipe
        ${MKNOD} "${pipe}" p
        ${filt} < "${pipe}" &
        exec 2> "${pipe}"
        exec 1> "${pipe}"
        if [ -n "${CMOD_VERBOSE}" ] ; then
            echo "stderr coming through pipe ${filt}: C::bb::oo" 1>&2
            echo "stdout coming through pipe ${filt}: C::bb::oo"
        fi
    fi
}
