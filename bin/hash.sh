#!/bin/sh -f

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2017

# A simple hash table interface. All functions have the name of the
# hash itself as first followed by the key.
. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"
import match

endPreamble $*

# For hashout this is preceded by the name of the variable in which
# the lookup will be stored.
hashout () {
    # local var=$1
    # local hash="$2"
    # local key="$3"
    # local def="$4"
    hashoutName="HASH_$2_IN_$3_HSAH"
    eval "$1=\"\${${hashoutName}:-$4}\""
}
hashin () {
    # local hash="$1"
    # local key="$2"
    # local val="$3"
    eval "HASH_$1_IN_$2_HSAH='$3'"
}
hashEcho () {
    hashout hashEchoRet $*
    echo "${hashEchoRet}"
}
# This collects data in a hash position as a ;-separated list
hashadd () {
    local hash="$1"
    local key="$2"
    local val="$3"
    local prev
    hashout prev "$1" "$2" ""
    if [ -n "${prev}" -a "${prev}" != "${val}" ]; then
        val="${prev};${val}"
    fi
    eval "HASH_${hash}_IN_${key}_HSAH='${val}'"
}
