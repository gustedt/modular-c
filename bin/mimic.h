#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <locale.h>
#include <limits.h>
#include <complex.h>

#ifdef __GNUC__
# define attr_const __attribute__((const))
#else
# define attr_const
#endif

#define cmod_printf(F, ...) printf(F "\n", __VA_ARGS__)

#define cmod_stringify_(...) #__VA_ARGS__
#define cmod_stringify(...) cmod_stringify_(__VA_ARGS__)

typedef signed char _sc;
typedef unsigned char _uc;
typedef unsigned short _us;
typedef unsigned long _ul;
typedef unsigned long long _ull;
typedef signed long _sl;
typedef signed long long _sll;
typedef long double _ld;

/* A type generic expression to produce defines that */
/* detects if X is an integer constant expression.   */
#define CMOD_ICE(X, Y, N) _Generic((1 ? (double*)0 : (void*)((X)-(X))), double*: (Y) , default: (N))

/* Return the integer rank of an expression. */
#define CMOD_RANK(X) (size_t)_Generic((X) ,     \
_Bool: 0,                                       \
  unsigned char: 1,                             \
  unsigned short: 2,                            \
  unsigned: 3,                                  \
  unsigned long: 4,                             \
  unsigned long long: 5,                        \
  signed char: 1,                               \
  signed short: 2,                              \
  signed: 3,                                    \
  signed long: 4,                               \
  signed long long: 5,                          \
  char: 1,                                      \
 default: 6)

/* A straight forward function to return the most significant
   bit. Nothing fancy, optimization would be completely insignificant,
   here. */
attr_const inline size_t cmod_msb(uintmax_t a);

inline
size_t cmod_msb(uintmax_t a) {
  size_t ret = 0;
  while (a) {
    ++ret;
    a >>= 1;
  }
  return ret;
}

size_t cmod_msb(uintmax_t a);


/* A type generic expression to produce defines that */
/* have the same type as the original expression.    */
#define cmod_frmt(X) _Generic((X),                      \
   char*:              "#define %s \"%s\"\n",           \
   char const*:        "#define %s \"%s\"\n",           \
   _Bool:              "#define %s ((_Bool)%+d)\n",     \
   char:               "#define %s %c\n",               \
   signed char:        "#define %s ((_sc)%+hhd)\n",     \
   unsigned char:      "#define %s ((_uc)(%hhu+0U))\n", \
   short:              "#define %s ((short)%+hd)\n",    \
   unsigned short:     "#define %s ((_us)(%hu+0U))\n",  \
   int:                "#define %s %d\n",               \
   long:               "#define %s %ldL\n",             \
   long long:          "#define %s %lldLL\n",           \
   unsigned int:       "#define %s %uU\n",              \
   unsigned long:      "#define %s %luUL\n",            \
   unsigned long long: "#define %s %lluULL\n",          \
   float:              "#define %s %aF\n",              \
   double:             "#define %s %a\n",               \
   long double:        "#define %s %LaL\n"              \
)

#define cmod_xtype(X) _Generic((X),                                     \
   char*:                         "char*",                              \
   char const*:                   "char const*",                        \
   char volatile*:                "char volatile*",                     \
   char const volatile*:          "char const volatile*:*",             \
   signed char*:                  "signed char*",                       \
   signed char const*:            "signed char const*",                 \
   signed char volatile*:         "signed char volatile*",              \
   signed char const volatile*:   "signed char const volatile*:*",      \
   unsigned char*:                "unsigned char*",                     \
   unsigned char const*:          "unsigned char const*",               \
   unsigned char volatile*:       "unsigned char volatile*",            \
   unsigned char const volatile*: "unsigned char const volatile*:*",    \
   void*:                         "void*",                              \
   void const*:                   "void const*",                        \
   void volatile*:                "void volatile*",                     \
   void const volatile*:          "void const volatile*:*",             \
   _Bool:                         "_Bool",                              \
   char:                          "char",                               \
   signed char:                   "signed char",                        \
   unsigned char:                 "unsigned char",                      \
   short:                         "short",                              \
   unsigned short:                "unsigned short",                     \
   int:                           "int",                                \
   long:                          "long",                               \
   long long:                     "long long",                          \
   unsigned int:                  "unsigned int",                       \
   unsigned long:                 "unsigned long",                      \
   unsigned long long:            "unsigned long long",                 \
   float:                         "float",                              \
   double:                        "double",                             \
   long double:                   "long double",                        \
   _Complex float:                "_Complex float",                     \
   _Complex double:               "_Complex double",                    \
   _Complex long double:          "_Complex long double",               \
   default: 0                                                           \
)

#define cmod_type(T) cmod_xtype((T){0})

#define cmod_atype(T) cmod_xtype(0+(T)+0)

#ifndef __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1
# define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 0
#endif
#ifndef __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2
# define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 0
#endif
#ifndef __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4
# define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 0
#endif
#ifndef __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8
# define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 0
#endif
#ifndef __GCC_HAVE_SYNC_COMPARE_AND_SWAP_16
# define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 0
#endif
