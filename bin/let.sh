#!/bin/sh

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2018

### Evaluate the argument list as an expression and substitue as a variable
# the name of the variable must be the first of the arguments
#
# the remaining substitution is then done as if we had
#
#pragma CMOD foreach NAME = VALUE
#
# where NAME is the variable name and VALUE is the computed value
. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"

# Since this uses the shell, we have to be careful with special
# characters in the expression. In particular all combinations of <,
# >, | or & are forbidden. You may use -ls for left shift and similar
# operators.

import match
import arguments

endPreamble $*

arguments name val

val=$(echo -n ${val} | ${SED} '
s+-a+ && +g
s+-ba+ & +g
s+-bo+ | +g
s+-eq+ == +g
s+-ge+ <= +g
s+-gt+ < +g
s+-le+ <= +g
s+-ls+ << +g
s+-lt+ < +g
s+-nar+ : +g
s+-ne+ != +g
s+-o+ || +g
s+-rs+ >> +g
s+-ter+ ? +g
')

val="$((${val}))"

CMOD_AMEND_ARGUMENTS="${name}=${val}" exec "${EXEDIR}/bind.sh"
