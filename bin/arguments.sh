#!/bin/sh -f

### Obtain an argument list and separate it into first word and rest.
##
##  This removes an optional = sign between the first word and the rest.

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"

endPreamble $*

arguments () {
    while [ $# -gt 1 ] ; do
        eval "$1='${CMOD_AMEND_ARGUMENTS%%[[:blank:]=]*}'"
        CMOD_AMEND_ARGUMENTS="${CMOD_AMEND_ARGUMENTS#*[[:blank:]=]}"
        # remove an optional = sign
        CMOD_AMEND_ARGUMENTS="${CMOD_AMEND_ARGUMENTS#*=}"
        shift
    done
    eval "$1='${CMOD_AMEND_ARGUMENTS}'"
    CMOD_AMEND_ARGUMENTS=""
    unset CMOD_AMEND_ARGUMENTS
}
