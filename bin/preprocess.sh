#!/bin/sh -f

# Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2018, all rights reserved.

# @brief preprocess source files for utf8 support and amendments

# @brief preprocess the source

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"

import tmpd
import brackets
import join

export CMOD_AMENDS="${CMOD_AMEND:=
bind
do
env
eval
foreach
let
}"

endPreamble $*

DUMP8=${DUMP8:=${EXEDIR}/tools-dump8}
HIDE=${HIDE:=${EXEDIR}/tools-hide}
DEF=${DEF:=${EXEDIR}/tools-def}
CMOD_EXPAND=${CMOD_EXPAND:=${EXEDIR}/expand}
SHOW=${SHOW:=${EXEDIR}/tools-show}
ID=${ID:=${EXEDIR}/tools-reduce}
SEQ="${EXEDIR}/Cmod_seq"

join "\\|" ${CMOD_AMENDS}

export _CMOD_AMENDS="${joinRet}"

# preprocess the source such that no spaces may surround # characters
hashtag="(_HASHCHAR(${jobid}))"
jointag="(_JOINTAG(${jobid}))"
septag="A(_SEPCHAR(${jobid}))A"
comptag="A(_COMPCHAR(${jobid}))A"
uc4tag="A-_UC4CHAR${jobid}-A"
uc8tag="A-_UC8CHAR${jobid}-A"
vatag="_VAARGS_${jobid}"
vateg="_VAERGS_${jobid}"
hashrep="(_HASHREP(${jobid}))"
seprep="(_SEPREP(${jobid}))"
comprep="(_COMPREP(${jobid}))"
uc4rep="_UC4REP${jobid}"
uc8rep="_UC8REP${jobid}"
pragma="_PRAGMA${jobid}"
backtag="_BACKSLASH${jobid}"

specop="@C28@"
speccl="@C29@"

incpat=${incpat:=_DONT_MATCH_IF_NOT_SET_PREVIOUSLY${jobid}}
compoU=${compoU:=_SOME_NONESENSE${jobid}}
separU=${separU:=_SOME_NONESENSE${jobid}}

idex='[[:alpha:]_][[:alnum:]_]*'

preprocess () {
    # This project needs a C compiler (sought in the CC environment
    # variable) that is conforming as much as possible to C11. Newer
    # versions of gcc or clang should do.

    local CC=${CMOD_CC:=cc}

    # For text processing, we also use the preprocessing phase of the C
    # compiler.

    local CPP="${CC} -E -x c"

    local source="$1"
    local target="$2"
    shift 2
    local tmpExpand="${tmpd}/tmpExpand.c"
    local tmpPre="${tmpd}/tmpPre.c"
    # regexp to capture pragma lines for this tool
    local start='#pragma[[:blank:]][[:blank:]]*CMOD[[:blank:]][[:blank:]]*'
    #local start='#pragma[[:blank:]][[:blank:]]*[A-Z]'

    sed3 "${source}" 3<<LONGFOR   >${tmpExpand}
    :again
    ## handle backspace
    /[\\]\$/{
             ### join long lines
             :line
             /[\\]\$/{
               N
               b line
             }
             s![\\]\n!!g
             s![\\]!${backtag}!g
             ### if this is not part of a directive, ouput the line with a following line marker
             /^${start}/!b mark
    }
    s![\\]!${backtag}!g
    /^${start}/{
             ### expand directives that are implemented as amendments
             ### be careful to replace sequences of blanks to just one space
             s!^${start}\(${_CMOD_AMENDS}\)[[:blank:]]\{1,\}!#pragma CMOD amend \1 !
             s!^${start}amend[[:blank:]]\{1,\}!#pragma CMOD amend !
             s!^${start}insert[[:blank:]]\{1,\}\(.*\)!#pragma CMOD amend \1\n#pragma CMOD done!
             s!^${start}!#pragma CMOD !
             b mark
    }
    /^[[:blank:]]*\#[[:blank:]]*/{
             ### mark all pragmas
             b mark
    }
    # See if this line ends in an opening brace.
    # This test can even result in marking inside comments, but
    # will be repaired later.
    /[{][[:blank:]]*\$/!{
      p
      d
    }
    : mark
    ### mark with the source line number
    \$!{
        n
        i ${jointag}
        =
        ### we already have read a new line
        b again
    }
LONGFOR
    ${CMOD_EXPAND} < ${tmpExpand}                       \
        | ${SED} "
s!${backtag}!\\\\!g
s!^${incpat}.*<\(.*\)\.h>.*!#pragma CMOD2 include \1!
s!^${incpat}.*\"\(.*\)\.h\".*!#pragma CMOD2 include \1!"       \
        | ${DUMP8} | ${HIDE} | sed3 3<<TRANSFORM      \
        | ${CPP} $* '-D_HASHCHAR(X)=_HASHREP(X)'      \
                    '-D_SEPCHAR(X)=_SEPREP(X)'        \
                    '-D_COMPCHAR(X)=_COMPREP(X)'      \
                    "-D_UC4CHAR${jobid}=${uc4rep}"    \
                    "-D_UC8CHAR${jobid}=${uc8rep}"    \
                    - > "${tmpPre}"
###### TRANSFORM
        /^${jointag}\$/{
            # delete this line if it is the last one
            \$ d
            N
            s!^${jointag}[^[:digit:]]*\([[:digit:]]*\).*!_Cmod_source_line \1 \"${source}\"!
            p
            d
        }
        /#/s![#]!${hashtag}!g
        # do the real processing
        :process
        # map \uxxxx escapes to uppercase hex
        /[\\]u[[:xdigit:]]\{4,\}/{
          :processua
          s!\([\\]u[[:digit:]A-F]\{0,3\}\)a!\1A!g
          t processua
          :processub
          s!\([\\]u[[:digit:]A-F]\{0,3\}\)b!\1B!g
          t processub
          :processuc
          s!\([\\]u[[:digit:]A-F]\{0,3\}\)c!\1C!g
          t processuc
          :processud
          s!\([\\]u[[:digit:]A-F]\{0,3\}\)d!\1D!g
          t processud
          :processue
          s!\([\\]u[[:digit:]A-F]\{0,3\}\)e!\1E!g
          t processue
          :processuf
          s!\([\\]u[[:digit:]A-F]\{0,3\}\)f!\1F!g
          t processuf
        }
        # map \Uxxxxxxx escapes to uppercase hex
        /[\\]U[[:xdigit:]]\{8,\}/{
          :processUa
          s!\([\\]U[[:digit:]A-F]\{0,7\}\)a!\1A!g
          t processUa
          :processUb
          s!\([\\]U[[:digit:]A-F]\{0,7\}\)b!\1B!g
          t processUb
          :processUc
          s!\([\\]U[[:digit:]A-F]\{0,7\}\)c!\1C!g
          t processUc
          :processUd
          s!\([\\]U[[:digit:]A-F]\{0,7\}\)d!\1D!g
          t processUd
          :processUe
          s!\([\\]U[[:digit:]A-F]\{0,7\}\)e!\1E!g
          t processUe
          :processUf
          s!\([\\]U[[:digit:]A-F]\{0,7\}\)f!\1F!g
          t processUf
        }
        # isolate brackets such that they don't collide with neighboring identifiers
        /[\\]u[[:xdigit:]]\{4,\}/{
           s!${brackets}! & !g
        }
        ${compoS}
        ${separS}
        /[\\]u/{
           s![\\]u00D7=!${specop}*= ${speccl}!g    # ×=
           s![\\]u00D7!${specop}* ${speccl}!g      # ×  (should only be used as binary operator)
           s![\\]u00F7=!${specop}/= ${speccl}!g    # ÷=
           s![\\]u00F7!${specop}/ ${speccl}!g      # ÷
           s![\\]u2026!${specop}... ${speccl}!g    # …
           s![\\]u2192!${specop}-> ${speccl}!g     # →
           s![\\]u2227!${specop}\&\& ${speccl}!g   # ∧
           s![\\]u2228!${specop}|| ${speccl}!g     # ∨
           s![\\]u2229=!${specop}\&= ${speccl}!g   # ∩=
           s![\\]u2229!${specop}\& ${speccl}!g     # ∩
           s![\\]u222A=!${specop}|= ${speccl}!g    # ∪=
           s![\\]u222A!${specop}| ${speccl}!g      # ∪
           s![\\]u223C!${specop}~ ${speccl}!g      # ∼
           s![\\]u2261!${specop}== ${speccl}!g     # ≡
           s![\\]u2264!${specop}<= ${speccl}!g     # ≤
           s![\\]u2265!${specop}>= ${speccl}!g     # ≥
           s![\\]u2AA1=!${specop}<<= ${speccl}!g   # ⪡=
           s![\\]u2AA1!${specop}<< ${speccl}!g     # ⪡
           s![\\]u2AA2=!${specop}>>= ${speccl}!g   # ⪢=
           s![\\]u2AA2!${specop}>> ${speccl}!g     # ⪢
           s/[\\]u00AC/${specop}! ${speccl}/g      # ¬
           s/[\\]u203C/${specop}!! ${speccl}/g     # ‼
           s/[\\]u2212/${specop}- ${speccl}/g      # −
           s/[\\]u2260/${specop}!= ${speccl}/g     # ≠
           s/[\\]u2254/${specop}= ${speccl}/g      # ≔
           s/[\\]u2A74/${specop}${specop}= ${speccl}${speccl}/g     # ⩴
           s![\\]u!${uc4tag}!g
        }
        /[\\]U/s![\\]U!${uc8tag}!g
        /__/s!\<__\(${idex}\)!${vatag}\1${vateg}!g
       # Add tags to anonymous enumeration types. The tag itself becomes
       # a normal local indentifier and is therefore mangled later in
       # the process. Unfortunately this is not sufficient to guarantee
       # uniqueness because these can end up in snippets, and an importer
       # could import enum's from the same line number in different files.
       #
       # First see what is after an eventual isolated enum tag
       :enum
       /\<enum\>[[:blank:]]*\$/{
          N
          s!\n!!g
          b enum
       }
       /\<typedef\>/!{
         /\<enum[[:blank:]]*{/{
           H
           s!\(.*\<enum[[:blank:]]*\)\({.*\)!\1 _MODULE_tag_${jobid}_!
           p
           =
           x
           s!\(.*\<enum[[:blank:]]*\)\({.*\)!\2!
         }
       }
       s!\<_Pragma\>!${pragma}!g
TRANSFORM
    cat ${tmpPre} | sed3 3<<REASSEMBLE > "${target}"
##### REASSEMBLE
              # put together the enum tags
              /_MODULE_tag_${jobid}_\$/{
                N
                N
                s!\n!!g
              }
              /_HASH/{
                        s![[:blank:]]*${hashrep}[[:blank:]]*!#!g
                        s!${hashtag}!#!g
                       }
              /_COMP/{
                        s!A[[:blank:]]*${comprep}[[:blank:]]*A!${comprep}!g
                       }
              /_SEP/{
                        s!A[[:blank:]]*${seprep}[[:blank:]]*A!${seprep}!g
                       }
              /_UC4/{
                        s!A-${uc4rep}-A!_UC4!g
                        s!${uc4tag}!\\\\u!g
                       }
              /_UC8/{
                        s!A-${uc8rep}-A!_UC8!g
                        s!${uc8tag}!\\\\U!g
                       }
              /_COMP/{
                        # prefix all components with _ such that they can start with numbers
                        s!${comprep}!${compsep}_!g
                        s!${comptag}!\\${compoU}!g
                       }
              /_SEP/{
                        s!\(${idex}\|[.][.]\)${seprep}\(\(${idex}\|[.][.]\)${seprep}\)*${idex}!⸢&⸣!g
                        s!${seprep}!#!g
                        s!${septag}!\\${separU}!g
                       }
              /_VAARGS/s!${vatag}\(${idex}\)${vateg}!__\1!g
              /\n/s!\n!!g
REASSEMBLE
}
