#!/bin/sh -f

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2017

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"
import echo

SED=${SED:=sed}
LESS=${LESS:=less}
MORE=${MORE:=more}
CAT=${CAT:=cat}

if [ "${WD}" = "" ] ; then
    WD="$(pwd)"
fi


match () {
    eval "case '$2' in $1) return 0;; *) return 1;; esac"
    return $?
}

replace () {
    rplret=
    rplnex="${1}"
    while true; do
        rplpre="${rplnex%%${2}*}"
        if [ "${rplnex}" = "${rplpre}" ] ; then
            rplret="${rplret}${rplnex}"
            return
        fi
        rplnex="${rplnex#*${2}}"
        rplret="${rplret}${rplpre}${3}"
    done
}

# sed that reads the commands from fd 3
sed3 () {
    command=$(cat <&3)
    ${SED} -e "${command}" $*
}

# sed that reads the commands from $1 and the text from the remaining
# arguments
sed2 () {
    command="$1"
    shift
    ${SED} -e "${command}" <<EOF
$*
EOF
}

realpath () {
    path=$1
    if match "${path}" "./*" ; then
        path=${WD}${path#\./}
        path="${path}/"
    elif [ "${path##/}" = "${path}" ] ; then
        path=${WD}"/"${path}
    fi
    sed3 3<<'EOF' <<PATH
      s![^/]*/[.][.]/!!g
      s!/[.]/!/!g
      s!/[.]$!!g
EOF
${path}
PATH
}

getPreamble() {
    if [ -z "${endPreambleCalled}" ] ; then
        complain "getPreamble needs a preceding call of 'endPreamble', aborting"
        exit 1
    fi
    sed3 -n $0 3<<'EOF'
/^[[:blank:]]*endPreamble/q
p
EOF
}

pager () {
    exec 2>/dev/null
    ${LESS} -R || ${MORE} || ${CAT}
}

getHelp() {
    getPreamble | sed3 3<<'EOF' | pager
s!^#!!
s!@\([[:alnum:]]\{1,\}\)!\1:!g
EOF
    exit 0
}

getDoxy() {
    getPreamble | sed3 3<<'EOF' | pager
/^#!/d
/^# Cmod/{
  s!^# \(.*\)!/// @addtogroup tools Tools for working with Modular C\n\n/// @defgroup _Cmod \1\n/// @ingroup tools\n///!
  p
  d
}
/^#/!{
  #/[^ ]/s!.*!<code>&</code><br>!
  /./!{
    i ///
    d
  }
  i /// @code
  :REPEAT
  $ {
    p
    i /// @endcode
    q
  }
  s!^!/// !
  p
  N
  s!.*\n!!
  /^#/!b REPEAT
  i /// @endcode
}
s!^#!!
s!^!/// !
EOF
    exit 0
}
