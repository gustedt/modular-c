#!/bin/sh

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2017

### Temporary files and garbage collection
# If not otherwise specified temporary files are created in a specific
# directory below /tmp
#
# If this module is used by recursive scripts, the temporary
# directories for them will be nested according to the call
# dependency.

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"

import list
import echo
import match

endPreamble $*


TMP=${TMP:-/tmp}
RM=${RM:-rm}
MKTEMP=${MKTEMP:-mktemp}

garbage () {
    append garbageBin $*
}

## Signal handling
## Make sure to clean up on all exits that we might be able to capture
cleanup () {
    # close stdout and stderr such that the filter can terminate
    exec 1>&-
    exec 2>&-
    # wait for any filter
    wait
    ${RM} -rf ${garbageBin}
}

killup () {
    sig="$?"
    complain "exiting on signal (${sig}), cleaning up"
    cleanup
}

trap cleanup EXIT
for sig in  HUP INT QUIT ILL ABRT BUS FPE SEGV PIPE TERM IO SYS ; do
    trap killup ${sig} || true
done

# we need some temporary
tmpd=$(${MKTEMP} -d "${TMP}/cmod-tmpd.XXXXXXXXXXXXXXXX")
if [ -z "${CMOD_KEEP}" ] ; then
    garbage "${tmpd}"
fi

jobid="${tmpd#*.[[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]]}"

# have recursive calls use the new directory
export TMP="${tmpd}"

tmpdCount=0
newTmp () {
    local var="$1"
    local ext="${2:-c}"
    eval "${var}='${tmpd}/tmpNo-${tmpdCount}.${ext}'"
    tmpdCount="$((${tmpdCount}+1))"
}
