#!/bin/sh -f

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2017

#! @file
#! @brief Implement an import function.

# This needs an include guard. Later sources that are used with
# "import" don't need such include guards.
if [ -z "${IMPORT_PATH}" ] ; then
    WE="${WE:-${0##*/}}"
    export EXEDIR="${EXEDIR:-${0%%/${WE}}}"
    IMPORT_PATH="${IMPORT_PATH:-${WE}}"

    #! Import sh skript $1 exactly once.
    import () {
        local known
        eval "known=\${IMPORT_${1}}"
        if [ -z ${known} ] ; then
            local IMPORT_SRC="$1"
            local IMPORT_PATH="${IMPORT_PATH}:$1"
            #echo "importing ${IMPORT_PATH}" >&2
            eval "IMPORT_$1=1"
            . "${EXEDIR}/$1.sh"
        fi
    }

endPreamble () {
    if [ "${IMPORT_PATH}" = "${0##*/}" ] ; then
        if [ -n "${endPreambleCalled}" ] ; then
            complain "endPreamble called from ${IMPORT_PATH} more than once, aborting"
            exit 1
        fi
        readonly endPreambleCalled=1
        case "$@" in
            (*--help*)
                getHelp
                ;;
            (*--doxy*)
                getDoxy
                ;;
        esac
    fi
}

fi
