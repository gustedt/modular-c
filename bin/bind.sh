#!/bin/sh -f

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2018

### Bind variables to values
#
# This expects arguments in the form
#
# NAME0=VALUE0 NAME1=VALUE1 ...
#
# (without special characters) and replaces all occurrences of
# "${NAME0}" in the submitted text by "VALUE0" etc.

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"

import match
import arguments

endPreamble $*

prog=""
name=""

addProg () {
    name="$1"
    shift
    # add an "s" rule to the sed program
    prog="${prog}
            s!\${${name}}!$*!g"
}

arguments args

if [ -z "${args}" ] ; then
    args="$*"
fi

for comb in ${args} ; do
    case "${comb}" in
        # each argument has the form NAME=VALUE
        (*=*)
            if [ -n "${name}" ] ; then
                addProg "${name}" ${val}
            fi
            name="${comb%%[[:blank:]=]*}"
            val="${comb#*[[:blank:]=]}"
            ;;
        # if not, this a trailing word to a previous NAME
        (*)
            val="${val} ${comb}"
            ;;
    esac
done

if [ -n "${name}" ] ; then
    addProg "${name}" ${val}
fi

exec ${SED} "${prog}"
