#!/bin/sh -f

# Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2018, all rights reserved.

# @brief rudimentary command line processing

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"

import list

endPreamble $*

cmdline () {
    argv=""
    target=""
    source=""
    extensions="$1"
    shift
    while [ $# -gt 0 ]; do
        case $1 in
            -[cEMS])
                if [ -n "${job}" ]; then
                    echo "Warning: $1 set, but job is already ${job}, ignoring" >&2
                else
                    job="$1"
                fi
                ;;
            -o)
                shift
                target="$1"
                ! match '-*' "${target}" \
                    || die "Error: ${target} is not a valid argument to -o\nAborting compilation"
                ;;
            -o*)
                target="${1#-o}"
                ! match '-*' "${target}" \
                    || die "Error: ${target} is not a valid argument to -o\nAborting compilation"
                ;;
            -L)
                library="$(realpath ${2})"
                if [ -z "${CMOD_LIBRARY_PATH}" ] ; then
                    CMOD_LIBRARY_PATH="${library}"
                else
                    CMOD_LIBRARY_PATH="${CMOD_LIBRARY_PATH}:${library}"
                fi
                append argv "-L${library}"
                argv="${argv##[ ]}"
                report "asking for ld library ${library}: ${argv}"
                shift
                ;;
            -L*)
                library="$(realpath ${1#-L})"
                if [ -z "${CMOD_LIBRARY_PATH}" ] ; then
                    CMOD_LIBRARY_PATH="${library}"
                else
                    CMOD_LIBRARY_PATH="${CMOD_LIBRARY_PATH}:${library}"
                fi
                append argv "-L${library}"
                argv="${argv##[ ]}"
                report "asking for ld library ${library}: ${argv}"
                ;;
            -static)
                append argv "$1"
                argv="${argv##[ ]}"
                static="1"
                ;;
            -*)
                append argv "$1"
                argv="${argv##[ ]}"
                ;;
            *[.]${extensions})
                append source "$1"
                source="${source##[ ]}"
                ;;
            *)
                append argv "$1"
                argv="${argv##[ ]}"
                ;;
        esac
        shift
    done
}
