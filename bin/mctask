#!/bin/sh -f

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"
import mangle

ID () {
    mangle eilck task $*
}

source="${tmpd}/mctask$$.txt"
code="${tmpd}/mctask-code$$.txt"
cat > "${source}"

name="${CMOD_AMEND_ARGUMENTS%%[[:blank:]=]*}"
if [ "${name}" = "${CMOD_AMEND_ARGUMENTS}" ] ; then
    export CMOD_AMEND_ARGUMENTS=""
else
    export CMOD_AMEND_ARGUMENTS="${CMOD_AMEND_ARGUMENTS#*[[:blank:]=]}"
    # remove an optional = sign
    export CMOD_AMEND_ARGUMENTS="${CMOD_AMEND_ARGUMENTS#*=}"
fi

case "${name}" in
    (specialize)
        for val in ${CMOD_AMEND_ARGUMENTS} ; do
            echo "if (${name} == ${val}) {"
            cat "${source}"
            echo -n "} else "
        done

        echo "{"
        cat "${source}"
        echo "}"
        ;;
    (alternate)
        # Duplicate the enclosed code. Expects two identifiers, whos
        # roles will be interchanged in the second copy.
        #
        # Each copy still counts as one user-code iteration. The
        # task::iteration() macro will take this into account.
        #
        # Can be combined with a surrounding "duplicate" directive.
        A="${CMOD_AMEND_ARGUMENTS%%[[:blank:]=]*}"
        B="${CMOD_AMEND_ARGUMENTS#*[[:blank:]=]}"
        if [ -z "${A}" -o -z "${B}" ] ; then
            complain "alternate needs two identifiers, found '${A}' and '${B}'"
        fi
        echo "/* MC ALTERNATE */"
        echo "$(ID itIncAltg) = 2;"
        echo "enum { $(ID itIncAlt) = 2, };"
        echo "{"
        echo "{"
        cat "${source}"
        echo "}"
        complain "alternate, original"
        echo "{"
        echo "enum { $(ID itOffAlt) = 1, };"
        sed "
s!\<${A}\>!_ALTERNATE_A_!g
s!\<${B}\>!${A}!g
s!\<_ALTERNATE_A_\>!${B}!g
" "${source}"
        echo "}"
        complain "alternate, copy"
        echo "}"
        ;;
    (duplicate)
        # Replicate the enclosed code. The dependency calculation will
        # only be done for the first sibling, and all others will just
        # get the one of the first copied.
        #
        # Each copy still counts as one user-code iteration. The
        # task::iteration() macro will take this into account.
        #
        # Can be combined with an enclosed "alternate" directive.
        #
        # Expects the number of copies as argument which defaults to
        # two, if omitted.
        num="${CMOD_AMEND_ARGUMENTS}"
        if [ -z "${num}" ] ; then
            num=2
        else
            num=$((${num}))
        fi
        echo "/* MC START */"
        echo "$(ID itIncDupg) = ${num};"
        echo "enum { $(ID itIncDup) = ${num}, };"
        echo "{"
        echo "{"
        cat "${source}"
        echo "}"
        complain "duplicate, original copy"
        i=1
        while [ $i -lt "${num}" ] ; do
            complain "duplicate, copy number ${i}"
            echo "{"
            echo "/* MC START */"
            echo "enum { $(ID itOffDup) = ${i}, };"
        sed "
s!$(ID emark)!$(ID omark)!g
s!$(ID imark)!$(ID omark)!g
s!$(ID emarks)!$(ID omarks)!g
s!$(ID imarks)!$(ID omarks)!g" "${source}"
        echo "}"
            i=$((${i} + 1))
        done
        echo "}"
        ;;
    (log)
        # Activate a log file.
        #
        # There are three different log files that can be set:
        # runtime, fifo and greedy.
        fn="${CMOD_AMEND_ARGUMENTS%%[[:blank:]=]*}"
        export CMOD_AMEND_ARGUMENTS="${CMOD_AMEND_ARGUMENTS#*[[:blank:]=]}"
        case "${fn}" in
            (runtime)
                fn=$(ID rt)
                ;;
            (fifo)
                fn=$(ID fi)
                ;;
            (greedy)
                fn=$(ID gr)
                ;;
        esac
        cat <<EOF
#line 1 "<mctask runtime>"
         $(ID DECL)(char const* ${fn} =
             $(mangle C mem cpy)($(mangle C lib malloc)(1+$(mangle C str len)(${CMOD_AMEND_ARGUMENTS})),
                                 ${CMOD_AMEND_ARGUMENTS},
                                 1+$(mangle C str len)(${CMOD_AMEND_ARGUMENTS})))
EOF
        cat "${source}"
        ;;
    (groups)
        # Set the number of groups into which each "split" or "steps"
        # task will be divided.
        echo "$(ID DECL)(unsigned const $(ID numgroups) = ${CMOD_AMEND_ARGUMENTS})"
        echo "$(ID DECL)($(mangle C size) _Atomic $(ID itend) = -1, $(ID ittotal) = -1)"
        cat "${source}"
        ;;
    (iterate)
        # Identify the code block over which this program iterates.
        #
        # The argument is the number of iterations. Set it to a high
        # value if you want to stop iteration via a finishing
        # criteria.
        echo "#line 1 \"<mctask setup>\""
        echo "{"
        echo "unsigned $(ID itIncDupg) = 1;"
        echo "unsigned $(ID itIncAltg) = 1;"
        echo "enum { $(ID itIncDup) = 1, };"
        echo "enum { $(ID itIncAlt) = 1, };"
        echo "enum { $(ID itOffDup) = 0, };"
        echo "enum { $(ID itOffAlt) = 0, };"
        echo "$(mangle eilck obj blks)* $(ID fifos) = 0;"
        ittotal="${CMOD_AMEND_ARGUMENTS}"
        ones=$(sed '
/^\/\* MC STEPS \*\/$/{
  s!.*!steps!
  p
}
/^\/\* MC SPLIT \*\/$/{
  s!.*!split!
  p
}
/^\/\* MC BLOCK \*\/$/{
  s!.*!block!
  p
}
d
' "${source}")
        count=0
        for val in ${ones} ; do
            count=$((${count}+1))
        done
        echo "enum { $(ID numtasks) = ${count}, };"
        echo "$(mangle C attr maybe_unused) unsigned const $(ID numthreads) = 1 /* for main? */"
        for val in ${ones} ; do
            case "${val}" in
                (steps)
                    echo "+$(ID numgroups) +1 /* for team handling */"
                    ;;
                (block|split)
                    echo "+1"
                    ;;
            esac
        done
        echo ";"
        echo "$(mangle C attr maybe_unused) unsigned const $(ID totgroups) = "
        for val in ${ones} ; do
            case "${val}" in
                (steps|split)
                    echo "+$(ID numgroups)"
                    ;;
                (block)
                    echo "+1"
                    ;;
            esac
        done
        echo ";"
        echo "$(mangle C io fprintf)($(mangle C io err), \"mctask:\\\\t%u\\\\tgroups\\\\n\", $(ID totgroups));"
        echo "$(mangle C io fprintf)($(mangle C io err), \"mctask:\\\\t%u\\\\tthreads\\\\n\", $(ID numthreads));"
        echo "$(mangle eilck#task)* $(ID tasks)[$(ID numtasks)] = {"
        for val in ${ones} ; do
            case "${val}" in
                (steps|split)
                    echo "$(ID alloc)($(ID numgroups)),"
                    ;;
                (block)
                    echo "$(ID alloc)(1),"
                    ;;
            esac
        done
        echo "};"
        cat <<EOF
{
 unsigned $(ID position) = 0;
 for (unsigned $(ID i) = 0; $(ID i) < $(ID numtasks); ++$(ID i)) {
    for (unsigned $(ID j) = 0; $(ID j) < $(ID tasks)[$(ID i)]->len; ++$(ID j)) {
       $(ID tasks)[$(ID i)]->tab[$(ID j)].id = $(ID position);
       ++$(ID position);
    }
 }
}
EOF
        echo "unsigned const $(ID qlength) = $(ID totgroups) + 2;"
        complain "counted ${count} tasks"
        exec < "${source}"
        count=0
        maxrep=1
        while read line ; do
              case "${line}" in
                  (\/\*\ MC\ STEPS\ \*\/)
                      complain "establish steps task ${count}"
                      echo "#line 1 \"<mctask steps ${count} execution>\""        >>"${code}"
                      echo "$(ID current) = ${count};"                            >>"${code}"
                      echo "$(ID sibling) = ${count} - $(ID sibOffset);"           >>"${code}"
                      count=$((${count}+1))
                      ;;
                  (\/\*\ MC\ SPLIT\ \*\/)
                      complain "establish split task ${count}"
                      echo "#line 1 \"<mctask split task ${count} execution>\""   >>"${code}"
                      echo "$(ID current) = ${count};"                            >>"${code}"
                      echo "$(ID sibling) = ${count} - $(ID sibOffset);"           >>"${code}"
                      count=$((${count}+1))
                      ;;
                  (\/\*\ MC\ BLOCK\ \*\/)
                      complain "establish block ${count}"
                      echo "#line 1 \"<mctask block ${count} execution>\""        >>"${code}"
                      echo "$(ID current) = ${count};"                            >>"${code}"
                      echo "$(ID sibling) = ${count} - $(ID sibOffset);"           >>"${code}"
                      count=$((${count}+1))
                      ;;
                  (\/\*\ MC\ START\ \*\/)
                      complain "starting duplicated task set at task ${count}"
                      echo "#line 1 \"<mctask duplicate ${count} execution>\""    >>"${code}"
                      echo "enum { $(ID sibOffset) = ${count}, };"                  >>"${code}"
                      ;;
                  (\/\*\ MC\ ALTERNATE\ \*\/)
                      complain "starting alternating task set at task ${count}"
                      echo "#line 1 \"<mctask alternate ${count} execution>\""    >>"${code}"
                      alternate=2
                      ;;
                  (enum\ \{\ $(ID itIncDup)\ =\ *,\ \}\;)
                      rep=${line##*=}
                      rep=${rep%%,*}
                      if [ "${rep}" -gt "${maxrep}" ] ; then
                          maxrep="${rep}"
                      fi
                      complain "found replication of size ${rep}, maximum ${maxrep}"
                      echo "${line}"                                              >>"${code}"
                  ;;
                  (*)
                      echo "${line}"                                              >>"${code}"
                      ;;
              esac
        done
        if [ "1" -lt "${maxrep}" ] ; then
            complain "maximal replication of size ${maxrep}"
            enumtotal="(${ittotal})/${maxrep}+!!((${ittotal})%${maxrep})"
            complain "adjusting number of effective iterations to ${enumtotal}"
        else
            enumtotal="${ittotal}"
        fi
        if [ -n "${alternate}" ] ; then
            complain "taking into account alternation"
            enumtotal="(${enumtotal})/2+!!((${enumtotal})%2)"
            complain "adjusting number of effective iterations to ${enumtotal}"
        fi
        cat <<EOF
if ($(ID fi)) $(mangle eilck hdl group open)($(ID fi), $(ID totgroups), "τ:i:φ \\\\ μtask", "%zu");
#line 1 "<mctask classification>"
if ($(ID itend) > $(ID all)) {
$(ID THREADS);
$(ID ITERATE_seq)(0, 0, $(ID all)) {
EOF
        sed "
s!$(ID DEPENDS)!$(ID DEPENDS_seq)!g
s!$(ID ITERATE_BLOCK_INNER)!$(ID ITERATE_BLOCK_INNER_seq)!g
s!$(ID ITERATE_TASK_INNER)!$(ID ITERATE_TASK_INNER_seq)!g
s!$(ID ITERATE_TASK)!$(ID ITERATE_TASK_seq)!g
s!$(ID ONCE_TASKS)!$(ID ONCE_TASKS_seq)!g
s!$(ID ONCE_TASK)!$(ID ONCE_TASK_seq)!g
s!$(ID BLOCK)!$(ID BLOCK_seq)!g
s!$(ID SPLIT)!$(ID SPLIT_seq)!g
s!$(ID STEPS)!$(ID STEPS_seq)!g" "${code}"
        cat <<EOF
 }
 $(mangle eilck obj blks free_owner)($(ID fifos));
}
if ($(ID fi)) $(mangle eilck hdl group close)();
if ($(ID gr)) $(ID greedy)($(ID gr), 5, $(ID numtasks), $(ID tasks));
if ($(ID rt)) $(mangle eilck hdl group open)($(ID rt), $(ID numtasks), "event (nsec)", "τ%zu");
#line 1 "<mctask execution>"
$(mangle C io fprintf)($(mangle C io err), "mctask: execution phase\n");
{
$(ID THREADS);
$(ID ITERATE_par)($(ID all), ${ittotal}, ${enumtotal}) {
EOF
        sed "
s!$(ID emark)!$(ID omark)!g
s!$(ID imark)!$(ID omark)!g
s!$(ID emarks)!$(ID omarks)!g
s!$(ID imarks)!$(ID omarks)!g
s!$(ID DEPENDS)!$(ID DEPENDS_par)!g
s!$(ID ITERATE_BLOCK_INNER)!$(ID ITERATE_BLOCK_INNER_par)!g
s!$(ID ITERATE_TASK_INNER)!$(ID ITERATE_TASK_INNER_par)!g
s!$(ID ITERATE_TASK)!$(ID ITERATE_TASK_par)!g
s!$(ID ONCE_TASKS)!$(ID ONCE_TASKS_par)!g
s!$(ID ONCE_TASK)!$(ID ONCE_TASK_par)!g
s!$(ID BLOCK)!$(ID BLOCK_par)!g
s!$(ID SPLIT)!$(ID SPLIT_par)!g
s!$(ID STEPS)!$(ID STEPS_par)!g" "${code}"
        cat <<EOF
  }
 }
#line 1 "<mctask cleanup>"
if ($(ID rt)) $(mangle eilck hdl group close)();
for (unsigned $(ID i) = 0; $(ID i) < $(ID numtasks); ++$(ID i)) {
    $(ID free)($(ID tasks)[$(ID i)]);
}
$(mangle C lib free)((void*)$(ID rt));
$(mangle C lib free)((void*)$(ID fi));
$(mangle C lib free)((void*)$(ID gr));
$(mangle eilck obj blks free)($(ID fifos));
}
EOF
        ;;
    (block)
        # Identify a code block that constitutes a task, but which
        # will not split up into a group.
        complain "block, execution"
        echo "/* MC BLOCK */"
        echo "$(ID BLOCK)"
        cat "${source}"
        ;;
    (steps)
        # Identify a code block that constitutes a task, and the by
        # itself loops over a fixed amount of steps. This loop will be
        # split up at regular intervals into groups. Each group will
        # be started as a thread of its own.
        complain "task with ${CMOD_AMEND_ARGUMENTS} equivalent steps"
        echo "/* MC STEPS */"
        echo "$(ID STEPS)(${CMOD_AMEND_ARGUMENTS})"
        cat "${source}"
        ;;
    (split)
        # Identify a code block that constitutes a task, and the by
        # itself loops over an unkown amount of steps. This loop will
        # be counted, first, and then dynamically split up into groups
        # during execution. All groups of one task will run
        # sequentially inside the same thread, but each task will have
        # a thread of its own.
        complain "task that splits following loop"
        echo "/* MC SPLIT */"
        echo "$(ID SPLIT)"
        cat "${source}"
        ;;
    (lvalue)
        # Mark a list of identifiers as being accessed as lvalue, that
        # is an object that is written to during the group of the task
        # where this directive is placed.
        echo "$(ID emarks)(${CMOD_AMEND_ARGUMENTS});"
        cat "${source}"
        ;;
    (rvalue)
        # Mark a list of identifiers as being accessed as rvalue, that
        # is an object that is only read from during the group of the
        # task where this directive is placed.
        echo "$(ID imarks)(${CMOD_AMEND_ARGUMENTS});"
        cat "${source}"
        ;;
    (depends)
        ta="${CMOD_AMEND_ARGUMENTS%%,*}"
        st="${CMOD_AMEND_ARGUMENTS#*,}"
        echo "$(ID DEPENDS)((${ta}), (${st}))"
        cat "${source}"
        ;;
    (circular)
        ta="$(ID ID)()"
        st="${CMOD_AMEND_ARGUMENTS}"
        if [ -z "${st}" ] ; then
            st='1'
        fi
        st="$(ID micro)()-(${st})"
        echo "$(ID DEPENDS)((${ta}), (${st}))"
        cat "${source}"
        ;;
    (linear)
        ta="$(ID ID)()"
        st="${CMOD_AMEND_ARGUMENTS}"
        if [ -z "${st}" ] ; then
            st='1'
        fi
        st="($(ID micro)()-(${st})) < depLen ? ($(ID micro)()-(${st})) : $(ID micro)()"
        echo "$(ID DEPENDS)((${ta}), (${st}))"
        cat "${source}"
        ;;
    (*)
        echo "#error unknown MC command ${name}: aborting"
        ;;
esac
