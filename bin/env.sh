#!/bin/sh

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2018

### Evaluate the argument list as the names of environment variables and substitue with the value

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"

import match
import arguments

endPreamble $*

mexp="/xxxNONxxxSENSExxx"

arguments args

for name in ${args} ; do
    #eval "val=\${${name}}"
    mexp="${mexp}\|${name}="
done

mexp="${mexp}/p"

values=$(env | ${SED} -n "${mexp}")

CMOD_AMEND_ARGUMENTS="${values}" exec "${EXEDIR}/bind.sh"
