#!/bin/sh -f

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2017

### Evaluate the depending code fragment with protected directives several times.
#
#   This allows to use construts such as ${N} also as arguments to
#   subsequent CMOD directives.
#
#   In it simplest form, something like
#
#pragma CMOD amend eval HERE
#
# will first replace the "HERE" in occurences such as
#
#pragma HERE amend ...
#
# with "CMOD" and then call the expansion procedure with input the
# modified program part, again.
#
# Several identifiers in the line for "eval" have the code fragment
# processed as often as there are such "labels", last is inner most.

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"

import match
import arguments
import preprocess

progam="$0"

endPreamble $*

evaluation () {
    if [ $# -ge 1 ] ; then
        name=$1
    else
        name="EVAL"
    fi
    command="
s!^[[:blank:]]*\#[[:blank:]]*pragma[[:blank:]]\{1,\}${name}[[:blank:]]\{1,\}!\#pragma CMOD !
s!^[[:blank:]]*\#[[:blank:]]*[[:blank:]]*pragma CMOD[[:blank:]]\{1,\}\(${_CMOD_AMENDS}\)[[:blank:]]!\#pragma CMOD amend \1 !
s!^[[:blank:]]*\#[[:blank:]]*[[:blank:]]*pragma CMOD[[:blank:]]\{1,\}\(${_CMOD_AMENDS}\)\$!\#pragma CMOD amend \1!
"
    report "${command}"
    if [ $# -gt 1 ] ; then
        shift
        CMOD_EXPAND_LEVEL=$((${CMOD_EXPAND_LEVEL} + 1))         \
        CMOD_AMEND_ARGUMENTS="$*" $0                            \
            | ${SED} "${command}"                               \
            | "${EXEDIR}/expand"
    else
        ${SED} "${command}" | "${EXEDIR}/expand"
    fi
}

arguments args

evaluation ${args}
