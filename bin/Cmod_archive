#!/bin/sh

CC=${CC:=cc}
LDSHARED=${LDSHARED:=-shared -Bdynamic -lrt -lgcc}
RPATH=${RPATH:=-Wl,-rpath,}

match () {
    eval "case '$2' in $1) return 0;; *) return 1;; esac"
    return $?
}

WD="$(pwd)"

realpath () {
    path=$1
    if match "${path}" "./*" ; then
        path=${WD}${path#\./}
        path="${path}/"
    elif [ "${path##/}" = "${path}" ] ; then
        path=${WD}"/"${path}
    fi
    echo ${path} | sed 's|[^/]*/[.][.]/||g; s|/[.]/|/|g'
}

OBJECTS=

while [ $# -gt 0 ]; do
    case $1 in
        -o)
            shift
            TARGET=$(realpath "$1")
            ;;
        *)
            OBJECTS="${OBJECTS} $(realpath $1)"
            ;;
    esac
    shift
done

#echo "producing ${TARGET} from ${OBJECTS}" >&2

if [ -z "${TARGET}" -o -z "${OBJECTS}" ] ; then
    echo "Usage: $0 -o target  object-or-archive" >&2
    exit 1
fi

TMP=$(mktemp -d)

cleanup () {
    rm -rf ${TMP}
}
abort () {
    echo "exiting on signal ($?), cleaning up" >&2
    cleanup
}

trap cleanup 0
trap abort 1 2 3 4 6 7 8 11 13 15

cp ${OBJECTS} ${TMP}
cd ${TMP}
for f in *.a ; do
    ar x "${f}" 2>&1 >/dev/null
    rm -f $f
done

case "${TARGET}" in
    *.a)
        ar rs ${TARGET} * 2>&1 >/dev/null
        ;;
    *.so)
        if [ -n "${CMOD_NOINIT}" ] ; then
            rm -f *-_Init.o
        fi
        rm -f *-_Entry.o
        project="${TARGET%.so}"
        project="${project##*/}"
        project="${project#lib}"
        lib="$(realpath $0)"
        lib="${lib%/bin/*}/lib"
        if [ -d "${lib}" ] ; then
             LDSHARED="${LDSHARED} -L${lib}"
             if [ -n "${RPATH}" ] ; then
                 LDSHARED="${LDSHARED} ${RPATH}${lib}"
             fi
        fi
        for l in $(cat *-_Links.txt) ; do
            if [ "${l}" != "-l${project}" ] ; then
                LINKS="${LINKS} ${l}"
            fi
        done
        ${CC} ${LDSHARED} -o ${TARGET} *.o ${LINKS}
        ;;
    *)
        echo "what to do for this type of file? ${TARGET}" >&2
        exit 1
esac
