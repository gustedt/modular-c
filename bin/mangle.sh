#!/bin/sh -f

# part of Cmod -- a reference implementation of Modular C
# © Jens Gustedt, 2017

. "${EXEDIR:-${0%%/${0##*/}}}/import.sh"
import join
import match
import echo
import list

# We use mangling of the form _ZN#1s1#2s2...E where #1 is the length
# of identifier s1 etc. All names that Modular C uses are prefixed
# with _C, such that they will never be in conflict with other
# indentifiers, e.g from the C library.
manglePrefix=${manglePrefix:="_C"}

endPreamble $*

# This is the core of the mangle algorithm. A list of identifiers is
# transformed to a string that precedes each of the identifiers by its
# character length.
mangleRaw () {
    mangleRawRet=""
    while [ $# -gt 0 ] ; do
        mangleRawRet="${mangleRawRet}${#1}${1}"
        shift
    done
}

mangleRaw ${manglePrefix}
mangleStart="_ZN${mangleRawRet}"
mangleEnd="E"

compsep="QQQQQQ"

compStart="I"
compEnd="E"

# mangle a sequence of indentifiers as it where a template with
# arguments
mangleComp () {
    mangleRaw $1
    mangleCompRet="${mangleRawRet}"
    shift
    if [ "$#" -gt 0 ] ; then
        mangleRaw $*
        mangleCompRet="${mangleCompRet}${compStart}${mangleRawRet}${compEnd}"
    fi
}

splitlong () {
    splitlongRet=""
    w="$1"
    while true ; do
        case "${w}" in
            (*${compsep}*)
                true
                ;;
            (*)
                break
                ;;
        esac
        append splitlongRet "${w%%${compsep}*}"
        w="${w#*${compsep}}"
    done
    append splitlongRet "${w}"
}

mangleSpec () {
    mangleIDRet=
    while [ $# -gt 0 ] ; do
        if [ ${#1} -gt 0 ] ; then
            case $1 in
                (*${compsep}*)
                    splitlong "$1"
                    mangleComp ${splitlongRet}
                    mangleIDRet="${mangleIDRet}${mangleCompRet}"
                    ;;
                (*)
                    mangleRaw "$1"
                    mangleIDRet="${mangleIDRet}${mangleRawRet}"
                    ;;
            esac
        fi
        shift
    done
}

# partially mangle a list of identifiers separated by '#'
mangleIDlocal () {
    local IFS=" 	#"
    case "$*" in
        (*${compsep}*)
            mangleSpec $*
            ;;
        (*)
            mangleRaw $*
            mangleIDRet="${mangleRawRet}"
            ;;
    esac
}

mangleID () {
    mangleIDlocal $*
    echo -n "${mangleIDRet}"
}

# completely mangle a composed identifier
mangleLocal () {
    if [ "${1###}" = "$1" ]; then
        mangleIDlocal $*
        mangleRet="${mangleStart}${mangleIDRet}${mangleEnd}"
    else
        mangleRet="${1###}"
    fi
}

mangle () {
    mangleLocal $*
    echo "${mangleRet}"
}

camelCase () {
    camelCaseRet="$1"
    shift
    local start
    local end
    while [ $# -gt 0 ] ; do
        end="${1#[[:lower:]]}"
        start=$(echo ${1%${end}} | ${SED} y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/)
        end=$(echo ${end} | ${SED} y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/)
        camelCaseRet="${camelCaseRet}${start}${end}"
        shift
    done
    echo -n ${camelCaseRet}
}

legacy () {
    res=$(echo $* | ${SED} "s!${compsep}! !g")
    case "${CMOD_LEGSEP}" in
        (C)
            res=$(split "#" ${res})
            camelCase $res
            ;;
        (P)
            res=$(split "#" ${res})
            camelCase "" $res
            ;;
        (*)
            sj "#" "${CMOD_LEGSEP}" ${res}
            echo "${joinRet}"
            ;;
    esac
}
