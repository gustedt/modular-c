/**
 ** @defgroup POSIX POSIX: the Modular C interface for POSIX' standard C library
 **
 ** This should give an interface to the C library as it is
 ** implemented on POSIX platforms.
 **
 ** @warning This interface is still incomplete and lacks important
 ** features.
 **
 ** @{
 ** @}
 **/
