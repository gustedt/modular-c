/**
 ** @defgroup Linux Linux: the Modular C interface for Linux' standard C library
 **
 ** This should give an interface to the C library as it is
 ** implemented on Linux platforms.
 **
 ** @warning This interface is still incomplete and lacks important
 ** features.
 **
 ** @remark This is mostly implemented through softlinks to the
 ** corresponding @ref POSIX "POSIX" files.
 **
 ** @{
 ** @}
 **/
