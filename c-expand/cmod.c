/**
 ** @mainpage Modular C

 ** _Modular C_ proposes a consistent naming convention to traditional
 ** C that is meant to cope with modularity and encapsulation. As a
 ** tool it is mainly based on textual rewriting source to source,
 ** with features that are orthogonal to those of C's preprocessing
 ** phase.
 **
 ** _Modular C_ implements a handful of directives and a naming scheme
 ** that transform traditional translation units into _modules_. The
 ** add-on to the C language is minimal since we only add one feature,
 ** composed identifiers, to the core language.
 **
 ** Our modules can import other modules as long as the import relation
 ** remains acyclic and a module can refer to its own identifiers and
 ** those of the imported modules through freely chosen abbreviations.
 **
 ** Other than traditional C <code>include</code>, our
 ** @ref import "import" directive ensures complete encapsulation
 ** between modules.
 **
 ** The abbreviation scheme allows to seamlessly replace an imported
 ** module by another one with equivalent interface.
 **
 ** In addition to the export of symbols, we provide parameterized code
 ** injection through the import of _snippets_. This implements a
 ** mechanism that allows for code reuse, similar to X~macros or
 ** templates. Additional features of our proposal are a simple dynamic
 ** module initialization scheme, a structured approach to the C
 ** library and a migration path for existing software projects.
 **
 ** You can find a more detailed description of the approach in a
 ** [research report on HAL](https://hal.inria.fr/hal-01169491/en).
 ** The software is open source and can be found on [Inria's source
 ** forge](https://gforge.inria.fr/scm/?group_id=7398)
 **
 ** - @subpage composed   "Modules and composed identifiers"
 ** - @subpage visibility "Identifier visibility"
 ** - @subpage importing  "Importing and abbreviating"
 ** - @subpage snippet    "Code insertion"
 ** - @subpage execution  "Execution directives"
 ** - @subpage manipulation  "Code manipulation"
 ** - @subpage platform   "Interfacing with platform specific libraries"
 ** - @subpage extensions "Extensions of the C language"
 **/

/**
 ** @page composed Modules and composed identifiers
 **
 ** @synopsis
 ** @anchor module
 ** @anchor separator
 ** @anchor composer
 ** @code
#pragma CMOD module  [ abbrev = ] [ long-name ]
#pragma CMOD separator character
#pragma CMOD composer  character
 ** @endcode

 ** The central feature of _Modular C_ to access features (variables,
 ** functions, macros, ...) across different modules are composed
 ** identifiers. Each module has a _name_ that is usually composed of
 ** several particles. E.g in the C library interface there is a module
 ** @code
#pragma CMOD module C∷real∷double
 ** @endcode
 ** The three components of this composed identifier are
 ** <code>C</code>, <code>real</code>, <code>double</code>. This
 ** composed identifier states that the feature that it refers to is
 ** member of a module <code>C∷real</code> which in turn is a member of
 ** the module <code>C</code>.
 **
 ** The character between the components is called
 ** <code>separator</code>. It can be chosen almost freely, just as the
 ** coding style of the project imposes. For example
 ** @code
#pragma CMOD module C¯real¯double
 ** @endcode
 ** could have worked equally well if we wanted the separator to be
 ** <code>¯</code> instead of <code>∷</code>, see @ref unicode "Unicode".
 **
 ** This naming scheme extends to the members of each module. Each
 ** non-composed identifier that is used in a module receives the
 ** module name as a prefix to form its external name. E.g the module
 ** @ref C∷real∷double "C∷real∷double" has a member called @c SIZE and so the external
 ** name is @ref C∷real∷double∷SIZE "C∷real∷double∷SIZE." This naming scheme is independent
 ** of the separator character. If another module uses <code>⋏</code>,
 ** it would refer to the same feature by using @c C⋏real⋏double⋏SIZE.
 **
 ** This composition feature of identifiers helps to ensure uniqueness
 ** across different modules. E.g other modules could also have a
 ** member @c SIZE that can clearly be distinguished from @ref
 ** C∷real∷double∷SIZE "C∷real∷double∷SIZE."
 **
 ** - @subpage Defaults "Defaults"
 ** - @subpage composerchar "The composer character"
 ** - @subpage modulename "The module name is special"
 ** - @subpage macroabbrev "Abbreviating with macros"
 **
 ** Next: @ref visibility
 **/

/**

 ** @page Defaults Defaults
 ** The @ref separator "separator" directive is optional, if the separator can be
 ** guessed from the @ref module "module" directive. If the @ref module "module"
 ** directive is omitted, the module name is taken from the filename of
 ** the module. E.g the module name @ref C∷real∷double "C∷real∷double" could usually
 ** be guessed from the file name "C-real-double.X".

 ** Next: @ref composerchar
*/

/**

 ** @page composerchar The composer character

 ** The @ref composer "composer" directive gives you an additional level of
 ** identifier decomposition. It is not central to the naming module
 ** naming scheme (which is almost entirely based on the @ref separator "separator") but
 ** only meant to provide you an internal level of name composition
 ** inside a module.
 **
 ** It allows you to distinguish different features of the same kind
 ** with any meaning that you may want to give to "kind". If you don't
 ** specify such a @ref composer "composer", it defaults to the use of
 ** —, Unicode point u2014.

 ** @code
#pragma CMOD module  proj∷functions
#pragma CMOD composer —

#define variant—1(X) funci(X)
#define variant—2(X) funcii(X)

#define trigon—even C∷math∷cos
#define trigon—odd  C∷math∷sin
 ** @endcode

 ** Here the module now has two macros that are some @c trigon (here
 ** for trigonometric functions) and two "kinds" of them the @c even
 ** and the @c odd one. Regardless what @ref composer "composer" another
 ** importing module chooses it can access these identifiers by using
 ** this composer, and not the one of @c proj∷functions. E.g if
 ** another module chooses ↔ as their @ref composer, "composer," they would
 ** access the above macros as @c proj∷functions∷trigon↔even and
 ** @c proj∷functions∷trigon↔odd.
 **
 ** The only point where the @ref composer "composer" is used by
 ** _Modular C_ directly is for the disambiguation of @ref fill
 ** "fills" when @link abbrevprefix another module is imported several
 ** times@endlink.

 ** Next: @ref modulename
*/

/**

 ** @page modulename The module name is special
 ** A module's name is special, since it can also be used to describe a
 ** feature of the module itself. In many cases the module name will be
 ** used for a central feature of the module, e.g a data structure or a
 ** function. For example the @ref C∷io "C∷io" module is centered around the
 ** opaque data structure ::C∷io that corresponds to standard C's @C
 ** FILE. The prototype of @c fprintf becomes

 ** @code
int C∷io∷fprintf(C∷io* stream, char const format[], ...);
 ** @endcode

 ** in _Modular C_. So here the data structure is called
 ** <code>C∷io</code> and all functions of the module that use it are
 ** prefixed <code>C∷io∷</code>.

 ** The module name can be abbreviated by the optional short name that
 ** is given in the @ref module "module" directive. Its use is similar to
 ** abbreviations for @ref import "import" directives of @ref importing.

 ** Next: @ref macroabbrev
*/

/**

 ** @page macroabbrev Abbreviating with macros
 ** C provides a simple method to abbreviate long composed names,
 ** macros. If you think that C∷real∷double∷SIZE is a bit long to type,
 ** you can easily create a local shortcut for it:

 ** @code
#define SIZE C∷real∷double∷SIZE
 ** @endcode

 ** @par
 ** This will not lead to naming conflicts with other modules since @c
 ** SIZE here is a local name that will be expanded @c
 ** whatever∷module∷this∷is∷SIZE

*/

/**

 ** @page tags Struct, union and enumeration tags aren't special

 ** In traditional C, tags of @c struct, @c union and @c enum types are
 ** special: they constitute a name space of their own. _Modular C_
 ** deviates from that by silently adding @c typedef declarations that
 ** move such a tag name into the "normal" identifier name space. In
 ** fact, a declaration as for the following types
 ** @code
struct pair {
   double v[2];
};
enum numbers { zero, one, two, three };
 ** @endcode

 ** is rewritten as
 ** @code
typedef struct pair pair;
struct pair {
   double v[2];
};
enum numbers { zero, one, two, three };
typedef enum numbers numbers;
 ** @endcode

 ** That is, a @c struct or @c union declaration is directly preceded
 ** by a @c typedef that uses the same identifier, and an @c enum is
 ** immediately followed by such a @c typedef.

 ** This is a bit disruptive with traditional C that allows a type
 ** <code>struct stat</code> and a function <code>stat</code> to
 ** co-exist. But doing so is considered bad coding style in most
 ** communities and is only found in some places where backwards
 ** compatibility with bad historic choices has to be guaranteed.

 ** It is particularly convenient for data structures that contain a
 ** reference to themselves such as:
 ** @code
struct element {
   double v[2];
   element* next;
};
 ** @endcode

 ** - Next: @ref unicode "Unicode"

*/


/**
 ** @page unicode Unicode

 ** Unicode is nowadays the international standard for character codes
 ** and we shouldn't constrain programmers to the traditional ASCII
 ** set (or similar) that just happened to be there first.
 **
 ** _Modular C_ is meant to cope with most of Unicode naturally, such
 ** as using <code>α</code> as a variable name or <code>√</code> for a
 ** function, or using almost any Unicode character as @ref separator "separator"
 ** or @ref composer "composer" characters, but please see below.
 **
 ** Some Unicode characters, though, are reserved by _Modular
 ** C_. These are mostly characters that have a direct mapping to C
 ** operators and which are therefore taken as synonyms for these:
 **
 ** |character  | hex  | operator | remark             |
 ** |-----------|------|----------|--------------------|
 ** | ¬         | 00ac | !        | logical negation   |
 ** | ×         | 00d7 | *        | multiplication, should only be used as binary operator to distinguish from unary * |
 ** | ÷         | 00f7 | /        | division, may add blank to separate from following unary *|
 ** | …         | 2026 | ...      | ellipsis           |
 ** | ‼         | 203c | !!       | logical conversion |
 ** | →         | 2192 | ->       | pointer to member  |
 ** | ∧         | 2227 | &&       | logical and        |
 ** | ∨         | 2228 | \|\|     | logical or         |
 ** | ∩         | 2229 | &        | intersection, bitwise and, may add blank to separate from following unary &|
 ** | ∪         | 222a | \|       | union, bitwise or  |
 ** | ≔         | 2254 | =        | assignment         |
 ** | ≠         | 2260 | !=       | inequality test    |
 ** | ≡         | 2261 | ==       | equality test      |
 ** | ≤         | 2264 | <=       | less or equal      |
 ** | ≥         | 2265 | >=       | greater or equal   |
 ** | ⩴        | 2a74 | =        | initialization     |
 ** | ⪡         | 2aa1 | <<       | left shift (this is not the same as «, the left angle quote) |
 ** | ⪢         | 2aa2 | >>       | right shift (this is not the same as », the right angle quote) |

 **
 ** In addition the special parenthesis <code>⸢ ⸣</code> are reserved
 ** for internal use by _Modular C_.
 **
 ** good sense
 ** ----------
 **
 ** Overuse of Unicode can quickly become unreadable for people that
 ** are not used to the individual characters. So be careful.
 **
 ** - Don't use mathematical symbols or any other characters that have
 **   a strong semantic connotation out of their context. E.g the
 **   character ∫ is fine to use with _Modular C_ code, but only if it
 **   represents something like a mathematical integral.
 **
 ** - Don't use symbols in a way that differs from their syntactical
 **   classification. In particular symbols that are parenthesis
 **   (opening or closing) should always be used as such, e.g as
 **   contexts, see @ref context.
 **/

/**
 ** @page visibility Identifier visibility

 ** @synopsis
 ** @anchor intern
 ** @anchor extern
 ** @anchor declaration
 ** @anchor definition
 ** @code
#pragma CMOD intern      id
#pragma CMOD extern      id
#pragma CMOD declaration
#pragma CMOD definition
 ** @endcode

 ** ### Default visibility

 ** _Modular C_ follows C's main lines of identifier visibility (called
 ** _linkage_ in the C standard). If you don't do anything special,
 ** exactly the symbols with external linkage are visible to other
 ** modules:

 ** - Per default all @c extern functions and variable declarations are
 ** visible to other modules.

 ** If for example your module @c proj∷arith contains something like
 ** @code
double add(double a, double b) {
   return a + b;
}
static double add1(double a) {
   return a + 1;
}
 ** @endcode

 ** Other modules can use the identifier @c proj∷arith∷add and "see" a
 ** declaration that is equivalent to
 ** @code
extern double proj∷arith∷add(double a, double b);
 ** @endcode

 ** In contrast to that, because it is @c static, @c add1 is hidden and
 ** cannot be used by another module than @c proj∷arith.

 ** - @subpage makevisible "Make identifiers visible"
 ** - @subpage finetuning "Fine tuning with extern and intern directives"

 ** Next: @ref importing

*/

/**
 ** @page makevisible Make identifiers visible
 ** This visibility mode only provides global symbols (variables or
 ** functions) that have a definition to other modules that try to use
 ** them. By default the following are not visible

 ** - type declarations (@c struct, @c union, @c enum or @c typedef)
 ** - macros (@c #define)
 ** - @c inline function definitions

 ** To make such declarations visible you have to place them in a @ref
 ** declaration "declaration" section.

 ** @code
#pragma module pair = proj∷arith∷pair

#pragma CMOD declaration
struct pair {
  double v[2];
  pair* next;
};

#pragma CMOD definition
double add(pair p) {
   return p.[v0] + p.v[1];
}
 ** @endcode

 ** This exposes the type @c proj∷arith∷pair (see below) and the function @c
 ** proj∷arith∷pair∷add to the outer world just as if each user of that
 ** module would see a C include with the following:
 ** @code
typedef struct proj∷arith∷pair proj∷arith∷pair;
struct proj∷arith∷pair {
  double v[2];
  proj∷arith∷pair* next;
};
extern double proj∷arith∷pair∷add(proj∷arith∷pair p);
 ** @endcode

 ** If we would not have add the @c pair abbreviation to the @ref
 ** module "module" directive, @c pair inside this module would in fact refer
 ** to a composed identifier @c proj∷arith∷pair∷pair. Using such an
 ** abbreviation is a convenient way to emphasize and ease the use of
 ** the module name @c proj∷arith∷pair as the name of its central data
 ** structure.

 ** A @ref declaration "declaration" directive makes all code that comes after it
 ** visible to the outer world, until we hit a @ref definition "definition"
 ** directive which re-enables the default mode described above.

 ** Next: @ref finetuning

*/

/**
 ** @page finetuning Fine tuning with extern and intern directives
 ** Under some circumstances this switching of visibility between @ref
 ** declaration "declaration" and @ref definition "definition" mode is not subtle enough. The two
 ** other directives in this group allow to individually manage the
 ** visibility of identifiers.

 ** An @ref intern "intern" directive makes an identifier inaccessible, even if
 ** it is declared inside a @ref declaration "declaration" section. This can be
 ** useful for some global type or data that has to been shared between
 ** the functions of your module, but that should not directly be used
 ** by others.

 ** @code
#pragma module pair = proj∷twister

#pragma CMOD declaration
#pragma CMOD intern TWIST

#define TWIST (sizeof(int) > 4)
#define DOIT(X) (TWIST ? (X) : 4)
 ** @endcode

 ** This has two macro definitions that are "technically" visible to
 ** importers, but only @c DOIT is meant to be part of the API. @c
 ** TWIST is only for intern (private) use of the module, here e.g in
 ** the expansion of @c DOIT.
 **
 ** Any user can use @c DOIT in their code. Indirectly this would then
 ** also use @c TWIST, but they cannot use it directly. Doing so would
 ** result in a syntax error.

 ** The @ref extern "extern" directive is probably rarely useful. It ensures
 ** that the identifier in question uses the external name mangling of
 ** _Modular C_ even if it is declared inside a @ref definition "definition"
 ** section. Its syntax is similar to the one for @ref intern "intern".

*/

/**
 ** @page importing Importing and abbreviating
 ** @anchor import
 ** @anchor context
 ** @code
#pragma CMOD import  [ abbrev     = ] path
#pragma CMOD context [ abbrev     = ] opening closing
#pragma CMOD context [ abbrev     = ] [ opening closing ] path
 ** @endcode

 ** Identifiers that are exported by other modules are usable as such,
 ** no extra _Modular C_ feature would be required. The @ref import "import"
 ** directive comes into play when we want to shorten long module paths
 ** or when we want @ref snippet.

 ** The abbreviation feature is in fact quite simple, and similar to
 ** the abbreviation @c pair for the module name @c proj∷arith∷pair,
 ** see @ref makevisible. Writing

 ** @code
#pragma module      proj∷arith∷compute
#pragma import p2 = proj∷arith∷pair
double sum(C∷size n, p2 A[n]) {
   double accu = 0;
   for (C∷size i = 0; i < n; ++i) {
      accu += p2∷add(A[i]);
   }
   return accu;
}
 ** @endcode

   is similar to
 ** @code
#pragma module        proj∷arith∷compute
double sum(C∷size n, proj∷arith∷pair A[n]) {
   double accu = 0;
   for (C∷size i = 0; i < n; ++i) {
      accu += proj∷arith∷pair∷add(A[i]);
   }
   return accu;
}
 ** @endcode

 ** That is, using the abbreviation @c p2 with the @ref import "import" lets us
 ** write shorter and more readable code. Also, it enables us to modify
 ** the whole import easily and to switch to a different module with
 ** the same interface.

 ** @remark We can use any non-composed name for the abbreviation, as
 ** long as it doesn't conflict with other local names that we are
 ** using.

 ** @remark Using a composed module name implies that all "parent"
 ** modules are implicitly imported. In example above, modules @c proj
 ** and @c proj∷arith are imported implicitly.

 ** @remark Import of _Modular C_ modules is never purely textual. To
 ** be imported, a module has always to be compiled, such that the
 ** importer may refer to that compiled information about the module.

 ** @remark @ref import "Import" directives may use abbreviations
 ** that have been defined through previous @ref import "import"
 ** directives.

 ** @remark Import "paths" may contain <code>..</code> to move up in
 ** the module hierarchy. E.g a leading <code>..</code> denotes the
 ** parent module of the current module.

 ** - @subpage relation "The import relation"
 ** - @subpage filling "Filling slots"
 ** - @subpage contexts "Expression contexts"
 ** - @subpage constants "Precomputing constants"

*/

/**
 ** @page relation The import relation

 ** The fact that one module @c A uses identifiers from another module
 ** @c B establishes a particular relation between @c A and @c B, the
 ** _import relation_.
 **
 ** Module @c A imports from @c B if
 ** - @c B's name is a proper prefix of @c A's name
 ** - @c A uses @c B's name or any other identifier from @c B, or
 ** - @c A imports from @c C and @c C imports from @c B.
 **
 ** _Modular C_ must extract the declarations of module @c B before it
 ** can be used by module @c A. Therefore, we impose that @c B must be
 ** compiled (to a @c .a file) before it can be used.
 **
 ** If there would be a cycle in import relation, we would face a hen
 ** and egg problem: none of the modules on such a cycle could be
 ** compiled before the others, and so compilation could never start.
 **
 ** @remark The import relation must not have cycles.
 **
 ** - Next: @ref filling
*/

/**
 ** @page filling Filling slots
 ** @anchor fill
 ** @anchor defill
 ** @code
#pragma CMOD fill      abbrev∷id  =   identifier
#pragma CMOD defill    abbrev∷id  =   replacement tokens
 ** @endcode

 ** Module with a code snippet, see @ref snippet, insert their snippet
 ** code into the module that explicitly imports it.
 **
 ** @remark The insertion of the snippet code takes place at the exact
 ** location of the @ref import directive.
 **
 ** For that reason, we have to ensure that all declarations that an
 ** imported snippet needs are present at that point of
 ** insertion. Therefore, as rule of thumb, it is better to place the
 ** @ref import "import" after all its corresponding @ref fill "fill"
 ** and @ref defill "defill" directives.
 **
 ** Thereby all
 ** identifiers that are declared in the snippet become identifiers,
 ** there, at the importing side if the import is done without using
 ** an abbreviation. This has the consequence that a naming choice
 ** that was done by the writer of the snippet is imposed on the
 ** importer. This leads easily to naming conflicts, if we want to
 ** import two different modules that both promote the same name in a
 ** snippet, say @c add, or if we want to use the same snippet twice.

 ** The first feature that allows us to avoid such name conflicts are
 ** abbreviations that we give to an import. Local names from a
 ** snippet are then prefixed in the form @c abbrev—name, see the
 ** @ref snippet "snippet" directive for an explanation.

 ** Two directives can also help us to avoid such naming conflicts: on
 ** the snippet side a @ref slot "slot" directive marks an identifier that
 ** is replaceable, and on the importer side a @ref fill "fill" associates a
 ** new name to such an imported slot.

 ** Let us have a look at the following module with snippet:
 ** @code
#pragma CMOD module    proj∷Nary
#pragma CMOD snippet   none
#pragma CMOD slot      OP    = none
#pragma CMOD slot      T     = complete
#pragma CMOD slot      func  = extern T func(C∷size n, T const A[n])

T func(C∷size n, T const A[n]) {
    T accu = { 0 };
    for (C∷size i = 0; i < n; ++i) {
        accu = accu OP A[i];
    }
    return accu;
}
 ** @endcode

 ** We can use that snippet twice to implement vector addition and
 ** vector multiplication, by giving two different abbreviations @c s
 ** and @c m in @ref import "import" directives:

 ** @code
#pragma CMOD module    proj∷arith∷ldouble

#pragma CMOD declaration

typedef long double ldouble;

#pragma CMOD defill s∷OP  = +
#pragma CMOD fill s∷T     = ldouble
#pragma CMOD fill s∷func  = sum
// import should come after the typedef and the fills
#pragma CMOD import s =    proj∷Nary

#pragma CMOD defill m∷OP  = *
#pragma CMOD fill m∷T     = ldouble
#pragma CMOD fill m∷func  = prod
// import should come after the typedef and the fills
#pragma CMOD import m =    proj∷Nary
 ** @endcode

 ** This has the effect of implementing two different functions,
 ** equivalent to something like

 ** @code
#pragma CMOD module    proj∷arith∷ldouble

long double sum(C∷size n, long double const A[n]) {
    long double accu = { 0 };
    for (C∷size i = 0; i < n; ++i) {
        accu = accu + A[i];
    }
    return accu;
}

long double prod(C∷size n, long double const A[n]) {
    long double accu = { 0 };
    for (C∷size i = 0; i < n; ++i) {
        accu = accu * A[i];
    }
    return accu;
}
 ** @endcode

 ** Observe that the identifiers of the three slots are used in a
 ** different way by this mechanism. @c OP and @c ldouble are declared
 ** by the importer, whereas @c func is actually the contribution of
 ** the snippet.

 ** @remark If the module that provides the snippet declares a default
 ** for any slot, importers can omit that slot. Otherwise an omitted
 ** slot may later lead to an error during the compilation or linking
 ** of the importer.
 **
 ** @remark Only identifiers that have been marked with a @ref fill "fill"
 ** directive can be used with a @ref slot "slot" directive. Using other
 ** slots will produce an error.
 **
 ** ### Define and fill in one directive.
 **
 ** A @ref defill "defill" directive fulfills a very common task, namely to
 ** combine the definition of a macro that is passed as an identifier
 ** to a @ref fill "fill" directive. Above we wanted to fill a slot @c
 ** OP with an operator token. All together we could have written this as follows
 **
 ** @code
#pragma CMOD declaration
#define PLUS +
#pragma CMOD fill   s∷OP  = PLUS
#pragma CMOD import s     = proj∷Nary
 ** @endcode
 **
 ** That is, to choose some name for a macro that just serves to be
 ** passed into a @ref fill "fill" directive. With @ref defill
 ** "defill" this simplifies to
 ** @code
#pragma CMOD defill s∷OP  = +
#pragma CMOD import s     = proj∷Nary
 ** @endcode
 **
 ** And the whole import part from above shortens to the
 ** lines as we have seen them.
 **
 ** This automatically chooses unique macro names for the operators
 ** and the types and fills them into the slots.

 ** - Next: @ref context
 **/

/**
 ** @page contexts Expression contexts
 ** Expression contexts offer the possibility to rewrite arithmetic
 ** expressions such that they use specific functions instead of the
 ** standard C operators.
 **
 ** @code
 ** #pragma CMOD module    𝔽257  = cmod∷F257
 ** #pragma CMOD context   ⟦    ⟧
 **
 ** #pragma CMOD declaration
 **
 ** // Define operations in the number field 𝔽₂₅₇
 ** typedef unsigned 𝔽257;
 ** #define mod 257u
 ** #define ⟦A + B⟧ (((A)+(B))%mod)
 ** #define ⟦A - B⟧ (((A)+(mod-(B)))%mod)
 ** #define ⟦A * B⟧ (((A)*(B))%mod)
 ** inline
 ** 𝔽257 ⟦𝔽257 a / 𝔽257 b⟧ {
 **    // do something clever
 ** }
 **
 ** #pragma CMOD entry main
 **
 ** #pragma CMOD definition
 **
 ** void main(void) {
 **   C∷io∷printf("%u\n", ⟦ 22*37 - 17 ⟧);
 ** }
 ** @endcode
 ** This defines a context that does all arithmetic with
 ** modulo 257. The code is rewritten to something like
 ** @code
 ** // Define operations in the number field 𝔽₂₅₇
 ** typedef unsigned cmod∷F257;
 ** #define cmod∷F257∷mod 257u
 ** #define cmod∷F257∷_Operator—add(A , B)  (((A)+(B))%cmod∷F257∷mod)
 ** #define cmod∷F257∷_Operator—sub(A , B)  (((A)+(cmod∷F257∷mod-(B)))%cmod∷F257∷mod)
 ** #define cmod∷F257∷_Operator—prod(A , B) (((A)*(B))%cmod∷F257∷mod)
 ** inline
 ** 𝔽257 cmod∷F257∷_Operator—div(𝔽257 a, 𝔽257 b) {
 **    // do something clever
 ** }
 **
 ** void cmod∷F257∷main(void) {
 **   C∷io∷printf("%u\n", cmod∷F257∷_Operator—sub(cmod∷F257∷_Operator—prod(22, 37 ), 17 ));
 ** }
 ** @endcode
 **
 ** Observe that the rewriting is purely textual, as most of the rest
 ** of _Modular C_. E.g the text <code>⟦A + B⟧</code> is rewritten to
 ** <code>cmod∷F257∷_Operator—add(A , B)</code>, even in the
 ** definition of a macro. The text <code>⟦𝔽257 a / 𝔽257 b⟧</code>
 ** results in <code>cmod∷F257∷_Operator—div(𝔽257 a, 𝔽257 b)</code>
 ** even as the "arguments" of the <code>/</code> operator contain
 ** spaces.
 **
 ** A context will rewrite the the following binary operators
 ** @code
 ** + - * / % << >> & | ^ < > <= >= ==
 ** @endcode
 ** and unary operators
 ** @code
 ** !! ~ ~~
 ** @endcode
 **
 ** Also the Unicode equivalents
 ** @code
 ** × ÷ ∩ ∪ ≤ ≥ ≡ ‼
 ** @endcode
 ** are handled early, before the context replacement.
 **
 ** Here, the operators <code>!!</code> and <code>~~</code> are meant
 ** to map their argument to a Boolean 0/1 type and to an arithmetic
 ** representation, respectively.
 **
 ** Other operators may not be overwritten, but resolve to a
 ** predefined meaning. Operators
 **
 ** @code
 ** = ?: && || -> * .
 ** @endcode
 **
 ** keep the meaning they have in C. Other assignment operators are
 ** then composed from the previous ones in the obvious way:
 **
 ** @code
 ** += -= *= /= %= <<= >>= &= |= ^=
 ** @endcode
 **
 ** E.g <code>⟦a += b⟧</code> translates to <code>a =
 ** cmod∷F257∷_Operator—add(a , b)</code>.
 **
 ** Some unary operators follow special rules
 **
 ** unary operator  | replacement              | remark
 ** ----------------|--------------------------|----------
 ** <code>+X</code> | <code>0+X</code>         | evaluation
 ** <code>-X</code> | <code>0-X</code>         | negation
 ** <code>++X</code>| <code>X = (X+1)</code>   | increment
 ** <code>--X</code>| <code>X = (X-1)</code>   | decrement
 ** <code>!X</code> | <code>!(!!X)</code>      | first convert to Boolean then negate
 ** <code>~X</code> | <code>~(~~X)</code>      | first convert to arithmetic, then complement
 **
 ** For the unary arithmetic operators the replacement operations are
 ** with literal @c 0 and @c 1, so a context specification that uses
 ** these operators must be able to deal with these integer constants.
 **
 ** choice of the delimiters
 ** =========================
 **
 ** In principle any string without spaces can be used as context
 ** delimiter, e.g you could also use "keywords" such as <code>BEGIN
 ** ... END</code>. If you use punctuation, you should use characters
 ** that line up well as opening and closing parenthesis, otherwise
 ** you will garble other peoples editors. Possible pairs with good
 ** properties include
 ** @code
⟦ ⟧  // used in the example for modulo arithmetic
〚 〛   // used as a default for C::attributes
⌈ ⌉
⦑ ⦒
 ** @endcode
 **
 ** You should probably avoid using punctuation that can be confounded
 ** with standard C operators. E.g the pair <code>｛ ｝</code> may be
 ** indistinguishable from <code>{ }</code>.
 **
 ** importing contexts from other modules
 ** =====================================
 **
 ** A @ref context "context" directive that contains a module path
 ** argument, imports the context from that module. If no opening and
 ** closing characters are specified the original ones from the
 ** imported module are used. Otherwise they are overwritten, such
 ** that we may import contexts from different modules that internally
 ** use the same opening and closing.
 **
 ** Contexts can also be named locally with the abbreviation syntax.
 **
 ** - Next: @ref constants
 **/

/**
 ** @page slots Providing identifiers as slots
 ** @anchor slot
 ** @code
#pragma CMOD slot      id     =   constraint
 ** @endcode

 ** A @ref slot "slot" directive indicates that a certain local identifier
 ** in a snippet can be replaced by an importer to their liking, see
 ** @ref filling for an example.

 ** In fact, declaring a slot without filling it on the importer's side
 ** has the sole effect that the name in question is just replaced by
 ** an expanded name of the providing module. A simple import as

 ** @code
#pragma CMOD module    proj∷arith∷bogus
#pragma CMOD import    proj∷Nary
 ** @endcode

 ** would be equivalent to

 ** @code
#pragma CMOD module    proj∷arith∷bogus

proj∷Nary∷T proj∷Nary∷func(C∷size n, proj∷Nary∷T const A[n]) {
    proj∷Nary∷T accu = { 0 };
    for (C∷size i = 0; i < n; ++i) {
        accu = accu proj∷Nary∷OP A[i];
    }
    return accu;
}
 ** @endcode

 ** That is, with a @ref slot "slot" directive an exporting module provides a slot in
 ** its own name space to the importing modules. Whether or not these want
 ** to overwrite this name by a @ref fill "fill" directive is entirely their
 ** choice. In the example above not doing so would probably lead to a
 ** compilation error, because the identifiers @c proj∷Nary∷T and @c
 ** proj∷Nary∷OP are not specified, but an exporter could also provide
 ** a default declaration for such an identifier.

 ** @code
#pragma CMOD module    proj∷initialize

#pragma CMOD declaration

#define INIT(P) { 0 }
#define func    init

#pragma CMOD snippet   none
#pragma CMOD slot      INIT  = { T* p; T a = INIT(p); }
#pragma CMOD slot      T     = complete
#pragma CMOD slot      func  = extern T* func(T* p)

T* func(T* p) {
   if (p) {
      *p = (T)INIT(p);
   }
   return p;
}
 ** @endcode

 ** Then an imports of the snippet without specifying some of the slots
 ** would be valid:

 ** @code
#pragma CMOD module  keyval  proj∷keyval

#pragma CMOD declaration

struct keyval {
  C∷size key;
  double val;
  keyval* next;
};

#pragma CMOD fill   sing∷T     = keyval
//                  sing∷INIT    not filled
#pragma CMOD fill   sing∷func  = singleton
#pragma CMOD import sing       = proj∷initialize

#pragma CMOD fill   cyc∷T     = keyval
#pragma CMOD defill cyc∷INIT  = { .next = (P), }
#pragma CMOD fill   cyc∷func  = cycleton
#pragma CMOD import cyc       = proj∷initialize
 ** @endcode

 ** This results in two functions that implement two different
 ** initialization strategies.
 ** @code
#pragma CMOD module  keyval  proj∷keyval

#pragma CMOD declaration

typedef struct keyval keyval;
struct keyval {
  C∷size key;
  double val;
  keyval* next;
};

#pragma CMOD definition

keyval* singleton(keyval* p) {
   if (p) {
      *p = (keyval){ 0 };
   }
   return p;
}
keyval* cycleton(keyval* p) {
   if (p) {
      *p = (keyval){ .next = (p), };
   }
   return p;
}
 ** @endcode
 ** - Next: @ref constraints
 **/

/**
 ** @page constraints Constraining possible fills
 ** The slots of a snippet are identifiers at the disposal of the
 ** importer that can represent very different features. To enforce
 ** certain properties, the exporting module can place a _constraint_
 ** as the second part of a @ref slot "slot" directive. The most common use
 ** of a constraint would probably be an @c extern declaration as we
 ** have given it for @c func in @ref slots. Such a constraint is an
 ** interface contract that binds exporter and importer.
 **
 ** The @c INIT slot has a more subtle constraint, namely
 ** @code
{ T* p; T a = INIT(p); }
 ** @endcode
 ** This code is never executed, but only has to compile properly,
 ** once @c T and @c INIT are declared. Here, this enforces that @c
 ** INIT is a "something" that takes a @c T* as an argument and that
 ** can be used as an initializer of an object of type @c T. In the
 ** first @ref fill "fill" of the example, the default @c
 ** proj∷initialize∷INIT receives an argument, ignores it and produces
 ** C's default initializer. The other fill, @c proj∷keyval∷CYC uses
 ** the pointer that it receives as argument to initialize a
 ** cyclically linked data item.
 **
 ** Constraints that are not inside <code>{ }</code> are specified by
 ** keywords:
 ** - @c extern, @c static or @c inline followed by an appropriate
 **   declaration, enforce an interface.
 ** - @c none imposes no constraint.
 ** - @c ice: The identifier is expected to represent an integer value
 **   that can be determined at compile time, an integer constant
 **   expression.
 ** - @c typedef: The identifier represents a type that could be placed
 **   into a @c typedef declaration.

 ** - @c complete: The identifier represents a complete object type,
 **   that is, a data type that has already been completely declared
 **   and who's size and structure is already known. Incomplete data
 **   types are @c void and array types
 **
 ** - @c global: The identifier refers to the corresponding global
 **   name of the exporter. Overwriting from within an importer is not
 **   possible.
 **
 ** - @c intern: The identifier refers to a unique name that cannot be
 **   directly accessed by the importer. Multiple imports refer each
 **   to a different internal name. This is similar to an @ref intern "intern"
 **   directive, only that each identifier is only usable inside its
 **   copy of the snippet, see @ref finetuning.
 **
 ** - @c init, @c atexit and @c at_quick_exit constraints act as if
 **   the importer had added analogous @ref init, "init," @ref atexit "atexit" and
 **   @ref at_quick_exit "at_quick_exit" directives, see @ref modstartup and @ref
 **   modatexit. E.g a slot that is marked with an @ref init "init"
 **   constraint must refer to a function with that can be used for an
 **   @ref init "init" directive. In addition to this run time property,
 **   slots with these constraints are also marked @c intern.
 **/

/**
 ** @page snippet Code insertion
 ** @anchor snippet
 ** @synopsis
 ** @code
#pragma CMOD snippet [ abbrev = ] constraint
 ** @endcode

 ** Using identifiers from other modules makes the declaration of these
 ** identifiers visible to the current module, but this feature import
 ** doesn't allow to share code between modules.

 ** Such code sharing is done with a @ref snippet "snippet" directive: all the
 ** code that comes after such a directive can be inserted directly
 ** into the importing module.

 ** @code
#pragma CMOD module  proj∷allocation
#pragma CMOD snippet T = complete
T* alloc(void) {
   T* ret = C∷lib∷malloc(sizeof *T);
   if (ret) {
     *ret = (T){ 0 };
   }
   return ret;
}
 ** @endcode

 ** Such a simple "snippet" module that has no code before the @ref
 ** snippet "snippet" directive doesn't export any symbols. It only
 ** becomes relevant when it is directly imported in place by another
 ** module.

 ** @code
#pragma module pair = proj∷arith∷pair
#pragma CMOD declaration
struct pair {
  double v[2];
  pair* next;
};
#pragma import        proj∷allocation
 ** @endcode

 ** Here the module @c proj∷arith∷pair defines the type @c
 ** proj∷arith∷pair and the import automatically generates a new
 ** function @c proj∷arith∷pair∷alloc. The @ref import "import"
 ** directive must be positioned *after* the full type declaration,
 ** because the @c sizeof that is used inside the generated function
 ** only works when we have seen the full declaration.
 **
 ** Concerning visibility by other modules, this is then analogous to

 ** @code
typedef struct proj∷arith∷pair proj∷arith∷pair;
struct proj∷arith∷pair {
  double v[2];
  proj∷arith∷pair* next;
};
extern proj∷arith∷pair* proj∷arith∷pair∷alloc(void);
 ** @endcode

 ** The code that is compiled is as if we had written

 ** @code
typedef struct proj∷arith∷pair proj∷arith∷pair;
struct proj∷arith∷pair {
  double v[2];
  proj∷arith∷pair* next;
};
proj∷arith∷pair* alloc(void) {
   proj∷arith∷pair* ret = C∷lib∷malloc(sizeof *proj∷arith∷pair);
   if (ret) {
     *ret = (proj∷arith∷pair){ 0 };
   }
   return ret;
}
 ** @endcode

 ** That is, all occurrences of @c T in the snippet are replaced by the
 ** name of the importing module, @c proj∷arith∷pair, and the
 ** function is integrated textually into the the rest of the module.

 ** ### Disambiguating names imported from snippets
 ** @anchor abbrevprefix
 ** If we have several such imports into the same module this can
 ** easily lead to naming conflicts if two imported modules declare
 ** the same identifiers in their snippets. An easy way to cope with
 ** such situations is to import a module by additionally giving it an
 ** abbreviation.

 ** @code
#pragma module pair = proj∷arith∷pair
#pragma CMOD declaration
struct pair {
  double v[2];
  pair* next;
};
#pragma import two  = proj∷allocation
 ** @endcode

 ** Now, all local identifiers from the snippet are prefixed by @c
 ** two—, where @c — is the @ref composer "composer"
 ** character. So instead of

 ** @code
extern proj∷arith∷pair* proj∷arith∷pair∷alloc(void);
 ** @endcode

 ** this import now declares

 ** @code
extern proj∷arith∷pair* proj∷arith∷pair∷two—alloc(void);
 ** @endcode

 ** Since all abbreviations must be unique, this strategy provides
 ** unique names for the whole module.

 ** @remark Snippets are only inserted if their module is imported
 ** explicitly through an @ref import "import" directive, see @ref importing.
 **
 ** @remark Per default a snippet constitutes a @ref definition "definition" section.
 **
 ** @remark Snippet code can be distinguished into @ref declaration "declaration"
 ** and @ref definition "definition" sections, just as the main code of a module.
 **
 ** - @subpage slots "Providing identifiers as slots"
 ** - @ref filling
 ** - @subpage constraints "Constraining possible fills"
 **/

/**
 ** @page execution Execution directives
 ** @anchor entry
 ** @anchor init
 ** @anchor atexit
 ** @anchor at_quick_exit
 ** @synopsis
 ** @code
#pragma CMOD entry         identifier
#pragma CMOD init          identifier
#pragma CMOD atexit        identifier
#pragma CMOD at_quick_exit identifier
 ** @endcode
 **
 ** _Modular C_ has four different directives to specify functions
 ** that will be executed at well specified moments during a program
 ** run.
 **
 ** The most important of these is the @ref entry "entry" directive. Similar
 ** to standard C's @c main, it specifies a ``user'' function that
 ** will be executed at the startup of the program and that can
 ** receive arguments from the command line. The other three assure
 ** that the functions in question are run once before or after all
 ** user code that uses the module is executed.
 **
 ** ### Program entry
 **
 ** A function name that is presented to an @ref entry "entry" directive must
 ** have one of the following prototypes:
 **
 ** @code
 ** // prototypes that return an exit code
 ** int identifier(void);
 ** int identifier(int, char**);
 ** int identifier(int, char const**);
 ** int identifier(int*, char***);
 ** // prototypes that don't return an exit code
 ** void identifier(void);
 ** void identifier(int, char**);
 ** void identifier(int, char const**);
 ** void identifier(int*, char***);
 ** // for POSIX systems
 ** #if _XOPEN_VERSION > 0
 ** extern char** environ;
 ** int identifier(int, char**, char**);
 ** void identifier(int, char**, char**);
 ** #endif
 ** @endcode
 **
 ** The first two correspond to those prototypes that are usually
 ** allowed for @c main. The semantic of the others is derived from that.
 **
 ** - For the prototypes that have a @c void return type, the function
 **   doesn't need to terminate by returning an exit code. In cases of
 **   a normal termination (without calling ::C∷lib∷exit) an exit
 **   code corresponding to ::C∷lib∷SUCCESS is returned to the run
 **   time environment.
 ** - The prototypes that have triple pointers can be used to change
 **   the whole argument vector.
 **
 ** A _Modular C_ program is not limited to a single entry point. This
 ** can be useful for libraries that need to process command line
 ** arguments in a certain way before proceeding to execute the user's
 ** application:
 **
 ** @code
 ** #pragma CMOD module proj∷run
 ** #pragma CMOD entry  toto∷helper∷getops
 ** #pragma CMOD entry  processOpts
 ** #pragma CMOD entry  enter
 ** void processOpts(int* argcp, char* (*argvp)[*argcp+1]) {
 **    if (*argcp > 1 && !C∷str∷cmp((*argv)[1], "--special")) {
 **      initialize_something_special();
 **      // shift remaning comandline to the left
 **      for (C∷size i = 1; i < *argcp; ++i) {
 **         (*argvp)[i] = (*argvp)[i+1];
 **      }
 **      --(*argcp);
 **    }
 **    // can never fail
 ** }
 ** int enter(int argc, char const* argv[argc+1]) {
 **    // ... here we go ...
 ** }
 ** @endcode
 **
 ** This code has three entry points, two that are defined here and
 ** one that comes from another module @c toto∷helper. These three
 ** entries are assembled by modular C to something that is equivalent
 ** to the following C code:
 **
 ** @code
 ** int toto∷helper∷getops(int* argcp, char* (*argvp)[*argcp+1]);
 ** void proj∷run∷processOpts(int* argcp, char* (*argvp)[*argcp+1]);
 ** int proj∷run∷(int argc, char const* argv[argc+1]);
 **
 ** int main(int argc, char* argv[argc+1]) {
 **    int ret = toto∷helper∷getops(&argc, &argv);
 **    if (ret) return ret;
 **    proj∷run∷processOpts(&argc, &argv);
 **    return proj∷run∷enter(argc, argv);
 ** }
 ** @endcode
 **
 ** @remark Any entry function that returns an exit code that is not
 ** ::C∷lib∷SUCCESS terminates the current program execution.
 **
 ** @remark If a module has several @ref entry "entry" directives, the
 ** functions are executed in the order in which the directives
 ** appear.
 **
 ** @remark The argument count and argument vector are passed on from
 ** one entry to the next.
 **
 ** - @subpage modstartup "Module initialization"
 ** - @subpage modatexit  "Module post-processing"
 **/

/**
 ** @page modstartup Module initialization
 **
 ** Some modules want to initialize data dynamically at program
 ** startup. Examples are dynamic memory allocation, initialization of
 ** a random seed or a time stamp.
 **
 ** @code
 ** #pragma CMOD module        proj∷bench
 ** #pragma CMOD import spec = C∷time∷spec
 ** #pragma CMOD init   stamp
 **
 ** static spec start;
 ** void stamp(void) {
 **   spec∷get(&start, C∷time∷UTC);
 ** }
 ** @endcode
 **
 ** All @ref init "init" handlers must have a prototype of
 ** @code
 ** void identifier(void);
 ** @endcode
 **
 ** As we can see from the example above, such init functions may call
 ** any function of other modules to do its job, as long as the import
 ** dependency relation remains acyclic, see @ref relation.
 **
 ** @remark All functions from @ref init "init" directives are executed in
 ** an order that is consistent with the import relation.
 **
 ** @remark If module @c A imports from module @c B, @c A's init
 ** functions are run after @c B's.
 **
 ** Next: @ref modatexit
 **/

/**
 ** @page modatexit Module post-processing
 **
 ** Symmetrically, _Modular C_ also allows to execute code when the
 ** program execution ends. Using an @ref atexit "atexit" directive is similar
 ** to a use of the C::lib::atexit function. Handlers register with
 ** that feature are executed in a well defined order at program
 ** exit. Similarly, an @ref at_quick_exit "at_quick_exit" handler runs when the
 ** program is terminated with C::lib::quick_exit as if it were
 ** registered by C::lib::at_quick_exit.
 **
 ** All @ref atexit "atexit" and @ref at_quick_exit "at_quick_exit" handlers must have a
 ** prototype of
 **
 ** @code
 ** void identifier(void);
 ** @endcode
 **
 ** @remark If the program terminates normally, none of the @ref
 ** at_quick_exit "at_quick_exit" handlers are called.
 **
 ** @remark If the program terminates by a call to C::lib::quick_exit,
 ** none of the @ref atexit "atexit" handlers are called.
 **
 ** @remark If module @c A imports from module @c B, @c A's @ref
 ** atexit "atexit" handlers are run before @c B's.
 **
 ** @remark If module @c A imports from module @c B, @c A's @ref
 ** at_quick_exit "at_quick_exit" handlers are run before @c B's.
 **/



/**
 ** @page manipulation Code manipulation
 **
 ** Modular C allows you to manipulate whole chunks of program code at
 ** once. There are two specific directives for code unrolling (@ref
 ** do "do" and @ref foreach "foreach") and a generic one that allows
 ** to apply any form of textual replacement to the corresponding
 ** text.
 **
 ** All three types of code manipulation directives (@c do, @c foreach
 ** and @c amend) can be nested.
 **
 ** - @subpage unrolling "Code unrolling"
 ** - @subpage amendments "Implementing amendments"
 **/

/**
 ** @page unrolling Code unrolling
 ** @anchor foreach
 ** @anchor do
 ** @anchor done
 ** @synopsis
 ** @code
#pragma CMOD foreach NAME = EL0 EL1 ....
#pragma CMOD done

#pragma CMOD do NAME = [ START ] END [ OPERATION ]
#pragma CMOD done
 ** @endcode
 **
 ** ### foreach
 **
 ** This feature allows us to repeat code a fixed number of times such
 ** that at each copy @c ${NAME} is replace by the words @c EL0, @c
 ** EL1 etc. E.g to create a type generic maximum macro:
 **
 ** @code
 ** #pragma CMOD module max  =  proj∷max
 ** #pragma CMOD composer    —
 **
 ** #pragma CMOD declaration
 **
 ** #pragma CMOD foreach T = float double unsigned signed
 ** inline
 ** ${T} max—${T}(${T} a, ${T} b) {
 **   return a < b ? b ? a;
 ** }
 ** #pragma CMOD done
 **
 ** #define max(A, B) _Generic((A)+(B),         \
 **                      default: max—double,   \
 **                      float:   max—float,    \
 **                      unsigned:max—unsigned, \
 **                      signed: max—signed)    \
 **                   ((A), (B))
 ** @endcode
 **
 ** This creates four functions and a type generic interface macro.
 **
 ** The @ref foreach "foreach" directive is not limited to executable code. It
 ** unrolls every programming text:
 ** @code
 ** #pragma CMOD foreach T = float double unsigned signed
 **    struct pair—${T} { ${T} v; };
 ** #pragma CMOD done
 ** static double A[] = {
 ** #pragma CMOD foreach X = 0 1 2 3 4 5 6 7 8 9
 **    ${X}*${X},
 ** #pragma CMOD done
 ** }
 ** @endcode
 **
 ** Here to declare four different types and to statically initialize
 ** an array with 10 different computed values.
 **
 ** ### do
 **
 ** A @ref do "do" directive is another possibility to expand code a fixed
 ** number of times. The difference to @ref foreach "foreach" is that the
 ** domain is arithmetic. The last @ref foreach "foreach" directive could be
 ** replaced as follows:
 ** @code
 ** static double A[] = {
 ** #pragma CMOD do X = 0 10 +1
 **    ${X}*${X},
 ** #pragma CMOD done
 ** }
 ** @endcode
 **
 ** where the @c 0 and @c +1 are even optional and
 **
 ** @code
 ** static double A[] = {
 ** #pragma CMOD do X = 10
 **    ${X}*${X},
 ** #pragma CMOD done
 ** }
 ** @endcode
 **
 ** would be equivalent.
 **
 ** The rules for the indices are "C-like". If not specified, the @c
 ** START value is @c 0 and @c END is an exclusive upper or lower
 ** bound. The permitted operators in @c OPERATIONS are <code>+, -,
 ** *</code> and @c /. The update operation is to perform <code>NAME =
 ** NAME OPERATION</code> so in our example it would correspond to
 ** <code>X = X +1</code>. If the operation in @c OPERATION is @c + or
 ** @c *, @c END is taken as an upper bound, if it is @c - or @c / it
 ** is a lower bound.
 **
 ** So
 ** @code
 ** #pragma CMOD do P = 1 16 *2
 ** something
 ** #pragma CMOD done
 ** @endcode
 **
 ** expands the inner code block 4 times, for values of @c P of @c 1,
 ** @c 2, @c 4 and @c 8.
 **
 ** @code
 ** #pragma CMOD do P = 8 0 /2
 ** something
 ** #pragma CMOD done
 ** @endcode
 **
 ** iterates over the same values but in different order, for values
 ** of @c P is set to @c 8, @c 4, @c 2, and then @c 1.
 **
 ** Next: @ref amend
 **/

/**
 ** @page amendments Implementing amendments
 ** @anchor amend
 ** @anchor insert
 ** @synopsis
 ** @code
#pragma CMOD amend FEATURE ARG0 ...
  ... any text ...
#pragma CMOD done

#pragma CMOD insert FEATURE ARG0 ...
 ** @endcode
 **
 ** ### amend
 **
 ** As the name indicates this directive allows to implement
 ** amendments to Modular C to your liking. They splice the text
 ** between the two pragmas out, run it through a filter and insert
 ** the resulting text in-place.
 **
 ** The only sensible amendments that are implemented for now are @c
 ** do and @c foreach (the same as the directives with the same name)
 ** and @c specialize.
 ** @code
#pragma CMOD amend specialize i = x y
  a[i] = f(i);
#pragma CMOD done
 ** @endcode
 **
 ** expands to something similar to
 ** @code
  if (i ≡ x) {
    a[i] = f(i);
  } else if (i ≡ y) {
    a[i] = f(i);
  } else {
    a[i] = f(i);
  }
 ** @endcode
 **
 ** That is, two copies of the code for which the compiler is forced
 ** to a specific value for the variable, and a third, for the general
 ** case.
 **
 ** The things that can be implemented with this feature are only
 ** limited by your imagination, any kind of text processing that
 ** results in valid Modular C code is fine.
 **
 ** ### insert
 **
 ** The @c insert directive is a special version of @c amend for cases
 ** where there are no lines to input to the feature.
 ** @code
#pragma CMOD insert hello I am here
 ** @endcode
 **
 ** is be equivalent to
 ** @code
#pragma CMOD amend hello I am here
#pragma CMOD done
 ** @endcode
 **/

/**
 ** @page constants Precomputing constants
 ** @anchor constant
 ** @synopsis
 ** @code
#pragma CMOD constant  name = [ compound literal = ] expression
 ** @endcode
 **
 ** C is very restricted syntactically for possible named constants,
 ** even for most of its base types. The only type for which there is
 ** a possibility to define named constants is @c int, but even here
 ** the syntactic rule is weird, namely the declaration of an @c enum
 ** type and constant.
 **
 ** A @ref constant "constant" directive defines a macro that has the value of
 ** the evaluated expression. As much as this is possible the type of
 ** the expression is respected. That is, if the type is an arithmetic
 ** type and there are literals for this type or it is a character
 ** array, such a literal is produced. For narrow types such as @c
 ** char or @c short, a "reasonable" approximation of such a literal
 ** is produced.
 **
 ** Arbitrary types produce a macro call to @c C∷OVERLAY that
 ** reconstructs the computed value as a sequence of bytes.
 **
 ** @remark The evaluation is done *after* compiling the
 ** module. Thereby the expression can use any suitable Modular C code
 ** that would be needed. Thus, in the context of the module itself,
 ** the constant is in fact not a constant and can usually not be used
 ** in all places where we would like to use such a constant.
 **
 ** @remark Such constants can be used by all *importers* in all
 ** contexts where compile time constants of the underlying type are
 ** required.
 **
 **/

/**
 ** @page platform Interfacing with platform specific libraries
 ** @anchor mimic
 ** @anchor typedef
 ** @anchor define
 ** @anchor alias
 ** @anchor defexp
 ** @anchor defstruct
 ** @anchor defreg
 ** @anchor defrex
 ** @synopsis
 ** @code
#pragma CMOD mimic <header.h>
#pragma CMOD typedef   [ abbrev = ] external_name
#pragma CMOD define    [ abbrev = ] external_name
#pragma CMOD alias     [ abbrev = ] external_name
#pragma CMOD defexp      abbrev =   expression
#pragma CMOD defstruct   abbrev =   [ struct ] external_name member0 member1 ...

#pragma CMOD defreg      expr      = regexp
#pragma CMOD defrex      expr      = regexp

#pragma CMOD link        linker_option
 ** @endcode
 **
 ** Interfacing libraries and platforms can be a tedious
 ** task. Therefore _Modular C_ proposes this set of directives to
 ** lift external interfaces into modules.
 **
 ** ### Targeting a specific platform interface.
 **
 ** The @ref mimic "mimic" directive receives a C header specification as its
 ** argument. Features of this header can then be detected by using
 ** the other directives from this set.
 **
 ** @code
#pragma CMOD module              proj∷lib
#pragma CMOD mimic              <stdlib.h>
#pragma CMOD typedef   size    = size_t
#pragma CMOD define    FAILURE = EXIT_FAILURE
#pragma CMOD define    SUCCESS = EXIT_SUCCESS
#pragma CMOD alias     allocate= malloc

#pragma CMOD declaration

void* allocate(size s);
 ** @endcode
 **
 ** This creates an additional interface to features of the standard C
 ** library. Namely it mirrors the type @c size_t as proj∷lib∷size,
 ** two macros proj∷lib∷FAILURE and proj∷lib∷SUCCESS, and a function
 ** proj∷lib∷allocate. For the latter we give a conventional prototype
 ** *and* an @ref alias "alias" directive. This directive ensures that all
 ** calls to proj∷lib∷allocate are resolved with the symbol @c malloc
 ** of the standard C library. The @ref define "define" directives extract the
 ** macros from the target platform as they are, that is without
 ** evaluating the macro on the target platform.
 ** @code
 ** @endcode
 **
 ** With the exception of @ref defstruct "defstruct" all these
 ** directives result the generated features don't have any
 ** dependencies from the module. Therefore the placement of the
 ** directives inside the module has no importance.
 **
 ** ### Extracting more complex features of the target platform.
 **
 ** In contrast to a @ref constant "constant" directive, all that is described
 ** here is done *before* the module itself is compiled. So the result
 ** of all evaluations is available for all code of the module.
 **
 ** - @ref defexp "defexp" allows to evaluate an expression as conventional C
 **   code during compilation. The result of that evaluation is then
 **   place into a macro with macro name @c abbrev.
 **
 ** - @ref defreg "defreg" and @ref defrex "defrex" extract a whole pattern that
 **   corresponds to a regular expression, where @ref defreg "defreg"
 **   generalizes a @ref define "define" directive and @ref defrex "defrex" a @ref
 **   defexp "defexp" directive.
 **
 ** - A @ref defstruct "defstruct" directive extracts the definition of a
 **   structure @c external_name from the target. The known field
 **   names are given in the list. The result is a @c struct
 **   declaration with the listed fields at the correct offsets. Gaps
 **   that occur, because some field is not known to the programmer or
 **   because the structure has padding are filled with spare bytes.
 **
 ** @code
#pragma CMOD defreg \2=\(LC_\([a-zA-Z_0-9]*\)\)
 ** @endcode
 **
 ** extracts all macros that start with @c LC_ and are followed by
 ** alpha-numerics and imports them to a @c #define without @c LC_
 ** prefix.

 ** @code
#pragma CMOD defrex \2=\(E\([a-zA-Z][a-zA-Z0-9_]*\)\)
 ** @endcode

 ** extracts all macros that start with @c LC_ and are followed by
 ** alpha-numerics and imports them to a @c #define without @c E
 ** prefix. In contrast to the previous, here all @c E macros are
 ** evaluated and the new macro is defined with the numerical value
 ** that results from this.

 ** The @c defstruct is special, as it inserts a @c struct definition
 ** *in-place*. So at that very moment, the types of the listed fields
 ** must be known. In addition, as for all newly defined types,
 ** placement in a @c declaration section makes the new @c struct type
 ** visible to all importers; placement in a @c definition section
 ** hides the complete definition from importers and only shows them
 ** an incomplete forward declaration.
 **
 ** The following defines a structure type in place:
 **
 ** @code
#define divType_type_rem  int
#define divType_type_quot int
#pragma CMOD defstruct divType = div_t rem quot
 ** @endcode

 ** This supposes that the platform feature that we "mimic" has a @c
 ** typedef @c div_t with at least two fields, @c rem and @c
 ** quot. This should expand to something similar to
 **
 ** @code
#define divType_type_rem  int
#define divType_type_quot int
typedef struct divType divType;
struct divType {
  divType_type_rem rem;
  divType_type_quot quot;
};
 ** @endcode

 only that the order of the fields is the one of the target platform
 and that there might be additional "dummy" fields for platform
 specific members that we didn't knew about.

 If the mimicked feature is not present as a @c typedef but as a
 simple @struct with a tagname, the syntactical variant with the @c
 struct keyword can be used.

 **/

/**
 ** @page extensions Extensions of the C language and library
 **
 ** _Modular C_ also implements some straight forward extensions that
 ** make coding easier.
 **
 ** ### Bless late C features by reserving them in the global name space.
 **
 ** The following identifiers are promoted to keywords for _Modular C_
 ** and their ugly replacements such as @c _Bool are deprecated:
 **
 ** <code>
 ** I,
 ** NULL,
 ** alignas,
 ** alignof,
 ** bool,
 ** complex,
 ** false,
 ** noreturn,
 ** nullptr,
 ** offsetof,
 ** static_assert,
 ** true.
 ** </code>
 **
 ** - @subpage tags "Struct, union and enumeration tags aren't special"
 ** - @subpage unicode    "Unicode"
 ** - @subpage macros "Macro programming"
 **/

/**
 ** @page macros Macro programming
 **
 ** In the spirit of P99, _Modular C_ has a series of macro tools that
 ** may come handy when implementing libraries. Using the @ref do "do"
 ** directive, their implementation with _Modular C_ is much easier
 ** than it was for P99 with standard C.
 **
 ** - @ref C::comma "C::comma" provides tools to count commas in a
 **   macro argument list and builds logical operations upon this. E.g
 **   @ref C::comma::if "C::comma::if" can conditionally expand code
 **   depending on the number of arguments, @ref
 **   C::comma::generic::call "C::comma::generic::call" can make a
 **   choice between different function pointers according to the
 **   number of arguments it receives.
 **
 ** - The @ref C::COUNT "C::COUNT" module goes one step further and
 **   counts the number of elements in a list, returning @c 0 if there
 **   is nothing at all.
 **
 ** - @ref C::DEFARG "C::DEFARG" can be used to provide default
 **   arguments to functions.
 **
 ** - There are @ref snippet "snippets" that help to declare macros
 **   that "iterate" over their argument list, @ref C::snippet::map,
 **   @ref C::snippet::for.
 **/
